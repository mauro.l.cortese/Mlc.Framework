﻿═══════════════════════════
Mlc.Framework 
Internal Dependency
═══════════════════════════

Mlc.Shell

Mlc.Data
    │
    └───► Mlc.Shell

Mlc.Gui.NTier
    │
    └───► Mlc.Shell

Mlc.Office
    │
    ├───► Mlc.Shell
    └───► Mlc.Data

Mlc.Gui.DevExpress
    │
    ├───► Mlc.Shell
    └───► Mlc.Data

Mlc.Rights
    │
    ├───► Mlc.Shell
    └───► Mlc.Data

Mlc.Pdf
    │
    ├───► Mlc.Shell
    └───► Mlc.Data

Mlc.Rights.Gui
    │
    ├───► Mlc.Shell
    ├───► Mlc.Data
    ├───► Mlc.Rights
    ├───► Mlc.Gui.NTier
    └───► Mlc.Gui.DevExpress


─ └ ┴ ┼ ═ ►◄