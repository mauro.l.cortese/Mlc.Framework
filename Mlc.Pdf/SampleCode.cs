﻿
using DevExpress.Pdf;
using System.Collections.Generic;
using System.Drawing;


namespace Mlc.Pdf
{
    /// <summary>
    /// Class Program.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The drawing dpi
        /// </summary>
        const float DrawingDpi = 72f;

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            using (PdfDocumentProcessor processor = new PdfDocumentProcessor())
            {
                processor.LoadDocument("..\\..\\RotatedDocument.pdf");
                using (SolidBrush textBrush = new SolidBrush(Color.FromArgb(0, 0, 0)))
                    AddGraphics(processor, "text", textBrush);
                processor.SaveDocument("..\\..\\RotatedDocumentWithGraphics.pdf");
            }
        }

        /// <summary>
        /// Adds the graphics.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <param name="text">The text.</param>
        /// <param name="textBrush">The text brush.</param>
        static void AddGraphics(PdfDocumentProcessor processor, string text, SolidBrush textBrush)
        {
            IList<PdfPage> pages = processor.Document.Pages;
            for (int i = 0; i < pages.Count; i++)
            {
                PdfPage page = pages[i];
                using (PdfGraphics graphics = processor.CreateGraphics())
                {
                    SizeF actualPageSize = PrepareGraphics(page, graphics);
                    using (Font font = new Font("Arial", 30, FontStyle.Regular))
                    {
                        SizeF textSize = graphics.MeasureString(text, font, PdfStringFormat.GenericDefault);
                        PointF topLeft = new PointF(30, 30);
                        PointF bottomRight = new PointF(actualPageSize.Width - textSize.Width, actualPageSize.Height - textSize.Height);
                        //graphics.DrawString(text, font, textBrush, topLeft);
                        //graphics.DrawString(text, font, textBrush, bottomRight);

                        Brush b = new SolidBrush(Color.FromArgb(255, 255, 255));
                        Pen p = new Pen(b);

                        graphics.FillRectangle(b, new RectangleF(new PointF(20, 20), new SizeF(200, 200)));
                        //graphics.DrawRectangle(p, new RectangleF(new PointF(20, 20), new SizeF(100, 100)));



                        graphics.AddToPageForeground(page, DrawingDpi, DrawingDpi);
                    }
                }
                using (PdfGraphics graphics = processor.CreateGraphics())
                {
                    SizeF actualPageSize = PrepareGraphics(page, graphics);
                    using (Font font = new Font("Arial", 30, FontStyle.Regular))
                    {
                        SizeF textSize = graphics.MeasureString(text, font, PdfStringFormat.GenericDefault);
                        PointF topLeft = new PointF(30, 30);
                        PointF bottomRight = new PointF(actualPageSize.Width - textSize.Width, actualPageSize.Height - textSize.Height);
                        graphics.DrawString(text, font, textBrush, topLeft);
                        graphics.DrawString(text, font, textBrush, bottomRight);

 



                        Brush b = new SolidBrush(Color.FromArgb(255, 255, 255));
                        Pen p = new Pen(b);

                        //graphics.FillRectangle(b, new RectangleF(new PointF(20, 20), new SizeF(100, 100)));
                        //graphics.DrawRectangle(p, new RectangleF(new PointF(20, 20), new SizeF(100, 100)));



                        graphics.AddToPageForeground(page, DrawingDpi, DrawingDpi);
                    }
                }


            }



        }

        /// <summary>
        /// Prepares the graphics.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="graphics">The graphics.</param>
        /// <returns>SizeF.</returns>
        static SizeF PrepareGraphics(PdfPage page, PdfGraphics graphics)
        {
            PdfRectangle cropBox = page.CropBox;
            float cropBoxWidth = (float)cropBox.Width;
            float cropBoxHeight = (float)cropBox.Height;

            switch (page.Rotate)
            {
                case 90:
                    graphics.RotateTransform(-90);
                    graphics.TranslateTransform(-cropBoxHeight, 0);
                    return new SizeF(cropBoxHeight, cropBoxWidth);
                case 180:
                    graphics.RotateTransform(-180);
                    graphics.TranslateTransform(-cropBoxWidth, -cropBoxHeight);
                    return new SizeF(cropBoxWidth, cropBoxHeight);
                case 270:
                    graphics.RotateTransform(-270);
                    graphics.TranslateTransform(0, -cropBoxWidth);
                    return new SizeF(cropBoxHeight, cropBoxWidth);
            }
            return new SizeF(cropBoxWidth, cropBoxHeight);
        }
    }
}
