﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 12-03-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-03-2019
// ***********************************************************************
// <copyright file="WatermarkAlignments.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Pdf
{
    /// <summary>
    /// Enum WatermarkAlignments
    /// </summary>
    internal enum WatermarkAlignments
    {
        /// <summary>
        /// The undefine
        /// </summary>
        Undefine,
        /// <summary>
        /// The left
        /// </summary>
        Left,
        /// <summary>
        /// The middle
        /// </summary>
        Middle,
        /// <summary>
        /// The right
        /// </summary>
        Right,
    }
}