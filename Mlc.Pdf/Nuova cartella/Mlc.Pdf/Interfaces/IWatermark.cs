
using System;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IWatermark : IDataRowMapping
	{
		#region Proprietà
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Type
        /// </summary>    
        Int32 Type { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdWatermark
        /// </summary>    
        Int32 IdWatermark { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Caption
        /// </summary>    
        String Caption { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdType
        /// </summary>    
        Int32 IdType { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Width
        /// </summary>    
        Double Width { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Height
        /// </summary>    
        Double Height { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo BackColor
        /// </summary>    
        String BackColor { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ForeColor
        /// </summary>    
        String ForeColor { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ContentColor
        /// </summary>    
        String ContentColor { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo CaptionFont
        /// </summary>    
        String CaptionFont { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ContentFont
        /// </summary>    
        String ContentFont { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo BorderWidth
        /// </summary>    
        Double BorderWidth { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdAlignment
        /// </summary>    
        Int32 IdAlignment { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo XOrigin
        /// </summary>    
        Double XOrigin { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo YOrigin
        /// </summary>    
        Double YOrigin { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}