
using System;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IWatermarkLayout : IDataRowMapping
	{
		#region Proprietà
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdLayout
        /// </summary>    
        Int32 IdLayout { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFormat
        /// </summary>    
        Int32 IdFormat { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFamily
        /// </summary>    
        Int32 IdFamily { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo XOrigin
        /// </summary>    
        Double XOrigin { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo YOrigin
        /// </summary>    
        Double YOrigin { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}