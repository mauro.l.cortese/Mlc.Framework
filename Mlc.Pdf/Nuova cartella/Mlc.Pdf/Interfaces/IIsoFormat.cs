
using System;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IIsoFormat : IDataRowMapping
	{
		#region Proprietà
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFormat
        /// </summary>    
        Int32 IdFormat { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Width
        /// </summary>    
        Int32 Width { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Height
        /// </summary>    
        Int32 Height { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo WidthPixRounded
        /// </summary>    
        Int32 WidthPixRounded { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo HeightPixRounded
        /// </summary>    
        Int32 HeightPixRounded { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}