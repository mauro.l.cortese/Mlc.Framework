using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class WatermarkBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "Watermark";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public WatermarkBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public WatermarkBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Proprietà
        /// <summary>Valore mappato sul campo Type</summary>
        protected Int32 type = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Type
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 Type { get { return this.type; } set { this.type = value; } }

        /// <summary>Valore mappato sul campo IdWatermark</summary>
        protected Int32 idwatermark = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdWatermark
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdWatermark { get { return this.idwatermark; } set { this.idwatermark = value; } }

        /// <summary>Valore mappato sul campo Name</summary>
        protected String name = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Valore mappato sul campo Caption</summary>
        protected String caption = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Caption
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Caption { get { return this.caption; } set { this.caption = value; } }

        /// <summary>Valore mappato sul campo IdType</summary>
        protected Int32 idtype = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdType
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdType { get { return this.idtype; } set { this.idtype = value; } }

        /// <summary>Valore mappato sul campo Width</summary>
        protected Double width = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Width
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double Width { get { return this.width; } set { this.width = value; } }

        /// <summary>Valore mappato sul campo Height</summary>
        protected Double height = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Height
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double Height { get { return this.height; } set { this.height = value; } }

        /// <summary>Valore mappato sul campo BackColor</summary>
        protected String backcolor = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo BackColor
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String BackColor { get { return this.backcolor; } set { this.backcolor = value; } }

        /// <summary>Valore mappato sul campo ForeColor</summary>
        protected String forecolor = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ForeColor
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String ForeColor { get { return this.forecolor; } set { this.forecolor = value; } }

        /// <summary>Valore mappato sul campo ContentColor</summary>
        protected String contentcolor = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ContentColor
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String ContentColor { get { return this.contentcolor; } set { this.contentcolor = value; } }

        /// <summary>Valore mappato sul campo CaptionFont</summary>
        protected String captionfont = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo CaptionFont
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String CaptionFont { get { return this.captionfont; } set { this.captionfont = value; } }

        /// <summary>Valore mappato sul campo ContentFont</summary>
        protected String contentfont = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo ContentFont
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String ContentFont { get { return this.contentfont; } set { this.contentfont = value; } }

        /// <summary>Valore mappato sul campo BorderWidth</summary>
        protected Double borderwidth = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo BorderWidth
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double BorderWidth { get { return this.borderwidth; } set { this.borderwidth = value; } }

        /// <summary>Valore mappato sul campo IdAlignment</summary>
        protected Int32 idalignment = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdAlignment
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdAlignment { get { return this.idalignment; } set { this.idalignment = value; } }

        /// <summary>Valore mappato sul campo XOrigin</summary>
        protected Double xorigin = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo XOrigin
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double XOrigin { get { return this.xorigin; } set { this.xorigin = value; } }

        /// <summary>Valore mappato sul campo YOrigin</summary>
        protected Double yorigin = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo YOrigin
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double YOrigin { get { return this.yorigin; } set { this.yorigin = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "Type".</summary>
            public const string Type = "Type";
            /// <summary>Nome campo "IdWatermark".</summary>
            public const string IdWatermark = "IdWatermark";
            /// <summary>Nome campo "Name".</summary>
            public const string Name = "Name";
            /// <summary>Nome campo "Caption".</summary>
            public const string Caption = "Caption";
            /// <summary>Nome campo "IdType".</summary>
            public const string IdType = "IdType";
            /// <summary>Nome campo "Width".</summary>
            public const string Width = "Width";
            /// <summary>Nome campo "Height".</summary>
            public const string Height = "Height";
            /// <summary>Nome campo "BackColor".</summary>
            public const string BackColor = "BackColor";
            /// <summary>Nome campo "ForeColor".</summary>
            public const string ForeColor = "ForeColor";
            /// <summary>Nome campo "ContentColor".</summary>
            public const string ContentColor = "ContentColor";
            /// <summary>Nome campo "CaptionFont".</summary>
            public const string CaptionFont = "CaptionFont";
            /// <summary>Nome campo "ContentFont".</summary>
            public const string ContentFont = "ContentFont";
            /// <summary>Nome campo "BorderWidth".</summary>
            public const string BorderWidth = "BorderWidth";
            /// <summary>Nome campo "IdAlignment".</summary>
            public const string IdAlignment = "IdAlignment";
            /// <summary>Nome campo "XOrigin".</summary>
            public const string XOrigin = "XOrigin";
            /// <summary>Nome campo "YOrigin".</summary>
            public const string YOrigin = "YOrigin";

		}
		#endregion
	}
}
