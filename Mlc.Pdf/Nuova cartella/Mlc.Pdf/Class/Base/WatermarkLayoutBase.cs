using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class WatermarkLayoutBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "WatermarkLayout";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public WatermarkLayoutBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public WatermarkLayoutBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Proprietà
        /// <summary>Valore mappato sul campo IdLayout</summary>
        protected Int32 idlayout = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdLayout
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdLayout { get { return this.idlayout; } set { this.idlayout = value; } }

        /// <summary>Valore mappato sul campo IdFormat</summary>
        protected Int32 idformat = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFormat
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdFormat { get { return this.idformat; } set { this.idformat = value; } }

        /// <summary>Valore mappato sul campo IdFamily</summary>
        protected Int32 idfamily = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFamily
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdFamily { get { return this.idfamily; } set { this.idfamily = value; } }

        /// <summary>Valore mappato sul campo Name</summary>
        protected String name = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Valore mappato sul campo Description</summary>
        protected String description = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Description { get { return this.description; } set { this.description = value; } }

        /// <summary>Valore mappato sul campo XOrigin</summary>
        protected Double xorigin = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo XOrigin
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double XOrigin { get { return this.xorigin; } set { this.xorigin = value; } }

        /// <summary>Valore mappato sul campo YOrigin</summary>
        protected Double yorigin = default(Double);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo YOrigin
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Double YOrigin { get { return this.yorigin; } set { this.yorigin = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "IdLayout".</summary>
            public const string IdLayout = "IdLayout";
            /// <summary>Nome campo "IdFormat".</summary>
            public const string IdFormat = "IdFormat";
            /// <summary>Nome campo "IdFamily".</summary>
            public const string IdFamily = "IdFamily";
            /// <summary>Nome campo "Name".</summary>
            public const string Name = "Name";
            /// <summary>Nome campo "Description".</summary>
            public const string Description = "Description";
            /// <summary>Nome campo "XOrigin".</summary>
            public const string XOrigin = "XOrigin";
            /// <summary>Nome campo "YOrigin".</summary>
            public const string YOrigin = "YOrigin";

		}
		#endregion
	}
}
