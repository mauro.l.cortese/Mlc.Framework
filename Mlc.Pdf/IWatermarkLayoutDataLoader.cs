﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-09-2019
// ***********************************************************************
// <copyright file="IWatermarkLayoutDataLoader.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

using DevExpress.Pdf;
using System.Data;


namespace Mlc.Pdf
{
    /// <summary>
    /// Interface IWatermarkLayoutDataLoader
    /// </summary>
    public interface IWatermarkLayoutDataLoader
    {
        /// <summary>
        /// Loads the iso format.
        /// </summary>
        /// <param name="pdfRectangle">The PDF rectangle.</param>
        /// <returns>IsoFormat.</returns>
        IsoFormat LoadIsoFormat(PdfRectangle pdfRectangle);

        /// <summary>
        /// Loads the watermark layout.
        /// </summary>
        /// <param name="isoFormat">The iso format.</param>
        /// <param name="familyName">Name of the family.</param>
        /// <returns>WatermarkLayout.</returns>
        WatermarkLayout LoadWatermarkLayout(IsoFormat isoFormat, string familyName);

        /// <summary>
        /// Loads the watermarks.
        /// </summary>
        /// <param name="watermarkLayout">The watermark layout.</param>
        /// <returns>WatermarkCollection.</returns>
        WatermarkCollection LoadWatermarks(WatermarkLayout watermarkLayout);

    }
}