// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 07-08-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-08-2019
// ***********************************************************************
// <copyright file="IWatermarkContent.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Pdf
{
    /// <summary>
    /// Interface IWatermarkContent
    /// </summary>
    public interface IWatermarkContent
	{
        #region Propriet�
        /// <summary>
        /// Gets or sets the content value.
        /// </summary>
        /// <value>The content value.</value>
        object ContentValue { get; set; }
        #endregion
	}
}