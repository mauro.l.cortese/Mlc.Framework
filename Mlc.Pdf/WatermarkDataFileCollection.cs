﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 12-05-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-05-2019
// ***********************************************************************
// <copyright file="WatermarkDataFileCollection.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;


namespace Mlc.Pdf
{
    /// <summary>
    /// Class WatermarkDataFileCollection.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List{Mlc.Pdf.WatermarkDataFile}" />
    public class WatermarkDataFileCollection : List<WatermarkDataFile>, IDisposable
    {
        private WatermarkMgr watermarkMgr = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WatermarkDataFileCollection" /> class.
        /// </summary>
        /// <param name="watermarkMgr">The watermark MGR.</param>
        public WatermarkDataFileCollection(WatermarkMgr watermarkMgr)
        {
            this.watermarkMgr = watermarkMgr;
        }

        /// <summary>
        /// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite.
        /// </summary>
        public void Dispose()
        {
            if (this.watermarkMgr != null)
                this.watermarkMgr.Dispose();
        }

        /// <summary>
        /// Processes all file.
        /// </summary>
        public void ProcessAllFile()
        {
			foreach (WatermarkDataFile watermarkDataFile in this)
				this.watermarkMgr.ProcessWatermarks(watermarkDataFile);
		}
	}
}