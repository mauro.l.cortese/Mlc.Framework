﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-04-2019
// ***********************************************************************
// <copyright file="ContentValues.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;

namespace Mlc.Pdf
{
    /// <summary>
    /// Class WatermarkDataFile.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.Object}" />
    /// <seealso cref="Dictionary{String, Object}" />
    public class WatermarkDataFile : Dictionary<string, object>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WatermarkDataFile"/> class.
        /// </summary>
        /// <param name="pdfSource">The PDF source.</param>
        /// <param name="pdfDestination">The PDF destination.</param>
        public WatermarkDataFile(string pdfSource, string pdfDestination)
        {
            this.PdfSource = pdfSource;
            this.PdfDestination = pdfDestination;
        }

        /// <summary>
        /// Gets or sets the PDF source.
        /// </summary>
        /// <value>The PDF source.</value>
        public string PdfSource { get; set; }
        /// <summary>
        /// Gets or sets the PDF destination.
        /// </summary>
        /// <value>The PDF destination.</value>
        public string PdfDestination { get; set; }
    }
}