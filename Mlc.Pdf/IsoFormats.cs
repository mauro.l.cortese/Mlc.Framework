﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 07-06-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-06-2019
// ***********************************************************************
// <copyright file="IsoFormat.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Pdf
{
    /// <summary>
    /// Enum IsoFormat
    /// </summary>
    public enum IsoFormats
    {
        /// <summary>
        /// The undefine/
        /// </summary>
        Undefine,
        /// <summary>
        /// The hor a0
        /// </summary>
        A0H,
        /// <summary>
        /// The ver a0
        /// </summary>
        A0V,
        /// <summary>
        /// The hor a1
        /// </summary>
        A1H,
        /// <summary>
        /// The hor a2
        /// </summary>
        A2H,
        /// <summary>
        /// The ver a1
        /// </summary>
        A1V,
        /// <summary>
        /// The ver a2
        /// </summary>
        A2V,
        /// <summary>
        /// The hor a3
        /// </summary>
        A3H,
        /// <summary>
        /// The ver a3
        /// </summary>
        A3V,
        /// <summary>
        /// The hor a4
        /// </summary>
        A4H,
        /// <summary>
        /// The ver a4
        /// </summary>
        A4V,
    }
}