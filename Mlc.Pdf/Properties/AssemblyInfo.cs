﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 07-06-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-06-2019
// ***********************************************************************
// <copyright file="AssemblyInfo.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Le informazioni generali relative a un assembly sono controllate dal seguente 
// set di attributi. Modificare i valori di questi attributi per modificare le informazioni
// associate a un assembly.
[assembly: AssemblyTitle("Mlc.Pdf")]
[assembly: AssemblyDescription("Library tools for PDF document")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MLC")]
[assembly: AssemblyProduct("Mlc.Pdf")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("MLC")]
[assembly: AssemblyCulture("")]

// Se si imposta ComVisible su false, i tipi in questo assembly non saranno visibili
// ai componenti COM. Se è necessario accedere a un tipo in questo assembly da
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID seguente verrà utilizzato come ID della libreria dei tipi
[assembly: Guid("21dfeb45-5721-42f5-9143-6d6f5921c4c4")]

// Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
//
//      Versione principale
//      Versione secondaria
//      Numero di build
//      Revisione
//
// È possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build
// usando l'asterisco '*' come illustrato di seguito:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("22.4.1.0")]
[assembly: AssemblyFileVersion("22.4.1.0")]
