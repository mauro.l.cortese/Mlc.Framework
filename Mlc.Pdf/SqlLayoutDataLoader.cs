﻿// ***********************************************************************
// Assembly         : WinFormPdfTest
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-02-2019
// ***********************************************************************
// <copyright file="SqlLayoutDataLoader.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Pdf;
using Mlc.Data;
using Mlc.Shell;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Mlc.Pdf
{
	/// <summary>
	/// Enum SqlKeys
	/// </summary>
	internal enum SqlKeys
	{
		/// <summary>
		/// The get database types
		/// </summary>
		GetDbTypes,
		/// <summary>
		/// The get iso data
		/// </summary>
		GetIsoData,
		/// <summary>
		/// The get watermark layout data
		/// </summary>
		GetWatermarkLayoutData,
		/// <summary>
		/// The get watermarks
		/// </summary>
		GetWatermarks
	}

	/// <summary>
	/// Enum MappingDataNames
	/// </summary>
	internal enum MappingDataNames
	{
		/// <summary>
		/// The iso format
		/// </summary>
		IsoFormat,
		/// <summary>
		/// The table
		/// </summary>
		Table,
		/// <summary>
		/// The watermark layout
		/// </summary>
		WatermarkLayout,
		/// <summary>
		/// The watermark
		/// </summary>
		Watermark
	}

	/// <summary>
	/// Class SqlLayoutDataLoader.
	/// </summary>
	/// <seealso cref="Mlc.Pdf.IWatermarkLayoutDataLoader" />
	public class SqlLayoutDataLoader : IWatermarkLayoutDataLoader
	{
		#region Constants
		/// <summary>
		/// The par width pix rounded
		/// </summary>
		private const string par_WidthPixRounded = "@WidthPixRounded";
		/// <summary>
		/// The par height pix rounded
		/// </summary>
		private const string par_HeightPixRounded = "@HeightPixRounded";
		/// <summary>
		/// The par identifier format
		/// </summary>
		private const string par_IdFormat = "@IdFormat";
		/// <summary>
		/// The par identifier family
		/// </summary>
		private const string par_FamilyName = "@FamilyName";
		/// <summary>
		/// The par identifier layout
		/// </summary>
		private const string par_IdLayout = "@IdLayout";
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The SQL command engine
		/// </summary>
		private SqlCmdEngine sqlCmdEngine;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlLayoutDataLoader"/> class.
		/// </summary>
		/// <param name="connStrBuilder">The connection string builder.</param>
		public SqlLayoutDataLoader(SqlConnectionStringBuilder connStrBuilder) : this(connStrBuilder.ConnectionString)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="SqlLayoutDataLoader"/> class.
		/// </summary>
		/// <param name="sqlStringConnection">The SQL string connection.</param>
		public SqlLayoutDataLoader(string sqlStringConnection)
			: this(new SqlCmdEngine() { StringConnection = sqlStringConnection })
		{
			//this.sqlCmdEngine = new SqlCmdEngine() { StringConnection = sqlStringConnection };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SqlLayoutDataLoader"/> class.
		/// </summary>
		/// <param name="sqlCmdEngine">The SQL command engine.</param>
		public SqlLayoutDataLoader(SqlCmdEngine sqlCmdEngine)
		{
			this.sqlCmdEngine = sqlCmdEngine;
			//this.sqlCmdEngine.BeforeExecuteSql += (s, e) => { Console.WriteLine(e.SqlCmd.CommandText); };
			sqlCmdEngine.LoadQuery();
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Loads the iso format.
		/// </summary>
		/// <param name="pdfRectangle">The PDF rectangle.</param>
		/// <returns>IsoFormat.</returns>
		public IsoFormat LoadIsoFormat(PdfRectangle pdfRectangle)
		{
			IsoFormat isoFormat = null;
			try
			{
				sqlCmdEngine.SetSqlData(SqlKeys.GetIsoData);
				using (SqlCommand sqlCmd = sqlCmdEngine.GetStoredProcedure())
				{
					sqlCmd.Parameters.Add(par_WidthPixRounded, SqlDbType.Int).Value = Math.Round(pdfRectangle.Width, 0);
					sqlCmd.Parameters.Add(par_HeightPixRounded, SqlDbType.Int).Value = Math.Round(pdfRectangle.Height, 0);
					DataTable resultTable = sqlCmdEngine.ExecuteSpReader();
					if (resultTable.Rows.Count == 1)
						isoFormat = new IsoFormat(resultTable.Rows[0]);

					sqlCmd.Connection.Dispose();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return isoFormat;
		}

		/// <summary>
		/// Loads the watermark layout.
		/// </summary>
		/// <param name="isoFormat">The iso format.</param>
		/// <param name="familyName">Name of the family.</param>
		/// <returns>WatermarkLayout.</returns>
		public WatermarkLayout LoadWatermarkLayout(IsoFormat isoFormat, string familyName)
		{
			WatermarkLayout watermarkLayout = null;
			try
			{
				sqlCmdEngine.SetSqlData(SqlKeys.GetWatermarkLayoutData);
				using (SqlCommand sqlCmd = sqlCmdEngine.GetStoredProcedure())
				{
					sqlCmd.Parameters.Add(par_IdFormat, SqlDbType.Int).Value = isoFormat.IdFormat;
					sqlCmd.Parameters.Add(par_FamilyName, SqlDbType.NVarChar).Value = familyName;
					DataTable resultTable = sqlCmdEngine.ExecuteSpReader();
					
					
					
					if (resultTable.Rows.Count == 1)
						watermarkLayout = new WatermarkLayout(resultTable.Rows[0]);

					sqlCmd.Connection.Dispose();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return watermarkLayout;
		}

		/// <summary>
		/// Loads the watermarks.
		/// </summary>
		/// <param name="watermarkLayout">The watermark layout.</param>
		/// <returns>WatermarkCollection.</returns>
		public WatermarkCollection LoadWatermarks(WatermarkLayout watermarkLayout)
		{
			WatermarkCollection watermarkCollection = null;
			try
			{
				sqlCmdEngine.SetSqlData(SqlKeys.GetWatermarks);
				using (SqlCommand sqlCmd = sqlCmdEngine.GetStoredProcedure())
				{
					sqlCmd.Parameters.Add(par_IdLayout, SqlDbType.Int).Value = watermarkLayout.IdLayout;
					DataTable resultTable = sqlCmdEngine.ExecuteSpReader();
					if (resultTable != null)
						watermarkCollection = new WatermarkCollection() { DataTable = resultTable };
					sqlCmd.Connection.Dispose();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return watermarkCollection;
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}
}