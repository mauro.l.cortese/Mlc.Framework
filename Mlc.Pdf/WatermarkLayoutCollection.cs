﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-07-2019
// ***********************************************************************
// <copyright file="WatermarkLayoutCollection.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;

namespace Mlc.Pdf
{
    /// <summary>
    /// Class WatermarkLayoutCollection.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Pdf.WatermarkLayout}" />
    public class WatermarkLayoutCollection : Dictionary<string, WatermarkLayout>
    {
        /// <summary>
        /// Adds the specified watermark layout data.
        /// </summary>
        /// <param name="watermarkLayoutData">The watermark layout data.</param>
        internal void Add(WatermarkLayout watermarkLayoutData)
        {
            base.Add(watermarkLayoutData.Name, watermarkLayoutData);
        }
    }
}