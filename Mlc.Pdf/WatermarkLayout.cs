﻿using System;
using System.Drawing;
using DevExpress.Pdf;

namespace Mlc.Pdf
{
    /// <summary>
    /// Class Origin.
    /// </summary>
    public class WatermarkLayout
    {
        private PdfPage pdfPage = null;
        private const double mf = 0.35278f;

        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        /// <value>The origin.</value>
        public PointF Origin { get; set; } = new PointF(0, 0);

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        /// <value>The property.</value>
        public WatermarkCollection Watermarks { get; set; } = new WatermarkCollection();

        public IsoFormat IsoFormat { get; set; }

        public PdfPage Page{ get; set; }
           }
}