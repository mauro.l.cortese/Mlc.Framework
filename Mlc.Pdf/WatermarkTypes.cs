﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-08-2019
// ***********************************************************************
// <copyright file="WatermarkTypes.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Pdf
{
    /// <summary>
    /// Enum WatermarkTypes
    /// </summary>
    internal enum WatermarkTypes
    {
        /// <summary>
        /// The undefine
        /// </summary>
        Undefine,
        /// <summary>
        /// The text
        /// </summary>
        Text,
        /// <summary>
        /// The image
        /// </summary>
        Image,
		/// <summary>
		/// The qr code
		/// </summary>
		QRCode,
		/// <summary>
		/// The qr code
		/// </summary>
		TextWithOutBackground,
	}
}