﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 07-06-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-06-2019
// ***********************************************************************
// <copyright file="IBox.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;

namespace Mlc.Pdf
{
    /// <summary>
    /// Interface IBox
    /// </summary>
    public interface IWatermarkOld
    {
        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>The caption.</value>
        string Caption { get; set; }
        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>The label.</value>
        string Label { get; set; }
        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>The position.</value>
        PointF Position { get; set; }
        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The size.</value>
        SizeF Size { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; set; }
    }
}