﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-02-2019
// ***********************************************************************
// <copyright file="WatermarkMgr.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Pdf
{
    /// <summary>
    /// Delegate SetDataPage
    /// </summary>
    /// <param name="watermarkLayout">The watermark layout.</param>
    internal delegate void SetDataPage(WatermarkLayout watermarkLayout);
}
