// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-03-2019
// ***********************************************************************
// <copyright file="Watermark.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.BarCodes;
using DevExpress.Pdf;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Data;
using Mlc.Data;

namespace Mlc.Pdf
{
    /// <summary>
    /// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator" />
    /// modificare solo questa classe per aggiungere funzionalità
    /// </summary>
    /// <seealso cref="Mlc.Pdf.WatermarkBase" />
    /// <seealso cref="Mlc.Pdf.IWatermark" />
    /// <seealso cref="Mlc.Pdf.IWatermarkContent" />
    public class Watermark : WatermarkBase, IWatermark, IWatermarkContent
    {
        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Watermark"/> class.
        /// </summary>
        public Watermark()
        { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataRow">DataRow su cui mappare l'istanza</param>
        public Watermark(DataRow dataRow)
            : base(dataRow)
        {
        }
        #endregion

        #region Event Handlers - CommandsEngine
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// Gets or sets the drawing dpi.
        /// </summary>
        /// <value>The drawing dpi.</value>
        public static float DrawingDpi { get; set; } = 72f;

        /// <summary>
        /// Gets or sets the mk.
        /// </summary>
        /// <value>The mk.</value>
        public static float Mk { get; set; } = 2.83464761904762f; // 0.352777535126563

        /// <summary>
        /// Gets or sets the pt to px.
        /// </summary>
        /// <value>The pt to px.</value>
        public static float PtToPx { get; set; } = 1.3281472327365f; // 0.75292857248934

        /// <summary>
        /// Gets or sets the Kal text alignment coefficient.
        /// </summary>
        /// <value>The fs coeff.</value>
        public static float TacK { get; set; } = 0.78f;

        /// <summary>
        /// Gets or sets the content value.
        /// </summary>
        /// <value>The content value.</value>
        public virtual object ContentValue { get; set; }

        private PointF location = default(PointF);
        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <value>The location.</value>
        public PointF Location
        {
            get { return this.location; }
            internal set
            {
                this.location = value;
                this.Rectangle = new RectangleF(this.Location, this.getSize());
            }
        }

        /// <summary>
        /// Gets the rectangle.
        /// </summary>
        /// <value>The rectangle.</value>
        public RectangleF Rectangle { get; internal set; }

        /// <summary>
        /// Gets the PDF graphics.
        /// </summary>
        /// <value>The PDF graphics.</value>
        public PdfGraphics PdfGraphics { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [design mode].
        /// </summary>
        /// <value><c>true</c> if [design mode]; otherwise, <c>false</c>.</value>
        public bool DesignMode { get; internal set; }
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Plots the content text.
        /// </summary>
        public void PlotContentText(bool plotBackGroud = true)
        {
            string textValue = this.ContentValue.ToString();
            using (Font font = this.getContentFont())
            {
                Color contentcolor = this.ContentColor.ParseColor();

                using (SolidBrush sbrush = new SolidBrush(contentcolor))
                {
                    if (plotBackGroud)
                    {
                        RectangleF rect = this.getContentRectangle();
                        this.PdfGraphics.DrawString(textValue, font, sbrush, rect);
                        this.drawBorder(rect, textValue, font);
                    }
                    else
						this.PdfGraphics.DrawString(textValue, font, sbrush, this.getCaptionPoint());
				}
            }
        }


        /// <summary>
        /// Plots the caption.
        /// </summary>
        public void PlotCaption()
        {
            try
            {
                if (string.IsNullOrEmpty(this.Caption)) return;

                using (Font font = this.getCaptionFont())
                using (SolidBrush sbrush = new SolidBrush((this.ForeColor.ParseColor())))
                {
                    this.PdfGraphics.DrawString(this.Caption, font, sbrush, this.getCaptionPoint());
                    this.drawBorder(this.Caption, font);
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        /// <summary>
        /// Plots the content qr code.
        /// </summary>
        public void PlotContentQRCode()
        {
            try
            {
                Color contentcolor = this.ContentColor.ParseColor();
                Image barCode = this.getBarCode((string)this.ContentValue, Symbology.QRCode, contentcolor, this.BackColor.ParseColor());
                using (SolidBrush sbrush = new SolidBrush(contentcolor))
                    this.PdfGraphics.DrawImage(barCode, new RectangleF(this.Location, new SizeF((float)(this.Width * Watermark.Mk), (float)(this.Height * Watermark.Mk))), new RectangleF(0, 0, barCode.Width, barCode.Height), GraphicsUnit.Document);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }


        /// <summary>
        /// Plots the border.
        /// </summary>
        public void PlotBorder()
        {
            float borderWidth = (float)this.BorderWidth;
            if (borderWidth > 0f)
            {
                using (Brush brush = new SolidBrush(this.ForeColor.ParseColor()))
                using (Pen pen = new Pen(brush, borderWidth))
                    this.PdfGraphics.DrawRectangle(pen, new RectangleF(this.Location, this.getSize()));
            }
        }

        /// <summary>
        /// Plots the backgroud.
        /// </summary>
        public void PlotBackgroud()
        {
            try
            {
                using (Brush brush = new SolidBrush(this.BackColor.ParseColor()))
                    this.PdfGraphics.FillRectangle(brush, new RectangleF(this.Location, this.getSize()));
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>Watermark.</returns>
        internal new Watermark Clone()
        {
            return (Watermark)base.Clone();
        }
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Gets the bar code.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="symbology">The symbology.</param>
        /// <param name="foreColor">Color of the fore.</param>
        /// <param name="bakColor">Color of the backup.</param>
        /// <returns>Image.</returns>
        private Image getBarCode(string text, Symbology symbology, Color foreColor, Color bakColor)
        {
            Image image = null;
            try
            {
                BarCode barCode = new()
                {
                    Symbology = symbology,
                    CodeText = text,
                    BackColor = bakColor,
                    ForeColor = foreColor,
                    RotationAngle = 0,
                    CodeBinaryData = Encoding.Default.GetBytes(text),
                    Module = 1f
                };

                barCode.Options.QRCode.CompactionMode = QRCodeCompactionMode.Byte;
                barCode.Options.QRCode.ErrorLevel = QRCodeErrorLevel.Q;
                barCode.Options.QRCode.ShowCodeText = false;

                image = barCode.BarCodeImage;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return image;
        }

        /// <summary>
        /// Gets the font.
        /// </summary>
        /// <param name="fontTxt">The font text.</param>
        /// <returns>Font.</returns>
        private Font getFont(string fontTxt)
        {
            Font font = null;
            try
            {
                font = fontTxt.StringToFont();
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            if (font == null)
                font = new Font("Verdana", 5.5f, FontStyle.Regular);

            //Console.WriteLine(font.FontToString());
            return font;
        }

        /// <summary>
        /// Draws the border.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="font">The font.</param>
        /// <param name="width">The width.</param>
        private void drawBorder(string text, Font font, float width = 0.3f)
        {
            if (width == 0 || !this.DesignMode) return;
            using (Brush brush = new SolidBrush(Color.Red))
            using (Pen pen = new Pen(brush, width))
                this.PdfGraphics.DrawRectangle(pen, new RectangleF(this.Location, this.getRectangleText(text, font)));
        }

        private void drawBorder(RectangleF rect, string textValue, Font font, float width = 0.3f)
        {
            if (width == 0 || !this.DesignMode) return;
            using (Brush brush = new SolidBrush(Color.Red))
            using (Pen pen = new Pen(brush, width))
                this.PdfGraphics.DrawRectangle(pen, rect);
        }


        /// <summary>
        /// Gets the rectangle text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="font">The font.</param>
        /// <returns>SizeF.</returns>
        private SizeF getRectangleText(string text, Font font)
        {
            if (string.IsNullOrEmpty(text))
                return default(SizeF);

            SizeF sizeText = this.PdfGraphics.MeasureString(text, font);
            sizeText = new SizeF
                (
                sizeText.Width, //* FsCoeff,
                sizeText.Height //* FsCoeff
                );
            return sizeText;
        }

        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <returns>SizeF.</returns>
        private SizeF getSize()
        {
            float width = (float)this.Width * Mk;
            float height = (float)this.Height * Mk;
            SizeF sizeF = new SizeF(width, height);
            return sizeF;
        }

        /// <summary>
        /// Gets the caption point.
        /// </summary>
        /// <returns>PointF.</returns>
        private PointF getCaptionPoint()
        {
            return this.Location;
        }

        ///// <summary>
        ///// Gets the content point.
        ///// </summary>
        ///// <returns>PointF.</returns>
        //private PointF getContentPoint()
        //{
        //    PointF point = this.Location;

        //    WatermarkAlignments watermarkAlignments = (WatermarkAlignments)this.IdAlignment;
        //    switch (watermarkAlignments)
        //    {
        //        case WatermarkAlignments.Undefine:
        //            break;
        //        case WatermarkAlignments.Left:
        //            break;
        //        case WatermarkAlignments.Middle:
        //            point = new PointF
        //                (
        //                this.Location.X + ((float)this.Width * Mk * 0.5f),
        //                this.Location.Y + ((float)this.Height * Mk * 0.5f)
        //                );
        //            break;
        //        case WatermarkAlignments.Right:
        //            break;
        //    }

        //    return point;
        //}

        /// <summary>
        /// Gets the content rectangle.
        /// </summary>
        private RectangleF getContentRectangle() //(SizeF sizeF)
        {
            PointF pointText = this.Location;
            WatermarkAlignments watermarkAlignments = (WatermarkAlignments)this.IdAlignment;

            float hCap = this.getRectangleText(this.Caption, this.getCaptionFont()).Height;
            SizeF sizeTextContent = this.getRectangleText(this.ContentValue.ToString(), this.getContentFont());
            float deltaX = (this.Rectangle.Width - sizeTextContent.Width * TacK) / 2;
            float deltaY = (this.Rectangle.Height - hCap - sizeTextContent.Height) / 2 + hCap;

            switch (watermarkAlignments)
            {
                case WatermarkAlignments.Undefine:
                    break;
                case WatermarkAlignments.Left:
                    pointText = new PointF
                        (
                            this.Location.X,
                            this.Location.Y + deltaY
                        );
                    break;
                case WatermarkAlignments.Middle:
                    pointText = new PointF
                        (
                            this.Location.X + deltaX,
                            this.Location.Y + deltaY
                        );
                    break;
                case WatermarkAlignments.Right:
                    pointText = new PointF
                        (
                            this.Location.X + deltaX * 2,
                            this.Location.Y + deltaY
                        );
                    break;
            }
            RectangleF rectangleF = new RectangleF(pointText, sizeTextContent);
            return rectangleF;
        }

        /// <summary>
        /// Gets the caption font.
        /// </summary>
        /// <returns>Font.</returns>
        private Font getCaptionFont()
        {
            return getFont(this.CaptionFont);
        }

        /// <summary>
        /// Gets the content font.
        /// </summary>
        /// <returns>Font.</returns>
        private Font getContentFont()
        {
            return getFont(this.ContentFont);
        }
        #endregion

        #region Event Handlers
        #endregion
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
