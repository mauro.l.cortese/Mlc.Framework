
using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// modificare solo questa classe per aggiungere funzionalità
	/// </summary>
	public class IsoFormat : IsoFormatBase, IIsoFormat 
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public IsoFormat()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public IsoFormat(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion
		
		#region Propriet�
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// Restituisce una nuova istanza ottenuta dalla copia di quella corrente
		/// </summary>
		/// <returns>Nuova istanza ottenuta dalla copia di quella corrente</returns>
		public new IsoFormat Clone()
		{
			return (IsoFormat)base.Clone();
		}
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}
}
