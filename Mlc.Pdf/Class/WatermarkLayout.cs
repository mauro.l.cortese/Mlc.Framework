using System.Drawing;
using System;
using System.Data;
using Mlc.Data;
using DevExpress.Pdf;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// modificare solo questa classe per aggiungere funzionalità
	/// </summary>
	public class WatermarkLayout : WatermarkLayoutBase, IWatermarkLayout 
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public WatermarkLayout()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public WatermarkLayout(DataRow dataRow)
			:base(dataRow)
		{
        }

        public IsoFormat IsoFormat { get; set; }

        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        /// <value>The origin.</value>
        public PointF Origin { get; set; } = new PointF(0, 0);

        public PdfPage Page { get; set; }

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        /// <value>The property.</value>
        public WatermarkCollection Watermarks { get; set; } = new WatermarkCollection();

        #endregion

        #region Propriet�
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce una nuova istanza ottenuta dalla copia di quella corrente
        /// </summary>
        /// <returns>Nuova istanza ottenuta dalla copia di quella corrente</returns>
        public new WatermarkLayout Clone()
		{
			return (WatermarkLayout)base.Clone();
		}
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}
}
