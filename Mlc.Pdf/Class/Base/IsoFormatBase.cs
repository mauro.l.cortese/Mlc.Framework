using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Pdf
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class IsoFormatBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "IsoFormat";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public IsoFormatBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public IsoFormatBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Propriet�
        /// <summary>Valore mappato sul campo IdFormat</summary>
        protected Int32 idformat = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdFormat
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdFormat { get { return this.idformat; } set { this.idformat = value; } }

        /// <summary>Valore mappato sul campo Name</summary>
        protected String name = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Valore mappato sul campo Description</summary>
        protected String description = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Description { get { return this.description; } set { this.description = value; } }

        /// <summary>Valore mappato sul campo Width</summary>
        protected Int32 width = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Width
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 Width { get { return this.width; } set { this.width = value; } }

        /// <summary>Valore mappato sul campo Height</summary>
        protected Int32 height = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Height
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 Height { get { return this.height; } set { this.height = value; } }

        /// <summary>Valore mappato sul campo WidthPixRounded</summary>
        protected Int32 widthpixrounded = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo WidthPixRounded
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 WidthPixRounded { get { return this.widthpixrounded; } set { this.widthpixrounded = value; } }

        /// <summary>Valore mappato sul campo HeightPixRounded</summary>
        protected Int32 heightpixrounded = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo HeightPixRounded
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 HeightPixRounded { get { return this.heightpixrounded; } set { this.heightpixrounded = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "IdFormat".</summary>
            public const string IdFormat = "IdFormat";
            /// <summary>Nome campo "Name".</summary>
            public const string Name = "Name";
            /// <summary>Nome campo "Description".</summary>
            public const string Description = "Description";
            /// <summary>Nome campo "Width".</summary>
            public const string Width = "Width";
            /// <summary>Nome campo "Height".</summary>
            public const string Height = "Height";
            /// <summary>Nome campo "WidthPixRounded".</summary>
            public const string WidthPixRounded = "WidthPixRounded";
            /// <summary>Nome campo "HeightPixRounded".</summary>
            public const string HeightPixRounded = "HeightPixRounded";

		}
		#endregion
	}
}
