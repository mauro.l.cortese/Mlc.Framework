﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-07-2019
// ***********************************************************************
// <copyright file="WatermarkCollection.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;
using System.Data;

namespace Mlc.Pdf
{
    /// <summary>
    /// Class WatermarkCollection.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Pdf.Watermark}" />
    public class WatermarkCollection : Dictionary<string, Watermark>
    {
        /// <summary>
        /// Sets the data table.
        /// </summary>
        /// <value>The data table.</value>
        public DataTable DataTable
        {
            set
            {
                DataTable dataTable = value;
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    this.Add(new Watermark(dataRow));
                }
            }
        }

        /// <summary>
        /// Adds the specified watermark.
        /// </summary>
        /// <param name="watermark">The watermark.</param>
        private void Add(Watermark watermark)
        {
            this.Add(watermark.Name, watermark);
        }
    }
}