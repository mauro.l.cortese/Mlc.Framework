﻿// ***********************************************************************
// Assembly         : Mlc.Pdf
// Author           : mauro.luigi.cortese
// Created          : 11-28-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-02-2019
// ***********************************************************************
// <copyright file="WatermarkMgr.cs" company="MLC">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Pdf;
using Mlc.Data;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Mlc.Pdf
{



	/// <summary>
	/// Class PdfMgr.
	/// </summary>
	/// <seealso cref="System.IDisposable" />
	public class WatermarkMgr : IDisposable
	{

		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields        
		/// <summary>
		/// The processor
		/// </summary>
		private PdfDocumentProcessor processor = new PdfDocumentProcessor();

		/// <summary>
		/// The pages
		/// </summary>
		private IList<PdfPage> pages = null;

		/// <summary>
		/// Gets or sets the file PDF.
		/// </summary>
		/// <value>The file PDF.</value>
		private WatermarkDataFile watermarkDataFile = null;

		/// <summary>
		/// Gets or sets the scheme.
		/// </summary>
		/// <value>The scheme.</value>
		private WatermarkLayoutCollection watermarkLayouts = null;

		private string familyName = null;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="WatermarkMgr"/> class.
		/// </summary>
		/// <param name="dataLoader">The data loader.</param>
		/// <param name="familyName">Name of the family.</param>
		/// <param name="designMode">if set to <c>true</c> [design mode].</param>
		public WatermarkMgr(IWatermarkLayoutDataLoader dataLoader, string familyName, bool designMode = false)
		{
			this.DataLoader = dataLoader;
			this.familyName = familyName;
			this.DesingMode = designMode;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the data loader.
		/// </summary>
		/// <value>The data loader.</value>
		public IWatermarkLayoutDataLoader DataLoader { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [desing mode].
		/// </summary>
		/// <value><c>true</c> if [desing mode]; otherwise, <c>false</c>.</value>
		public bool DesingMode { get; set; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite.
		/// </summary>
		public void Dispose()
		{
			this.processor.Dispose();
		}

		/// <summary>
		/// Loads the PDF scheme.
		/// </summary>
		/// <param name="watermarkDataFile">The watermark data file.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool ProcessWatermarks(WatermarkDataFile watermarkDataFile)
		{
			bool retval = false;

			this.watermarkDataFile = watermarkDataFile;
			try
			{
				processor.LoadDocument(this.watermarkDataFile.PdfSource);
				pages = processor.Document.Pages;
				this.watermarkLayouts = new WatermarkLayoutCollection();
				if (pages != null)
					foreach (PdfPage page in pages)
						this.initPage(page);
				retval = true;
				this.updatePdfAndSave();
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retval;
		}

		/// <summary>
		/// Resizes the specified image.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <param name="newSize">The new size.</param>
		/// <returns>Image.</returns>
		public Image Resize(Image image, Size newSize)
		{
			Image newImage = new Bitmap(newSize.Width, newSize.Height);
			using (var graphics = Graphics.FromImage(newImage))
			{
				graphics.SmoothingMode = SmoothingMode.Default;
				graphics.InterpolationMode = InterpolationMode.Default;
				graphics.PixelOffsetMode = PixelOffsetMode.Default;
				graphics.DrawImage(image, new Rectangle(0, 0, newSize.Width, newSize.Height));
			}
			return newImage;
		}

		/// <summary>
		/// Writes the database types.
		/// </summary>
		/// <param name="nameSpace">The name space.</param>
		/// <param name="rootFolder">The root folder.</param>
		/// <param name="stringConnection">The string connection.</param>
		public static void WriteDbTypes(string nameSpace, string rootFolder, SqlConnectionStringBuilder connStrBuilder)
		{
			WriteDbTypes(nameSpace, rootFolder, connStrBuilder.ConnectionString);
		}

		/// <summary>
		/// Writes the database types.
		/// </summary>
		/// <param name="nameSpace">The name space.</param>
		/// <param name="rootFolder">The root folder.</param>
		/// <param name="stringConnection">The string connection.</param>
		public static void WriteDbTypes(string nameSpace, string rootFolder, string stringConnection)
		{
			const string _interfaces = "Interfaces";
			const string _class = "Class";
			const string _enums = "Enums";

			SqlCmdEngine sqlCmdEngine = new SqlCmdEngine() { StringConnection = stringConnection };
//			sqlCmdEngine.BeforeExecuteSql += (s, ea) => { Console.WriteLine(ea.SqlCmd.CommandText); };
			sqlCmdEngine.LoadQuery();


			Dictionary<string, DataTable> tables = null;

			try
			{
				sqlCmdEngine.SetSqlData(SqlKeys.GetDbTypes);
				using (SqlCommand sqlCmd = sqlCmdEngine.GetStoredProcedure())
				{
					tables = sqlCmdEngine.ExecuteSpReaders(
						MappingDataNames.IsoFormat,
						MappingDataNames.WatermarkLayout,
						MappingDataNames.Watermark
						);
					sqlCmd.Connection.Dispose();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			TypeCodeCreator tcc = new TypeCodeCreator();
			tcc.TableEnum = typeof(MappingDataNames);
			tcc.FolderCode = $"{rootFolder}\\{_class}";
			tcc.FolderCodeInterface = $"{rootFolder}\\{_interfaces}";
			tcc.FolderCodeEnums = $"{rootFolder}\\{_enums}";
			tcc.NameSpace = nameSpace;

			foreach (MappingDataNames dataName in (MappingDataNames[])Enum.GetValues(typeof(MappingDataNames)))
			{
				if (dataName == MappingDataNames.Table) continue;

				// Application
				try { tcc.WriteCodeTypeClass(tables[dataName.ToString()], dataName); }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}

		}

		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the point f.
		/// </summary>
		/// <param name="PointX">The point x.</param>
		/// <param name="PointY">The point y.</param>
		/// <returns>PointF.</returns>
		private PointF getPointF(double PointX, double PointY)
		{
			float x = (float)PointX * Watermark.Mk;
			float y = (float)PointY * Watermark.Mk;
			PointF pointF = new PointF(x, y);
			return pointF;
		}

		/// <summary>
		/// Initializes the page.
		/// </summary>
		/// <param name="page">The page.</param>
		private void initPage(PdfPage page)
		{
			IsoFormat isoFormat = this.DataLoader?.LoadIsoFormat(page.CropBox);
			if (isoFormat != null)
			{
				WatermarkLayout watermarkLayout = this.DataLoader?.LoadWatermarkLayout(isoFormat, this.familyName);
				if (watermarkLayout != null)
				{
					watermarkLayout.Page = page;
					watermarkLayout.IsoFormat = isoFormat;
					watermarkLayout.Watermarks = this.DataLoader?.LoadWatermarks(watermarkLayout);
					this.watermarkLayouts.Add(watermarkLayout);
				}
			}
		}

		/// <summary>
		/// Prepares the graphics.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="graphics">The graphics.</param>
		/// <returns>SizeF.</returns>
		private SizeF prepareGraphics(PdfPage page, PdfGraphics graphics)
		{
			PdfRectangle cropBox = page.CropBox;
			float cropBoxWidth = (float)cropBox.Width;
			float cropBoxHeight = (float)cropBox.Height;

			switch (page.Rotate)
			{
				case 90:
					graphics.RotateTransform(-90);
					graphics.TranslateTransform(-cropBoxHeight, 0);
					return new SizeF(cropBoxHeight, cropBoxWidth);
				case 180:
					graphics.RotateTransform(-180);
					graphics.TranslateTransform(-cropBoxWidth, -cropBoxHeight);
					return new SizeF(cropBoxWidth, cropBoxHeight);
				case 270:
					graphics.RotateTransform(-270);
					graphics.TranslateTransform(0, -cropBoxWidth);
					return new SizeF(cropBoxHeight, cropBoxWidth);
			}
			return new SizeF(cropBoxWidth, cropBoxHeight);
		}

		/// <summary>
		/// Updates the PDF and save.
		/// </summary>
		/// <param name="values">The values.</param>
		private void updatePdfAndSave()
		{
			try
			{
				foreach (WatermarkLayout watermarkLayout in this.watermarkLayouts.Values)
				{
					using (PdfGraphics pdfGraphics = processor.CreateGraphics())
					{
						SizeF actualPageSize = this.prepareGraphics(watermarkLayout.Page, pdfGraphics);
						foreach (Watermark watermark in watermarkLayout.Watermarks.Values)
						{
							watermark.DesignMode = this.DesingMode;
							watermark.Location = this.getPointF(watermarkLayout.XOrigin + watermark.XOrigin, watermarkLayout.YOrigin + watermark.YOrigin);
							watermark.PdfGraphics = pdfGraphics;

							// draw the backgroud
							watermark.PlotBackgroud();

							// draw the caption
							watermark.PlotCaption();

							// draw the content value
							if (this.watermarkDataFile.ContainsKey(watermark.Name))
							{
								watermark.ContentValue = this.watermarkDataFile[watermark.Name];
								WatermarkTypes watermarkTypes = (WatermarkTypes)watermark.IdType;
								switch (watermarkTypes)
								{
									case WatermarkTypes.Text:
										watermark.PlotContentText();
										break;
									case WatermarkTypes.Image:
										break;
									case WatermarkTypes.QRCode:
										watermark.PlotContentQRCode();
										break;
									case WatermarkTypes.TextWithOutBackground:
										watermark.PlotContentText(false);
										break;
								}
							}

							// draw the border
							watermark.PlotBorder();
						}
						pdfGraphics.AddToPageForeground(watermarkLayout.Page, Watermark.DrawingDpi, Watermark.DrawingDpi);
					}
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			finally
			{
				this.OnBeforeSave(new EventArgsPdfSave(this.watermarkDataFile));
				this.processor.SaveDocument(this.watermarkDataFile.PdfDestination);
				this.OnAfterSave(new EventArgsPdfSave(this.watermarkDataFile));
			}
		}


		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions

		#region Event BeforeSave
		/// <summary>
		/// Event BeforeSave.
		/// </summary>
		public event EventHandlerBeforeSave BeforeSave;

		/// <summary>
		/// Occurs when [after save].
		/// </summary>
		public event EventHandlerAfterSave AfterSave;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnBeforeSave(EventArgsPdfSave e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.BeforeSave?.Invoke(this, e);
		}
		
		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnAfterSave(EventArgsPdfSave e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.AfterSave?.Invoke(this, e);
		}
		#endregion

		#region BeforeSave event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerBeforeSave(object sender, EventArgsPdfSave e);

		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerAfterSave(object sender, EventArgsPdfSave e);

		/// <summary>
		/// BeforeSave arguments.
		/// </summary>
		public class EventArgsPdfSave : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initialized a new instance
			/// </summary>
			/// <param name="campo"></param>
			public EventArgsPdfSave(WatermarkDataFile watermarkDataFile) => this.WatermarkDataFile = watermarkDataFile;
			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			public WatermarkDataFile WatermarkDataFile { get; set; }
			#endregion
		}
		#endregion


		#endregion

		#region Embedded Types
		#endregion
	}
}
