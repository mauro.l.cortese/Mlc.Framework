﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mlc.Shell;
using Mlc.Data;

using Mlc.Office.Excel;

namespace Mlc.RnD
{
	public partial class MainForm : DevExpress.XtraEditors.XtraForm
	{
		public MainForm()
		{
			InitializeComponent();
		}

		public void LoadTableFromExcel()
		{
			string fileXlsx = $"{new SysInfo().ExecutableFolder}\\DataTest.xlsx";
			File.WriteAllBytes(fileXlsx, Properties.Resources.ExcelData);
			OpenXmlExcel openXmlExcel = new OpenXmlExcel(fileXlsx);
			DataTable table = openXmlExcel.ReadTable("S01", typeof(ExcelRow));

			DataMapper<ExcelRow>.Rows rows = new ExcelRow().GetDictionary(table, "Id");
			ExcelRow row = new ExcelRow().Init(table.Rows[2]);


			row.Name = "Mauro Luigi";
			row.BirtDay = new DateTime(1972, 05, 22);

			row.UpdateData();

			DataRow drow = table.Rows[2];

			rows.RefreshItems();

			openXmlExcel.WriteTable("S01", rows);

		}

		private void form1_Load(object sender, EventArgs e)
		{
			this.LoadTableFromExcel();
		}
	}


	internal class ExcelRow : DataMapper<ExcelRow>
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public DateTime BirtDay { get; set; }
		public float FloatValue { get; set; }
		public int IntegerValue { get; set; }
	}


}
