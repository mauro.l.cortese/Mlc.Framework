﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
    /// <summary>
    /// Class FormUntil.
    /// </summary>
    public class DataPage
    {

        /// <summary>
        /// From
        /// </summary>
        internal int from = 0;
        /// <summary>
        /// The until
        /// </summary>
        internal int until = 0;
        /// <summary>
        /// The page range
        /// </summary>
        internal int pageRange = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPage" /> class.
        /// </summary>
        /// <param name="pageRange">The page range.</param>
        public DataPage(int pageRange)
        {
            this.pageRange = pageRange;
            this.Reset();
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        internal void Reset()
        {
            this.from = this.pageRange * -1 + 1;
            this.setButtonVisibility();
        }

        /// <summary>
        /// Moves the first.
        /// </summary>
        internal void MoveFirst()
        {
            this.Reset();
        }


        /// <summary>
        /// Moves the last.
        /// </summary>
        internal void MoveLast()
        {
            this.from = this.RowCount / this.pageRange * this.pageRange;
            this.setButtonVisibility();
        }


        /// <summary>
        /// Moves the next.
        /// </summary>
        internal void MoveNext()
        {
            this.from += this.pageRange;
            this.setButtonVisibility();
        }

        /// <summary>
        /// Moves the next.
        /// </summary>
        internal void MovePrevious()
        {
            this.from -= this.pageRange;
            this.setButtonVisibility();
        }

        /// <summary>
        /// Gets from.
        /// </summary>
        /// <value>From.</value>
        public string From { get => this.from.ToString(); }

        /// <summary>
        /// Gets the until.
        /// </summary>
        /// <value>The until.</value>
        public string Until { get => this.until.ToString(); }
        /// <summary>
        /// Gets or sets the row count.
        /// </summary>
        /// <value>The row count.</value>
        public int RowCount { get; set; }
        /// <summary>
        /// Gets or sets the result row count.
        /// </summary>
        /// <value>The result row count.</value>
        public int ResultRowCount { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [next button visibile].
        /// </summary>
        /// <value><c>true</c> if [next button visibile]; otherwise, <c>false</c>.</value>
        public bool NextButtonVisibile { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [first button visibile].
        /// </summary>
        /// <value><c>true</c> if [first button visibile]; otherwise, <c>false</c>.</value>
        public bool FirstButtonVisibile { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [previous button visibile].
        /// </summary>
        /// <value><c>true</c> if [previous button visibile]; otherwise, <c>false</c>.</value>
        public bool PreviousButtonVisibile { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [last button visibile].
        /// </summary>
        /// <value><c>true</c> if [last button visibile]; otherwise, <c>false</c>.</value>
        public bool LastButtonVisibile { get; set; }

        public void SetButtonVisibility(int rowCount, int resultRowCount)
        {
            this.RowCount = rowCount;
            this.ResultRowCount = resultRowCount;
            this.setButtonVisibility();
        }

        /// <summary>
        /// Sents the unitil.
        /// </summary>
        private void setButtonVisibility()
        {
            this.until = this.from + this.pageRange - 1;

            this.FirstButtonVisibile = false;
            this.NextButtonVisibile = false;
            this.PreviousButtonVisibile = false;
            this.LastButtonVisibile = false;

            if (this.RowCount > this.pageRange && this.from > 1)
            {
                this.PreviousButtonVisibile = true;
                this.FirstButtonVisibile = true;
            }
            if (this.until < this.RowCount)
            {
                this.NextButtonVisibile = true;
                this.LastButtonVisibile = true;
            }



            //if (this.ResultRowCount <= this.pageRange)
            //{
            //    this.FirstButtonVisibile = false;
            //    this.NextButtonVisibile = false;
            //    this.PreviousButtonVisibile = false;
            //    this.LastButtonVisibile = false;
            //}
        }

    }


    /// <summary>
    /// Enum PaginalRange
    /// </summary>
    public enum DataPageRange
    {
        NextPage,
        PreviousPage,
        SamePage,
        FirstPage,
        LastPage,
    }

    /// <summary>
    /// Class FromUntilMgr.
    /// </summary>
    public class DataPageMgr
    {
        /// <summary>
        /// The dic from until
        /// </summary>
        Dictionary<Enum, DataPage> dicFromUntil = new Dictionary<Enum, DataPage>();
        /// <summary>
        /// The maximum row per page
        /// </summary>
        private int maxRowPerPage;

        public DataPageMgr(int maxRowPerPage)
        {
            this.maxRowPerPage = maxRowPerPage;
        }


        /// <summary>
        /// Gets the <see cref="DataPage"/> at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>DataPage.</returns>
        public DataPage this[Enum index, DataPageRange paginalRange = DataPageRange.NextPage]
        { get { return this.getDataPage(index, paginalRange); } }

        /// <summary>
        /// Gets the <see cref="DataPage"/> at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>DataPage.</returns>
        public DataPage this[Enum index]
        { get { return this.getDataPage(index, DataPageRange.SamePage); } }


        ///// <summary>
        ///// Gets from until limit.
        ///// </summary>
        ///// <param name="enumSqlCmd">The enum SQL command.</param>
        ///// <param name="paginalRange">The paginal range.</param>
        ///// <returns>FromUntil.</returns>
        //public DataPage GetFirstPage(Enum enumSqlCmd)
        //{
        //    this.Reset(enumSqlCmd);
        //    return this.getDataPage(enumSqlCmd);
        //}

        /// <summary>
        /// Gets from until limit.
        /// </summary>
        /// <param name="index">The enum SQL command.</param>
        /// <param name="paginalRange">The paginal range.</param>
        /// <returns>FromUntil.</returns>
        private DataPage getDataPage(Enum index, DataPageRange paginalRange = DataPageRange.NextPage)
        {
            DataPage fromUntil = null;
            if (!dicFromUntil.ContainsKey(index))
                dicFromUntil.Add(index, new DataPage(this.maxRowPerPage));

            fromUntil = dicFromUntil[index];

            switch (paginalRange)
            {
                case DataPageRange.FirstPage:
                    fromUntil.Reset();
                    fromUntil.MoveNext();
                    break;
                case DataPageRange.NextPage:
                    fromUntil.MoveNext();
                    break;
                case DataPageRange.PreviousPage:
                    fromUntil.MovePrevious();
                    break;
                case DataPageRange.LastPage:
                    fromUntil.MoveLast();
                    break;
                case DataPageRange.SamePage:
                    break;
            }

            if (fromUntil.from < 0)
            {
                fromUntil.Reset();
                fromUntil.MoveNext();
            }
            return fromUntil;
        }

        /// <summary>
        /// Resets from until limit.
        /// </summary>
        /// <param name="enumSqlCmd">The enum SQL command.</param>
        public void Reset(Enum enumSqlCmd)
        {
            if (dicFromUntil.ContainsKey(enumSqlCmd))
                dicFromUntil[enumSqlCmd].Reset();
        }
    }
}
