﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	/// <summary>
	/// Class RepositoryAttributeCollection.
	/// Implements the <see cref="Dictionary{String, FieldAttribute}" />
	/// </summary>
	/// <seealso cref="Dictionary{String, FieldAttribute}" />
	public class RepositoryAttributeCollection : Dictionary<string, FieldAttribute> { }
}
