﻿// ***********************************************************************
// Assembly         : Mlc.Data
// Author           : Cortese Mauro Luigi
// Created          : 12-01-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-01-2017
// ***********************************************************************
// <copyright file="FieldAttribute.cs" company="MLC Development">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	/// <summary>
	/// Enum HorzAlignment
	/// </summary>
	public enum FixedStyleCol
	{
		/// <summary>
		/// The none
		/// </summary>
		None,
		/// <summary>
		/// The left
		/// </summary>
		Left,
		/// <summary>
		/// The right
		/// </summary>
		Right,
	}
}
