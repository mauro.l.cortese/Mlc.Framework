﻿// ***********************************************************************
// Assembly         : Mlc.Data
// Author           : Cortese Mauro Luigi
// Created          : 12-01-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-01-2017
// ***********************************************************************
// <copyright file="FieldAttribute.cs" company="MLC Development">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	/// <summary>
	/// Class FieldAttribute.
	/// </summary>
	/// <seealso cref="System.Attribute" />
	[AttributeUsage(AttributeTargets.Property)]
	public class FieldAttribute : Attribute
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldAttribute"/> class.
		/// </summary>
		public FieldAttribute()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldAttribute"/> class.
		/// </summary>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		public FieldAttribute(bool visible)
			: this(visible, true)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldAttribute"/> class.
		/// </summary>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		/// <param name="allowEdit">if set to <c>true</c> [read only].</param>
		public FieldAttribute(bool visible, bool allowEdit)
			: this(visible, allowEdit, HorzAlignment.Left)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldAttribute" /> class.
		/// </summary>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		/// <param name="allowEdit">if set to <c>true</c> [read only].</param>
		/// <param name="hAlign">The h align.</param>
		public FieldAttribute(bool visible, bool allowEdit, HorzAlignment hAlign)
			: this(visible, allowEdit, hAlign, RepositoryType.None)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldAttribute" /> class.
		/// </summary>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		/// <param name="allowEdit">if set to <c>true</c> [read only].</param>
		/// <param name="hAlign">The h align.</param>
		/// <param name="repositoryType">Type of the repository.</param>
		public FieldAttribute(bool visible, bool allowEdit, HorzAlignment hAlign, RepositoryType repositoryType)
		{
			this.Visible = visible;
			this.AllowEdit = allowEdit;
			this.HAlign = hAlign;
			this.RepositoryType = repositoryType;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [allow focus].
		/// </summary>
		/// <value><c>true</c> if [allow focus]; otherwise, <c>false</c>.</value>
		public bool AllowFocus { get; set; } = true;

		/// <summary>
		/// Gets or sets a value indicating whether [allow size].
		/// </summary>
		/// <value><c>true</c> if [allow size]; otherwise, <c>false</c>.</value>
		public bool AllowSize { get; set; } = true;

		/// <summary>
		/// Gets or sets the fixed style.
		/// </summary>
		/// <value>The fixed style.</value>
		public FixedStyleCol FixedStyle { get; set; } = FixedStyleCol.None;

		/// <summary>
		/// Gets or sets a value indicating whether [fixed width].
		/// </summary>
		/// <value><c>true</c> if [fixed width]; otherwise, <c>false</c>.</value>
		public bool FixedWidth { get; set; } = false;

		/// <summary>
		/// Gets or sets the format.
		/// </summary>
		/// <value>The format.</value>
		public Format Format { get; set; } = Format.None;

		/// <summary>
		/// Gets or sets the h align.
		/// </summary>
		/// <value>The h align.</value>
		public HorzAlignment HAlign { get; set; } = HorzAlignment.Left;

		/// <summary>
		/// Gets or sets a value indicating whether [read only].
		/// </summary>
		/// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
		public bool AllowEdit { get; set; } = false;

		/// <summary>
		/// Gets or sets the type of the repository.
		/// </summary>
		/// <value>The type of the repository.</value>
		public string RepositoryKey { get; set; } = string.Empty;

		/// <summary>
		/// Gets or sets a value indicating whether [read only].
		/// </summary>
		/// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
		public RepositoryType RepositoryType { get; set; } = RepositoryType.None;

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="FieldAttribute"/> is visible.
		/// </summary>
		/// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
		public bool Visible { get; set; } = true;

		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>The width.</value>
		public int Width { get; set; } = 0;
	}
}
