﻿using System;
using System.Collections.Generic;

namespace Mlc.Data
{
    public class DsRowList<T> : List<T> where T : DsRowMapBase
    {
        public DsRowList()
        {
            this.Add(Activator.CreateInstance(typeof(T), true) as T);
        }
    }
}
