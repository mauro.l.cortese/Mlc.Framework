using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace Mlc.Data
{
	/// <summary>
	/// Ereditare da questa classe per implementare tipi mappati sui dati di <see cref="System.Data.DataRow"/>
	/// </summary>
	public abstract class DsRowMapBase : IDataRowMapping
	{
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public DsRowMapBase()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="DsRowMapBase"/> class.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		[Bindable(BindableSupport.No)]
		[Browsable(false)]
		public DsRowMapBase(DataRow dataRow)
		{
			this.DataRow = dataRow;
		}

		public object GetValue(string fieldName)
		{
			return this.GetValueProperty(fieldName);
		}

		#region Membri di ICloneable
		/// <summary>
		/// restituisce una nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/> copia di quella corrente 
		/// </summary>
		/// <returns>Nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/></returns>
		public object Clone()
		{
			return this.MemberwiseClone();
		}
		#endregion

		/// <summary>DataRow mappato sull'istanza</summary>
		internal DataRow dataRow = null;
		/// <summary>
		/// Restituisce o imposta il DataRow mappato sull'istanza
		/// </summary>
		[Field(Visible = false)]
		public DataRow DataRow
		{
			get
			{
				this.OutRowMapping();
				return this.dataRow;
			}
			set
			{
				this.dataRow = value;
				this.InRowMapping();
			}
		}

		/// <summary>
		/// Updates the with.
		/// </summary>
		/// <param name="dsRowMapBase">The ds row map base.</param>
		public void UpdateWith(DsRowMapBase dsRowMapBase)
		{
			dsRowMapBase.CopyToDataRow(this.dataRow);
			this.InRowMapping();
		}

		/// <summary>
		/// Fills the table.
		/// </summary>
		/// <param name="retTable">The ret table.</param>
		/// <param name="dsRows">The ds rows.</param>
		public static void FillTable(DataTable retTable, Array dsRows)
		{
			foreach (object o in dsRows)
				if (o is DsRowMapBase dsRowMap)
					dsRowMap.InitNewDataRow(retTable);
		}

		/// <summary>
		/// Initializes the new data row.
		/// </summary>
		/// <param name="retTable">The ret table.</param>
		public void InitNewDataRow(DataTable retTable)
		{
			this.dataRow = retTable.NewRow();
			retTable.Rows.Add(this.dataRow);
			this.OutRowMapping();
		}
	}
}
