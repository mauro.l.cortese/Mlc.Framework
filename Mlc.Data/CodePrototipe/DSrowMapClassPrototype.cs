﻿using System;
using System.Data;
using Mlc.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class //ClassName>Base : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "//DbTableName>";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "//DsTableName>";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public //ClassName>Base()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public //ClassName>Base(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Proprietà
//Properties>        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
//PropertiesConst>
		}
		#endregion
	}
}
//Split>
using System;
using System.Data;
using Mlc.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// modificare solo questa classe per aggiungere funzionalità
	/// </summary>
	public class //ClassName> : //ClassName>Base, I//ClassName> 
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public //ClassName>()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public //ClassName>(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion
		
		#region Proprietà
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// Restituisce una nuova istanza ottenuta dalla copia di quella corrente
		/// </summary>
		/// <returns>Nuova istanza ottenuta dalla copia di quella corrente</returns>
		public new //ClassName> Clone()
		{
			return (//ClassName>)base.Clone();
		}
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}
}
//Split>
using System;
using Mlc.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface I//ClassName> : IDataRowMapping
	{
		#region Proprietà
//IProperties>        #endregion

		#region Metodi 
		#endregion
	}
}