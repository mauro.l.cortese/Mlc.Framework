﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mx.Core;
using Mx.Core.Db;
using Mx.Core.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public class //ClassProxyName>Base : DsProxyBase
	{
		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataSet">DataSet su cui mappare l'istanza</param>
		public //ClassProxyName>Base(DataSet dataSet)
			: base(dataSet)
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataSet">DataSet su cui mappare l'istanza</param>
		/// <param name="iDbConn">Gestore della base dati</param>
		public //ClassProxyName>Base (DataSet dataSet, IDbConnector iDbConn)
			: base(dataSet, iDbConn)
		{ }
		#endregion

		#region Proprietà index
		/// <summary>
		/// Restituisce il DataTable in base all'enumerato passato
		/// </summary>
		/// <param name="table">Enumerato della tabella</param>
		/// <returns>DataTable dell'enumerato richiesto</returns>
		public DataTable this[//ClassProxyName>Tables table]
		{
			get { return base.DataSet.Tables[table.ToString()]; }
		}
		#endregion
	}
}
//Split>
using System.Data;
using Mx.Core;
using Mx.Core.Db;
using Mx.Core.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// modificare solo questa classe per aggiungere funzionalità
	/// </summary>
	public class //ClassProxyName> : //ClassProxyName>Base
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public //ClassProxyName> (DataSet dataSet)
			:base(dataSet)
		{}

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataSet">DataSet su cui mappare l'istanza</param>
		/// <param name="iDbConn">Gestore della base dati</param>
		public //ClassProxyName> (DataSet dataSet, IDbConnector iDbConn)
			: base(dataSet, iDbConn)
		{ }
		#endregion

		#region Proprietà
		#endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}
}

//Split>
using System.Data;
using Mx.Core;
using Mx.Core.Db;
using Mx.Core.Data;

namespace //NameSpace>
{
	/// <summary>
	/// Enumerazione delle tabelle
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public enum //ClassProxyName>Tables
	{
		None,
//EnumTables>	}
}