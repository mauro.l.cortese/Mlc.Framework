﻿using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	public class LogSqlFormatter : LogFormatter
	{
		/// <summary>
		/// Formats the log.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		/// <returns>System.String.</returns>
		public override string FormatLog(ref LogRow logRow)
		{
			string retVal;
			if (logRow.LogObject is SqlCmdEngine.EventArgsExecuteSql argsExecuteSql)
			{
				SqlCmdEngine sce = argsExecuteSql.SqlCmdEngine;
				StringBuilder sb = new();
				int pad = argsExecuteSql.Command.Key.Length > sce.SqlCommandKey.Length ? argsExecuteSql.Command.Key.Length : sce.SqlCommandKey.Length;

				sb.AppendLine(base.getSepString('='));
				if (argsExecuteSql.Command != null)
				{
					// ID:[001] PROG:[001] CHECK:[data]
					string index = string.Format("Id:[{0}] Progressiv:[{1}] Check:[{2}] Key:[{3}]", 
						sce.Id.ToString("000"),
						sce.Index.ToString("000"), 
						sce.LastCheck,
						sce.SqlCommandKey);

				sb.AppendLine($"Command       : {argsExecuteSql.Command.Key.PadLeft(pad)} => {argsExecuteSql.Command.Description}");
				sb.AppendLine($"IdSql         : {index}");
				sb.AppendLine($"Sql Comment   : {sce.Comment}");
				}
				else 
					sb.AppendLine($"{sce.Comment}");
				sb.AppendLine(base.getSepString('-'));
				sb.AppendLine($"Connection    : {sce.SqlCommand.Connection.ConnectionString} [State: {sce.SqlCommand.Connection.State.ToString().ToUpper()}]");
				sb.AppendLine($"Command Info  : Type [{sce.SqlCommand.CommandType}]" +
										   $" - Execute Mode [{argsExecuteSql.SqlExecuteMode}]" +
										   $" - Command Timeout [{sce.SqlCommand.CommandTimeout}]");
				sb.AppendLine(base.getSepString('='));
				sb.AppendLine(this.getMessage(sce, argsExecuteSql.SqlExecuteMode));
				sb.AppendLine(base.getSepString('='));
				logRow.LogObject = sb;
			}
			else
				logRow.Ignore = string.IsNullOrEmpty(logRow.Message) ? false : true;
			Logger20.TrimLine = false;
			retVal = base.FormatLog(ref logRow);
			Logger20.TrimLine = true;
			return retVal;
		}


		/// <summary>
		/// Gets the message.
		/// </summary>
		/// <param name="sqlCmdEngine">The SQL command engine.</param>
		/// <param name="sqlExecuteMode">The SQL execute mode.</param>
		/// <returns>System.String.</returns>
		private string getMessage(SqlCmdEngine sqlCmdEngine, SqlCmdEngine.SqlExecuteMode sqlExecuteMode)
		{
			string retVal = "";
			if (sqlCmdEngine.SqlCommand.CommandType == CommandType.StoredProcedure)
			{
				StringBuilder sbSqlStr = new();
				sbSqlStr.AppendLine($"USE [{sqlCmdEngine.SqlCommand.Connection.Database}]");
				sbSqlStr.AppendLine();
				sbSqlStr.AppendLine("DECLARE @RC INT");

				try
				{
					foreach (SqlParameter sqlPar in sqlCmdEngine.SqlCommand.Parameters)
						if (sqlPar.SqlDbType == SqlDbType.Structured)
						{
							if (sqlCmdEngine.UserTypeTable.ContainsKey(sqlPar.ParameterName))
							{
								sbSqlStr.AppendLine($"DECLARE {sqlPar.ParameterName} {sqlCmdEngine.UserTypeTable[sqlPar.ParameterName]}");
								if (sqlPar.Value != null)
									if (sqlPar.Value is DataTable table)
									{

										sbSqlStr.AppendLine($"INSERT INTO {sqlPar.ParameterName} VALUES");
										int cnt = 0;
										foreach (DataRow row in table.Rows)
											sbSqlStr.AppendLine(this.getDataRowstr(row, ref cnt));
										
									}
									else
										throw new Exception("Sql Parametr must be a DataTable!");
							}
						}
						else
							sbSqlStr.AppendLine($"DECLARE {sqlPar.ParameterName} {this.getParDbType(sqlPar)} {this.getParDbValue(sqlPar)}");
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }


				sbSqlStr.AppendLine();

				string dpCmd = $"EXECUTE @RC = {sqlCmdEngine.SqlCommand.CommandText} ";

				List<SqlParameter> parOut = new ();

				if (sqlCmdEngine.SqlCommand.Parameters.Count == 0)
					sbSqlStr.AppendLine(dpCmd);
				else
					foreach (SqlParameter sqlPar in sqlCmdEngine.SqlCommand.Parameters)
					{
						bool parOutput = sqlPar.Direction == ParameterDirection.Output ? true : false;
						if (parOutput)
							parOut.Add(sqlPar);

						string direction = parOutput ? " OUTPUT" : string.Empty;
						sbSqlStr.AppendLine($"{dpCmd}{sqlPar.ParameterName}{direction}");
						dpCmd = new string(' ', dpCmd.Length - 1) + ',';
					}

				if (parOut.Count > 0)
				{
					sbSqlStr.AppendLine();
					sbSqlStr.Append("SELECT ");
					foreach (SqlParameter sqlPar in parOut)
						sbSqlStr.Append($"{sqlPar.ParameterName} {sqlPar.ParameterName.Replace('@',' ').Trim()}, ");
					sbSqlStr.Remove(sbSqlStr.Length - 2, 2);
				}

				retVal = sbSqlStr.ToString().TrimEnd();
			}

			else if (sqlCmdEngine.SqlCommand.CommandType == CommandType.Text)
			{
				StringBuilder sbSqlStr = new();
				sbSqlStr.AppendLine($"USE [{sqlCmdEngine.SqlCommand.Connection.Database}]");
				sbSqlStr.AppendLine();
				sbSqlStr.AppendLine(sqlCmdEngine.SqlCommand.CommandText);
				retVal = sbSqlStr.ToString().TrimEnd();
			}
			return retVal;
		}

		/// <summary>
		/// Gets the data rowstr.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="cnt">The count.</param>
		/// <returns>System.String.</returns>
		private string getDataRowstr(DataRow row, ref int cnt)
		{
			cnt += 1;
			StringBuilder sb = new();
			sb.Append("\t,(");
			foreach (DataColumn col in row.Table.Columns)
			{
				sb.Append($"{this.getValue(row, col)},");

			}
			sb.Remove(sb.Length - 1, 1);
			sb.Append(")");

			if (cnt == 1)
				sb[1] = ' ';
			return sb.ToString();
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="col">The col.</param>
		/// <returns>System.String.</returns>
		private string getValue(DataRow row, DataColumn col)
		{
			string retVal = "";
			switch (col.DataType.FullName)
			{
				case "System.String":
					retVal = $"'{row[col]}'";
					break;
				case "System.Boolean":
					retVal = (bool)row[col] ? "1" : "0";
					break;
				case "System.DateTime":
					retVal = $"'{((DateTime)row[col]).Serialize()}'";
					break;
				default:
					retVal = $"{row[col]}";
					break;
			}

			if (row[col] is DBNull)
				retVal = "null";

			return retVal.Replace(",",".");
		}

		private string getParDbValue(SqlParameter sqlPar)
		{
			string retVal = sqlPar.Value == null ? string.Empty : sqlPar.Value.ToString();
			retVal = sqlPar.Direction == ParameterDirection.Output ? string.Empty : retVal;

			switch (sqlPar.DbType)
			{
				case DbType.AnsiString:
					break;
				case DbType.Binary:
					break;
				case DbType.Byte:
					break;
				case DbType.Boolean:
					retVal = retVal == "True" ? "1" : "0";
					break;
				case DbType.Currency:
					break;
				case DbType.Date:
				case DbType.DateTime:
					retVal = $"'{retVal.ToDateTime().Serialize()}'";
					break;
				case DbType.Decimal:
					break;
				case DbType.Double:
					break;
				case DbType.Guid:
					break;
				case DbType.Int16:
					break;
				case DbType.Int32:
					break;
				case DbType.Int64:
					break;
				case DbType.Object:
					break;
				case DbType.SByte:
					break;
				case DbType.Single:
					break;
				case DbType.AnsiStringFixedLength:
				case DbType.StringFixedLength:
				case DbType.String:
					retVal = $"N'{retVal.Replace("'", "''")}'";
					break;
				case DbType.Time:
					break;
				case DbType.UInt16:
				case DbType.UInt32:
				case DbType.UInt64:
					break;
				case DbType.VarNumeric:
					break;
				case DbType.Xml:
					break;
				case DbType.DateTime2:
					break;
				case DbType.DateTimeOffset:
					break;
				default:
					break;
			}
			retVal = !string.IsNullOrEmpty(retVal) ? $"= {retVal}" : string.Empty;


			return retVal;
		}

		private string getParDbType(SqlParameter sqlPar)
		{
			string retVal = "";
			switch (sqlPar.DbType)
			{
				case DbType.AnsiString:
					break;
				case DbType.Binary:
					break;
				case DbType.Byte:
					break;
				case DbType.Boolean:
					retVal = "BIT";
					break;
				case DbType.Currency:
					break;
				case DbType.Date:
					retVal = "DATE";
					break;
				case DbType.DateTime:
					retVal = "DATETIME";
					break;
				case DbType.Decimal:
					break;
				case DbType.Double:
					retVal = "FLOAT";
					break;
				case DbType.Guid:
					break;
				case DbType.Int16:
					retVal = "TINYINT";
					break;
				case DbType.Int32:
					retVal = "INT";
					break;
				case DbType.Int64:
					break;
				case DbType.Object:
					break;
				case DbType.SByte:
					break;
				case DbType.Single:
					break;
				case DbType.String:
					int len = 30;
					if (sqlPar.Value != null)
						len = sqlPar.Value.ToString().Length == 0 ? 1 : sqlPar.Value.ToString().Length;
					retVal = $"NVARCHAR(MAX)"; //$"NVARCHAR({len})";
					break;
				case DbType.Time:
					break;
				case DbType.UInt16:
				case DbType.UInt32:
				case DbType.UInt64:
					retVal = "INT";
					break;
				case DbType.VarNumeric:
					break;
				case DbType.AnsiStringFixedLength:
					break;
				case DbType.StringFixedLength:
					break;
				case DbType.Xml:
					break;
				case DbType.DateTime2:
					break;
				case DbType.DateTimeOffset:
					break;
				default:
					break;
			}
			return retVal;
		}


	}

}
