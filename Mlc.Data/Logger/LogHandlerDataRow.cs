﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-16-2020
// ***********************************************************************
// <copyright file="Logger.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Data;
using Mlc.Data;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Reflection;
using Mlc.Shell.Gui;
using Mlc.Shell;
using System.ComponentModel;
using System.Windows.Forms;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class LogHandlerFile.
    /// </summary>
    /// <seealso cref="Mlc.Shell.IO.LogHandlerBase" />
    public class LogHandlerDataRow<T> : LogHandlerBase, IDisposable where T : LogRow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogHandlerFile"/> class.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="name">The name.</param>
        public LogHandlerDataRow(Logger20.Level level, string name) : base(name, level)
        {

            this.TableLog = DataTableTools.GetTableFromType(typeof(T));

        }

        /// <summary>
        /// Gets the table log.
        /// </summary>
        /// <value>The table log.</value>
        public DataTable TableLog { get; } = null;

        /// <summary>
        /// Writes the specified log row.
        /// </summary>
        /// <param name="logRow">The log row.</param>
        public override void Write(LogRow logRow)
        {
            if (logRow is T)
            {
                PropertyInfo[] pInfos = logRow.GetType().GetProperties();
                try
                {
                    DataRow dataRow = this.TableLog.NewRow();
                    foreach (PropertyInfo pInfo in pInfos)
                        if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
                            dataRow[pInfo.Name] = pInfo.GetValue(logRow);
                            //dataTable.Columns.Add(pInfo.Name, pInfo.PropertyType);
                    this.TableLog.Rows.Add(dataRow);
                }
                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            }
        }
    }
}
