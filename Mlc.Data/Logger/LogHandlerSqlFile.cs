﻿using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Mlc.Data
{
	/// <summary>
	/// Class LogHandlerCommandFile.
	/// Implements the <see cref="Mlc.Shell.IO.LogHandlerFile" />
	/// </summary>
	/// <seealso cref="Mlc.Shell.IO.LogHandlerFile" />
	public class LogHandlerSqlFile : LogHandlerFile
	{
		public LogHandlerSqlFile(Logger20.Level level, string name, string folder = "")
			: base(level, name, folder)
		{
			// this.Formatter = new LogCommandFormatter();

			// Defines the formatters for SQL types
			base.Formatters.Add(
				new List<Type>() {
					typeof(SqlCmdEngine),
					typeof(SqlCmdEngine.EventArgsExecuteSql),
					typeof(SqlCommand),
					typeof(SqlConnection),
				},
				new LogSqlFormatter());
		}
	}
}
