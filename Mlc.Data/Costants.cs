﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
    /// <summary>
    /// Costanti per la generazione di codice C#
    /// </summary>
    public class CodeTag
    {
        /// <summary>Sostituire questa costante con il name space</summary>
        public const string NameSpace = "//NameSpace>";
        /// <summary>Sostituire questa costante con il nome della classe</summary>
        public const string ClassName = "//ClassName>";
        /// <summary>Sostituire questa costante con il nome della tabella in DB</summary>
        public const string DbTableName = "//DbTableName>";
        /// <summary>Sostituire questa costante con il nome della tabella nel dataset DB</summary>
        public const string DsTableName = "//DsTableName>";
        /// <summary>Sostituire questa costante con la data e l'ora</summary>
        public const string Time = "//Time>";
        /// <summary>Sostituire questa costante con il codice delle proprità</summary>
        public const string Properties = "//Properties>";
        /// <summary>Sostituire questa costante con il nome del campo su cui si appoggia una proprietà</summary>
        public const string FieldName = "//FieldName>";
        /// <summary>Sostituire questa costante con il nome della proprietà</summary>
        public const string PropertyName = "//PropertyName>";
        /// <summary>Sostituire questa costante con la descrizione di un diritto utente</summary>
        public const string RightDescription = "//RightDescription>";
        /// <summary>Sostituire questa costante con le proprietà delle catagorie dei diritti</summary>
        public const string Categories = "//Categories>";
        /// <summary>Sostituire questa costante con una descrizione</summary>
        public const string Description = "//Description>";
        /// <summary>Sostituire questa costante con il nome del tipo necessario</summary>
        public const string Type = "//Type>";
        /// <summary>Sostituire questa costante con il nome del campo in DB</summary>
        public const string DbFiledName = "//DbFiledName>";
        /// <summary>Costante per la divione del testo in più file</summary>
        public const string Split = "//Split>";
        /// <summary>Sostituire questa costante con la definizione delle proprietà in un interfaccia</summary>
        public const string IProperties = "//IProperties>";
        /// <summary>Sostituire questa costante con la definizione delle costanti per i nomi delle prorpietà</summary>
        public const string PropertiesConst = "//PropertiesConst>";
        /// <summary>Sostituire questa costante con la definizione delle costanti per i nomi dei campi in DB</summary>
        public const string DbFieldsConst = "//DbFieldsConst>";
        /// <summary>Sostituire questa costante con il nome della classe Proxy</summary>
        public const string ClassProxyName = "//ClassProxyName>";
        /// <summary>Sostituire questa costante con la definizione dell'enumerato delle tabelle</summary>
        public const string EnumTables = "//EnumTables>";
        /// <summary>Sostituire questa costante con la definizione dell'enumerato dei reparti</summary>
        public const string EnumDepartments = "//EnumDepartments>";
        /// <summary>Sostituire questa costante con la definizione dell'enumerato delle applicazioni</summary>
        public const string EnumApplications = "//EnumApplications>";
        /// <summary>Sostituire questa costante con la definizione dell'enumerato dei profili</summary>
        public const string EnumProfiles = "//EnumProfiles>";
		/// <summary>The repository attribute</summary>
		public const string DbFieldAttribute = "//DbFieldAttribute>";
	}
}
