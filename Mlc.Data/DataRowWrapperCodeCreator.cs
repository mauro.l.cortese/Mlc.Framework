using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Mlc.Data.Resources;
using System.Reflection;

using Mlc.Shell;

//using Mx.Core.Constants;
//using Mx.Core.Resource;
//using Mx.Core;
//using Mx.Core.Extensions;


namespace Mlc.Data
{
    /// <summary>
    /// Genera il codice sorgente per i tipi mappati sui dati di un DataSet/&gt;
    /// </summary>
    public class DataRowWrapperCodeCreator
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        #endregion

        #region Campi
        ///// <summary>Istanza per la generazione del codice sorgente</summary>
        //private StringBuilder codeClass = new();
        /// <summary>DataSet su cui mappare i tipi generati</summary>
        private DataSet dataSet = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataSet">DataSet per cui scrivere il codice dei tipi mappati sui dati</param> 
        public DataRowWrapperCodeCreator()
        {
        }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataSet">DataSet per cui scrivere il codice dei tipi mappati sui dati</param> 
        public DataRowWrapperCodeCreator(DataSet dataSet)
        {
            this.dataSet = dataSet;

            foreach (DataTable table in this.dataSet.Tables)
            {
                TypeMapInfo tmi = new TypeMapInfo(table.TableName);
                this.TypeMap.Add(tmi);
            }
        }
        #endregion

        #region Propriet�
        /// <summary>dizionario di mappatura dei tipi</summary>
        private TypeMapInfoCollection typeMap = new TypeMapInfoCollection();
        /// <summary>
        /// Restituisce il dizionario di mappatura dei tipi (Nome tabella -> Nome tipo record)
        /// Attenzione Nome tabella � un alias del nome tabella in in DB che può anche non esistere poichè si ricava come join di più tabelle o viste
        /// </summary>
        public TypeMapInfoCollection TypeMap { get { return this.typeMap; } }

        /// <summary>
        /// Restituisce o imposta lo spazio dei nomi
        /// </summary>
        public string NameSpace { get; set; }

        /// <summary>
        /// Restituisce o imposta la cartella di output del codice sorgente
        /// </summary>
        public string FolderCode { get; set; }

        /// <summary>
        /// Restituisce o imposta la cartella di output del codice sorgente
        /// </summary>
        public string FolderCodeInterface { get; set; }

        /// <summary>
        /// Restituisce o imposta la cartella di output del codice delle enumerazioni
        /// </summary>
        public string FolderCodeEnums { get; set; }
        public Type TableEnum { get; set; }
		#endregion

		#region Metodi pubblici


		/// <summary>
		/// Writes the code type class.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="tableEnum">The table enum.</param>
		/// <param name="rac">The rac.</param>
		public void WriteCodeTypeClass(DataTable table, Enum tableEnum, RepositoryAttributeCollection rac = null)
        {
            this.WriteCodeTypeClass("", table, tableEnum, rac);
        }


		/// <summary>
		/// Writes the code type class.
		/// </summary>
		/// <param name="dbTableName">Name of the database table.</param>
		/// <param name="table">The table.</param>
		/// <param name="tableEnum">The table enum.</param>
		/// <param name="rac">The rac.</param>
		public void WriteCodeTypeClass(string dbTableName, DataTable table, Enum tableEnum, RepositoryAttributeCollection rac)
        {

            if (table.Columns.Count == 0)
                return;


            string nameClass = Enum.GetName(this.TableEnum, tableEnum);
            table.TableName = $"{nameClass}Table";

            // genera il codice di definizione della classe
            StringBuilder codeClass = new StringBuilder(CodeMasterResource.DataRowWrapperClassCodePrototipe);

            codeClass.Replace(CodeTag.NameSpace, this.NameSpace);
            codeClass.Replace(CodeTag.ClassName, nameClass);


            DateTime t = DateTime.Now;
            codeClass.Replace(CodeTag.Time, string.Concat(t.ToShortDateString(), " ", t.ToLongTimeString()));

            // genera il codice delle Propriet�
            codeClass.Replace(CodeTag.Properties, this.getProperties(table, rac));

			//// genera il codice dell'interfaccia delle Propriet�
			//codeClass.Replace(CodeTag.IProperties, this.getIProperties(table));

			// genera il codice delle costanti delle Propriet�
			codeClass.Replace(CodeTag.PropertiesConst, this.getPropertiesConst(table));

            string[] code = codeClass.ToString().Split(new string[] { CodeTag.Split }, StringSplitOptions.RemoveEmptyEntries);
            string fileName = string.Concat(this.FolderCode, Chars.BackSlash, Chars.BackSlash, nameClass, ".cs");

            string folder = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            File.WriteAllText(fileName, code[0]);

            //fileName = string.Concat(this.FolderCode, Chars.BackSlash, nameClass, ".cs");
            //// se il file della classe non esiste
            //if (!File.Exists(fileName))
            //    // lo scrivo
            //    File.WriteAllText(fileName, code[1]);

            //fileName = string.Concat(this.FolderCodeInterface, Chars.BackSlash, "I", nameClass, ".cs");

            //folder = Path.GetDirectoryName(fileName);
            //if (!Directory.Exists(folder))
            //    Directory.CreateDirectory(folder);

            //File.WriteAllText(fileName, code[2]);

            //            this.writeCodeClass(table);
        }
        
		
		///// <summary>
        ///// Serializza il codice dei tipi mappati sui dati
        ///// </summary>
        ///// <param name="proxyName">Nome della classe proxy</param>
        //public void CodeSerialize(string proxyName)
        //{
        //    if (this.dataSet == null)
        //        return;

        //    if (!Directory.Exists(this.FolderCode + "\\Base"))
        //        Directory.CreateDirectory(this.FolderCode + "\\Base");

        //    if (!Directory.Exists(this.FolderCodeInterface))
        //        Directory.CreateDirectory(this.FolderCodeInterface);

        //    if (!Directory.Exists(this.FolderCodeEnums))
        //        Directory.CreateDirectory(this.FolderCodeEnums);

        //    //ciclo le tabelle
        //    foreach (DataTable dt in this.dataSet.Tables)
        //        this.writeCodeClass(dt);

        //    // codice del proxy
        //    this.writeProxyClass(proxyName);
        //}
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        ///// <summary>
        ///// Scrive il codice della classe proxy
        ///// </summary>
        ///// <param name="proxyName">Nome della classe</param>
        //private void writeProxyClass(string proxyName)
        //{
        //    // genera il codice di definizione della classe
        //    this.codeClass = new StringBuilder(CodeMasterResource.DSrowMapClassProxyPrototipe);
        //    codeClass.Replace(CodeTag.NameSpace, this.NameSpace);
        //    codeClass.Replace(CodeTag.ClassProxyName, proxyName);

        //    DateTime t = DateTime.Now;
        //    codeClass.Replace(CodeTag.Time, string.Concat(t.ToShortDateString(), " ", t.ToLongTimeString()));

        //    StringBuilder sbTable = new();
        //    foreach (string tab in this.TypeMap.Keys)
        //    {
        //        sbTable.AppendLine(string.Format("\t\t\t/// <summary>Tabella {0}</summary>", tab));
        //        sbTable.AppendLine(string.Format("\t\t\t{0},", tab));
        //    }
        //    codeClass.Replace(CodeTag.EnumTables, sbTable.ToString());


        //    string[] code = codeClass.ToString().Split(new string[] { CodeTag.Split }, StringSplitOptions.RemoveEmptyEntries);
        //    string fileName = string.Concat(this.FolderCode, Chars.BackSlash, "Base", Chars.BackSlash, proxyName, "Base.cs");
        //    File.WriteAllText(fileName, code[0]);

        //    fileName = string.Concat(this.FolderCode, Chars.BackSlash, proxyName, ".cs");
        //    // se il file della classe non esiste
        //    if (!File.Exists(fileName))
        //        // lo scrivo
        //        File.WriteAllText(fileName, code[1]);

        //    fileName = string.Concat(this.FolderCodeEnums, Chars.BackSlash, proxyName, "Tables.cs");
        //    File.WriteAllText(fileName, code[2]);
        //}

        ///// <summary>
        ///// Scrive il codice del tipo mappabile sulla tabella
        ///// </summary>
        ///// <param name="dataTable">Istanza DataTable per cui scrivere il codice</param>
        //private void writeCodeClass(DataTable dataTable)
        //{
        //    TypeMapInfo tmi = this.typeMap[dataTable.TableName];

        //    // genera il codice di definizione della classe
        //    this.codeClass = new StringBuilder(CodeMasterResource.DataRowWrapperClassCodePrototipe);
        //    codeClass.Replace(CodeTag.NameSpace, this.NameSpace);
        //    codeClass.Replace(CodeTag.ClassName, tmi.ClassName);
        //    codeClass.Replace(CodeTag.DbTableName, tmi.DbTableName);
        //    codeClass.Replace(CodeTag.DsTableName, tmi.DsTableName);
        //    DateTime t = DateTime.Now;
        //    codeClass.Replace(CodeTag.Time, string.Concat(t.ToShortDateString(), " ", t.ToLongTimeString()));

        //    // genera il codice delle Propriet�
        //    codeClass.Replace(CodeTag.Properties, this.getProperties(dataTable));

        //    // genera il codice dell'interfaccia delle Propriet�
        //    codeClass.Replace(CodeTag.IProperties, this.getIProperties(dataTable));

        //    // genera il codice delle costanti delle Propriet�
        //    codeClass.Replace(CodeTag.PropertiesConst, this.getPropertiesConst(dataTable));

        //    string[] code = codeClass.ToString().Split(new string[] { CodeTag.Split }, StringSplitOptions.RemoveEmptyEntries);
        //    string fileName = string.Concat(this.FolderCode, Chars.BackSlash, Chars.BackSlash, tmi.ClassName, ".cs");
        //    File.WriteAllText(fileName, code[0]);

        //    //fileName = string.Concat(this.FolderCode, Chars.BackSlash, tmi.ClassName, ".cs");
        //    //// se il file della classe non esiste
        //    //if (!File.Exists(fileName))
        //    //    // lo scrivo
        //    //    File.WriteAllText(fileName, code[1]);

        //    fileName = string.Concat(this.FolderCodeInterface, Chars.BackSlash, "I", tmi.ClassName, ".cs");
        //    File.WriteAllText(fileName, code[1]);
        //}

        /// <summary>
        /// Restituisce le dichiarazioni delle costanti dei nomi delle Propriet�
        /// </summary>
        /// <param name="dataTable">Data table per cui generare il codice delle costanti</param>
        /// <returns>Testo con il codice di dichiarazione delle costanti dei nomi delle Propriet�</returns>
        private string getPropertiesConst(DataTable dataTable)
        {
            StringBuilder sb = new();
            foreach (DataColumn col in dataTable.Columns)
            {
                sb.AppendLine(string.Format("            /// <summary>Field name: \"{0}\".</summary>", col.ColumnName));
                sb.AppendLine(string.Format("            public const string {0} = \"{0}\";", col.ColumnName));
            }
            return sb.ToString();
        }

        ///// <summary>
        ///// Restituisce il testo delle Propriet� della classe mappate sui dati
        ///// </summary>
        ///// <returns>Codice sorgente per le Propriet� dell///a classe mappate sui dati</returns>
        //private string getIProperties(DataTable dataTable)
        //{
        //    StringBuilder retVal = new();

        //    DateTime t = DateTime.Now;
        //    foreach (DataColumn col in dataTable.Columns)
        //    {
        //        //Console.WriteLine(col.ColumnName);
        //        StringBuilder propertyPrototipe = new StringBuilder(CodeMasterResource.DSrowMapIPropertyPrototype);
        //        propertyPrototipe.Replace(CodeTag.PropertyName, col.ColumnName);
        //        propertyPrototipe.Replace(CodeTag.DbFiledName, col.ColumnName);
        //        propertyPrototipe.Replace(CodeTag.FieldName, col.Caption.GetUpperCamelCase().ToLower());
        //        propertyPrototipe.Replace(CodeTag.Type, col.DataType.Name);
        //        propertyPrototipe.Replace(CodeTag.Description, col.ColumnName);
        //        propertyPrototipe.Replace(CodeTag.Time, string.Concat(t.ToShortDateString(), " ", t.ToLongTimeString()));
        //        retVal.AppendLine(propertyPrototipe.ToString());
        //        retVal.AppendLine();
        //    }

        //    return retVal.ToString();

        //}

		/// <summary>
		/// Restituisce il testo delle Propriet� della classe mappate sui dati
		/// </summary>
		/// <param name="dataTable">The data table.</param>
		/// <param name="rac">The rac.</param>
		/// <returns>Codice sorgente per le Propriet� della classe mappate sui dati</returns>
		private string getProperties(DataTable dataTable, RepositoryAttributeCollection rac)
        {
            StringBuilder retVal = new();

            DateTime t = DateTime.Now;
            foreach (DataColumn col in dataTable.Columns)
            {
                StringBuilder propertyPrototipe = new StringBuilder(CodeMasterResource.DataRowWrapperPropertyCodePrototipe);
                propertyPrototipe.Replace(CodeTag.PropertyName, col.ColumnName);
                propertyPrototipe.Replace(CodeTag.DbFiledName, col.ColumnName);
                propertyPrototipe.Replace(CodeTag.FieldName, col.Caption.GetUpperCamelCase().ToLower());
                propertyPrototipe.Replace(CodeTag.Type, col.DataType.Name);
                propertyPrototipe.Replace(CodeTag.Description, col.ColumnName);
                propertyPrototipe.Replace(CodeTag.Time, string.Concat(t.ToShortDateString(), " ", t.ToLongTimeString()));

				if (rac == null || !rac.ContainsKey(col.ColumnName))
					propertyPrototipe.Replace(CodeTag.DbFieldAttribute, CodeMasterResource.FieldAttributeDefaultPrototype);
				else
				{
					FieldAttribute fa = rac[col.ColumnName];
					propertyPrototipe.Replace(CodeTag.DbFieldAttribute, 
						string.Format(CodeMasterResource.FieldAttributePrototype
						, fa.AllowFocus.ToString().ToLower()
						, fa.AllowSize.ToString().ToLower()
						, fa.FixedStyle.ToString()
						, fa.FixedWidth.ToString().ToLower()
						, fa.Format.ToString()
						, fa.HAlign.ToString()
						, fa.AllowEdit.ToString().ToLower()
						, fa.RepositoryKey
						, fa.RepositoryType.ToString()
						, fa.Visible.ToString().ToLower()
						, fa.Width.ToString()
						));
				}

				retVal.AppendLine(propertyPrototipe.ToString());
                retVal.AppendLine();
            }

            return retVal.ToString();
        }
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion

    }
}
