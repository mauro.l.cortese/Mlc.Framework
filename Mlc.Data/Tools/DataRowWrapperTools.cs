﻿// ***********************************************************************
// Assembly         : Mlc.Data
// Author           : mauro.luigi.cortese
// Created          : 03-06-2023
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-04-2023
// ***********************************************************************
// <copyright file="DataRowWrapperTools.cs" company="MLC">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Shell;
using System.Data;

namespace Mlc.Data
{
	/// <summary>
	/// Class DataRowWrapperTools.
	/// </summary>
	public static class DataRowWrapperTools
	{
		/// <summary>
		/// Selects the rows.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dataRowWrapper">The data row wrapper.</param>
		/// <param name="dataRowDefault">The data row default.</param>
		/// <returns>DataTable.</returns>
		public static void SetToDefaultValues<T>(this DataRowWrapper<T> dataRowWrapper, DataRowWrapper<T> dataRowDefault)
			where T : IDataRowWrapper<T>
		{
			DataWrapperIterator<T> dwi = dataRowWrapper.GetIterator();
			while (dwi.MoveNext())
				foreach (DataColumn col in dataRowWrapper.Table.Columns)
					if (dwi.DataRowWrapper[col.ColumnName].Equals(col.DataType.GetDefaultValue()))
						dwi.DataRowWrapper[col.ColumnName] = dataRowDefault[col.ColumnName];
		}

	}
}
