﻿// ***********************************************************************
// Assembly         : Mlc.Data
// Author           : mauro.luigi.cortese
// Created          : 06-27-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-27-2019
// ***********************************************************************
// <copyright file="SqlCommandTableData.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Data.Resources;
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mlc.Data
{
	/// <summary>
	/// Class SqlData.
	/// </summary>
	public class SqlCmdEngine
	{
		#region Constants
		/// <summary>The string SQL load SQL from database</summary>
		private const string _SqlLoadSqlFromDb = "sp_get_sql_instructions"; // EXECUTE dbo.
		/// <summary>The key</summary>
		private const string _Key = "Key";
		/// <summary>The SQL command</summary>
		private const string _SqlCommand = "SqlCommand";
		/// <summary>The user type</summary>
		private const string _UserType = "UserType";
		/// <summary>The comment</summary>
		private const string _Comment = "Comment";
		/// <summary>The commandTimeout</summary>
		private const string _CommandTimeout = "CommandTimeout";
		/// <summary>The check</summary>
		private const string _LastCheck = "LastCheck";
		/// <summary>The index</summary>
		private const string _Index = "Index";
		/// <summary>The ID</summary>
		private const string _Id = "Id";
		#endregion

		#region Enumerations
		/// <summary>
		/// Enum SqlExecuteMode
		/// </summary>
		public enum SqlExecuteMode
		{
			/// <summary>
			/// The execute reader
			/// </summary>
			ExecuteReader,
			/// <summary>
			/// The execute non query
			/// </summary>
			ExecuteNonQuery,
			/// <summary>
			/// The execute scalar
			/// </summary>
			ExecuteScalar,
			/// <summary>
			/// The execute bulk copy
			/// </summary>
			ExecuteBulkCopy,
		}

		/// <summary>
		/// Loads the query.
		/// </summary>
		public void LoadQuery()
		{
			try
			{
				this.SqlCommandText = _SqlLoadSqlFromDb;
				this.Comment = LocalText.MsgInfoLoadSqlFromDb;
				using (SqlCommand sqlCmd = this.GetStoredProcedure())
				{
					this.SqlTable = this.ExecuteSpReader();
					sqlCmd.Connection.Dispose();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			//this.SqlCommandText = _SqlLoadSqlFromDb;
			//this.Comment = LocalText.MsgInfoLoadSqlFromDb;
			//this.SqlTable = this.ExecuteReader(_SqlInstructions);
		}
		#endregion

		#region Fields
		/// <summary>
		/// The log
		/// </summary>
		Logger20.Log log = Logger20.GetInstance().DefaultLog;
		#endregion

		#region Constructors        
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlCmdEngine"/> class.
		/// </summary>
		public SqlCmdEngine() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SqlCmdEngine"/> class.
		/// </summary>
		public SqlCmdEngine(DataTable sqlTable) => this.SqlTable = sqlTable;
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the dt SQL.
		/// </summary>
		/// <value>The dt SQL.</value>
		public DataTable SqlTable { get; set; }

		/// <summary>
		/// Gets or sets the SQL command.
		/// </summary>
		/// <value>The SQL command.</value>
		public SqlCommand SqlCommand { get; set; }

		/// <summary>
		/// Gets or sets the SQL command.
		/// </summary>
		/// <value>The SQL command.</value>
		public string SqlCommandText { get; set; }

		/// <summary>
		/// Gets or sets the comment.
		/// </summary>
		/// <value>The comment.</value>
		public string Comment { get; set; }

		/// <summary>
		/// Gets or sets the string connection.
		/// </summary>
		/// <value>The string connection.</value>
		public string StringConnection { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [check parameters].
		/// </summary>
		/// <value><c>true</c> if [check parameters]; otherwise, <c>false</c>.</value>
		public bool CheckParams { get; set; } = true;

		/// <summary>
		/// Gets or sets the executable time.
		/// </summary>
		/// <value>The executable time.</value>
		public Double ExeTime { get; set; }

		/// <summary>
		/// Gets or sets the user type table.
		/// </summary>
		/// <value>The user type table.</value>
		public Dictionary<string, string> UserTypeTable { get; set; } = new();

		/// <summary>
		/// Gets or sets the SQL command key.
		/// </summary>
		/// <value>The SQL command key.</value>
		public string SqlCommandKey { get; private set; } = string.Empty;

		/// <summary>
		/// Gets the command time out.
		/// </summary>
		/// <value>The command time out.</value>
		public int CommandTimeOut { get; private set; } = 30;

		/// <summary>
		/// Gets the check.
		/// </summary>
		/// <value>The check.</value>
		public DateTime LastCheck { get; private set; }

		/// <summary>
		/// Gets the index.
		/// </summary>
		/// <value>The index.</value>
		public int Id { get; private set; }

		/// <summary>
		/// Gets the index.
		/// </summary>
		/// <value>The index.</value>
		public int Index { get; private set; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Tests the connection.
		/// </summary>
		/// <param name="stringSqlConn">The SQL connection.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool CheckDbConnection()
		{
			if (string.IsNullOrEmpty(this.StringConnection)) return false;

			bool retVal = false;
			using (SqlConnection sqlConn = new SqlConnection(this.StringConnection))
			{
				try
				{
					int cnt = 0;
					sqlConn.Open();
					while (sqlConn.State != ConnectionState.Open)
					{
						if (cnt > 10000) break;
						cnt++;
					}
					if (sqlConn.State == ConnectionState.Open)
					{
						this.log.Debug($"Connection state: [{sqlConn.State}] for => {sqlConn.ConnectionString}");
						sqlConn.Close();
						retVal = true;
					}
				}
				catch (Exception ex)
				{
					ExceptionsRegistry.GetInstance().Add(ex);
					retVal = false;
				}
			}
			return retVal;
		}

		/// <summary>
		/// SQLs the initialize.
		/// </summary>
		public void SqlInit()
		{
			this.SqlCommandText = Tools.SqlTools.SqlInit;
			this.ExecuteNonQuery();
			this.SqlCommandText = Tools.SqlTools.SqlInitProcedure;
			this.ExecuteNonQuery();
		}

		/// <summary>
		/// Sets the SQL command text.
		/// </summary>
		/// <param name="sql">The SQL.</param>
		/// <returns>SqlCmdEngine.</returns>
		public SqlCmdEngine SetSqlCommandText(string sql)
		{
			this.SqlCommandKey = string.Empty;
			this.SqlCommandText = sql;
			return this;
		}


		/// <summary>
		/// Gets the SQL connection.
		/// </summary>
		/// <returns>SqlConnection.</returns>
		public SqlConnection GetSqlConnection() => this.getSqlConn(this.StringConnection);

		/// <summary>
		/// Gets the SQL query.
		/// </summary>
		/// <param name="sqlCmd">The key.</param>
		/// <returns>System.String.</returns>
		public SqlCmdEngine SetSqlData(Enum sqlCmd) => this.SetSqlData(sqlCmd.ToString());

		/// <summary>
		/// Gets the SQL query.
		/// </summary>
		/// <param name="sqlKey">The SQL key.</param>
		/// <returns>System.String.</returns>
		public SqlCmdEngine SetSqlData(string sqlKey) => this.SetSqlData(sqlKey, null);

		/// <summary>
		/// Gets the SQL query.
		/// </summary>
		/// <param name="sqlCmd">The key.</param>
		/// <param name="pars">The pars.</param>
		/// <returns>System.String.</returns>
		public SqlCmdEngine SetSqlData(Enum sqlCmd, params object[] pars) => this.SetSqlData(sqlCmd.ToString(), pars);

		/// <summary>
		/// Gets the SQL query.
		/// </summary>
		/// <param name="sqlKey">The SQL key.</param>
		/// <param name="pars">The pars.</param>
		/// <returns>System.String.</returns>
		public SqlCmdEngine SetSqlData(string sqlKey, params object[] pars)
		{
			try
			{
				this.SqlCommandKey = sqlKey;
				string select = $"{_Key}='{sqlKey}'";
				DataRow[] row = this.SqlTable.Select(select);
				string[] parameters = null;
				if (row.Length == 1)
				{
					this.Comment = row[0][_Comment].ToString();
					this.CommandTimeOut = (int)row[0][_CommandTimeout];
					this.LastCheck = (DateTime)row[0][_LastCheck];
					this.Index = (int)row[0][_Index];
					this.Id = (int)row[0][_Id];
					this.setUserTypeTable(row[0][_UserType].ToString());

					if (pars != null)
					{
						parameters = new string[pars.Length];
						for (int i = 0; i < pars.Length; i++)
							parameters[i] = this.CheckParams ? pars[i].ToString().CheckForSql() : pars[i].ToString();
					}
					SqlCommandText = parameters != null ? string.Format(row[0][_SqlCommand].ToString(), parameters) : row[0][_SqlCommand].ToString();

				}
				return this;
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
				SqlCommandText = string.Empty;
			}
			return null;
		}

		/// <summary>
		/// Gets the stored procedure.
		/// </summary>
		/// <returns>SqlCommand.</returns>
		public SqlCommand GetStoredProcedure() => this.GetStoredProcedure(this.SqlCommandText);

		/// <summary>
		/// Gets the stored procedure.
		/// </summary>
		/// <param name="spname">The spname.</param>
		/// <param name="initStoredProcedure">The initialize stored procedure.</param>
		/// <returns>SqlCommand.</returns>
		public SqlCommand GetStoredProcedure(string spname)
		{
			this.SqlCommand = new SqlCommand(spname, this.GetSqlConnection());
			this.SqlCommand.CommandType = CommandType.StoredProcedure;
			this.SqlCommandText = this.SqlCommand.CommandText;
			this.SqlCommand.CommandTimeout = this.CommandTimeOut;
			return this.SqlCommand;
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		public DataTable ExecuteReader(Enum tableName) => ExecuteReader(tableName.ToString());

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		public DataTable ExecuteReader(string tableName = "DataTable")
		{
			this.CheckParams = true;
			DataTable dataTable = new DataTable(tableName);
			try
			{
				using (SqlConnection sqlConn = this.getSqlConn(this.StringConnection))
				using (this.SqlCommand = new SqlCommand(this.SqlCommandText, sqlConn))
				{
					this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader));
					using (SqlDataReader reader = this.SqlCommand.ExecuteReader())
						dataTable.Load(reader);
					this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader, dataTable));
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return dataTable;
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		public DataTable ExecuteSpReader(string tableName = "DataTable")
		{
			if (this.SqlCommand == null) return null;
			DataTable dataTable = new DataTable(tableName);
			try
			{
				this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader));
				using (SqlDataReader reader = this.SqlCommand.ExecuteReader())
					dataTable.Load(reader);
				this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader, dataTable));
			}

			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return dataTable;
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		public object ExecuteSpScalar()
		{
			object retVal = null;
			if (this.SqlCommand == null) return null;
			try
			{
				this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteScalar));
				retVal = this.SqlCommand.ExecuteScalar();
				this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteScalar));
			}

			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		public Dictionary<string, DataTable> ExecuteSpReaders(params object[] tables)
		{
			if (this.SqlCommand == null) return null;
			Dictionary<string, DataTable> dataTables = new Dictionary<string, DataTable>();
			try
			{
				DataSet dataSet = new DataSet();
				string[] tableNames = new string[tables.Length];
				for (int i = 0; i < tables.Length; i++)
					tableNames[i] = tables[i].ToString();

				this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader));
				using (SqlDataReader reader = this.SqlCommand.ExecuteReader())
					dataSet.Load(reader, LoadOption.OverwriteChanges, tableNames);

				this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader, dataSet));

				foreach (DataTable dataTable in dataSet.Tables)
					dataTables.Add(dataTable.TableName, dataTable);

			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return dataTables;
		}

		/// <summary>
		/// Executes the sp non query.
		/// </summary>
		/// <returns>System.Int32.</returns>
		public int ExecuteSpNonQuery()
		{
			if (this.SqlCommand == null) return 0;
			int retVal = 0;
			try
			{
				this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteNonQuery));
				retVal = this.SqlCommand.ExecuteNonQuery();
				this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteNonQuery));
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="sqlCmdEngine">The SQL data.</param>
		/// <param name="tables">The tables.</param>
		/// <returns>List&lt;DataTable&gt;.</returns>
		public Dictionary<string, DataTable> ExecuteReaders(params object[] tables)
		{
			this.CheckParams = true;
			Dictionary<string, DataTable> dataTables = new Dictionary<string, DataTable>();
			try
			{
				DataSet dataSet = new DataSet();

				using (SqlConnection sqlConn = this.getSqlConn(this.StringConnection))
				using (this.SqlCommand = new SqlCommand(this.SqlCommandText, sqlConn))
				{
					this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader));
					using (SqlDataReader reader = this.SqlCommand.ExecuteReader())
					{
						string[] tableNames = new string[tables.Length];
						for (int i = 0; i < tables.Length; i++)
							tableNames[i] = tables[i].ToString();
						dataSet.Load(reader, LoadOption.OverwriteChanges, tableNames);
						this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteReader, dataSet));
					}
				}
				foreach (DataTable dataTable in dataSet.Tables)
					dataTables.Add(dataTable.TableName, dataTable);
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			return dataTables;
		}


		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <returns>System.Object.</returns>
		public object ExecuteScalar()
		{
			this.CheckParams = true;
			object retVal = null;
			try
			{
				using (SqlConnection sqlConn = this.getSqlConn(this.StringConnection))
				using (this.SqlCommand = new SqlCommand(this.SqlCommandText, sqlConn))
				{
					this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteScalar));
					retVal = this.SqlCommand.ExecuteScalar();
					this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteScalar));
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		public void ExecuteNonQuery()
		{
			this.CheckParams = true;
			try
			{
				using (SqlConnection sqlConn = this.getSqlConn(this.StringConnection))
				using (this.SqlCommand = new SqlCommand(this.SqlCommandText, sqlConn))
				{
					this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteNonQuery));
					this.SqlCommand.ExecuteNonQuery();
					this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteNonQuery));
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Executes the bulk copy.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool ExecuteBulkCopy(DataTable table)
		{
			bool retVal = false;
			if (table == null) return retVal;
			try
			{
				this.ExeTime = 0;
				this.OnBeforeExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteBulkCopy));
				double IniTime = DateTime.Now.TimeOfDay.TotalMilliseconds;
				SqlConnection con = new SqlConnection(StringConnection);
				//create object of SqlBulkCopy which help to insert  
				SqlBulkCopy objbulk = new SqlBulkCopy(con);

				//assign Destination table name  
				objbulk.DestinationTableName = table.TableName;  //TFabbisognoLIV
																 //SqlCommand DELETECommand = new SqlCommand($"DELETE FROM {objbulk.DestinationTableName} WHERE CODIMP = '{(string)txtCODIMP.EditValue}'", con);

				foreach (DataColumn C in table.Columns)
				{
					objbulk.ColumnMappings.Add(C.ColumnName, C.ColumnName);
				}
				con.Open();

				//insert bulk Records into DataBase.  
				objbulk.WriteToServer(table);
				con.Close();

				double FinTime = DateTime.Now.TimeOfDay.TotalMilliseconds;
				this.ExeTime = FinTime - IniTime;
				this.OnAfterExecuteSql(new EventArgsExecuteSql(this, SqlExecuteMode.ExecuteBulkCopy));
				retVal = true;
			}

			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}

		public override string ToString()
		{
			if (this.StringConnection != null)
			{
				SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder(this.StringConnection);
				return $"Data Source={scsb.DataSource};Initial Catalog={scsb.InitialCatalog};User ID={scsb.UserID};Connect Timeout={scsb.ConnectTimeout}";
			}

			else
				return base.ToString();
		}


		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the SQL connection.
		/// </summary>
		/// <param name="stringSqlConn">The string SQL connection.</param>
		/// <returns>SqlConnection.</returns>
		private SqlConnection getSqlConn(string stringSqlConn)
		{
			SqlConnection retVal = new SqlConnection(stringSqlConn);

			try
			{
				int cnt = 0;
				retVal.Open();
				while (retVal.State != ConnectionState.Open)
				{
					if (cnt > 10000) break;
					cnt++;
				}
				return retVal;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return null;
		}

		/// <summary>
		/// Sets the user type table.
		/// </summary>
		/// <param name="userTypesStr">The user types string.</param>
		private void setUserTypeTable(string userTypesStr = null)
		{
			this.UserTypeTable.Clear();
			if (!string.IsNullOrEmpty(userTypesStr))
			{
				string[] strTuples = userTypesStr.Split(',');
				foreach (string strTuple in strTuples)
				{
					string[] tuple = strTuple.Split('=');
					if (tuple.Length == 2)
						this.UserTypeTable.Add(tuple[0], tuple[1]);
				}
			}
		}

		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions


		#region Dichiarazione Evento BeforeExecuteSql
		/// <summary>
		/// Occurs when [before execute SQL].
		/// </summary>
		public event EventHandlerExecuteSql BeforeExecuteSql;

		/// <summary>
		/// Called when [before execute SQL].
		/// </summary>
		/// <param name="e">The e.</param>
		protected virtual void OnBeforeExecuteSql(EventArgsExecuteSql e)
		{
			// If there are receivers listening, the event is thrown
			this.BeforeExecuteSql?.Invoke(this, e);
		}
		#endregion

		#region Dichiarazione Evento AfterExecuteSql
		/// <summary>
		/// Occurs when [after execute SQL].
		/// </summary>
		public event EventHandlerExecuteSql AfterExecuteSql;

		/// <summary>
		/// Called when [after execute SQL].
		/// </summary>
		/// <param name="e">The e.</param>
		protected virtual void OnAfterExecuteSql(EventArgsExecuteSql e)
		{
			// If there are receivers listening, the event is thrown
			this.AfterExecuteSql?.Invoke(this, e);
		}
		#endregion


		#region Definizione EventArgsExecuteSql
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public delegate void EventHandlerExecuteSql(object sender, EventArgsExecuteSql e);

		/// <summary>
		/// Classe per gli argomenti della generazione di un evento.
		/// </summary>
		public class EventArgsExecuteSql : System.EventArgs
		{
			#region Costruttori
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsExecuteSql"/> class.
			/// </summary>
			/// <param name="sqlCmdEngine">The SQL command engine.</param>
			/// <param name="sqlExecuteMode">The SQL execute mode.</param>
			/// <param name="resultTable">The result table.</param>
			public EventArgsExecuteSql(SqlCmdEngine sqlCmdEngine, SqlExecuteMode sqlExecuteMode, DataTable resultTable = null)
			{
				this.SqlCmdEngine = sqlCmdEngine;
				this.SqlCmd = this.SqlCmdEngine.SqlCommand;
				this.SqlExecuteMode = sqlExecuteMode;
				this.ResultTable = resultTable;
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsExecuteSql"/> class.
			/// </summary>
			/// <param name="sqlCmdEngine">The SQL command engine.</param>
			/// <param name="sqlExecuteMode">The SQL execute mode.</param>
			/// <param name="dataSet">The data set.</param>
			public EventArgsExecuteSql(SqlCmdEngine sqlCmdEngine, SqlExecuteMode sqlExecuteMode, DataSet dataSet)
			{
				this.SqlCmdEngine = sqlCmdEngine;
				this.SqlCmd = this.SqlCmdEngine.SqlCommand;
				this.SqlExecuteMode = sqlExecuteMode;
				this.DataSet = dataSet;
			}
			#endregion

			#region Proprietà            
			/// <summary>
			/// Gets or sets the SQL execute mode.
			/// </summary>
			/// <value>The SQL execute mode.</value>
			public SqlExecuteMode SqlExecuteMode { get; set; }

			/// <summary>
			/// Gets the SQL command engine.
			/// </summary>
			/// <value>The SQL command engine.</value>
			public SqlCmdEngine SqlCmdEngine { get; private set; }

			/// <summary>
			/// Gets or sets the SQL command.
			/// </summary>
			/// <value>The SQL command.</value>
			public SqlCommand SqlCmd { get; set; }
			/// <summary>
			/// Gets or sets the result table.
			/// </summary>
			/// <value>The result table.</value>
			public DataTable ResultTable { get; set; }
			/// <summary>
			/// Gets or sets the data set.
			/// </summary>
			/// <value>The data set.</value>
			public DataSet DataSet { get; set; }

			/// <summary>
			/// Gets or sets the command.
			/// </summary>
			/// <value>The command.</value>
			public ICommand Command { get; set; }
			#endregion
		}
		#endregion


		#endregion

		#region Embedded Types
		#endregion
	}
}
