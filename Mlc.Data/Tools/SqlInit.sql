﻿CREATE TABLE dbo.sys_sql_instructions (
  Category varchar(50) NOT NULL DEFAULT (''),
  [Key] nvarchar(50) NOT NULL,
  SqlCommand nvarchar(max) NULL,
  Comment nvarchar(200) NULL,
  CONSTRAINT PK_SqlQuery_Key PRIMARY KEY CLUSTERED ([Key])
)
