using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Mlc.Data;

namespace Mlc.Shell
{
	/// <summary>
	/// Estensioni per la classe Image.
	/// </summary>
	public static class DataTableTools
	{
		#region Public

		#region Method Static
		// #TableJoin
		// TODO: MAURO [20230124] => Implemenetare Join di data table dove serve
		//var results = from table1 in this.IssuesTable.AsEnumerable()
		//			  join table2 in this.regIssueRules.AsEnumerable() on (int)table1["CustID"] equals (int)table2["CustID"]
		//			  select new
		//			  {
		//				  CustID = (int)table1["CustID"],
		//				  ColX = (int)table1["ColX"],
		//				  ColY = (int)table1["ColY"],
		//				  ColZ = (int)table2["ColZ"]
		//			  };


		/// <summary>
		/// Combines the data tables.
		/// ASSUMPTION: All tables must have the same columns
		/// </summary>
		/// <param name="tables">The tables.</param>
		/// <returns>DataTable.</returns>
		public static DataTable CombineDataTables(params DataTable[] tables)
		{
			return tables.SelectMany(dt => dt.AsEnumerable()).CopyToDataTable();
		}

		/// <summary>
		/// Gets the table.
		/// </summary>
		/// <param name="rowMapType">Type of the row map.</param>
		/// <returns>DataTable.</returns>
		public static DataTable GetTableFromType(Type rowMapType)
		{
			DataTable dataTable = new();
			PropertyInfo[] pInfos = rowMapType.GetProperties();
			try
			{
				foreach (PropertyInfo pInfo in pInfos)
					if (pInfo.PropertyType != typeof(DataRow) && pInfo.PropertyType != typeof(DataTable))
					{
						bool addField = true;
						object[] attr = pInfo.GetCustomAttributes(typeof(Data.FieldAttribute), true);
						if (attr.Length == 1)
						{
							Data.FieldAttribute fieldAttribute = (Data.FieldAttribute)attr[0];
							addField = fieldAttribute.Visible;
						}
						if (addField)
							dataTable.Columns.Add(pInfo.Name, pInfo.PropertyType);
					}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return dataTable;
		}

		/// <summary>
		/// Gets the table from clipboard.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>DataTable.</returns>
		public static DataTable GetTableFromClipboard(Type type)
		{
			DataTable dataTable = null;
			try
			{
				if (Clipboard.ContainsText(TextDataFormat.CommaSeparatedValue))
				{
					dataTable = GetTableFromType(type);

					string text = (string)Clipboard.GetData(DataFormats.Text);
					string[] lines = text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
					Dictionary<string, int> titles = getTitles(lines[0]);
					string[] values = getValues(lines);

					// fill data table
					foreach (string rowitem in values)
					{
						DataRow row = dataTable.NewRow();
						string[] fieldVal = rowitem.Split('\t');
						foreach (KeyValuePair<string, int> title in titles)
							if (dataTable.Columns.Contains(title.Key))
								row[title.Key] = fieldVal[title.Value];
						dataTable.Rows.Add(row);
					}
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return dataTable;
		}

		/// <summary>
		/// Selects the rows.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <returns>DataTable.</returns>
		public static DataTable SelectToTable(this DataTable table, string filter)
		{
			if (table == null) return null;
			DataTable resultTable = table.Clone();
			DataRow[] rows = table.Select(filter);
			if (rows.Length > 0)
				foreach (DataRow row in rows)
					resultTable.ImportRow(row);
			return resultTable;
		}

		/// <summary>
		/// Deletes the rows.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="removeRows">The remove rows.</param>
		/// <param name="keyField">The key field.</param>
		/// <returns>DataTable.</returns>
		public static DataTable DeleteRows(this DataTable table, DataTable removeRows, string keyField)
		{
			try
			{
				DataTable resultTable = table.Clone();
				List<string> values = removeRows.GetFieldStringValues(keyField);
				string csv = removeRows.GetFieldCSV(keyField);
				DataRow[] rows = table.Select($"{keyField} NOT IN ({csv})");
				if (rows.Length > 0)
					foreach (DataRow row in rows)
						resultTable.ImportRow(row);
				return resultTable;
			}
			catch { }
			return null;
		}

		/// <summary>
		/// Gets the field CSV.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>System.String.</returns>
		public static string GetFieldCSV(this DataTable table, string fieldName) => string.Join(",", GetFieldStringValues(table, fieldName));

		/// <summary>
		/// Gets the field values.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="format">The format.</param>
		/// <returns>List&lt;System.String&gt;.</returns>
		public static List<string> GetFieldStringValues(this DataTable table, string fieldName, string format = null)
		{
			List<string> values = new List<string>();
			try
			{
				if (table.Columns.Contains(fieldName))
				{
					Type colType = table.Columns[fieldName].DataType;
					if (colType == typeof(int))
						values = table.AsEnumerable().Select(r => r.Field<int>(fieldName).ToString()).ToList();
					if (colType == typeof(string))
						if (string.IsNullOrEmpty(format))
							values = table.AsEnumerable().Select(r => r.Field<string>(fieldName)).ToList();
						else
							values = table.AsEnumerable().Select(r => string.Format(format, r.Field<string>(fieldName))).ToList();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return values;
		}

		/// <summary>
		/// Gets the fields table.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="fieldList">The field list.</param>
		/// <param name="distinct">if set to <c>true</c> [distinct].</param>
		/// <returns>DataTable.</returns>
		public static DataTable GetFieldsTable(this DataTable table, List<string> fieldList, bool distinct = false)
		{
			if (table == null)
				return null;

			return table.DefaultView.ToTable(distinct, fieldList.ToArray());
		}

		/// <summary>
		/// Updates the common field.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="tableNew">The table new.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="fieldKeyNew">The field key new.</param>
		public static void UpdateCommonField(this DataTable table, DataTable tableNew, string fieldKey, string fieldKeyNew = null, bool forceReadOnly = false)
		{
			if (fieldKeyNew == null)
				fieldKeyNew = fieldKey;

			// checks the key fields
			if (table.Columns.Contains(fieldKey) && tableNew.Columns.Contains(fieldKeyNew)
				&& table.Columns[fieldKey].DataType == tableNew.Columns[fieldKeyNew].DataType)
			{
				// checks common field
				List<string> commonCols = new List<string>();
				foreach (DataColumn col in tableNew.Columns)
					if (table.Columns.Contains(col.ColumnName) && table.Columns[col.ColumnName].DataType == col.DataType)
						commonCols.Add(col.ColumnName);

				// checks read only field
				List<DataColumn> readOnlyCols = new List<DataColumn>();
				if (forceReadOnly)
					foreach (DataColumn col in table.Columns)
						if (commonCols.Contains(col.ColumnName) && col.ReadOnly)
						{
							readOnlyCols.Add(col);
							col.ReadOnly = false;
						}

				// removes from common columns those that are read-only in the table to be updated
				foreach (DataColumn col in table.Columns)
					if (commonCols.Contains(col.ColumnName) && col.ReadOnly)
						commonCols.Remove(col.ColumnName);

				// prepare filter template
				string filterRow = null;
				if (table.Columns[fieldKey].DataType == typeof(string))
					filterRow = fieldKey + " = '{0}'";
				else
					filterRow = fieldKey + " = {0}";

				// it cycles through all the rows of the table with the new data
				// and updates the fields of the common rows on the one to be updated
				foreach (DataRow rowNew in tableNew.Rows)
				{
					try
					{
						var key = rowNew[fieldKeyNew];
						DataRow[] rows = table.Select(string.Format(filterRow, key));
						if (rows.Length == 1)
							foreach (string field in commonCols)
								rows[0][field] = rowNew[field];
					}
					catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				}

				table.AcceptChanges();
				foreach (DataColumn col in readOnlyCols)
					col.ReadOnly = true;
			}
		}

		/// <summary>
		/// Gets the fields table.
		/// </summary>
		/// <param name="valueList">The value list.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>DataTable.</returns>
		public static DataTable GetValuesTable(this List<string> valueList, string fieldName)
		{
			DataTable retTable = new();
			retTable.Columns.Add(fieldName, typeof(String)); 
			if (valueList != null && valueList.Count>0)
				for (int i = 0; i < valueList.Count(); i++)
					retTable.Rows.Add(valueList[i]);
			return retTable;
		}


		/// <summary>
		/// Gets the fields table.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="fieldList">The field list.</param>
		/// <param name="distinct">if set to <c>true</c> [distinct].</param>
		/// <returns>DataTable.</returns>
		public static void SetAttributeFromType(this DataTable table, Type type )
		{
			if (type == typeof(IDataRowWrapper) || type == typeof(DsRowMapBase))
			{ 
				
							
			
			}
		}

		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		/// <summary>
		/// Gets the titles.
		/// </summary>
		/// <param name="titles">The titles.</param>
		/// <returns>Dictionary&lt;System.String, System.Int32&gt;.</returns>
		private static Dictionary<string, int> getTitles(string titles)
		{
			Dictionary<string, int> fieldIndexer = new Dictionary<string, int>();
			// get titles
			string[] titleList = titles.Split('\t');
			for (int i = 0; i < titleList.Length; i++)
				fieldIndexer.Add(titleList[i], i);
			return fieldIndexer;
		}

		/// <summary>
		/// Gets the values.
		/// </summary>
		/// <param name="lines">The lines.</param>
		/// <returns>System.String[].</returns>
		private static string[] getValues(string[] lines)
		{
			string[] values = new string[lines.Length - 1];
			Array.Copy(lines, 1, values, 0, values.Length);
			return values;
		}
		#endregion

		#region Event Handlers
		#endregion
		#endregion
	}
}
