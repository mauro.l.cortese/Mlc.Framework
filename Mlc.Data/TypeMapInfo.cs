﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	/// <summary>
	/// Collezione delle informazioni necessarie per la generazione del codice dei tipi mappati sui dati
	/// </summary>
	public class TypeMapInfoCollection : Dictionary<string, TypeMapInfo>
	{
		/// <summary>
		/// Aggiunge un elemento alla collezione
		/// </summary>
		/// <param name="typeMapInfo">Istanza delle informazioni necessarie per la generazione del codice di un tipo mappato sui dati</param>
		public void Add(TypeMapInfo typeMapInfo)
		{
			this.Add(typeMapInfo.DsTableName, typeMapInfo);
		}
	}

	/// <summary>
	/// Definisce le informazioni necessarie per la generazione del codice di un tipo mappato sui dati
	/// </summary>
	public class TypeMapInfo
	{
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dsTableName">Nome della tabella del DataSet</param>
		public TypeMapInfo(string dsTableName)
			: this(dsTableName, string.Empty)
		{ }


		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dsTableName">Nome della tabella del DataSet</param>
		/// <param name="className">Nome della classe che definisce il tipo mappato sui dati</param>
		public TypeMapInfo(string dsTableName, string className)
			: this(dsTableName, string.Empty, className)
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dsTableName">Nome della tabella del DataSet</param>
		/// <param name="dbTableName">nome della tabella su DB (se esiste)</param>
		/// <param name="className">Nome della classe che definisce il tipo mappato sui dati</param>
		public TypeMapInfo(string dsTableName, string dbTableName, string className)
		{
			this.DsTableName = dsTableName;
			this.DbTableName = dbTableName;
			this.ClassName = className;
		}

		/// <summary>
		/// Restituisce o imposta il nome della classe che definisce il tipo mappato sui dati
		/// </summary>
		public string ClassName { get; set; }

		/// <summary>
		/// Nome della tabella del DataSet
		/// </summary>
		public string DsTableName { get; set; }

		/// <summary>
		/// Restituisce o imposta il nome della tabella su DB (se esiste)
		/// </summary>
		public string DbTableName { get; set; }

		/// <summary>
		/// Imposta le info accessorie
		/// </summary>
		/// <param name="dbTableName">nome della tabella su DB (se esiste)</param>
		/// <param name="className">Nome della classe che definisce il tipo mappato sui dati</param>
		public void SetInfo(string dbTableName, string className)
		{
			this.DbTableName = dbTableName;
			this.ClassName = className;
		}
	}
}
