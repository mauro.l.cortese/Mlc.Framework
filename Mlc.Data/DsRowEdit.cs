using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Mlc.Shell;


namespace Mlc.Data
{
	/// <summary>
	/// Estensioni per i tipi che ereditano da <see cref="Mx.Core.Data.DsRowMapBase"/>
	/// </summary>
	public static class DsRowEdit
	{
		/// <summary>
		/// Aggiorna l'istanza con i dati del reader
		/// </summary>
		/// <param name="iRowMap">Istanza da aggiornare</param>
		/// <param name="reader">Reader con cui aggiornare l'istanza</param>
		public static void UpdateInnerRow(this DsRowMapBase iRowMap, IDataReader reader)
		{
			DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
			Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();
			for (int i = 0; i < reader.FieldCount; i++)
			{
				string fname = reader.GetName(i);
				if (props.ContainsKey(fname))
				{
					Type pt = props[fname].PropertyType;
					object value = reader[fname];
					if (value == null || value is DBNull)
						value = defaultValueType.GetValue(pt);

					if (value.GetType() != pt)
						value = Convert.ChangeType(value, pt);

					props[fname].SetValue(iRowMap, value, null);
				}
			}
			OutRowMapping(iRowMap);
		}

		/// <summary>
		/// Aggiorna l'istanza con i dati del reader
		/// </summary>
		/// <param name="iRowMap">Istanza da aggiornare</param>
		/// <param name="dataRow">DataRow con cui aggiornare l'istanza</param>
		public static void UpdateInnerRow(this DsRowMapBase iRowMap, DataRow dataRow)
		{
			DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
			Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();

			DataTable dt = dataRow.Table;
			foreach (DataColumn col in dt.Columns)
			{
				string colName = col.ColumnName;
				if (props.ContainsKey(colName))
				{
					object value = dataRow[colName];
					if (value == null || value is DBNull)
						value = defaultValueType.GetValue(iRowMap.dataRow.Table.Columns[colName].DataType);
					props[colName].SetValue(iRowMap, value, null);
				}
			}
			OutRowMapping(iRowMap);
		}

		/// <summary>
		/// Copia i dati dell'istanza sul DataRow passato
		/// </summary>
		/// <param name="iRowMap">Istanza da cui copiare i dati</param>
		/// <param name="dataRow">DataRow in cui incollare i dati</param>
		/// <param name="exclusion">The exclusion.</param>
		public static void CopyToDataRow(this DsRowMapBase iRowMap, DataRow dataRow, List<string> exclusion = null)
		{
			Dictionary<string, PropertyInfo> props = iRowMap.GetProperties();
			DataTable dt = dataRow.Table;
			foreach (DataColumn col in dt.Columns)
			{
				string colName = col.ColumnName;
				bool process = exclusion == null;

				if (!process)
					process = !exclusion.Contains(colName);

				if (process)
					if (props.ContainsKey(colName))
						if (!dt.Columns[colName].ReadOnly)
						{
							var value = props[colName].GetValue(iRowMap, null);
							if (value != null)
								dataRow[colName] = value;
						}
			}
		}

		/// <summary>
		/// Gets the value property.
		/// </summary>
		/// <param name="iRowMap">The i row map.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>System.Object.</returns>
		public static object GetValueProperty(this DsRowMapBase iRowMap, string fieldName)
		{
			object retVal = null;
			try
			{
				PropertyInfo prop = iRowMap.GetType().GetProperty(fieldName);
				retVal = prop.GetValue(iRowMap);
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
			}
			return retVal;

		}


		/// <summary>
		/// Gets the value property.
		/// </summary>
		/// <param name="iRowMap">The i row map.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="value">The value.</param>
		/// <param name="updateSource">if set to <c>true</c> [udate source].</param>
		/// <param name="forceReadOnly">if set to <c>true</c> [force read only].</param>
		/// <returns>System.Object.</returns>
		public static void SetValueProperty(this DsRowMapBase iRowMap, string fieldName, object value, bool updateSource = true, bool forceReadOnly = false)
		{
			try
			{
				PropertyInfo prop = iRowMap.GetType().GetProperty(fieldName);
				prop.SetValue(iRowMap, value);
				if (updateSource)
				{
					DataColumn col = iRowMap.dataRow.Table.Columns[fieldName];
					if (col.ReadOnly && forceReadOnly)
					{
						col.ReadOnly = false;
						iRowMap.dataRow[fieldName] = value;
						col.ReadOnly = true;
					}
					else if (!col.ReadOnly)
						iRowMap.dataRow[fieldName] = value;
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Copia i dati dall'istanza al suo DataRow
		/// </summary>
		/// <param name="iRowMap">Istanza per cui aggiornare il DataRow associato</param>
		/// <param name="forceReadOnly">if set to <c>true</c> [force read only].</param>
		public static void OutRowMapping(this DsRowMapBase iRowMap, bool forceReadOnly = false)
		{
			try
			{
				if (iRowMap?.dataRow == null) return;

				foreach (PropertyInfo pInfo in iRowMap.GetProperties().Values)
					if (iRowMap.dataRow.Table.Columns.Contains(pInfo.Name))
					{
						DataColumn col = iRowMap.dataRow.Table.Columns[pInfo.Name];
						if (!col.AutoIncrement)
						{
							bool readOnly = col.ReadOnly;
							if (forceReadOnly && readOnly)
							{
								col.ReadOnly = false;
								iRowMap.dataRow[pInfo.Name] = pInfo.GetValue(iRowMap, null);
								col.ReadOnly = true;
							}
							else if (!col.ReadOnly)
								iRowMap.dataRow[pInfo.Name] = pInfo.GetValue(iRowMap, null);
						}
					}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Copia i dati del DataRow sull'istanza
		/// </summary>
		/// <param name="iRowMap">Istanza da aggiornare in base al DataRow associato</param>
		public static void InRowMapping(this DsRowMapBase iRowMap)
		{
			DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
			if (iRowMap == null || iRowMap.dataRow == null) return;
			foreach (PropertyInfo pInfo in iRowMap.GetProperties().Values)
				try
				{
					if (iRowMap.dataRow.Table.Columns.Contains(pInfo.Name))
					{
						if (pInfo.CanWrite)
						{
							//Console.WriteLine(pInfo.Name);
							object value = iRowMap.dataRow[pInfo.Name];
							if (value == null || value is DBNull)
								value = defaultValueType.GetValue(pInfo.PropertyType);
							if (value != null)
								if (value.GetType() != pInfo.PropertyType)
									try { value = Convert.ChangeType(value, pInfo.PropertyType); }
									catch { }

							pInfo.SetValue(iRowMap, value, null);
						}
					}
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}
	}
}
