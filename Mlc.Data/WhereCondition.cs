﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
    /// <summary>
    /// Class WhereCondition.
    /// </summary>
    public class WhereCondition
    {
        /// <summary>
        /// The where rows
        /// </summary>
        List<string> andWhereRows = new List<string>();

        /// <summary>
        /// The where rows
        /// </summary>
        List<string> orWhereRows = new List<string>();

        /// <summary>
        /// Adds the or condition.
        /// </summary>
        /// <param name="condition">The condition.</param>
        public void AddOrCondition(string condition) { orWhereRows.Add(condition); }

        /// <summary>
        /// Adds the and condition.
        /// </summary>
        /// <param name="condition">The condition.</param>
        public void AddAndCondition(string condition) { andWhereRows.Add(condition); }

        /// <summary>
        /// The where
        /// </summary>
        private string where;
        /// <summary>
        /// Gets or sets the where.
        /// </summary>
        /// <value>The where.</value>
        public string Where { get { return this.getWhere(); } set { this.where = value; } }

        /// <summary>
        /// Gets the where.
        /// </summary>
        /// <returns>System.String.</returns>
        private string getWhere()
        {
            string retVal = string.Empty;

            StringBuilder sbWhere = new();

            bool thereIsOr = orWhereRows.Count != 0;
            bool thereIsAnd = andWhereRows.Count != 0; ;

            if (thereIsOr || thereIsAnd)
                sbWhere.AppendLine("    WHERE");
            else
                return retVal;

            if (thereIsOr)
            {
                string or = "       ";
                sbWhere.AppendLine("    (");
                foreach (string row in orWhereRows)
                {
                    sbWhere.AppendLine(string.Format("{0} {1}", or, row.Trim()));
                    or = "        OR";
                }
                sbWhere.AppendLine("    )");
            }

            if (thereIsAnd)
            {
                string and = thereIsOr ? "        AND" : "       ";
                foreach (string row in andWhereRows)
                {
                    sbWhere.AppendLine(string.Format("{0} {1}", and, row.Trim()));
                    and = "        AND";
                }
            }

            retVal = sbWhere.ToString();
            return retVal.TrimEnd();
        }
    }
}
