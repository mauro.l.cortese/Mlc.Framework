﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Data
{
	/// <summary>
	/// Class DataValidator.
	/// </summary>
	public class DataValidator
	{
		/// <summary>
		/// The value
		/// </summary>
		private string value;

		/// <summary>
		/// Initializes a new instance of the <see cref="DataValidator"/> class.
		/// </summary>
		/// <param name="value">The value.</param>
		public DataValidator(string value) => this.value = value;

		/// <summary>
		/// Initializes a new instance of the <see cref="DataValidator"/> class.
		/// </summary>
		/// <param name="value">The value.</param>
		public DataValidator(object value)
			: this(value != null ? value.ToString() : string.Empty)
		{ }

		/// <summary>
		/// Validates the iban.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool ValidateIBAN()
		{
			if (string.IsNullOrEmpty(this.value)) return true;
			string iban = this.value.ToUpper(); //IN ORDER TO COPE WITH THE REGEX BELOW
			if (String.IsNullOrEmpty(iban))
				return false;
			else if (System.Text.RegularExpressions.Regex.IsMatch(iban, "^[A-Z0-9]"))
			{
				iban = iban.Replace(" ", String.Empty);
				string bank =
				iban.Substring(4, iban.Length - 4) + iban.Substring(0, 4);
				int asciiShift = 55;
				StringBuilder sb = new();
				foreach (char c in bank)
				{
					int v;
					if (Char.IsLetter(c)) v = c - asciiShift;
					else v = int.Parse(c.ToString());
					sb.Append(v);
				}
				string checkSumString = sb.ToString();
				int checksum = int.Parse(checkSumString.Substring(0, 1));
				for (int i = 1; i < checkSumString.Length; i++)
				{
					int v = int.Parse(checkSumString.Substring(i, 1));
					checksum *= 10;
					checksum += v;
					checksum %= 97;
				}
				return checksum == 1;
			}
			else
				return false;
		}

	}
}
