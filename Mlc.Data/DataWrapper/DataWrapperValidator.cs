using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using Mlc.Shell;
namespace Mlc.Data
{
	/// <summary>
	/// Class DataWrapperValidator.
	/// </summary>
	public class DataWrapperValidator
	{
		#region Fields
		/// <summary>The data row wrapper</summary>
		private IDataRowWrapper dataRowWrapper;
		/// <summary>The table</summary>
		private DataTable table;
		/// <summary>The key row</summary>
		private string keyRow;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DataWrapperValidator"/> class.
		/// </summary>
		/// <param name="dataRowWrapper">The data row wrapper.</param>
		/// <param name="keyRow">The key row.</param>
		/// <param name="regexRules">The regex rules.</param>
		public DataWrapperValidator(IDataRowWrapper dataRowWrapper, string keyRow, RegexRules regexRules)
		{
			this.dataRowWrapper = dataRowWrapper;
			this.table = dataRowWrapper.Table;
			this.keyRow = keyRow;
			this.Rules = regexRules;
			this.TableErr = new DataTable();

			addCol(keyRow);

			foreach (string field in regexRules.Keys)
				addCol(field);


			this.Execute();

			void addCol(string field)
			{
				if (this.table.Columns.Contains(field))
				{
					DataColumn col = this.table.Columns[field];
					this.TableErr.Columns.Add(col.ColumnName, col.DataType);
				}
			}
		}

		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets the table error.
		/// </summary>
		/// <value>The table error.</value>
		public DataTable TableErr { get; }

		/// <summary>
		/// Gets the regex rules.
		/// </summary>
		/// <value>The regex rules.</value>
		public RegexRules Rules { get; }
		#endregion

		#region Method
		/// <summary>
		/// Checks if value is wrong.
		/// </summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="value">The value.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool CheckIfValueIsWrong(string fieldName, object value) => this.CheckIfValueIsWrong(fieldName, value.ToString());

		/// <summary>
		/// Checks if value is wrong.
		/// </summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="value">The value.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool CheckIfValueIsWrong(string fieldName, string value) => this.Rules.ContainsKey(fieldName) ? this.Rules[fieldName].Contains(value) : false;

		/// <summary>
		/// Executes this instance.
		/// </summary>
		public void Execute()
		{
			MatchCollection match;

			foreach (RegexRule regexRule in this.Rules.Values)
				if (this.table.Columns.Contains(regexRule.Field))
					foreach (DataRow row in this.table.Rows)
					{
						if (row[regexRule.Field] is string value)
						{
							match = Regex.Matches(value, regexRule.Pattern);
							switch (regexRule.Mode)
							{
								case RegexRule.MatchMode.Match:
									if (match.Count != 1)
										this.writeError(row, regexRule, value);
									break;
								case RegexRule.MatchMode.NoMatch:
									if (match.Count != 0)
										this.writeError(row, regexRule, value);
									break;
							}
						}
					}
		}

		#endregion
		#endregion

		#region Private
		#region Method
		/// <summary>
		/// Writes the error.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="regexRule">The regex rule.</param>
		/// <param name="value">The value.</param>
		private void writeError(DataRow row, RegexRule regexRule, string value)
		{

			try
			{
				DataRow[] rows = this.TableErr.Select($"{this.keyRow}={row[this.keyRow]}");
				if (rows.Length == 0)
				{
					DataRow rowErr = this.TableErr.NewRow();
					rowErr[this.keyRow] = row[this.keyRow];
					rowErr[regexRule.Field] = value;
					this.TableErr.Rows.Add(rowErr);
				}
				else if (rows.Length == 1)
					rows[0][regexRule.Field] = value;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			regexRule.Add(value);
		}

		#endregion
		#endregion

		#region Embedded Types
		/// <summary>
		/// Class RegexRules.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, System.String}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.String}" />
		public class RegexRules : Dictionary<string, RegexRule>
		{
			public new void Add(string field, RegexRule regexRule)
			{
				regexRule.Field = field;
				base.Add(field, regexRule);
			}

			public void Add(RegexRule regexRule) => base.Add(regexRule.Field, regexRule);
		}

		/// <summary>
		/// Class RegexRule.
		/// </summary>
		public class RegexRule : List<string>
		{
			/// <summary>
			/// Gets the field.
			/// </summary>
			/// <value>The field.</value>
			public string Field { get; internal set; }
			/// <summary>
			/// Gets the pattern.
			/// </summary>
			/// <value>The pattern.</value>
			public string Pattern { get; }
			/// <summary>
			/// Gets the mode.
			/// </summary>
			/// <value>The mode.</value>
			public MatchMode Mode { get; }

			/// <summary>
			/// Enum MatchMode
			/// </summary>
			public enum MatchMode
			{
				Match,
				NoMatch,
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="RegexRule"/> class.
			/// </summary>
			/// <param name="field">The field.</param>
			/// <param name="pattern">The pattern.</param>
			/// <param name="matchMode">The match mode.</param>
			public RegexRule(string pattern, MatchMode matchMode = MatchMode.Match)
				: this("", pattern, matchMode) { }

			/// <summary>
			/// Initializes a new instance of the <see cref="RegexRule"/> class.
			/// </summary>
			/// <param name="field">The field.</param>
			/// <param name="pattern">The pattern.</param>
			/// <param name="matchMode">The match mode.</param>
			public RegexRule(string field, string pattern, MatchMode matchMode = MatchMode.Match)
			{
				this.Field = field;
				this.Pattern = pattern;
				this.Mode = matchMode;
			}

		}
		#endregion
	}
}
