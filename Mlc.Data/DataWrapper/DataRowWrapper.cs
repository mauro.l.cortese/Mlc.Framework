using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using Newtonsoft.Json;
using Mlc.Shell;

namespace Mlc.Data
{
	/// <summary>
	/// Ereditare da questa classe per implementare tipi mappati sui dati di <see cref="System.Data.DataRow" />
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class DataRowWrapper<T> : IDataRowWrapper<T> where T : IDataRowWrapper<T>
	{

		/// <summary>
		/// The string table name
		/// </summary>
		private const string strTableName = "EmptyTable";

		/// <summary>
		/// The allow initialize values
		/// </summary>
		private bool allowInitValues = true;

		#region Constructors
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		[Bindable(BindableSupport.No)]
		[Browsable(false)]
		public DataRowWrapper() : this(null) => this.Table = this.getNewTable(tableName);

		/// <summary>
		/// Initializes a new instance of the <see cref="DataRowWrapper{T}"/> class.
		/// </summary>
		/// <param name="table">The table.</param>
		[Bindable(BindableSupport.No)]
		[Browsable(false)]
		public DataRowWrapper(DataTable table) => this.Table = table;

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		/// <param name="addToTable">if set to <c>true</c> [add to table].</param>
		[Bindable(BindableSupport.No)]
		[Browsable(false)]
		public DataRowWrapper(DataRow dataRow, bool addToTable = false) => this.addDataRow(dataRow, addToTable);
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Restituisce o imposta il DataRow mappato sull'istanza
		/// </summary>
		[Field(Visible = false)]
		public DataRow DataRow { get; set; }

		/// <summary>
		/// The force readonly
		/// </summary>
		private bool forceReadonly = false;
		/// <summary>
		/// Gets or sets a value indicating whether [force readonly].
		/// </summary>
		/// <value><c>true</c> if [force readonly]; otherwise, <c>false</c>.</value>
		[Field(Visible = false)]
		public bool ForceReadonly
		{
			get => this.forceReadonly;
			set
			{
				this.forceReadonly = value;
				this.setReadOnlyOff();
			}
		}


		private DataTable table;
		/// <summary>
		/// Gets or sets the table.
		/// </summary>
		/// <value>The table.</value>
		[Field(Visible = false)]
		public DataTable Table
		{
			get => this.table;
			set
			{
				this.table = value;

				if (this.table != null)
				{
					this.TableName = this.table.TableName;
					if (this.table.Rows.Count > 0 && this.DataRow == null)
						this.DataRow = this.table.Rows[0];
				}

				//if (this.DataRow != null)
				//	this.InitValues();
			}
		}

		/// <summary>
		/// The table name
		/// </summary>
		private string tableName = strTableName;

		/// <summary>
		/// Gets or sets the name of the table.
		/// </summary>
		/// <value>The name of the table.</value>
		public string TableName
		{
			get => this.tableName;

			set
			{
				this.tableName = value;
				if (this.Table != null)
					this.Table.TableName = this.tableName;
			}
		}


		/// <summary>
		/// Gets or sets the <see cref="System.Object"/> with the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>System.Object.</returns>
		[Field(Visible = false)]
		public virtual object this[string key]
		{
			get
			{
				if (this.DataRow != null && this.DataRow.Table.Columns.Contains(key) && this.DataRow[key] != DBNull.Value)
					return this.DataRow[key];
				else if (this.DataRow != null && this.DataRow.Table.Columns.Contains(key))
					return this.DataRow.Table.Columns[key].DataType.GetDefaultValue();
				else
					return null;	
			}
			set
			{
				if (this.DataRow != null && this.DataRow.Table.Columns.Contains(key))
					this.DataRow[key] = value;
			}
		}
		#endregion

		#region Method
		/// <summary>
		/// Adds to table.
		/// </summary>
		/// <param name="rowNumber">The row number.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool AddToTable(int rowNumber)
		{
			try
			{

				if (this.Table != null && this.Table.Rows.Count < rowNumber)
				{
					int rowsDiff = rowNumber - this.Table.Rows.Count;

					for (int i = 0; i < rowsDiff; i++)
						this.Table.Rows.Add(this.Table.NewRow());
				}
				return true;
			}
			catch (Exception) { return false; }
		}

		/// <summary>
		/// restituisce una nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/> copia di quella corrente 
		/// </summary>
		/// <returns>Nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/></returns>
		public T Clone() => (T)this.MemberwiseClone();

		/// <summary>
		/// restituisce una nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/> copia di quella corrente 
		/// </summary>
		/// <returns>Nuova istanza di <see cref="Mx.Core.Data.IDataRowMapping"/></returns>
		public T Clone(DataTable table)
		{
			if (table != null && table.Rows.Count > 0)
			{
				T drw = this.Clone();
				drw.Table = table;
				drw.DataRow = drw.Table.Rows[0];
				return drw;
			}
			return default(T);
		}


		/// <summary>
		/// Gets the iterator.
		/// </summary>
		/// <returns>DataWrapperIterator.</returns>
		public DataWrapperIterator<T> GetIterator() => new DataWrapperIterator<T>(this);

		/// <summary>
		/// Gets the validator.
		/// </summary>
		/// <param name="keyRow">The key row.</param>
		/// <param name="regexRules">The regex rules.</param>
		/// <returns>DataWrapperValidator.</returns>
		public DataWrapperValidator GetValidator(string keyRow, DataWrapperValidator.RegexRules regexRules) => new(this, keyRow, regexRules);

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>A <see cref="System.String" /> that represents this instance.</returns>
		public override string ToString()
		{
			StringBuilder sb = new();
			if (this.Table != null && this.Table.Columns.Count > 0 && this.DataRow != null)
			{
				foreach (DataColumn col in this.Table.Columns)
					sb.Append($",[{col.ColumnName}]:{this.DataRow[col]}");
				sb.Remove(0, 1);
			}
			return sb.ToString();
		}

		/// <summary>
		/// Selects the specified filter.
		/// </summary>
		/// <param name="filter">The filter.</param>
		/// <returns>Mlc.Data.DataRowWrapper&lt;T&gt;.</returns>
		public T Select(string filter)
		{
			IDataRowWrapper retInst = null;
			try
			{
				if (this.Table != null)
				{
					DataTable table = this.Table.SelectToTable(filter);
					if (table != null && table.Rows.Count > 0)
						retInst = this.Clone(table);
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return (T)retInst;
		}

		/// <summary>
		/// Converts to json.
		/// </summary>
		/// <returns>System.String.</returns>
		public string ToJson()
		{
			string jsonStr = null;
			DataTable tmpTbl = this.Table;
			this.Table = tmpTbl.Clone();
			this.Table.TableName = this.TableName;
			jsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
			this.Table = tmpTbl;
			return jsonStr;
		}
		#endregion

		#region Method Static
		/// <summary>
		/// Converts to json.
		/// </summary>
		/// <returns>System.String.</returns>
		public static T FromJson(string val) => JsonConvert.DeserializeObject<T>(val);
		#endregion

		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Adds the data row.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		/// <param name="addToTable">if set to <c>true</c> [add to table].</param>
		private void addDataRow(DataRow dataRow, bool addToTable = false)
		{
			this.DataRow = dataRow;
			if (this.DataRow != null)
			{
				//this.InitValues();

				if (this.Table == null)
					this.Table = this.DataRow.Table;

				if (this.Table != null)
				{
					foreach (DataColumn col in this.Table.Columns)
						if (this.DataRow[col] == DBNull.Value)
							this.setDefautlValue(this.DataRow, col);

					this.TableName = this.Table.TableName;
				}

				if (addToTable)
					this.Table.Rows.Add(this.DataRow);
			}
		}

		/// <summary>
		/// Gets the new table.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>DataTable.</returns>
		private DataTable getNewTable(string tableName)
		{
			DataTable retTable = new(tableName);
			foreach (PropertyInfo pInfo in this.GetProperties().Values)
			{
				FieldAttribute fieldAttribute = this.getFieldAttribute(pInfo);
				if (fieldAttribute != null)
					if (fieldAttribute.Visible)
					{
						DataColumn col = new(pInfo.Name, pInfo.PropertyType);
						col.ReadOnly = this.forceReadonly ? true : fieldAttribute.AllowEdit;
						retTable.Columns.Add(col);
					}
			}
			if (retTable != null)
				this.addDataRow(retTable.NewRow(), true);

			return retTable;
		}

		/// <summary>
		/// Sets the defautl value.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		/// <param name="col">The col.</param>
		private void setDefautlValue(DataRow dataRow, DataColumn col) => dataRow[col] = col.DataType.GetDefaultValue();

		/// <summary>
		/// Sets the read only off.
		/// </summary>
		private void setReadOnlyOff()
		{
			if (this.Table != null)
				foreach (DataColumn col in this.Table.Columns)
					col.ReadOnly = false;
		}

		/// <summary>
		/// Gets the field attribute.
		/// </summary>
		/// <param name="pInfo">The p information.</param>
		/// <returns>FieldAttribute.</returns>
		private FieldAttribute getFieldAttribute(PropertyInfo pInfo)
		{
			object[] attr = pInfo.GetCustomAttributes(typeof(FieldAttribute), true);
			if (attr.Length == 1)
				return (FieldAttribute)attr[0];
			return null;
		}

		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Protected
		#region Method
		#endregion
		#endregion
	}
}
