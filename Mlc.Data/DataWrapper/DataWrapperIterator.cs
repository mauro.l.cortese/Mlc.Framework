using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using Mlc.Shell;

namespace Mlc.Data
{
	/// <summary>
	/// Ereditare da questa classe per implementare tipi mappati sui dati di <see cref="System.Data.DataRow"/>
	/// </summary>
	public class DataWrapperIterator<T> where T : IDataRowWrapper<T>
	{
		#region Fields
		/// <summary>The data row wrapper</summary>
		private IDataRowWrapper<T> dataRowWrapper;
		/// <summary>The table</summary>
		private DataTable table;
		/// <summary>The counter</summary>
		private int counter = -1;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DataWrapperIterator"/> class.
		/// </summary>
		/// <param name="dataRowWrapper">The data row wrapper.</param>
		public DataWrapperIterator(IDataRowWrapper<T> dataRowWrapper)
		{
			this.dataRowWrapper = dataRowWrapper;

			if (this.dataRowWrapper != null && this.dataRowWrapper.DataRow != null)
				this.table = this.dataRowWrapper.DataRow.Table;

		}
		#endregion

		#region Public
		/// <summary>
		/// Gets the data row wrapper.
		/// </summary>
		/// <value>The data row wrapper.</value>
		public T DataRowWrapper { get => (T)dataRowWrapper; }

		/// <summary>
		/// Gets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public Exception Exception { get; private set; }

		#region Method
		/// <summary>
		/// Moves the next.
		/// </summary>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool MoveNext()
		{
			bool retVal = false;
			try
			{
				if (this.table != null && this.table.Rows.Count > 0)
				{
					counter++;
					if (counter < this.table.Rows.Count)
						retVal = true;
					else
						counter = -1;

					if (retVal)
						this.dataRowWrapper.DataRow = this.table.Rows[counter];
				}
			}
			catch (Exception ex)
			{
				this.Exception = ex;
				ExceptionsRegistry.GetInstance().Add(ex);
			}
			return retVal;
		}
		#endregion
		#endregion
	}
}
