﻿using System.Data;

namespace Mlc.Data
{
	/// <summary>
	/// Interface IDataRowWrapper
	/// Implements the <see cref="Mlc.Data.IDataRowWrapper" />
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <seealso cref="Mlc.Data.IDataRowWrapper" />
	public interface IDataRowWrapper<T> :IDataRowWrapper where T : IDataRowWrapper<T>
	{
		/// <summary>
		/// Clones this instance.
		/// </summary>
		T Clone();

		/// <summary>
		/// Clones the specified table.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <returns>T.</returns>
		T Clone(DataTable table);

		/// <summary>
		/// Selects the specified filter.
		/// </summary>
		/// <param name="filter">The filter.</param>
		/// <returns>DataRowWrapper&lt;T&gt;.</returns>
		T Select(string filter);

		/// <summary>
		/// Gets the iterator.
		/// </summary>
		/// <returns>DataWrapperIterator.</returns>
		DataWrapperIterator<T> GetIterator();




	}

	/// <summary>
	/// Interface IDataRowWrapper
	/// </summary>
	public interface IDataRowWrapper
 	{
		/// <summary>
		/// Gets or sets the <see cref="System.Object"/> with the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>System.Object.</returns>
		object this[string key] { get; set; }
		/// <summary>
		/// Gets or sets the data row.
		/// </summary>
		/// <value>The data row.</value>
		DataRow DataRow { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [force readonly].
		/// </summary>
		/// <value><c>true</c> if [force readonly]; otherwise, <c>false</c>.</value>
		bool ForceReadonly { get; set; }
		/// <summary>
		/// Gets or sets the table.
		/// </summary>
		/// <value>The table.</value>
		DataTable Table { get; set; }
		/// <summary>
		/// Gets or sets the name of the table.
		/// </summary>
		/// <value>The name of the table.</value>
		string TableName { get; set; }

	}
}