﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Mlc.Shell;

namespace Mlc.Data
{
	public class TableAlreadyExistsException : Exception
	{
		public TableAlreadyExistsException(DataTable table)
			: base($"La tabella {table.TableName} è già assosciata a questa istanza")
		{ }
	}

	public interface IRows
	{
		DataTable GetTable();
		void RefreshItems();
	}


	public interface IDataMapper
	{
		void SetValues(DataRow dataRow);
		void Refresh();
	}

	public abstract class DataMapper<T> : IDataMapper where T : class, IDataMapper
	{
		private DataTable table = null;
		private DataRow dataRow = null;

		public Rows GetDictionary(DataTable table, string fieldKey)
		{
			Rows rows = null;
			this.table = table;
			if (this.table?.Rows.Count > 0)
			{
				rows = new Rows(this.table);

				if (table.Columns.Contains(fieldKey))
					foreach (DataRow row in table.Rows)
					{

						T r = Activator.CreateInstance(typeof(T), true) as T;
						r.SetValues(row);
						rows.Add(row[fieldKey].ToString(), r);
					}
			}
			return rows;
		}

		public void SetValues(DataRow dataRow)
		{
			this.dataRow = dataRow;
			this.table = this.dataRow?.Table;

			if (this.table == null)
				return;

			DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
			Dictionary<string, PropertyInfo> props = this.GetProperties();
			foreach (DataColumn col in this.table.Columns)
			{
				string colName = col.ColumnName;
				if (props.ContainsKey(colName))
				{
					object value = this.dataRow[colName];
					if (value == null || value is DBNull)
						value = defaultValueType.GetValue(this.table.Columns[colName].DataType);
					props[colName].SetValue(this, value, null);
				}
			}
		}

		/// <summary>
		/// Fills the table.
		/// </summary>
		/// <param name="retTable">The ret table.</param>
		/// <param name="items">The items.</param>
		public static void FillTable(DataTable retTable, List<T> items)
		{
			DefaultValueTypeBuilder defaultValueType = new DefaultValueTypeBuilder();
			Dictionary<string, PropertyInfo> props = null;

			foreach (var item in items)
			{
				if(props==null)
					props = item.GetProperties();

				DataRow drow = retTable.NewRow();

				foreach (DataColumn col in retTable.Columns)
				{
					string colName = col.ColumnName;
					if (props.ContainsKey(colName))
					{
						object value = props[colName].GetValue(item);
						if (value == null || value is DBNull)
							value = defaultValueType.GetValue(retTable.Columns[colName].DataType);

						drow[colName] = value;
					}
				}
				retTable.Rows.Add(drow);
			}
		}

		public virtual T Init(DataRow dataRow)
		{
			this.SetValues(dataRow);
			return this as T;
		}

		public void Add(DataTable table)
		{
			if (this.table == null)
			{
				this.table = table;
				this.dataRow = this.table.NewRow();
				this.table.Rows.Add(this.dataRow);
				this.UpdateData();
			}
			else
				throw new TableAlreadyExistsException(this.table);
		}

		public void ResetMapping()
		{
			this.table = null;
			this.dataRow = null;
		}


		public void UpdateData()
		{
			if (this.table == null || this.dataRow == null)
				return;

			Dictionary<string, PropertyInfo> props = this.GetProperties();
			foreach (DataColumn col in this.table.Columns)
			{
				string colName = col.ColumnName;
				if (props.ContainsKey(colName))
					if (!col.ReadOnly)
						this.dataRow[colName] = props[colName].GetValue(this, null);
			}
		}

		public void Refresh()
		{
			if (this.table == null || this.dataRow == null)
				return;
			this.SetValues(this.dataRow);
		}



		public class Rows : Dictionary<string, T>, IRows
		{
			private DataTable table;

			public Rows(DataTable table)
			{
				this.table = table;
			}

			public DataTable GetTable()
			{
				return this.table;
			}

			public void RefreshItems()
			{
				foreach (IDataMapper iMapper in this.Values)
					iMapper.Refresh();
			}
		}
	}

}
