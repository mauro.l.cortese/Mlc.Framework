﻿namespace Mlc.Rights.Gui
{
	partial class UserLoginControl
	{
		/// <summary> 
		/// Variabile di progettazione necessaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Pulire le risorse in uso.
		/// </summary>
		/// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Codice generato da Progettazione componenti

		/// <summary> 
		/// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
		/// il contenuto del metodo con l'editor di codice.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserLoginControl));
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.checkEditMemoryLogin = new DevExpress.XtraEditors.CheckEdit();
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.radioGroupCredenziali = new DevExpress.XtraEditors.RadioGroup();
			this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButtonLogin = new DevExpress.XtraEditors.SimpleButton();
			this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
			this.textEditDomain = new DevExpress.XtraEditors.TextEdit();
			this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
			this.textEditUserName = new DevExpress.XtraEditors.TextEdit();
			this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.checkEditMemoryLogin.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.radioGroupCredenziali.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditDomain.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.checkEditMemoryLogin);
			this.layoutControl1.Controls.Add(this.labelControl1);
			this.layoutControl1.Controls.Add(this.radioGroupCredenziali);
			this.layoutControl1.Controls.Add(this.simpleButtonCancel);
			this.layoutControl1.Controls.Add(this.simpleButtonLogin);
			this.layoutControl1.Controls.Add(this.textEditPassword);
			this.layoutControl1.Controls.Add(this.textEditDomain);
			this.layoutControl1.Controls.Add(this.pictureEdit1);
			this.layoutControl1.Controls.Add(this.textEditUserName);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.Root;
			this.layoutControl1.Size = new System.Drawing.Size(491, 213);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// checkEditMemoryLogin
			// 
			this.checkEditMemoryLogin.Location = new System.Drawing.Point(140, 141);
			this.checkEditMemoryLogin.Name = "checkEditMemoryLogin";
			this.checkEditMemoryLogin.Properties.Caption = "Memorizza per questo dispositivo";
			this.checkEditMemoryLogin.Size = new System.Drawing.Size(339, 20);
			this.checkEditMemoryLogin.StyleController = this.layoutControl1;
			this.checkEditMemoryLogin.TabIndex = 1;
			this.checkEditMemoryLogin.CheckedChanged += new System.EventHandler(this.checkEditMemoryLogin_CheckedChanged);
			// 
			// labelControl1
			// 
			this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Green;
			this.labelControl1.Appearance.Options.UseForeColor = true;
			this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
			this.labelControl1.Location = new System.Drawing.Point(140, 123);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(339, 14);
			this.labelControl1.StyleController = this.layoutControl1;
			this.labelControl1.TabIndex = 7;
			this.labelControl1.Text = "---";
			// 
			// radioGroupCredenziali
			// 
			this.radioGroupCredenziali.Location = new System.Drawing.Point(233, 84);
			this.radioGroupCredenziali.Name = "radioGroupCredenziali";
			this.radioGroupCredenziali.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.radioGroupCredenziali.Properties.Appearance.Options.UseBackColor = true;
			this.radioGroupCredenziali.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.radioGroupCredenziali.Properties.Columns = 3;
			this.radioGroupCredenziali.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Domain", "Dominio"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Local", "Locale"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Application", "Applicazione")});
			this.radioGroupCredenziali.Size = new System.Drawing.Size(246, 35);
			this.radioGroupCredenziali.StyleController = this.layoutControl1;
			this.radioGroupCredenziali.TabIndex = 3;
			this.radioGroupCredenziali.SelectedIndexChanged += new System.EventHandler(this.radioGroupCredenziali_SelectedIndexChanged);
			// 
			// simpleButtonCancel
			// 
			this.simpleButtonCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCancel.ImageOptions.Image")));
			this.simpleButtonCancel.Location = new System.Drawing.Point(218, 165);
			this.simpleButtonCancel.Name = "simpleButtonCancel";
			this.simpleButtonCancel.Size = new System.Drawing.Size(128, 36);
			this.simpleButtonCancel.StyleController = this.layoutControl1;
			this.simpleButtonCancel.TabIndex = 4;
			this.simpleButtonCancel.Text = "Annulla";
			this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
			// 
			// simpleButtonLogin
			// 
			this.simpleButtonLogin.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLogin.ImageOptions.Image")));
			this.simpleButtonLogin.Location = new System.Drawing.Point(350, 165);
			this.simpleButtonLogin.Name = "simpleButtonLogin";
			this.simpleButtonLogin.Size = new System.Drawing.Size(129, 36);
			this.simpleButtonLogin.StyleController = this.layoutControl1;
			this.simpleButtonLogin.TabIndex = 5;
			this.simpleButtonLogin.Text = "Login";
			this.simpleButtonLogin.Click += new System.EventHandler(this.simpleButtonLogin_Click);
			// 
			// textEditPassword
			// 
			this.textEditPassword.Location = new System.Drawing.Point(233, 60);
			this.textEditPassword.Name = "textEditPassword";
			this.textEditPassword.Properties.UseSystemPasswordChar = true;
			this.textEditPassword.Properties.ValidateOnEnterKey = true;
			this.textEditPassword.Size = new System.Drawing.Size(246, 20);
			this.textEditPassword.StyleController = this.layoutControl1;
			this.textEditPassword.TabIndex = 0;
			this.textEditPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEditPassword_KeyDown);
			// 
			// textEditDomain
			// 
			this.textEditDomain.Location = new System.Drawing.Point(233, 36);
			this.textEditDomain.Name = "textEditDomain";
			this.textEditDomain.Size = new System.Drawing.Size(246, 20);
			this.textEditDomain.StyleController = this.layoutControl1;
			this.textEditDomain.TabIndex = 2;
			this.textEditDomain.Validated += new System.EventHandler(this.textEditDomain_Validated);
			// 
			// pictureEdit1
			// 
			this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
			this.pictureEdit1.Location = new System.Drawing.Point(12, 12);
			this.pictureEdit1.Name = "pictureEdit1";
			this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
			this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
			this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
			this.pictureEdit1.Size = new System.Drawing.Size(124, 189);
			this.pictureEdit1.StyleController = this.layoutControl1;
			this.pictureEdit1.TabIndex = 6;
			// 
			// textEditUserName
			// 
			this.textEditUserName.Location = new System.Drawing.Point(233, 12);
			this.textEditUserName.Name = "textEditUserName";
			this.textEditUserName.Size = new System.Drawing.Size(246, 20);
			this.textEditUserName.StyleController = this.layoutControl1;
			this.textEditUserName.TabIndex = 1;
			// 
			// Root
			// 
			this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.Root.GroupBordersVisible = false;
			this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem7});
			this.Root.Name = "Root";
			this.Root.Size = new System.Drawing.Size(491, 213);
			this.Root.TextVisible = false;
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.AllowHotTrack = false;
			this.emptySpaceItem1.Location = new System.Drawing.Point(128, 153);
			this.emptySpaceItem1.MaxSize = new System.Drawing.Size(78, 0);
			this.emptySpaceItem1.MinSize = new System.Drawing.Size(78, 10);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(78, 40);
			this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.layoutControlItem2.Control = this.textEditUserName;
			this.layoutControlItem2.Location = new System.Drawing.Point(128, 0);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(343, 24);
			this.layoutControlItem2.Text = "Nome utente:";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.pictureEdit1;
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem3.MaxSize = new System.Drawing.Size(128, 0);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(128, 24);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(128, 193);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem3.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.layoutControlItem4.Control = this.textEditDomain;
			this.layoutControlItem4.Location = new System.Drawing.Point(128, 24);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(343, 24);
			this.layoutControlItem4.Text = "Dominio:";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.layoutControlItem5.Control = this.textEditPassword;
			this.layoutControlItem5.Location = new System.Drawing.Point(128, 48);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(343, 24);
			this.layoutControlItem5.Text = "Password:";
			this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.simpleButtonLogin;
			this.layoutControlItem6.Location = new System.Drawing.Point(338, 153);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(133, 40);
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.simpleButtonCancel;
			this.layoutControlItem8.Location = new System.Drawing.Point(206, 153);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(132, 40);
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextVisible = false;
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.radioGroupCredenziali;
			this.layoutControlItem9.Location = new System.Drawing.Point(128, 72);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(343, 39);
			this.layoutControlItem9.Text = "Utilizza credenziali:";
			this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 13);
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.labelControl1;
			this.layoutControlItem1.Location = new System.Drawing.Point(128, 111);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(14, 17);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(343, 18);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.checkEditMemoryLogin;
			this.layoutControlItem7.Location = new System.Drawing.Point(128, 129);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(343, 24);
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextVisible = false;
			// 
			// UserLoginControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.layoutControl1);
			this.Name = "UserLoginControl";
			this.Size = new System.Drawing.Size(491, 213);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.checkEditMemoryLogin.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.radioGroupCredenziali.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditDomain.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraEditors.PictureEdit pictureEdit1;
		private DevExpress.XtraEditors.TextEdit textEditUserName;
		private DevExpress.XtraLayout.LayoutControlGroup Root;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraEditors.SimpleButton simpleButtonLogin;
		private DevExpress.XtraEditors.TextEdit textEditPassword;
		private DevExpress.XtraEditors.TextEdit textEditDomain;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraEditors.RadioGroup radioGroupCredenziali;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraEditors.CheckEdit checkEditMemoryLogin;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
	}
}
