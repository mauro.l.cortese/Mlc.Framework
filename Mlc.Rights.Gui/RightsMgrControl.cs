﻿// ***********************************************************************
// Assembly         : Mlc.Rights.Gui
// Author           : mauro.luigi.cortese
// Created          : 06-26-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-27-2019
// ***********************************************************************
// <copyright file="RightsMgrControl.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars.Navigation;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid;
using Mlc.Shell;

namespace Mlc.Rights.Gui
{
	/// <summary>
	/// Class RightsMgrControl.
	/// </summary>
	/// <seealso cref="System.Windows.Forms.UserControl" />
	/// <seealso cref="Mlc.Rights.IRightsMgrControl" />
	public partial class RightsMgrControl : UserControl, IRightsMgrControl
	{

		private const string _Enable = "Enable";


		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields        
		///// <summary>
		///// The current profile
		///// </summary>
		//private Profile currentProfile = null;

		///// <summary>
		///// The focused application
		///// </summary>
		private App currentApp = null;

		/// <summary>
		/// The allow delete/
		/// </summary>
		bool allowDelete = true;
		#endregion

		#region Constructors        
		/// <summary>
		/// Initializes a new instance of the <see cref="RightsMgrControl" /> class.
		/// </summary>
		public RightsMgrControl()
		{
			InitializeComponent();
			navigationFrame.SelectedPage = navigationPageStart;
			accordionRootUserProfile.Expanded = true;

			#region Disable CANC key for delete currente when user editing value cell 
			EventHandler shownEditor = new EventHandler(this.gridView_ShownEditor);
			this.gridViewApplications.ShownEditor += shownEditor;
			this.gridViewProfiles.ShownEditor += shownEditor;
			this.gridViewRights.ShownEditor += shownEditor;
			this.gridViewAppRights.ShownEditor += shownEditor;

			KeyEventHandler keyDownHandler	= new KeyEventHandler(this.gridControl_ProcessGridKey);
			this.gridControlApplications.ProcessGridKey += keyDownHandler;
			this.gridControlProfiles.ProcessGridKey += keyDownHandler;
			this.gridControlRights.ProcessGridKey += keyDownHandler;
			this.gridControlAppRights.ProcessGridKey += keyDownHandler;
			#endregion
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The io rights
		/// </summary>
		private IORights ioRights = null;
		/// <summary>
		/// Gets or sets the io rights.
		/// </summary>
		/// <value>The io rights.</value>
		public IORights IORights
		{
			get => this.ioRights; set
			{
				this.ioRights = value;
				if (ioRights != null)
					this.ioRights.IRightsMgrControl = this;
			}
		}

		public string NameSpace { get => this.textEditNameSpace.Text; set => this.textEditNameSpace.Text = value; }
		public string ClassName { get => this.textEditClassName.Text; set => this.textEditClassName.Text = value; }
		public string PathClassRight { get => this.textEditPathClassRight.Text; set => this.textEditPathClassRight.Text = value; }
		public string NameSpaceBin { get => this.textEditNameSpaceBin.Text; set => this.textEditNameSpaceBin.Text = value; }
		public string NameRootFolderBin { get => this.textEditNameRootFolderBin.Text; set => this.textEditNameRootFolderBin.Text = value; }
		public bool ShowCodeGenerator { get => accordionRootCodeGenerator.Visible; set => this.accordionRootCodeGenerator.Visible = value; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Refreshes the data.
		/// </summary>
		public void RefreshData()
		{
			//App app = this.focusedApp == null ? this.IORights.App : this.focusedApp;

			string appName = string.Empty;
			if (!string.IsNullOrEmpty(this.IORights.AppName) && this.currentApp == null)
				appName = this.IORights.AppName;
			else if (this.currentApp != null)
				appName = this.IORights.App.Name;

			if (!string.IsNullOrEmpty(appName))
			{
				DataRow[] rows = this.IORights.GetAllApplications().Select($"Name='{appName}'");
				if (rows.Length == 1)
					this.IORights.App = new(rows[0]);

			}

			this.currentApp = this.IORights.App;


			this.lookUpEditProfiles.EditValueChanged -= new System.EventHandler(this.lookUpEditProfiles_EditValueChanged);
			this.textEditApplicationId.Text = this.IORights.App.IdApplication.ToString();
			this.textEditApplicationName.Text = this.IORights.App.Name;
			this.textEditApplicationDesc.Text = this.IORights.App.Description;

			this.loadAppProfiles(this.IORights.App);

			this.lookUpEditProfiles.EditValue = this.IORights.ProfileDefault.IdProfile;// this.IORights.ProfileDefault.IdProfile;

			this.repositoryItemLookUpEditProfiles.DataSource = this.IORights.Profiles;
			this.repositoryItemLookUpEditProfiles.ValueMember = Profile.Properties.IdProfile;
			this.repositoryItemLookUpEditProfiles.DisplayMember = Profile.Properties.Name;

			this.gridColumnProfileName.OptionsColumn.ReadOnly = false;
			this.FillProfiles();

			this.lookUpEditProfiles.EditValueChanged += new System.EventHandler(this.lookUpEditProfiles_EditValueChanged);

			Application.DoEvents();
		}


		/// <summary>
		/// Fills the profiles.
		/// </summary>
		public void FillProfiles()
		{
			//this.accordionControlElementRoot.Elements.Clear();
			Profile profile = new Profile();

			//App app = this.focusedApp == null ? this.IORights.App : this.focusedApp;

			this.IORights.Profiles = this.IORights.GetApplicationProfileRelations(this.IORights.App);
			if (this.IORights.Profiles != null)
			{
				foreach (DataRow row in this.IORights.Profiles.Rows)
				{
					profile.DataRow = row;
					if ((bool)row["Enable"])
						this.addProfile(profile);
					else
						this.removeProfile(profile);
				}
			}
		}

		/// <summary>
		/// Selects the application.
		/// </summary>
		public void SelectDefaultApplication()
		{
			try
			{
				if (this.IORights.App != null)
				{
					int rowHandle = gridViewApplications.LocateByValue(App.Properties.Name, this.IORights.App.Name);
					this.gridViewApplications.FocusedRowHandle = rowHandle;
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Shows the application registry.
		/// </summary>
		public void ShowAppRegistry()
		{
			this.navigationFrame.SelectedPage = navigationPageRegistryMgr;
			this.getApplicationRegistry();
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Adds the profile.
		/// </summary>
		/// <param name="profile">The profile.</param>
		private void addProfile(Profile profile)
		{
			string accordionName = $"accordionControlElement{profile.Name}";

			AccordionControlElement accordionControlElement = this.accordionRootUserProfile.Elements[accordionName];

			if (accordionControlElement == null)
			{
				accordionControlElement = new();

				accordionControlElement.Name = accordionName;
				accordionControlElement.Style = ElementStyle.Item;

				accordionControlElement.Click += accordionControlElementProfile_Click;
				accordionControlElement.Tag = profile.Clone();

				this.accordionRootUserProfile.Elements.Add(accordionControlElement);
			}

			Profile profileCurUser = this.IORights.ProfileCurrentUser;

			if (profile.IdProfile == this.IORights.ProfileDefault.IdProfile)
				accordionControlElement.Text = $"{profile.Name} - (default)";
			else if (profileCurUser != null && profile.IdProfile == profileCurUser.IdProfile)
				accordionControlElement.Text = $"{profile.Name} - (current user)";
			else
				accordionControlElement.Text = profile.Name;

			if (profileCurUser != null && profile.IdProfile == this.IORights.ProfileCurrentUser.IdProfile)
			{
				accordionControlElement.ImageOptions.Image = Properties.Resources.employee_16x16;
				Font font = new Font(accordionControlElement.Appearance.Normal.Font, FontStyle.Bold);
				accordionControlElement.Appearance.Normal.Font = font;
				accordionControlElement.Appearance.Pressed.Font = font;
				accordionControlElement.Appearance.Hovered.Font = font;
			}
			else
				accordionControlElement.ImageOptions.Image = Properties.Resources.employee_dis;
		}

		private void removeProfile(Profile profile)
		{
			string accordionName = $"accordionControlElement{profile.Name}";

			AccordionControlElement accordionControlElement = this.accordionRootUserProfile.Elements[accordionName];

			if (accordionControlElement != null)
				this.accordionRootUserProfile.Elements.Remove(accordionControlElement);
		}

		/// <summary>
		/// Gets the rights.
		/// </summary>
		private void getRightRegistry()
		{
			this.gridControlAppRights.DataSource = this.IORights.GetApplicationRightRelations(this.IORights.App);
			this.repositoryItemLookUpEditRightCategory.DataSource = this.IORights.GetRightCategory();
		}

		/// <summary>
		/// Gets the profile registry.
		/// </summary>
		private void getProfileRegistry()
		{
			DataTable table = this.IORights.GetApplicationProfileRelations(this.IORights.App);
			this.gridControlProfiles.DataSource = table;
		}

		/// <summary>
		/// Gets the application registry.
		/// </summary>
		private void getApplicationRegistry()
		{
			if (this.currentApp == null)
			{
				DataTable table = this.IORights.GetAllApplications();
				this.gridControlApplications.DataSource = table;
				this.SelectDefaultApplication();
			}
		}

		/// <summary>
		/// Gets all user profile.
		/// </summary>
		private void getAllUserProfile()
		{
			DataTable table = this.IORights.GetAllUserProfile();
			table.Columns[AllUserProfile.Properties.IdProfile].ReadOnly = false;
			table.Columns[AllUserProfile.Properties.Enable].ReadOnly = false;
			this.gridControlAllUserProfile.DataSource = table;
		}

		/// <summary>
		/// Gets the data row.
		/// </summary>
		/// <param name="gView">The sender.</param>
		/// <returns>DataRow.</returns>
		private DataRow getDataRow(object gView)
		{
			GridView gridView = (GridView)gView;
			DataRow row = this.getDataRow(gridView, gridView.FocusedRowHandle);
			return row;
		}

		/// <summary>
		/// Loads the application profiles.
		/// </summary>
		/// <param name="app">The application.</param>
		private void loadAppProfiles(App app)
		{
			this.lookUpEditProfiles.Properties.DataSource = this.IORights.GetApplicationProfileLookUp(app);
			this.lookUpEditProfiles.Properties.ValueMember = Profile.Properties.IdProfile;
			this.lookUpEditProfiles.Properties.DisplayMember = Profile.Properties.Name;
			Application.DoEvents();
		}

		/// <summary>
		/// Gets the data row.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ValidateRowEventArgs"/> instance containing the event data.</param>
		/// <returns>DataRow.</returns>
		private DataRow getDataRow(object sender, int rowHandle)
		{
			DataRow row = null;
			if (sender is GridView)
			{
				GridView gw = (GridView)sender;
				row = gw.GetDataRow(rowHandle);
			}
			return row;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers

		#region Application		
		/// <summary>
		/// Handles the ValidateRow event of the gridViewApplications control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
		private void gridViewApplications_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
		{
			DataRow row = this.getDataRow(sender, e.RowHandle);
			this.IORights.UpdateApplication(new App(row));
			this.IORights.Refresh();
			this.allowDelete = true;
		}


		/// <summary>
		/// Handles the FocusedRowChanged event of the gridViewApplications control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
		private void gridViewApplications_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			this.IORights.App = new(this.getDataRow(sender, e.FocusedRowHandle));


			this.getProfileRegistry();
			this.getRightRegistry();
			this.FillProfiles();
			this.RefreshData();
		}
		#endregion

		#region Profile
		private void gridViewProfiles_InitNewRow(object sender, InitNewRowEventArgs e)
		{
			gridViewProfiles.GetDataRow(e.RowHandle)[_Enable] = false;
		}


		/// <summary>
		/// Handles the ValidateRow event of the gridViewProfiles control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
		private void gridViewProfiles_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
		{
			DataRow row = this.getDataRow(sender, e.RowHandle);
			Profile profile = new Profile(row);
			bool enable = (bool)row[_Enable];
			this.IORights.UpdateAppProfile(this.IORights.App, profile, enable);
			this.IORights.Refresh();
			this.allowDelete = true;
		}

		/// <summary>
		/// Handles the FocusedRowChanged event of the gridViewProfiles control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
		private void gridViewProfiles_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			this.IORights.Profile = new Profile(this.getDataRow(sender, e.FocusedRowHandle));
		}

		/// <summary>
		/// Handles the EditValueChanged event of the lookUpEditProfiles control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void lookUpEditProfiles_EditValueChanged(object sender, EventArgs e)
		{
			Profile profile = this.IORights.GetProfile((int)this.lookUpEditProfiles.EditValue);
			this.IORights.SaveData(profile);

		}
		#endregion

		#region Right        
		/// <summary>
		/// Handles the InitNewRow event of the gridViewAppRights control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="InitNewRowEventArgs"/> instance containing the event data.</param>
		private void gridViewAppRights_InitNewRow(object sender, InitNewRowEventArgs e)
		{
			gridViewAppRights.GetDataRow(e.RowHandle)[_Enable] = false;
		}

		/// <summary>
		/// Handles the ValidateRow event of the gridViewAppRights control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
		private void gridViewAppRights_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
		{
			DataRow row = this.getDataRow(sender, e.RowHandle);
			Right right = new Right(row);
			bool enable = (bool)row[_Enable];
			this.IORights.UpdateAppRight(this.IORights.App, right, enable);
			this.IORights.Refresh();
			this.allowDelete = true;
		}

		/// <summary>
		/// Handles the ValidateRow event of the gridView1 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
		private void gridViewRights_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
		{
			DataRow row = this.gridViewRights.GetDataRow(e.RowHandle);
			if (row != null)
			{
				ProfileRights profileRights = new ProfileRights(row);
				this.IORights.SaveProfile(profileRights);
			}
			this.allowDelete = true;

		}


		#endregion

		#region AccordionControl
		/// <summary>
		/// Handles the Click event of the accordionControlElement3 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void accordionControlElement3_Click(object sender, EventArgs e)
		{
			navigationFrame.SelectedPage = navigationPageCodeCreator;
		}

		/// <summary>
		/// Handles the Click event of the accordionControlUserProfiles control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void accordionControlUserProfiles_Click(object sender, EventArgs e)
		{
			this.navigationFrame.SelectedPage = navigationPageUserProfiles;
			this.getAllUserProfile();
		}

		/// <summary>
		/// Handles the Click event of the accordionEditRegistry control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void accordionEditRegistry_Click(object sender, EventArgs e)
		{
			this.ShowAppRegistry();
		}

		/// <summary>
		/// Handles the Click event of the accordionControlElementProfile control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void accordionControlElementProfile_Click(object sender, EventArgs e)
		{
			navigationFrame.SelectedPage = navigationPageProfileRights;
			AccordionControlElement accordionControlElement = (AccordionControlElement)sender;
			this.IORights.Profile = (Profile)accordionControlElement.Tag;
			this.gridControlRights.DataSource = this.IORights.LoadProfileRights(this.ioRights.App, this.IORights.Profile);
			this.labelControlProfile.Text = accordionControlElement.Text;
		}

		#endregion

		#region Common
		/// <summary>
		/// Handles the ShownEditor event of the gridViewProfiles control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void gridView_ShownEditor(object sender, EventArgs e)
		{
			this.allowDelete = false;
		}

		/// <summary>
		/// Handles the ProcessGridKey event of the gridControlApplications control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
		private void gridControl_ProcessGridKey(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete && this.allowDelete)
			{
				GridControl gridControl = (GridControl)sender;
				DataRow row = getDataRow(gridControl.FocusedView);
				if (row != null)
				{
					MappingDataNames mappingDataNames = (MappingDataNames)Enum.Parse(typeof(MappingDataNames), row.Table.TableName);

					switch (mappingDataNames)
					{
						case MappingDataNames.App:
							App app = new(row);
							this.IORights.DeleteApp(app);
							this.getApplicationRegistry();
							break;
						case MappingDataNames.Profile:
							Profile profile = new Profile(row);
							this.IORights.DeleteProfile(profile);
							this.getProfileRegistry();
							break;
						case MappingDataNames.Category:
							break;
						case MappingDataNames.Right:
							Right right = new Right(row);
							this.IORights.DeleteRight(right);
							this.getRightRegistry();
							break;
						case MappingDataNames.ProfileRights:
							break;
						case MappingDataNames.Table:
							break;
						case MappingDataNames.AllUserProfile:
							break;
						case MappingDataNames.UserRightsRow:
							break;
						case MappingDataNames.AppProfiles:
							break;
					}
				}
			}

		}

		#endregion

		/// <summary>
		/// Handles the Click event of the simpleButton2 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void simpleButtonWriteRightClass_Click(object sender, EventArgs e)
		{
			this.IORights.WriteRightClass(this.textEditNameSpace.Text, this.textEditClassName.Text, this.textEditPathClassRight.Text);
		}


		/// <summary>
		/// Handles the Click event of the simpleButton3 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void simpleButtonWriteDbTypes_Click(object sender, EventArgs e)
		{
			this.IORights.WriteDbTypes(this.textEditNameSpaceBin.Text, this.textEditNameRootFolderBin.Text);
		}

		/// <summary>
		/// Handles the ValidateRow event of the gridViewAllUserProfile control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs"/> instance containing the event data.</param>
		private void gridViewAllUserProfile_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
		{
			DataRow row = (this.gridViewAllUserProfile.GetDataRow(e.RowHandle));
			if (row != null)
				this.IORights.UpdateAllUserProfile(new AllUserProfile(row));
			this.getAllUserProfile();
			this.allowDelete = true;
		}


		#endregion

		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion


		//private class Handlerhelper
		//{

		//	/// <summary>
		//	/// Initializes a new instance of the <see cref="Handlerhelper"/> class.
		//	/// </summary>
		//	/// <param name="gridView">The grid view.</param>
		//	/// <param name="ioRights">The io rights.</param>
		//	public Handlerhelper(GridView gridView, IORights ioRights)
		//	{

		//		gridView.InitNewRow += gridView_InitNewRow;
		//		gridView.ValidateRow += gridView_ValidateRow;
		//		gridView.KeyPress += gridView_KeyPress;
		//	}



		//	private void gridView_KeyPress(object sender, KeyPressEventArgs e)
		//	{
		//	}

		//	private void gridView_ValidateRow(object sender, ValidateRowEventArgs e)
		//	{
		//	}

		//	private void gridView_InitNewRow(object sender, InitNewRowEventArgs e)
		//	{
		//	}
		//}
	}
}
