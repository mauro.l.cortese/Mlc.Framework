﻿namespace Mlc.Rights.Gui
{
    partial class RightsMgrControl
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RightsMgrControl));
			this.accordionControl = new DevExpress.XtraBars.Navigation.AccordionControl();
			this.accordionRootUserProfile = new DevExpress.XtraBars.Navigation.AccordionControlElement();
			this.accordionEditRegistry = new DevExpress.XtraBars.Navigation.AccordionControlElement();
			this.accordionControlUserProfiles = new DevExpress.XtraBars.Navigation.AccordionControlElement();
			this.accordionControlSeparator2 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
			this.accordionRootCodeGenerator = new DevExpress.XtraBars.Navigation.AccordionControlElement();
			this.accordionControlCodeGenerator = new DevExpress.XtraBars.Navigation.AccordionControlElement();
			this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
			this.navigationFrame = new DevExpress.XtraBars.Navigation.NavigationFrame();
			this.navigationPageProfileRights = new DevExpress.XtraBars.Navigation.NavigationPage();
			this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
			this.gridControlRights = new DevExpress.XtraGrid.GridControl();
			this.gridViewRights = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.labelControlProfile = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.navigationPageCodeCreator = new DevExpress.XtraBars.Navigation.NavigationPage();
			this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
			this.textEditNameRootFolderBin = new DevExpress.XtraEditors.TextEdit();
			this.textEditNameSpaceBin = new DevExpress.XtraEditors.TextEdit();
			this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
			this.textEditPathClassRight = new DevExpress.XtraEditors.TextEdit();
			this.textEditClassName = new DevExpress.XtraEditors.TextEdit();
			this.textEditNameSpace = new DevExpress.XtraEditors.TextEdit();
			this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.navigationPageStart = new DevExpress.XtraBars.Navigation.NavigationPage();
			this.navigationPageUserProfiles = new DevExpress.XtraBars.Navigation.NavigationPage();
			this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
			this.gridControlAllUserProfile = new DevExpress.XtraGrid.GridControl();
			this.gridViewAllUserProfile = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumnUserFullName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnRightEnable = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnProfileName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.repositoryItemLookUpEditProfiles = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
			this.gridColumnProfileDescription = new DevExpress.XtraGrid.Columns.GridColumn();
			this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
			this.navigationPageRegistryMgr = new DevExpress.XtraBars.Navigation.NavigationPage();
			this.layoutControlRegistryMgr = new DevExpress.XtraLayout.LayoutControl();
			this.gridControlAppRights = new DevExpress.XtraGrid.GridControl();
			this.gridViewAppRights = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnIdCategory = new DevExpress.XtraGrid.Columns.GridColumn();
			this.repositoryItemLookUpEditRightCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
			this.gridControlProfiles = new DevExpress.XtraGrid.GridControl();
			this.gridViewProfiles = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnIdProfile = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnNameProfile = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnDescProfile = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridControlApplications = new DevExpress.XtraGrid.GridControl();
			this.gridViewApplications = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumnIdApp = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnNameApp = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumnDescApp = new DevExpress.XtraGrid.Columns.GridColumn();
			this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
			this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
			this.lookUpEditProfiles = new DevExpress.XtraEditors.LookUpEdit();
			this.textEditApplicationDesc = new DevExpress.XtraEditors.TextEdit();
			this.textEditApplicationName = new DevExpress.XtraEditors.TextEdit();
			this.textEditApplicationId = new DevExpress.XtraEditors.TextEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)(this.accordionControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
			this.layoutControlMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.navigationFrame)).BeginInit();
			this.navigationFrame.SuspendLayout();
			this.navigationPageProfileRights.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControlRights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewRights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			this.navigationPageCodeCreator.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
			this.layoutControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEditNameRootFolderBin.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditNameSpaceBin.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditPathClassRight.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditClassName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditNameSpace.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
			this.navigationPageUserProfiles.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
			this.layoutControl5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControlAllUserProfile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewAllUserProfile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditProfiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
			this.navigationPageRegistryMgr.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlRegistryMgr)).BeginInit();
			this.layoutControlRegistryMgr.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridControlAppRights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewAppRights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditRightCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControlProfiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewProfiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControlApplications)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewApplications)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lookUpEditProfiles.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationDesc.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationId.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			this.SuspendLayout();
			// 
			// accordionControl
			// 
			this.accordionControl.Dock = System.Windows.Forms.DockStyle.Left;
			this.accordionControl.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionRootUserProfile,
            this.accordionRootCodeGenerator});
			this.accordionControl.ExpandElementMode = DevExpress.XtraBars.Navigation.ExpandElementMode.Single;
			this.accordionControl.Location = new System.Drawing.Point(0, 0);
			this.accordionControl.Margin = new System.Windows.Forms.Padding(4);
			this.accordionControl.Name = "accordionControl";
			this.accordionControl.OptionsMinimizing.AllowMinimizeMode = DevExpress.Utils.DefaultBoolean.True;
			this.accordionControl.Size = new System.Drawing.Size(300, 670);
			this.accordionControl.TabIndex = 0;
			this.accordionControl.Text = "accordionControl1";
			// 
			// accordionRootUserProfile
			// 
			this.accordionRootUserProfile.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionEditRegistry,
            this.accordionControlUserProfiles,
            this.accordionControlSeparator2});
			this.accordionRootUserProfile.Expanded = true;
			this.accordionRootUserProfile.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionRootUserProfile.ImageOptions.Image")));
			this.accordionRootUserProfile.Name = "accordionRootUserProfile";
			this.accordionRootUserProfile.Text = "User Profiles";
			// 
			// accordionEditRegistry
			// 
			this.accordionEditRegistry.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionEditRegistry.ImageOptions.Image")));
			this.accordionEditRegistry.Name = "accordionEditRegistry";
			this.accordionEditRegistry.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
			this.accordionEditRegistry.Text = "Gestione anagrafiche";
			this.accordionEditRegistry.Click += new System.EventHandler(this.accordionEditRegistry_Click);
			// 
			// accordionControlUserProfiles
			// 
			this.accordionControlUserProfiles.ImageOptions.Image = global::Mlc.Rights.Gui.Properties.Resources.team_16x16;
			this.accordionControlUserProfiles.Name = "accordionControlUserProfiles";
			this.accordionControlUserProfiles.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
			this.accordionControlUserProfiles.Text = "Gestione utenti/profili";
			this.accordionControlUserProfiles.Click += new System.EventHandler(this.accordionControlUserProfiles_Click);
			// 
			// accordionControlSeparator2
			// 
			this.accordionControlSeparator2.Name = "accordionControlSeparator2";
			// 
			// accordionRootCodeGenerator
			// 
			this.accordionRootCodeGenerator.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlCodeGenerator});
			this.accordionRootCodeGenerator.Expanded = true;
			this.accordionRootCodeGenerator.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionRootCodeGenerator.ImageOptions.Image")));
			this.accordionRootCodeGenerator.Name = "accordionRootCodeGenerator";
			this.accordionRootCodeGenerator.Text = "Code generator";
			// 
			// accordionControlCodeGenerator
			// 
			this.accordionControlCodeGenerator.ImageOptions.Image = global::Mlc.Rights.Gui.Properties.Resources.csharp_16x16;
			this.accordionControlCodeGenerator.Name = "accordionControlCodeGenerator";
			this.accordionControlCodeGenerator.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
			this.accordionControlCodeGenerator.Text = "Genera la classe dei diritti utente";
			this.accordionControlCodeGenerator.Click += new System.EventHandler(this.accordionControlElement3_Click);
			// 
			// layoutControlMain
			// 
			this.layoutControlMain.Controls.Add(this.navigationFrame);
			this.layoutControlMain.Controls.Add(this.lookUpEditProfiles);
			this.layoutControlMain.Controls.Add(this.textEditApplicationDesc);
			this.layoutControlMain.Controls.Add(this.textEditApplicationName);
			this.layoutControlMain.Controls.Add(this.textEditApplicationId);
			this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControlMain.Location = new System.Drawing.Point(300, 0);
			this.layoutControlMain.Margin = new System.Windows.Forms.Padding(4);
			this.layoutControlMain.Name = "layoutControlMain";
			this.layoutControlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(674, 65, 650, 400);
			this.layoutControlMain.Root = this.layoutControlGroup1;
			this.layoutControlMain.Size = new System.Drawing.Size(1028, 670);
			this.layoutControlMain.TabIndex = 6;
			this.layoutControlMain.Text = "layoutControl1";
			// 
			// navigationFrame
			// 
			this.navigationFrame.Controls.Add(this.navigationPageProfileRights);
			this.navigationFrame.Controls.Add(this.navigationPageCodeCreator);
			this.navigationFrame.Controls.Add(this.navigationPageStart);
			this.navigationFrame.Controls.Add(this.navigationPageUserProfiles);
			this.navigationFrame.Controls.Add(this.navigationPageRegistryMgr);
			this.navigationFrame.Location = new System.Drawing.Point(13, 129);
			this.navigationFrame.Margin = new System.Windows.Forms.Padding(4);
			this.navigationFrame.Name = "navigationFrame";
			this.navigationFrame.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPageRegistryMgr,
            this.navigationPageProfileRights,
            this.navigationPageStart,
            this.navigationPageUserProfiles,
            this.navigationPageCodeCreator});
			this.navigationFrame.SelectedPage = this.navigationPageCodeCreator;
			this.navigationFrame.Size = new System.Drawing.Size(1002, 528);
			this.navigationFrame.TabIndex = 10;
			this.navigationFrame.Text = "navigationFrame";
			// 
			// navigationPageProfileRights
			// 
			this.navigationPageProfileRights.Caption = "navigationPageProfileRights";
			this.navigationPageProfileRights.Controls.Add(this.layoutControl2);
			this.navigationPageProfileRights.Margin = new System.Windows.Forms.Padding(4);
			this.navigationPageProfileRights.Name = "navigationPageProfileRights";
			this.navigationPageProfileRights.Size = new System.Drawing.Size(1002, 528);
			// 
			// layoutControl2
			// 
			this.layoutControl2.Controls.Add(this.gridControlRights);
			this.layoutControl2.Controls.Add(this.labelControlProfile);
			this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl2.Location = new System.Drawing.Point(0, 0);
			this.layoutControl2.Margin = new System.Windows.Forms.Padding(4);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(512, 254, 650, 400);
			this.layoutControl2.Root = this.layoutControlGroup5;
			this.layoutControl2.Size = new System.Drawing.Size(1002, 528);
			this.layoutControl2.TabIndex = 11;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// gridControlRights
			// 
			this.gridControlRights.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlRights.Location = new System.Drawing.Point(10, 78);
			this.gridControlRights.MainView = this.gridViewRights;
			this.gridControlRights.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlRights.Name = "gridControlRights";
			this.gridControlRights.Size = new System.Drawing.Size(982, 440);
			this.gridControlRights.TabIndex = 13;
			this.gridControlRights.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRights});
			// 
			// gridViewRights
			// 
			this.gridViewRights.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
			this.gridViewRights.DetailHeight = 431;
			this.gridViewRights.GridControl = this.gridControlRights;
			this.gridViewRights.Name = "gridViewRights";
			this.gridViewRights.OptionsView.ShowGroupPanel = false;
			this.gridViewRights.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewRights_ValidateRow);
			// 
			// gridColumn1
			// 
			this.gridColumn1.Caption = "Name";
			this.gridColumn1.FieldName = "Name";
			this.gridColumn1.MinWidth = 27;
			this.gridColumn1.Name = "gridColumn1";
			this.gridColumn1.OptionsColumn.ReadOnly = true;
			this.gridColumn1.Visible = true;
			this.gridColumn1.VisibleIndex = 0;
			this.gridColumn1.Width = 164;
			// 
			// gridColumn2
			// 
			this.gridColumn2.Caption = "Enable";
			this.gridColumn2.FieldName = "Enable";
			this.gridColumn2.MinWidth = 27;
			this.gridColumn2.Name = "gridColumn2";
			this.gridColumn2.Visible = true;
			this.gridColumn2.VisibleIndex = 1;
			this.gridColumn2.Width = 52;
			// 
			// gridColumn3
			// 
			this.gridColumn3.Caption = "Description";
			this.gridColumn3.FieldName = "Description";
			this.gridColumn3.MinWidth = 27;
			this.gridColumn3.Name = "gridColumn3";
			this.gridColumn3.OptionsColumn.ReadOnly = true;
			this.gridColumn3.Visible = true;
			this.gridColumn3.VisibleIndex = 2;
			this.gridColumn3.Width = 628;
			// 
			// labelControlProfile
			// 
			this.labelControlProfile.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelControlProfile.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.labelControlProfile.Appearance.Options.UseFont = true;
			this.labelControlProfile.Appearance.Options.UseForeColor = true;
			this.labelControlProfile.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControlProfile.Location = new System.Drawing.Point(10, 31);
			this.labelControlProfile.Margin = new System.Windows.Forms.Padding(4);
			this.labelControlProfile.Name = "labelControlProfile";
			this.labelControlProfile.Size = new System.Drawing.Size(982, 43);
			this.labelControlProfile.StyleController = this.layoutControl2;
			this.labelControlProfile.TabIndex = 10;
			this.labelControlProfile.Text = ".";
			// 
			// layoutControlGroup5
			// 
			this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup5.GroupBordersVisible = false;
			this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
			this.layoutControlGroup5.Name = "Root";
			this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup5.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup5.TextVisible = false;
			// 
			// layoutControlGroup4
			// 
			this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem8});
			this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup4.Name = "layoutControlGroup4";
			this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlGroup4.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup4.Text = "Diritti";
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.gridControlRights;
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 47);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(986, 444);
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.labelControlProfile;
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 47);
			this.layoutControlItem8.MinSize = new System.Drawing.Size(24, 47);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(986, 47);
			this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextVisible = false;
			// 
			// navigationPageCodeCreator
			// 
			this.navigationPageCodeCreator.Caption = "navigationPageCodeCreator";
			this.navigationPageCodeCreator.Controls.Add(this.layoutControl3);
			this.navigationPageCodeCreator.Margin = new System.Windows.Forms.Padding(0);
			this.navigationPageCodeCreator.Name = "navigationPageCodeCreator";
			this.navigationPageCodeCreator.Size = new System.Drawing.Size(1002, 528);
			// 
			// layoutControl3
			// 
			this.layoutControl3.Controls.Add(this.textEditNameRootFolderBin);
			this.layoutControl3.Controls.Add(this.textEditNameSpaceBin);
			this.layoutControl3.Controls.Add(this.simpleButton3);
			this.layoutControl3.Controls.Add(this.textEditPathClassRight);
			this.layoutControl3.Controls.Add(this.textEditClassName);
			this.layoutControl3.Controls.Add(this.textEditNameSpace);
			this.layoutControl3.Controls.Add(this.simpleButton2);
			this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl3.Location = new System.Drawing.Point(0, 0);
			this.layoutControl3.Margin = new System.Windows.Forms.Padding(0);
			this.layoutControl3.Name = "layoutControl3";
			this.layoutControl3.Root = this.layoutControlGroup6;
			this.layoutControl3.Size = new System.Drawing.Size(1002, 528);
			this.layoutControl3.TabIndex = 0;
			this.layoutControl3.Text = "layoutControl3";
			// 
			// textEditNameRootFolderBin
			// 
			this.textEditNameRootFolderBin.Location = new System.Drawing.Point(75, 158);
			this.textEditNameRootFolderBin.Margin = new System.Windows.Forms.Padding(4);
			this.textEditNameRootFolderBin.Name = "textEditNameRootFolderBin";
			this.textEditNameRootFolderBin.Size = new System.Drawing.Size(647, 20);
			this.textEditNameRootFolderBin.StyleController = this.layoutControl3;
			this.textEditNameRootFolderBin.TabIndex = 21;
			// 
			// textEditNameSpaceBin
			// 
			this.textEditNameSpaceBin.Location = new System.Drawing.Point(75, 128);
			this.textEditNameSpaceBin.Margin = new System.Windows.Forms.Padding(4);
			this.textEditNameSpaceBin.Name = "textEditNameSpaceBin";
			this.textEditNameSpaceBin.Size = new System.Drawing.Size(241, 20);
			this.textEditNameSpaceBin.StyleController = this.layoutControl3;
			this.textEditNameSpaceBin.TabIndex = 20;
			// 
			// simpleButton3
			// 
			this.simpleButton3.ImageOptions.Image = global::Mlc.Rights.Gui.Properties.Resources.programminglanguage_32x322;
			this.simpleButton3.Location = new System.Drawing.Point(726, 129);
			this.simpleButton3.Margin = new System.Windows.Forms.Padding(4);
			this.simpleButton3.Name = "simpleButton3";
			this.simpleButton3.Size = new System.Drawing.Size(263, 48);
			this.simpleButton3.StyleController = this.layoutControl3;
			this.simpleButton3.TabIndex = 19;
			this.simpleButton3.Text = "Scrive il codice della classe";
			this.simpleButton3.Click += new System.EventHandler(this.simpleButtonWriteDbTypes_Click);
			// 
			// textEditPathClassRight
			// 
			this.textEditPathClassRight.Location = new System.Drawing.Point(75, 63);
			this.textEditPathClassRight.Margin = new System.Windows.Forms.Padding(4);
			this.textEditPathClassRight.Name = "textEditPathClassRight";
			this.textEditPathClassRight.Size = new System.Drawing.Size(647, 20);
			this.textEditPathClassRight.StyleController = this.layoutControl3;
			this.textEditPathClassRight.TabIndex = 18;
			// 
			// textEditClassName
			// 
			this.textEditClassName.Location = new System.Drawing.Point(382, 33);
			this.textEditClassName.Margin = new System.Windows.Forms.Padding(4);
			this.textEditClassName.Name = "textEditClassName";
			this.textEditClassName.Size = new System.Drawing.Size(241, 20);
			this.textEditClassName.StyleController = this.layoutControl3;
			this.textEditClassName.TabIndex = 17;
			// 
			// textEditNameSpace
			// 
			this.textEditNameSpace.Location = new System.Drawing.Point(75, 33);
			this.textEditNameSpace.Margin = new System.Windows.Forms.Padding(4);
			this.textEditNameSpace.Name = "textEditNameSpace";
			this.textEditNameSpace.Size = new System.Drawing.Size(241, 20);
			this.textEditNameSpace.StyleController = this.layoutControl3;
			this.textEditNameSpace.TabIndex = 16;
			// 
			// simpleButton2
			// 
			this.simpleButton2.ImageOptions.Image = global::Mlc.Rights.Gui.Properties.Resources.programminglanguage_32x322;
			this.simpleButton2.Location = new System.Drawing.Point(726, 34);
			this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
			this.simpleButton2.Name = "simpleButton2";
			this.simpleButton2.Size = new System.Drawing.Size(263, 48);
			this.simpleButton2.StyleController = this.layoutControl3;
			this.simpleButton2.TabIndex = 15;
			this.simpleButton2.Text = "Scrive il codice della classe";
			this.simpleButton2.Click += new System.EventHandler(this.simpleButtonWriteRightClass_Click);
			// 
			// layoutControlGroup6
			// 
			this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup6.GroupBordersVisible = false;
			this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.emptySpaceItem3});
			this.layoutControlGroup6.Name = "layoutControlGroup6";
			this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup6.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup6.TextVisible = false;
			// 
			// layoutControlGroup7
			// 
			this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem13,
            this.layoutControlItem10,
            this.emptySpaceItem4});
			this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup7.Name = "layoutControlGroup7";
			this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 7, 7);
			this.layoutControlGroup7.Size = new System.Drawing.Size(1002, 95);
			this.layoutControlGroup7.Text = "Paremetri per la scrittura della classe dei diritti";
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.textEditNameSpace;
			this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem12.MaxSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem12.MinSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(307, 30);
			this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem12.Text = "Name Space";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(59, 13);
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.Control = this.textEditPathClassRight;
			this.layoutControlItem14.Location = new System.Drawing.Point(0, 30);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(713, 24);
			this.layoutControlItem14.Text = "Path";
			this.layoutControlItem14.TextSize = new System.Drawing.Size(59, 13);
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.Control = this.textEditClassName;
			this.layoutControlItem13.Location = new System.Drawing.Point(307, 0);
			this.layoutControlItem13.MaxSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem13.MinSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Size = new System.Drawing.Size(307, 30);
			this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem13.Text = "Class Name";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(59, 13);
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.simpleButton2;
			this.layoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.layoutControlItem10.Location = new System.Drawing.Point(713, 0);
			this.layoutControlItem10.MaxSize = new System.Drawing.Size(267, 52);
			this.layoutControlItem10.MinSize = new System.Drawing.Size(267, 52);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(267, 54);
			this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem10.TextVisible = false;
			this.layoutControlItem10.TrimClientAreaToControl = false;
			// 
			// emptySpaceItem4
			// 
			this.emptySpaceItem4.AllowHotTrack = false;
			this.emptySpaceItem4.Location = new System.Drawing.Point(614, 0);
			this.emptySpaceItem4.Name = "emptySpaceItem4";
			this.emptySpaceItem4.Size = new System.Drawing.Size(99, 30);
			this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlGroup8
			// 
			this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.emptySpaceItem1});
			this.layoutControlGroup8.Location = new System.Drawing.Point(0, 95);
			this.layoutControlGroup8.Name = "layoutControlGroup8";
			this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 7, 7);
			this.layoutControlGroup8.Size = new System.Drawing.Size(1002, 95);
			this.layoutControlGroup8.Text = "Parametri per la scrittura dei tipi mappati ai dati Mlc.Rights";
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.simpleButton3;
			this.layoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.layoutControlItem11.Location = new System.Drawing.Point(713, 0);
			this.layoutControlItem11.MaxSize = new System.Drawing.Size(267, 52);
			this.layoutControlItem11.MinSize = new System.Drawing.Size(267, 52);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(267, 54);
			this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem11.TextVisible = false;
			this.layoutControlItem11.TrimClientAreaToControl = false;
			// 
			// layoutControlItem15
			// 
			this.layoutControlItem15.Control = this.textEditNameSpaceBin;
			this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem15.MaxSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem15.MinSize = new System.Drawing.Size(307, 30);
			this.layoutControlItem15.Name = "layoutControlItem15";
			this.layoutControlItem15.Size = new System.Drawing.Size(307, 30);
			this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem15.Text = "Name Space";
			this.layoutControlItem15.TextSize = new System.Drawing.Size(59, 13);
			// 
			// layoutControlItem16
			// 
			this.layoutControlItem16.Control = this.textEditNameRootFolderBin;
			this.layoutControlItem16.Location = new System.Drawing.Point(0, 30);
			this.layoutControlItem16.Name = "layoutControlItem16";
			this.layoutControlItem16.Size = new System.Drawing.Size(713, 24);
			this.layoutControlItem16.Text = "Root Folder";
			this.layoutControlItem16.TextSize = new System.Drawing.Size(59, 13);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.AllowHotTrack = false;
			this.emptySpaceItem1.Location = new System.Drawing.Point(307, 0);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(406, 30);
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem3
			// 
			this.emptySpaceItem3.AllowHotTrack = false;
			this.emptySpaceItem3.Location = new System.Drawing.Point(0, 190);
			this.emptySpaceItem3.Name = "emptySpaceItem3";
			this.emptySpaceItem3.Size = new System.Drawing.Size(1002, 338);
			this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			// 
			// navigationPageStart
			// 
			this.navigationPageStart.Caption = "navigationPageStart";
			this.navigationPageStart.Margin = new System.Windows.Forms.Padding(4);
			this.navigationPageStart.Name = "navigationPageStart";
			this.navigationPageStart.Size = new System.Drawing.Size(1002, 528);
			// 
			// navigationPageUserProfiles
			// 
			this.navigationPageUserProfiles.Caption = "navigationPageUserProfiles";
			this.navigationPageUserProfiles.Controls.Add(this.layoutControl5);
			this.navigationPageUserProfiles.Margin = new System.Windows.Forms.Padding(4);
			this.navigationPageUserProfiles.Name = "navigationPageUserProfiles";
			this.navigationPageUserProfiles.Size = new System.Drawing.Size(1002, 528);
			// 
			// layoutControl5
			// 
			this.layoutControl5.Controls.Add(this.gridControlAllUserProfile);
			this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl5.Location = new System.Drawing.Point(0, 0);
			this.layoutControl5.Margin = new System.Windows.Forms.Padding(4);
			this.layoutControl5.Name = "layoutControl5";
			this.layoutControl5.Root = this.layoutControlGroup9;
			this.layoutControl5.Size = new System.Drawing.Size(1002, 528);
			this.layoutControl5.TabIndex = 0;
			this.layoutControl5.Text = "layoutControl5";
			// 
			// gridControlAllUserProfile
			// 
			this.gridControlAllUserProfile.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlAllUserProfile.Location = new System.Drawing.Point(10, 31);
			this.gridControlAllUserProfile.MainView = this.gridViewAllUserProfile;
			this.gridControlAllUserProfile.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlAllUserProfile.Name = "gridControlAllUserProfile";
			this.gridControlAllUserProfile.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditProfiles});
			this.gridControlAllUserProfile.Size = new System.Drawing.Size(982, 487);
			this.gridControlAllUserProfile.TabIndex = 4;
			this.gridControlAllUserProfile.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAllUserProfile});
			// 
			// gridViewAllUserProfile
			// 
			this.gridViewAllUserProfile.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnUserFullName,
            this.gridColumnRightEnable,
            this.gridColumnProfileName,
            this.gridColumnProfileDescription});
			this.gridViewAllUserProfile.DetailHeight = 431;
			this.gridViewAllUserProfile.GridControl = this.gridControlAllUserProfile;
			this.gridViewAllUserProfile.Name = "gridViewAllUserProfile";
			this.gridViewAllUserProfile.OptionsSelection.MultiSelect = true;
			this.gridViewAllUserProfile.OptionsView.ShowAutoFilterRow = true;
			this.gridViewAllUserProfile.OptionsView.ShowGroupPanel = false;
			this.gridViewAllUserProfile.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewAllUserProfile_ValidateRow);
			// 
			// gridColumnUserFullName
			// 
			this.gridColumnUserFullName.Caption = "Full Name";
			this.gridColumnUserFullName.FieldName = "FullName";
			this.gridColumnUserFullName.MinWidth = 27;
			this.gridColumnUserFullName.Name = "gridColumnUserFullName";
			this.gridColumnUserFullName.OptionsColumn.ReadOnly = true;
			this.gridColumnUserFullName.Visible = true;
			this.gridColumnUserFullName.VisibleIndex = 0;
			this.gridColumnUserFullName.Width = 223;
			// 
			// gridColumnRightEnable
			// 
			this.gridColumnRightEnable.Caption = "Enable";
			this.gridColumnRightEnable.FieldName = "Enable";
			this.gridColumnRightEnable.MinWidth = 27;
			this.gridColumnRightEnable.Name = "gridColumnRightEnable";
			this.gridColumnRightEnable.Visible = true;
			this.gridColumnRightEnable.VisibleIndex = 1;
			this.gridColumnRightEnable.Width = 112;
			// 
			// gridColumnProfileName
			// 
			this.gridColumnProfileName.Caption = "Profile";
			this.gridColumnProfileName.ColumnEdit = this.repositoryItemLookUpEditProfiles;
			this.gridColumnProfileName.FieldName = "IdProfile";
			this.gridColumnProfileName.MinWidth = 27;
			this.gridColumnProfileName.Name = "gridColumnProfileName";
			this.gridColumnProfileName.Visible = true;
			this.gridColumnProfileName.VisibleIndex = 2;
			this.gridColumnProfileName.Width = 117;
			// 
			// repositoryItemLookUpEditProfiles
			// 
			this.repositoryItemLookUpEditProfiles.AutoHeight = false;
			this.repositoryItemLookUpEditProfiles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryItemLookUpEditProfiles.DisplayMember = "Name";
			this.repositoryItemLookUpEditProfiles.Name = "repositoryItemLookUpEditProfiles";
			this.repositoryItemLookUpEditProfiles.ValueMember = "IdProfile";
			// 
			// gridColumnProfileDescription
			// 
			this.gridColumnProfileDescription.Caption = "Description";
			this.gridColumnProfileDescription.FieldName = "Description";
			this.gridColumnProfileDescription.MinWidth = 27;
			this.gridColumnProfileDescription.Name = "gridColumnProfileDescription";
			this.gridColumnProfileDescription.OptionsColumn.ReadOnly = true;
			this.gridColumnProfileDescription.Visible = true;
			this.gridColumnProfileDescription.VisibleIndex = 3;
			this.gridColumnProfileDescription.Width = 693;
			// 
			// layoutControlGroup9
			// 
			this.layoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup9.GroupBordersVisible = false;
			this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10});
			this.layoutControlGroup9.Name = "layoutControlGroup9";
			this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup9.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup9.TextVisible = false;
			// 
			// layoutControlGroup10
			// 
			this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18});
			this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup10.Name = "layoutControlGroup10";
			this.layoutControlGroup10.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlGroup10.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup10.Text = "Associazione utente/profilo";
			// 
			// layoutControlItem18
			// 
			this.layoutControlItem18.Control = this.gridControlAllUserProfile;
			this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem18.Name = "layoutControlItem18";
			this.layoutControlItem18.Size = new System.Drawing.Size(986, 491);
			this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem18.TextVisible = false;
			// 
			// navigationPageRegistryMgr
			// 
			this.navigationPageRegistryMgr.Controls.Add(this.layoutControlRegistryMgr);
			this.navigationPageRegistryMgr.Margin = new System.Windows.Forms.Padding(4);
			this.navigationPageRegistryMgr.Name = "navigationPageRegistryMgr";
			this.navigationPageRegistryMgr.Size = new System.Drawing.Size(1002, 528);
			// 
			// layoutControlRegistryMgr
			// 
			this.layoutControlRegistryMgr.Controls.Add(this.gridControlAppRights);
			this.layoutControlRegistryMgr.Controls.Add(this.gridControlProfiles);
			this.layoutControlRegistryMgr.Controls.Add(this.gridControlApplications);
			this.layoutControlRegistryMgr.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControlRegistryMgr.Location = new System.Drawing.Point(0, 0);
			this.layoutControlRegistryMgr.Margin = new System.Windows.Forms.Padding(4);
			this.layoutControlRegistryMgr.Name = "layoutControlRegistryMgr";
			this.layoutControlRegistryMgr.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1025, 261, 650, 400);
			this.layoutControlRegistryMgr.Root = this.layoutControlGroup11;
			this.layoutControlRegistryMgr.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlRegistryMgr.TabIndex = 0;
			this.layoutControlRegistryMgr.Text = "layoutControl6";
			// 
			// gridControlAppRights
			// 
			this.gridControlAppRights.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlAppRights.Location = new System.Drawing.Point(520, 30);
			this.gridControlAppRights.MainView = this.gridViewAppRights;
			this.gridControlAppRights.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlAppRights.Name = "gridControlAppRights";
			this.gridControlAppRights.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditRightCategory});
			this.gridControlAppRights.Size = new System.Drawing.Size(473, 489);
			this.gridControlAppRights.TabIndex = 6;
			this.gridControlAppRights.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAppRights});
			// 
			// gridViewAppRights
			// 
			this.gridViewAppRights.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn8,
            this.gridColumn7,
            this.gridColumnIdCategory});
			this.gridViewAppRights.DetailHeight = 431;
			this.gridViewAppRights.GridControl = this.gridControlAppRights;
			this.gridViewAppRights.Name = "gridViewAppRights";
			this.gridViewAppRights.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewAppRights.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewAppRights.OptionsSelection.MultiSelect = true;
			this.gridViewAppRights.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
			this.gridViewAppRights.OptionsView.ShowGroupPanel = false;
			this.gridViewAppRights.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewAppRights_InitNewRow);
			this.gridViewAppRights.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewAppRights_ValidateRow);
			// 
			// gridColumn4
			// 
			this.gridColumn4.Caption = "Id";
			this.gridColumn4.FieldName = "IdRight";
			this.gridColumn4.MinWidth = 27;
			this.gridColumn4.Name = "gridColumn4";
			this.gridColumn4.OptionsColumn.ReadOnly = true;
			this.gridColumn4.Visible = true;
			this.gridColumn4.VisibleIndex = 0;
			this.gridColumn4.Width = 53;
			// 
			// gridColumn5
			// 
			this.gridColumn5.Caption = "Name";
			this.gridColumn5.FieldName = "Name";
			this.gridColumn5.MinWidth = 27;
			this.gridColumn5.Name = "gridColumn5";
			this.gridColumn5.Visible = true;
			this.gridColumn5.VisibleIndex = 1;
			this.gridColumn5.Width = 112;
			// 
			// gridColumn8
			// 
			this.gridColumn8.Caption = "Enable";
			this.gridColumn8.FieldName = "Enable";
			this.gridColumn8.MinWidth = 27;
			this.gridColumn8.Name = "gridColumn8";
			this.gridColumn8.Visible = true;
			this.gridColumn8.VisibleIndex = 2;
			this.gridColumn8.Width = 65;
			// 
			// gridColumn7
			// 
			this.gridColumn7.Caption = "Description";
			this.gridColumn7.FieldName = "Description";
			this.gridColumn7.MinWidth = 27;
			this.gridColumn7.Name = "gridColumn7";
			this.gridColumn7.Visible = true;
			this.gridColumn7.VisibleIndex = 3;
			this.gridColumn7.Width = 313;
			// 
			// gridColumnIdCategory
			// 
			this.gridColumnIdCategory.Caption = "Category";
			this.gridColumnIdCategory.ColumnEdit = this.repositoryItemLookUpEditRightCategory;
			this.gridColumnIdCategory.FieldName = "IdCategory";
			this.gridColumnIdCategory.MinWidth = 27;
			this.gridColumnIdCategory.Name = "gridColumnIdCategory";
			this.gridColumnIdCategory.Visible = true;
			this.gridColumnIdCategory.VisibleIndex = 4;
			this.gridColumnIdCategory.Width = 100;
			// 
			// repositoryItemLookUpEditRightCategory
			// 
			this.repositoryItemLookUpEditRightCategory.AutoHeight = false;
			this.repositoryItemLookUpEditRightCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryItemLookUpEditRightCategory.DisplayMember = "Name";
			this.repositoryItemLookUpEditRightCategory.Name = "repositoryItemLookUpEditRightCategory";
			this.repositoryItemLookUpEditRightCategory.ValueMember = "IdCategory";
			// 
			// gridControlProfiles
			// 
			this.gridControlProfiles.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlProfiles.Location = new System.Drawing.Point(16, 269);
			this.gridControlProfiles.MainView = this.gridViewProfiles;
			this.gridControlProfiles.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlProfiles.Name = "gridControlProfiles";
			this.gridControlProfiles.Size = new System.Drawing.Size(469, 243);
			this.gridControlProfiles.TabIndex = 5;
			this.gridControlProfiles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProfiles});
			// 
			// gridViewProfiles
			// 
			this.gridViewProfiles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumnIdProfile,
            this.gridColumnNameProfile,
            this.gridColumnDescProfile});
			this.gridViewProfiles.DetailHeight = 431;
			this.gridViewProfiles.GridControl = this.gridControlProfiles;
			this.gridViewProfiles.Name = "gridViewProfiles";
			this.gridViewProfiles.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewProfiles.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewProfiles.OptionsSelection.MultiSelect = true;
			this.gridViewProfiles.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
			this.gridViewProfiles.OptionsView.ShowGroupPanel = false;
			this.gridViewProfiles.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewProfiles_InitNewRow);
			this.gridViewProfiles.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewProfiles_FocusedRowChanged);
			this.gridViewProfiles.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewProfiles_ValidateRow);
			// 
			// gridColumn6
			// 
			this.gridColumn6.Caption = "Enable";
			this.gridColumn6.FieldName = "Enable";
			this.gridColumn6.MinWidth = 27;
			this.gridColumn6.Name = "gridColumn6";
			this.gridColumn6.Visible = true;
			this.gridColumn6.VisibleIndex = 2;
			this.gridColumn6.Width = 65;
			// 
			// gridColumnIdProfile
			// 
			this.gridColumnIdProfile.Caption = "Id";
			this.gridColumnIdProfile.FieldName = "IdProfile";
			this.gridColumnIdProfile.MinWidth = 27;
			this.gridColumnIdProfile.Name = "gridColumnIdProfile";
			this.gridColumnIdProfile.OptionsColumn.ReadOnly = true;
			this.gridColumnIdProfile.Visible = true;
			this.gridColumnIdProfile.VisibleIndex = 0;
			this.gridColumnIdProfile.Width = 48;
			// 
			// gridColumnNameProfile
			// 
			this.gridColumnNameProfile.Caption = "Name";
			this.gridColumnNameProfile.FieldName = "Name";
			this.gridColumnNameProfile.MinWidth = 27;
			this.gridColumnNameProfile.Name = "gridColumnNameProfile";
			this.gridColumnNameProfile.Visible = true;
			this.gridColumnNameProfile.VisibleIndex = 1;
			this.gridColumnNameProfile.Width = 112;
			// 
			// gridColumnDescProfile
			// 
			this.gridColumnDescProfile.Caption = "Description";
			this.gridColumnDescProfile.FieldName = "Description";
			this.gridColumnDescProfile.MinWidth = 27;
			this.gridColumnDescProfile.Name = "gridColumnDescProfile";
			this.gridColumnDescProfile.Visible = true;
			this.gridColumnDescProfile.VisibleIndex = 3;
			this.gridColumnDescProfile.Width = 311;
			// 
			// gridControlApplications
			// 
			this.gridControlApplications.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlApplications.Location = new System.Drawing.Point(9, 30);
			this.gridControlApplications.MainView = this.gridViewApplications;
			this.gridControlApplications.Margin = new System.Windows.Forms.Padding(4);
			this.gridControlApplications.Name = "gridControlApplications";
			this.gridControlApplications.Size = new System.Drawing.Size(483, 207);
			this.gridControlApplications.TabIndex = 4;
			this.gridControlApplications.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewApplications});
			// 
			// gridViewApplications
			// 
			this.gridViewApplications.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIdApp,
            this.gridColumnNameApp,
            this.gridColumnDescApp});
			this.gridViewApplications.DetailHeight = 431;
			this.gridViewApplications.GridControl = this.gridControlApplications;
			this.gridViewApplications.Name = "gridViewApplications";
			this.gridViewApplications.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewApplications.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
			this.gridViewApplications.OptionsSelection.MultiSelect = true;
			this.gridViewApplications.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
			this.gridViewApplications.OptionsView.ShowGroupPanel = false;
			this.gridViewApplications.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewApplications_FocusedRowChanged);
			this.gridViewApplications.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewApplications_ValidateRow);
			// 
			// gridColumnIdApp
			// 
			this.gridColumnIdApp.Caption = "Id";
			this.gridColumnIdApp.FieldName = "IdApplication";
			this.gridColumnIdApp.MinWidth = 27;
			this.gridColumnIdApp.Name = "gridColumnIdApp";
			this.gridColumnIdApp.OptionsColumn.ReadOnly = true;
			this.gridColumnIdApp.Visible = true;
			this.gridColumnIdApp.VisibleIndex = 0;
			this.gridColumnIdApp.Width = 63;
			// 
			// gridColumnNameApp
			// 
			this.gridColumnNameApp.Caption = "Name";
			this.gridColumnNameApp.FieldName = "Name";
			this.gridColumnNameApp.MinWidth = 27;
			this.gridColumnNameApp.Name = "gridColumnNameApp";
			this.gridColumnNameApp.Visible = true;
			this.gridColumnNameApp.VisibleIndex = 1;
			this.gridColumnNameApp.Width = 116;
			// 
			// gridColumnDescApp
			// 
			this.gridColumnDescApp.Caption = "Description";
			this.gridColumnDescApp.FieldName = "Description";
			this.gridColumnDescApp.MinWidth = 27;
			this.gridColumnDescApp.Name = "gridColumnDescApp";
			this.gridColumnDescApp.Visible = true;
			this.gridColumnDescApp.VisibleIndex = 2;
			this.gridColumnDescApp.Width = 375;
			// 
			// layoutControlGroup11
			// 
			this.layoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup11.GroupBordersVisible = false;
			this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.layoutControlGroup14,
            this.splitterItem2});
			this.layoutControlGroup11.Name = "Root";
			this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup11.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlGroup11.TextVisible = false;
			// 
			// layoutControlGroup12
			// 
			this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlGroup13});
			this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup12.Name = "layoutControlGroup12";
			this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
			this.layoutControlGroup12.Size = new System.Drawing.Size(501, 528);
			this.layoutControlGroup12.Text = "Applicazioni";
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.gridControlApplications;
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(487, 211);
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextVisible = false;
			// 
			// layoutControlGroup13
			// 
			this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19});
			this.layoutControlGroup13.Location = new System.Drawing.Point(0, 211);
			this.layoutControlGroup13.Name = "layoutControlGroup13";
			this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
			this.layoutControlGroup13.Size = new System.Drawing.Size(487, 282);
			this.layoutControlGroup13.Text = "Applicazione -  Profili";
			// 
			// layoutControlItem19
			// 
			this.layoutControlItem19.Control = this.gridControlProfiles;
			this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem19.Name = "layoutControlItem19";
			this.layoutControlItem19.Size = new System.Drawing.Size(473, 247);
			this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem19.TextVisible = false;
			// 
			// layoutControlGroup14
			// 
			this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
			this.layoutControlGroup14.Location = new System.Drawing.Point(511, 0);
			this.layoutControlGroup14.Name = "layoutControlGroup14";
			this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
			this.layoutControlGroup14.Size = new System.Drawing.Size(491, 528);
			this.layoutControlGroup14.Text = "Applicazione - Diritti";
			// 
			// layoutControlItem20
			// 
			this.layoutControlItem20.Control = this.gridControlAppRights;
			this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem20.Name = "layoutControlItem20";
			this.layoutControlItem20.Size = new System.Drawing.Size(477, 493);
			this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem20.TextVisible = false;
			// 
			// splitterItem2
			// 
			this.splitterItem2.AllowHotTrack = true;
			this.splitterItem2.Location = new System.Drawing.Point(501, 0);
			this.splitterItem2.Name = "splitterItem2";
			this.splitterItem2.Size = new System.Drawing.Size(10, 528);
			// 
			// lookUpEditProfiles
			// 
			this.lookUpEditProfiles.Location = new System.Drawing.Point(830, 67);
			this.lookUpEditProfiles.Margin = new System.Windows.Forms.Padding(4);
			this.lookUpEditProfiles.Name = "lookUpEditProfiles";
			this.lookUpEditProfiles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.lookUpEditProfiles.Size = new System.Drawing.Size(172, 20);
			this.lookUpEditProfiles.StyleController = this.layoutControlMain;
			this.lookUpEditProfiles.TabIndex = 7;
			// 
			// textEditApplicationDesc
			// 
			this.textEditApplicationDesc.Location = new System.Drawing.Point(97, 97);
			this.textEditApplicationDesc.Margin = new System.Windows.Forms.Padding(4);
			this.textEditApplicationDesc.Name = "textEditApplicationDesc";
			this.textEditApplicationDesc.Size = new System.Drawing.Size(905, 20);
			this.textEditApplicationDesc.StyleController = this.layoutControlMain;
			this.textEditApplicationDesc.TabIndex = 6;
			// 
			// textEditApplicationName
			// 
			this.textEditApplicationName.Location = new System.Drawing.Point(256, 67);
			this.textEditApplicationName.Margin = new System.Windows.Forms.Padding(4);
			this.textEditApplicationName.Name = "textEditApplicationName";
			this.textEditApplicationName.Size = new System.Drawing.Size(148, 20);
			this.textEditApplicationName.StyleController = this.layoutControlMain;
			this.textEditApplicationName.TabIndex = 4;
			// 
			// textEditApplicationId
			// 
			this.textEditApplicationId.Location = new System.Drawing.Point(97, 67);
			this.textEditApplicationId.Margin = new System.Windows.Forms.Padding(4);
			this.textEditApplicationId.Name = "textEditApplicationId";
			this.textEditApplicationId.Properties.ReadOnly = true;
			this.textEditApplicationId.Size = new System.Drawing.Size(84, 20);
			this.textEditApplicationId.StyleController = this.layoutControlMain;
			this.textEditApplicationId.TabIndex = 5;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "Root";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlGroup1.Size = new System.Drawing.Size(1028, 670);
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlGroup3
			// 
			this.layoutControlGroup3.CustomizationFormText = "Gestione diritti utente";
			this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlGroup2});
			this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup3.Name = "layoutControlGroup3";
			this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlGroup3.Size = new System.Drawing.Size(1018, 660);
			this.layoutControlGroup3.Text = "Gestione diritti utente";
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.navigationFrame;
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 95);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlItem9.Size = new System.Drawing.Size(1002, 528);
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextVisible = false;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "Applicazione";
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.emptySpaceItem2});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 8, 7, 7);
			this.layoutControlGroup2.Size = new System.Drawing.Size(1002, 95);
			this.layoutControlGroup2.Text = "Applicazione selezionata";
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.textEditApplicationId;
			this.layoutControlItem4.CustomizationFormText = "IdApplication";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem4.MaxSize = new System.Drawing.Size(159, 30);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(159, 30);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(159, 30);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Text = "IdApplication";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(68, 13);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.textEditApplicationName;
			this.layoutControlItem3.CustomizationFormText = "Applicazione";
			this.layoutControlItem3.Location = new System.Drawing.Point(159, 0);
			this.layoutControlItem3.MaxSize = new System.Drawing.Size(223, 30);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(223, 30);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(223, 30);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.Text = "Applicazione";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(68, 13);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.lookUpEditProfiles;
			this.layoutControlItem6.CustomizationFormText = "Default profile";
			this.layoutControlItem6.Location = new System.Drawing.Point(733, 0);
			this.layoutControlItem6.MaxSize = new System.Drawing.Size(247, 30);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(247, 30);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(247, 30);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "Default profile";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(68, 13);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.textEditApplicationDesc;
			this.layoutControlItem5.CustomizationFormText = "Descrizione";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 30);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(980, 24);
			this.layoutControlItem5.Text = "Descrizione";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(68, 13);
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.AllowHotTrack = false;
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(382, 0);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(351, 30);
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// RightsMgrControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.Controls.Add(this.layoutControlMain);
			this.Controls.Add(this.accordionControl);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "RightsMgrControl";
			this.Size = new System.Drawing.Size(1328, 670);
			((System.ComponentModel.ISupportInitialize)(this.accordionControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
			this.layoutControlMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.navigationFrame)).EndInit();
			this.navigationFrame.ResumeLayout(false);
			this.navigationPageProfileRights.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControlRights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewRights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			this.navigationPageCodeCreator.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
			this.layoutControl3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textEditNameRootFolderBin.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditNameSpaceBin.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditPathClassRight.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditClassName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditNameSpace.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
			this.navigationPageUserProfiles.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
			this.layoutControl5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControlAllUserProfile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewAllUserProfile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditProfiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
			this.navigationPageRegistryMgr.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControlRegistryMgr)).EndInit();
			this.layoutControlRegistryMgr.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridControlAppRights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewAppRights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditRightCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControlProfiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewProfiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControlApplications)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViewApplications)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lookUpEditProfiles.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationDesc.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditApplicationId.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionRootUserProfile;
        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEditApplicationName;
        private DevExpress.XtraEditors.TextEdit textEditApplicationId;
        private DevExpress.XtraEditors.TextEdit textEditApplicationDesc;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditProfiles;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.Navigation.NavigationFrame navigationFrame;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPageProfileRights;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControlRights;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRights;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControlProfile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPageCodeCreator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEditNameSpace;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEditPathClassRight;
        private DevExpress.XtraEditors.TextEdit textEditClassName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionRootCodeGenerator;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlCodeGenerator;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPageStart;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.TextEdit textEditNameRootFolderBin;
        private DevExpress.XtraEditors.TextEdit textEditNameSpaceBin;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlUserProfiles;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator2;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPageUserProfiles;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraGrid.GridControl gridControlAllUserProfile;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAllUserProfile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUserFullName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRightEnable;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProfileName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProfileDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditProfiles;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionEditRegistry;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPageRegistryMgr;
        private DevExpress.XtraLayout.LayoutControl layoutControlRegistryMgr;
        private DevExpress.XtraGrid.GridControl gridControlProfiles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProfiles;
        private DevExpress.XtraGrid.GridControl gridControlApplications;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewApplications;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIdProfile;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNameProfile;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescProfile;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIdApp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNameApp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescApp;
        private DevExpress.XtraGrid.GridControl gridControlAppRights;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAppRights;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIdCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditRightCategory;
    }
}
