﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mlc.Rights.Gui
{
	public partial class RightsMgrForm : Form, IRightsMgrControl
	{

		public RightsMgrForm(IORights ioRights)
		{
			InitializeComponent();
			this.IORights = ioRights;
			this.rightsMgrControl.ClassName = "UserRights";
			this.rightsMgrControl.PathClassRight = @"D:\Development\Mlc.Framework\TestRight\UserRights.cs";
			this.rightsMgrControl.NameSpaceBin = "Mlc.Rights";
			this.rightsMgrControl.NameRootFolderBin = @"D:\Development\Mlc.Framework\Mlc.Rights\DataType";

			if (this.IORights != null)
				this.rightsMgrControl.IORights = ioRights;
			else
				throw new Exception("ioRights deve essere correttamente inizializzato");
		}



		public IORights IORights { get; set; }
		public string NameSpace { get => this.rightsMgrControl.NameSpace; set => this.rightsMgrControl.NameSpace = value; }
		public string ClassName { get => this.rightsMgrControl.ClassName; set => this.rightsMgrControl.ClassName = value; }
		public string PathClassRight { get => this.rightsMgrControl.PathClassRight; set => this.rightsMgrControl.PathClassRight = value; }
		public string NameSpaceBin { get => this.rightsMgrControl.NameSpaceBin; set => this.rightsMgrControl.NameSpaceBin = value; }
		public string NameRootFolderBin { get => this.rightsMgrControl.NameRootFolderBin; set => this.rightsMgrControl.NameRootFolderBin = value; }
		public bool ShowCodeGenerator { get => this.rightsMgrControl.ShowCodeGenerator; set => this.rightsMgrControl.ShowCodeGenerator = value; }

		/// <summary>
		/// Refreshes the data.
		/// </summary>
		public void RefreshData()
		{
			this.rightsMgrControl.RefreshData();
		}

		/// <summary>
		/// Handles the Shown event of the rightsMgrForm control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void rightsMgrForm_Shown(object sender, EventArgs e)
		{
			this.rightsMgrControl.ShowAppRegistry();
		}
	}
}