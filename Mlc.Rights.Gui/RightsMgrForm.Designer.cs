﻿namespace Mlc.Rights.Gui
{
    partial class RightsMgrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RightsMgrForm));
			this.rightsMgrControl = new Mlc.Rights.Gui.RightsMgrControl();
			this.SuspendLayout();
			// 
			// rightsMgrControl
			// 
			this.rightsMgrControl.ClassName = "";
			this.rightsMgrControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rightsMgrControl.IORights = null;
			this.rightsMgrControl.Location = new System.Drawing.Point(0, 0);
			this.rightsMgrControl.Margin = new System.Windows.Forms.Padding(4);
			this.rightsMgrControl.Name = "rightsMgrControl";
			this.rightsMgrControl.NameRootFolderBin = "";
			this.rightsMgrControl.NameSpace = "";
			this.rightsMgrControl.NameSpaceBin = "";
			this.rightsMgrControl.PathClassRight = "";
			this.rightsMgrControl.ShowCodeGenerator = true;
			this.rightsMgrControl.Size = new System.Drawing.Size(1075, 422);
			this.rightsMgrControl.TabIndex = 0;
			// 
			// RightsMgrForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(1075, 422);
			this.Controls.Add(this.rightsMgrControl);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "RightsMgrForm";
			this.Text = "Gestione diritti utente";
			this.Shown += new System.EventHandler(this.rightsMgrForm_Shown);
			this.ResumeLayout(false);

        }

        #endregion

        private RightsMgrControl rightsMgrControl;
    }
}