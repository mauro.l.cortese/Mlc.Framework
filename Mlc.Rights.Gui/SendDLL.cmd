IF "%username%"=="mauro.luigi.cortese" GOTO IfStatement
ECHO %username%
GOTO Other
EXIT

:IfStatement
SET Dest=\\FS001.livetech.local\ICT500$\Development\bin
CALL :SUB_COPY

:Other
SET Dest=C:\Development\GitLab-C# Projects\bin
CALL :SUB_COPY

EXIT

:SUB_COPY
ECHO Copia DLL su "%Dest%\mlc"
MKDIR "%Dest%\mlc"
COPY Mlc.*.dll "%Dest%\mlc\"
MKDIR "%Dest%\mlc\en"
COPY en "%Dest%\mlc\en"

ECHO Copia DLL su "%Dest%\devexpress"
MKDIR "%Dest%\devexpress"
COPY DevExpress.*.dll "%Dest%\devexpress\"

COPY lib\SevenZipSharp.dll "%Dest%"
COPY lib\7z(x32).dll "%Dest%"
COPY lib\7z(x64).dll "%Dest%"
COPY EPPlus.dll "%Dest%"