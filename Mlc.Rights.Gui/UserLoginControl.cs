﻿// ***********************************************************************
// Assembly         : Mlc.Rights.Gui
// Author           : mauro.luigi.cortese
// Created          : 05-25-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 05-26-2020
// ***********************************************************************
// <copyright file="UserLoginControl.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraLayout.Utils;

namespace Mlc.Rights.Gui
{
	/// <summary>
	/// Class UserLoginControl.
	/// Implements the <see cref="System.Windows.Forms.UserControl" />
	/// </summary>
	/// <seealso cref="System.Windows.Forms.UserControl" />
	public partial class UserLoginControl : UserControl
	{
		#region Constants
		#endregion

		#region Enumerations
		/// <summary>
		/// Enum CredientialsSourceEnum
		/// </summary>
		public enum CredientialsSourceEnum
		{
			/// <summary>
			/// The domain
			/// </summary>
			Domain = 0,
			/// <summary>
			/// The local
			/// </summary>
			Local = 1,
			/// <summary>
			/// The application
			/// </summary>
			Application = 2,
		}
		#endregion

		#region Fields
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="UserLoginControl"/> class.
		/// </summary>
		public UserLoginControl()
		{
			InitializeComponent();
			this.labelControl1.Text = "";
			this.CredientialsSource = CredientialsSourceEnum.Domain;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the credientials source.
		/// </summary>
		/// <value>The credientials source.</value>
		public CredientialsSourceEnum CredientialsSource
		{
			get
			{
				RadioGroupItem item = this.radioGroupCredenziali.Properties.Items[this.radioGroupCredenziali.SelectedIndex];
				return this.CredientialsSource = (CredientialsSourceEnum)Enum.Parse(typeof(CredientialsSourceEnum), item.Value.ToString());
			}
			set { this.radioGroupCredenziali.SelectedIndex = (int)value; }
		}

		/// <summary>
		/// Gets a value indicating whether [success login].
		/// </summary>
		/// <value><c>true</c> if [success login]; otherwise, <c>false</c>.</value>
		public bool SuccessLogin { get; private set; }

		/// <summary>
		/// Gets or sets the applications users.
		/// </summary>
		/// <value>The applications users.</value>
		public List<string> ApplicationsUsers { get; set; } = new List<string>();
		/// <summary>
		/// Gets the domain.
		/// </summary>
		/// <value>The domain.</value>
		public string Domain { get => this.textEditDomain.Text; }
		/// <summary>
		/// Gets the name of the user.
		/// </summary>
		/// <value>The name of the user.</value>
		public string UserName { get => this.textEditUserName.Text; }
		/// <summary>
		/// Gets or sets a value indicating whether [memory login].
		/// </summary>
		/// <value><c>true</c> if [memory login]; otherwise, <c>false</c>.</value>
		public bool MemLogin { get; set; }

		/// <summary>
		/// The credential
		/// </summary>
		private UserCredential credential = null;
		/// <summary>
		/// Gets or sets the credential.
		/// </summary>
		/// <value>The credential.</value>
		public UserCredential Credential
		{
			get { return credential; }
			set
			{
				this.credential = value;
				this.checkEditMemoryLogin.Checked = this.credential != null && credential.Valid;

				if (this.credential != null && this.credential.Valid)
				{
					this.textEditDomain.Text = this.credential.Domain;
					this.textEditUserName.Text = this.credential.UserName;
					this.textEditPassword.Text = this.credential.Password.Decrypt();
				}



			}
		}

		/// <summary>
		/// The allow chose mode login
		/// </summary>
		private bool allowChoseModeLogin;
		/// <summary>
		/// Gets or sets a value indicating whether [allow chose mode login].
		/// </summary>
		/// <value><c>true</c> if [allow chose mode login]; otherwise, <c>false</c>.</value>
		public bool AllowChoseModeLogin
		{
			get
			{
				return this.allowChoseModeLogin;
			}
			set
			{
				this.allowChoseModeLogin = value;
				this.layoutControlItem9.Visibility = this.allowChoseModeLogin ? LayoutVisibility.Always : LayoutVisibility.Never;


				if (!this.allowChoseModeLogin)
					this.CalculateHeiht = this.Height - this.layoutControlItem9.Height;
				else
					this.CalculateHeiht = this.Height;

				Form form = this.ParentForm;
				if (form != null)
				{
					int height = form.Height - form.ClientSize.Height + this.CalculateHeiht;
					form.MinimumSize = new Size(form.Width, height);
					form.MaximumSize = form.MinimumSize;
					form.Height = height;
				}
			}
		}

		/// <summary>
		/// Gets the calculate heiht.
		/// </summary>
		/// <value>The calculate heiht.</value>
		public int CalculateHeiht { get; private set; }

		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Sets the user list.
		/// </summary>
		/// <param name="users">The users.</param>
		public void SetUserList(string[] users)
		{
			this.setAutoCompleteCollection(this.textEditUserName, users);
			this.textEditUserName.Text = string.Empty;
		}

		/// <summary>
		/// Sets the user list.
		/// </summary>
		/// <param name="domains">The domains.</param>
		public void SetDomainList(string[] domains)
		{
			this.setAutoCompleteCollection(this.textEditDomain, domains);
		}

		/// <summary>
		/// Initializes the specified domain.
		/// </summary>
		/// <param name="domain">The domain.</param>
		/// <param name="username">The username.</param>
		/// <param name="credientialsSource">The credientials source.</param>
		public void Init(string domain, string username, CredientialsSourceEnum credientialsSource)
		{
			this.textEditDomain.Text = domain;
			this.textEditUserName.Text = username;
			this.CredientialsSource = credientialsSource;
			this.textEditPassword.Text = "";
			this.ParentForm.Shown += (s, e) => { this.textEditPassword.Focus(); };
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Sets the automatic complete collection.
		/// </summary>
		/// <param name="textEdit">The text edit.</param>
		/// <param name="values">The values.</param>
		private void setAutoCompleteCollection(TextEdit textEdit, string[] values)
		{
			AutoCompleteStringCollection collection = new();
			collection.AddRange(values);
			textEdit.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
			textEdit.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			textEdit.MaskBox.AutoCompleteCustomSource = collection;
		}


		/// <summary>
		/// Checks the credentials.
		/// </summary>
		private void checkCredentials()
		{

			ContextType contextType = ContextType.Domain;

			switch (this.CredientialsSource)
			{
				case CredientialsSourceEnum.Domain:
					contextType = ContextType.Domain;
					break;
				case CredientialsSourceEnum.Local:
					contextType = ContextType.Machine;
					break;
			}

			bool valid = false;
			try
			{
				using (PrincipalContext pc = new PrincipalContext(contextType, this.textEditDomain.Text))
					valid = pc.ValidateCredentials(this.textEditUserName.Text, this.textEditPassword.Text, ContextOptions.Negotiate);

				if (valid)
				{
					this.SuccessLogin = true;
					this.labelControl1.ForeColor = Color.Green;
					this.labelControl1.Text = "Accesso consentito con le credenziali immesse";
					Application.DoEvents();
					System.Threading.Thread.Sleep(500);

					if (this.MemLogin)
					{
						this.Credential = new UserCredential()
						{
							Domain = this.Domain,
							UserName = this.UserName,
							Password = this.textEditPassword.Text.Encrypt()
						};
					}
					else
						this.Credential = null;

					this.OnLoginCompleted(new EventArgs());
				}
				else
				{
					this.SuccessLogin = false;
					this.labelControl1.ForeColor = Color.DarkRed;
					this.labelControl1.Text = "Accesso NON consentito (verificare utente e password)";
				}
			}
			catch
			{
				switch (this.CredientialsSource)
				{
					case CredientialsSourceEnum.Local:
						this.labelControl1.ForeColor = Color.DarkRed;
						this.labelControl1.Text = "Credenziali non valide nel contesto locale";
						break;
				}
			}
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Handles the SelectedIndexChanged event of the radioGroupCredenziali control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void radioGroupCredenziali_SelectedIndexChanged(object sender, EventArgs e)
		{
			RadioGroup edit = sender as RadioGroup;
			RadioGroupItem item = edit.Properties.Items[edit.SelectedIndex];
			if (item != null)
				this.CredientialsSource = (CredientialsSourceEnum)Enum.Parse(typeof(CredientialsSourceEnum), item.Value.ToString());
		}

		/// <summary>
		/// Handles the KeyDown event of the textEditPassword control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
		private void textEditPassword_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				this.checkCredentials();
		}

		/// <summary>
		/// Handles the Click event of the simpleButtonLogin control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void simpleButtonLogin_Click(object sender, EventArgs e)
		{
			this.checkCredentials();
		}

		/// <summary>
		/// Handles the Click event of the simpleButtonCancel control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void simpleButtonCancel_Click(object sender, EventArgs e)
		{
			this.SuccessLogin = false;

			this.OnLoginCompleted(new EventArgs());
		}

		/// <summary>
		/// Handles the Validated event of the textEditDomain control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void textEditDomain_Validated(object sender, EventArgs e)
		{
			this.OnDomainChanged(new EventArgs());
		}

		/// <summary>
		/// Handles the CheckedChanged event of the checkEditMemoryLogin control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		private void checkEditMemoryLogin_CheckedChanged(object sender, EventArgs e)
		{
			this.MemLogin = (bool)this.checkEditMemoryLogin.Checked;
		}

		#endregion
		#endregion

		#region Event Definitions

		#region Event LoginCompleted                
		/// <summary>
		/// Event CredentialValidate.
		/// </summary>
		public event EventHandler LoginCompleted;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnLoginCompleted(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.LoginCompleted?.Invoke(this, e);
		}
		#endregion

		#region Event DomainChanged                
		/// <summary>
		/// Event DomainChanged.
		/// </summary>
		public event EventHandler DomainChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnDomainChanged(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.DomainChanged?.Invoke(this, e);
		}
		#endregion

		#endregion

		#region Embedded Types
		/// <summary>
		/// Class UserCredential.
		/// </summary>
		public class UserCredential
		{
			/// <summary>
			/// Gets or sets the domain.
			/// </summary>
			/// <value>The domain.</value>
			public string Domain { get; set; }
			/// <summary>
			/// Gets or sets the name of the user.
			/// </summary>
			/// <value>The name of the user.</value>
			public string UserName { get; set; }
			/// <summary>
			/// Gets or sets the password.
			/// </summary>
			/// <value>The password.</value>
			public string Password { get; set; }

			/// <summary>
			/// Gets a value indicating whether this <see cref="UserCredential"/> is valid.
			/// </summary>
			/// <value><c>true</c> if valid; otherwise, <c>false</c>.</value>
			public bool Valid
			{
				get
				{
					return !string.IsNullOrEmpty(this.Domain) &&
						   !string.IsNullOrEmpty(this.UserName) &&
						   !string.IsNullOrEmpty(this.Password);
				}
			}
		}
		#endregion
	}
}
