﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-25-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-27-2019
// ***********************************************************************
// <copyright file="IORights.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Data;

namespace Mlc.Rights
{
    /// <summary>
    /// Interface IORights
    /// </summary>
    public interface IORights
    {
        /// <summary>
        /// Saves the profile.
        /// </summary>
        /// <param name="profileRights">The profile rights.</param>
        void SaveProfile(ProfileRights profileRights);
        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        /// <value>The name of the application.</value>
        string AppName { get; set; }        
        /// <summary>
        /// Gets or sets the application.
        /// </summary>
        /// <value>The application.</value>
        App App { get; set; }
        /// <summary>
        /// Gets or sets the profiles.
        /// </summary>
        /// <value>The profiles.</value>
        DataTable Profiles { get; set; }
        /// <summary>
        /// Gets or sets the identifier profile default.
        /// </summary>
        /// <value>The identifier profile default.</value>
        Profile ProfileDefault { get; }
        /// <summary>
        /// Gets or sets the identifier profile current user.
        /// </summary>
        /// <value>The identifier profile current user.</value>
        Profile ProfileCurrentUser { get; }
        /// <summary>
        /// Saves the data.
        /// </summary>
        void SaveData(Profile defProfile);
        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        void Refresh();
        /// <summary>
        /// Loads the profile rights.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="profile">The profile.</param>
        /// <returns>DataTable.</returns>
        DataTable LoadProfileRights(App app, Profile profile);
        /// <summary>
        /// Gets the user rights.
        /// </summary>
        /// <returns>DataTable.</returns>
        DataTable GetUserRights();
        /// <summary>
        /// Writes the right class.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="pathClass">The path class.</param>
        void WriteRightClass(string nameSpace, string className, string pathClass);
        /// <summary>
        /// Writes the database types.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="rootFolder">The root folder.</param>
        void WriteDbTypes(string nameSpace, string rootFolder);
        /// <summary>
        /// Gets all user profile.
        /// </summary>
        /// <returns>DataTable.</returns>
        DataTable GetAllUserProfile();
        /// <summary>
        /// Gets or sets the rights MGR control.
        /// </summary>
        /// <value>The rights MGR control.</value>
        IRightsMgrControl IRightsMgrControl { get; set; }
        Profile Profile { get; set; }

        /// <summary>
        /// Updates all user profile.
        /// </summary>
        /// <param name="allUserProfile">All user profile.</param>
        void UpdateAllUserProfile(AllUserProfile allUserProfile);
        /// <summary>
        /// Gets the application.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>App.</returns>
        App GetApplication(string name);
        /// <summary>
        /// Gets all applications.
        /// </summary>
        /// <returns>DataTable.</returns>
        DataTable GetAllApplications();
        /// <summary>
        /// Gets the application profile relations.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>DataTable.</returns>
        DataTable GetApplicationProfileRelations(App app);
        /// <summary>
        /// Gets the default profile.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>Profile.</returns>
        Profile GetDefaultProfile(App app);
        /// <summary>
        /// Gets the application right relations.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>DataTable.</returns>
        DataTable GetApplicationRightRelations(App app);
        /// <summary>
        /// Updates the profile.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="profile">The profile.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        void UpdateAppProfile(App app, Profile profile, bool enable);
        /// <summary>
        /// Updates the application right.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="right">The right.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        void UpdateAppRight(App app, Right right, bool enable);
        /// <summary>
        /// Gets the right category.
        /// </summary>
        /// <returns>DataTable.</returns>
        DataTable GetRightCategory();
        /// <summary>
        /// Gets the application profile look up.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>DataTable.</returns>
        DataTable GetApplicationProfileLookUp(App app);
        /// <summary>
        /// Updates the application right.
        /// </summary>
        /// <param name="right">The right.</param>
        void DeleteRight(Right right);
        /// <summary>
        /// Deletes the profile.
        /// </summary>
        /// <param name="profile">The profile.</param>
        void DeleteProfile(Profile profile);
        /// <summary>
        /// Updates the application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="profile">The profile.</param>
        void UpdateApplication(App app, Profile profile= null);
        /// <summary>
        /// Deletes the application.
        /// </summary>
        /// <param name="app">The application.</param>
        void DeleteApp(App app);
        /// <summary>
        /// Gets the profile.
        /// </summary>
        /// <param name="idProfile">The identifier profile.</param>
        /// <returns>Profile.</returns>
        Profile GetProfile(int idProfile);
        ///// <summary>
        ///// Sets the profile default.
        ///// </summary>
        ///// <param name="editValue">The edit value.</param>
        //void SetProfileDefault(int editValue);
    }
}
