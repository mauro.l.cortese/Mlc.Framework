﻿using Mlc.Data;
using Mlc.Rights;
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Mlc.Rights
{

	/// <summary>
	/// Enum MappingDataNames
	/// </summary>
	public enum MappingDataNames
	{
		/// <summary>
		/// The application
		/// </summary>
		App,
		/// <summary>
		/// The profile
		/// </summary>
		Profile,
		/// <summary>
		/// The category
		/// </summary>
		Category,
		/// <summary>
		/// The right
		/// </summary>
		Right,
		/// <summary>
		/// The profile rights
		/// </summary>
		ProfileRights,
		/// <summary>
		/// The table
		/// </summary>
		Table,
		/// <summary>
		/// All user profile
		/// </summary>
		AllUserProfile,
		/// <summary>
		/// The user rights row
		/// </summary>
		UserRightsRow,
		/// <summary>
		/// The application profiles
		/// </summary>
		AppProfiles
	}


	/// <summary>
	/// Class serialize.
	/// </summary>
	/// <seealso cref="Mlc.Rights.IORights" />
	public class IORightsSql : IORights
	{
		#region Constants
		#endregion

		#region Enumerations
		/// <summary>
		/// Enum SqlKey
		/// </summary>
		enum SqlKey
		{
			/// <summary>
			/// The get application
			/// </summary>
			GetApplication,
			/// <summary>
			/// The get profiles
			/// </summary>
			GetProfiles,
			/// <summary>
			/// The get categories
			/// </summary>
			GetCategories,
			/// <summary>
			/// The get rights
			/// </summary>
			GetRights,
			/// <summary>
			/// The get default profile
			/// </summary>
			GetDefaultProfile,
			/// <summary>
			/// The get user profile
			/// </summary>
			GetUserProfile,
			/// <summary>
			/// The get all user profile
			/// </summary>
			GetAllUserProfile,
			/// <summary>
			/// The get profile rights only row
			/// </summary>
			GetAppProfileRightsOnlyRow,
			/// <summary>
			/// The get all user profile only row
			/// </summary>
			GetAllUserProfileOnlyRow,
			/// <summary>
			/// The get profile rights
			/// </summary>
			GetAppProfileRights,
			/// <summary>
			/// The update application data
			/// </summary>
			UpdateApplication,
			/// <summary>
			/// The update profile
			/// </summary>
			UpdateAppProfile,
			/// <summary>
			/// The update application profile right
			/// </summary>
			UpdateAppProfileRight,
			/// <summary>
			/// The get user rights
			/// </summary>
			GetUserRights,
			/// <summary>
			/// The get user rights only row
			/// </summary>
			GetUserRightsOnlyRow,
			/// <summary>
			/// The update all user profile
			/// </summary>
			UpdateAllUserProfile,
			/// <summary>
			/// The get all applications
			/// </summary>
			GetAllApplications,
			/// <summary>
			/// The get all profiles
			/// </summary>
			GetAllProfiles,
			/// <summary>
			/// The get application profile relations
			/// </summary>
			GetApplicationProfileRelations,
			/// <summary>
			/// The get application right relations
			/// </summary>
			GetApplicationRightRelations,
			/// <summary>
			/// The update application right
			/// </summary>
			UpdateAppRight,
			/// <summary>
			/// The delete right
			/// </summary>
			DeleteRight,
			/// <summary>
			/// The delete profile
			/// </summary>
			DeleteProfile,
			/// <summary>
			/// The delete application
			/// </summary>
			DeleteApplication,
			GetApplicationProfileLookUp,
			GetProfile,
		}
		#endregion

		#region Fields
		/// <summary>
		/// The ds
		/// </summary>
		private DataSet dataSet = new DataSet();
		/// <summary>
		/// The SQL connection
		/// </summary>
		private SqlConnection sqlConn = null;
		/// <summary>
		/// The identifier user
		/// </summary>
		private int idUser = 0;
		#endregion

		#region Constructors        
		/// <summary>
		/// Initializes a new instance of the <see cref="IORightsSql" /> class.
		/// </summary>
		/// <param name="userRightsMgrControl">The user rights MGR control.</param>
		/// <param name="appName">Name of the application.</param>
		/// <param name="idUser">The identifier user.</param>
		/// <param name="sqlConnString">The SQL connection string.</param>
		public IORightsSql(IRightsMgrControl userRightsMgrControl, string appName, int idUser, string sqlConnString)
			: this(appName, idUser, sqlConnString)
		{
			this.IRightsMgrControl = userRightsMgrControl;
			this.IRightsMgrControl.IORights = this;
			this.loadData();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IORightsSql" /> class.
		/// </summary>
		/// <param name="appName">Name of the application.</param>
		/// <param name="idUser">The identifier user.</param>
		/// <param name="sqlConnString">The SQL connection string.</param>
		public IORightsSql(string appName, int idUser, string sqlConnString)
		{
			this.AppName = appName;
			this.idUser = idUser;
			this.sqlConn = new SqlConnection(sqlConnString);
			this.SqlCmdEngine.StringConnection = sqlConnString;
			this.SqlCmdEngine.LoadQuery();
			this.App = this.GetApplication(appName);
			this.Profile = this.GetUserProfile(this.App, idUser);
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The SQL data
		/// </summary>
		public SqlCmdEngine SqlCmdEngine { get; } = new SqlCmdEngine();
		/// <summary>
		/// Gets or sets the application.
		/// </summary>
		/// <value>The application.</value>
		public App App { get; set; }
		/// <summary>
		/// Gets or sets the profiles.
		/// </summary>
		/// <value>The profiles.</value>
		public DataTable Profiles { get; set; }
		/// <summary>
		/// Gets or sets the identifier profile default.
		/// </summary>
		/// <value>The identifier profile default.</value>
		public Profile Profile { get; set; }
		/// <summary>
		/// Gets or sets the identifier profile default.
		/// </summary>
		/// <value>The identifier profile default.</value>
		public Profile ProfileDefault { get => this.GetDefaultProfile(this.App); }
		/// <summary>
		/// Gets or sets the identifier profile current user.
		/// </summary>
		/// <value>The identifier profile current user.</value>
		public Profile ProfileCurrentUser { get => this.GetUserProfile(this.App, this.idUser); }
		/// <summary>
		/// Gets or sets the rights MGR control.
		/// </summary>
		/// <value>The rights MGR control.</value>
		public IRightsMgrControl IRightsMgrControl { get; set; }

		/// <summary>
		/// The application name
		/// </summary>
		public string AppName { get; set; }
		#endregion

		#region Method Static
		#endregion

		#region Method        
		/// <summary>
		/// Deletes the application.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <exception cref="NotImplementedException"></exception>
		public void DeleteApp(App app)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.DeleteApplication,
				app.IdApplication
				).ExecuteNonQuery();
		}

		/// <summary>
		/// Deletes the profile.
		/// </summary>
		/// <param name="profile">The profile.</param>
		public void DeleteProfile(Profile profile)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.DeleteProfile,
				profile.IdProfile
				).ExecuteNonQuery();
		}

		/// <summary>
		/// Updates the application right.
		/// </summary>
		/// <param name="right">The right.</param>
		public void DeleteRight(Right right)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.DeleteRight,
				right.IdRight
				).ExecuteNonQuery();
		}

		/// <summary>
		/// Refreshes this instance.
		/// </summary>
		public void Refresh()
		{
			this.loadData();
			this.IRightsMgrControl.RefreshData();

		}

		/// <summary>
		/// Loads the profile rights.
		/// </summary>
		/// <param name="profile">The profile.</param>
		/// <returns>DataTable.</returns>
		public DataTable LoadProfileRights(App app, Profile profile)
		{
			return this.SqlCmdEngine.SetSqlData(SqlKey.GetAppProfileRights, app.IdApplication, profile.IdProfile).ExecuteReader(MappingDataNames.ProfileRights);
		}

		/// <summary>
		/// Saves the profile.
		/// </summary>
		/// <param name="profileRights">The profile rights.</param>
		public void SaveProfile(ProfileRights profileRights)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.UpdateAppProfileRight,
				profileRights.IdApplication,
				profileRights.IdProfile,
				profileRights.IdRight,
				profileRights.Enable ? "1" : "0"
			).ExecuteNonQuery();

		}

		/// <summary>
		/// Gets the user rights.
		/// </summary>
		/// <returns>DataTable.</returns>
		public DataTable GetUserRights()
		{
			string appname = this.App == null ? this.AppName : this.App.Name;
			return this.SqlCmdEngine.SetSqlData(SqlKey.GetUserRights, this.idUser, appname).ExecuteReader(MappingDataNames.UserRightsRow);
		}

		/// <summary>
		/// Gets all user profile.
		/// </summary>
		/// <returns>DataTable.</returns>
		public DataTable GetAllUserProfile()
		{
			return this.SqlCmdEngine.SetSqlData(SqlKey.GetAllUserProfile, AppName).ExecuteReader(MappingDataNames.AllUserProfile);
		}

		/// <summary>
		/// Gets the default profile.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <returns>Profile.</returns>
		public Profile GetDefaultProfile(App app)
		{
			if (app == null) return null;
			return this.getProfile(this.SqlCmdEngine.SetSqlData(SqlKey.GetDefaultProfile, app.IdApplication).ExecuteReader(MappingDataNames.Profile));
		}

		/// <summary>
		/// Gets the default profile.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <returns>Profile.</returns>
		public Profile GetUserProfile(App app, int IdUser)
		{
			if (app == null) return null;
			return this.getProfile(this.SqlCmdEngine.SetSqlData(SqlKey.GetUserProfile, app.IdApplication, idUser).ExecuteReader(MappingDataNames.Profile));
		}

		/// <summary>
		/// Gets the profile.
		/// </summary>
		/// <param name="idProfile">The identifier profile.</param>
		/// <returns>Profile.</returns>
		public Profile GetProfile(int idProfile)
		{
			return this.getProfile(this.SqlCmdEngine.SetSqlData(SqlKey.GetProfile, idProfile).ExecuteReader(MappingDataNames.Profile));
		}

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>App.</returns>
		public App GetApplication(string name)
		{
			DataRow[] rows = this.GetAllApplications().Select($"Name='{name}'");
			if (rows.Length == 1)
				return new(rows[0]);

			return null;
		}

		/// <summary>
		/// Gets all applications.
		/// </summary>
		/// <returns>DataTable.</returns>
		public DataTable GetAllApplications()
		{
			return this.SqlCmdEngine.SetSqlData(SqlKey.GetAllApplications).ExecuteReader(MappingDataNames.App);
		}

		/// <summary>
		/// Gets the application profile relations.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <returns>DataTable.</returns>
		public DataTable GetApplicationProfileRelations(App app)
		{
			if (app == null) return null;
			DataTable table = this.SqlCmdEngine.SetSqlData(SqlKey.GetApplicationProfileRelations, app.IdApplication).ExecuteReader(MappingDataNames.Profile);
			table.Columns[Profile.Properties.IdProfile].AllowDBNull = true;
			return table;
		}

		/// <summary>
		/// Gets the application profile look up.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <returns>DataTable.</returns>
		/// <exception cref="NotImplementedException"></exception>
		public DataTable GetApplicationProfileLookUp(App app)
		{
			if (app == null) return null;
			return this.SqlCmdEngine.SetSqlData(SqlKey.GetApplicationProfileLookUp, app.IdApplication).ExecuteReader(MappingDataNames.Profile);
		}

		public DataTable GetApplicationRightRelations(App app)
		{
			DataTable table = this.SqlCmdEngine.SetSqlData(SqlKey.GetApplicationRightRelations, app.IdApplication).ExecuteReader(MappingDataNames.Right);
			table.Columns[Right.Properties.IdRight].AllowDBNull = true;
			return table;
		}

		/// <summary>
		/// Gets the right category.
		/// </summary>
		/// <returns>DataTable.</returns>
		public DataTable GetRightCategory()
		{
			DataTable table = this.SqlCmdEngine.SetSqlData(SqlKey.GetCategories).ExecuteReader(MappingDataNames.Category);
			return table;
		}

		/// <summary>
		/// Updates all user profile.
		/// </summary>
		/// <param name="allUserProfile">All user profile.</param>
		/// <exception cref="NotImplementedException"></exception>
		public void UpdateAllUserProfile(AllUserProfile allUserProfile)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.UpdateAllUserProfile,
				this.App.IdApplication,
				allUserProfile.IdProfile,
				allUserProfile.IdUser,
				allUserProfile.Enable ? "1" : "0"
			).ExecuteNonQuery();
		}

		/// <summary>
		/// Updates the application.
		/// </summary>
		/// <param name="app">The application.</param>
		public void UpdateApplication(App app, Profile defProfile)
		{
			if (defProfile == null)
				defProfile = this.ProfileDefault;


			this.SqlCmdEngine.SetSqlData(SqlKey.UpdateApplication,
				app.IdApplication,
				app.Name,
				app.Description,
				defProfile.IdProfile
				).ExecuteNonQuery();
			this.Refresh();
		}

		/// <summary>
		/// Saves the data.
		/// </summary>
		public void SaveData(Profile defProfile)
		{
			this.UpdateApplication(this.App, defProfile);
		}

		///// <summary>
		///// Sets the profile default.
		///// </summary>
		///// <param name="idProfile">The identifier profile.</param>
		//public void SetProfileDefault(int idProfile)
		//{
		//    this.getProfile(idProfile);
		//}


		/// <summary>
		/// Updates the profile.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <param name="profile">The profile.</param>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		public void UpdateAppProfile(App app, Profile profile, bool enable)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.UpdateAppProfile,
				app.IdApplication,
				profile.IdProfile,
				profile.Name,
				profile.Description,
				enable ? "1" : "0"
			).ExecuteNonQuery();
		}


		/// <summary>
		/// Updates the application right.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <param name="right">The right.</param>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		public void UpdateAppRight(App app, Right right, bool enable)
		{
			this.SqlCmdEngine.SetSqlData(SqlKey.UpdateAppRight,
				app.IdApplication,
				right.IdRight,
				right.Name,
				right.Description,
				right.IdCategory,
				enable ? "1" : "0"
				).ExecuteNonQuery();
		}

		/// <summary>
		/// Writes the right class.
		/// </summary>
		/// <param name="nameSpace">The name space.</param>
		/// <param name="className">Name of the class.</param>
		/// <param name="pathClass">The path class.</param>
		public void WriteRightClass(string nameSpace, string className, string pathClass)
		{
			UserRightsMgr userRightsMgr = new UserRightsMgr(GetUserRights());
			userRightsMgr.WriteCodeClass(nameSpace, className, pathClass);
		}

		/// <summary>
		/// Writes the database types.
		/// </summary>
		/// <param name="nameSpace">The name space.</param>
		/// <param name="rootFolder">The root folder.</param>
		public void WriteDbTypes(string nameSpace, string rootFolder)
		{
			const string _interfaces = "Interfaces";
			const string _class = "Class";
			const string _enums = "Enums";

			TypeCodeCreator tcc = new TypeCodeCreator();
			tcc.TableEnum = typeof(MappingDataNames);
			tcc.FolderCode = $"{rootFolder}\\{_class}";
			tcc.FolderCodeInterface = $"{rootFolder}\\{_interfaces}";
			tcc.FolderCodeEnums = $"{rootFolder}\\{_enums}";
			tcc.NameSpace = nameSpace;

			foreach (MappingDataNames dataName in (MappingDataNames[])Enum.GetValues(typeof(MappingDataNames)))
			{
				if (dataName == MappingDataNames.Table) continue;

				// Application
				try { tcc.WriteCodeTypeClass(dataSet.Tables[dataName.ToString()], dataName); }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the profile.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <returns>Profile.</returns>
		private Profile getProfile(DataTable table)
		{
			if (table.Rows.Count == 1)
				return new Profile(table.Rows[0]);
			else
				return null;
		}


		/// <summary>
		/// Loads the data.
		/// </summary>
		private void loadData()
		{

			try
			{
				dataSet = new DataSet();
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetApplication, AppName).ExecuteReader(MappingDataNames.App));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetProfiles).ExecuteReader(MappingDataNames.Profile));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetCategories).ExecuteReader(MappingDataNames.Category));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetRights).ExecuteReader(MappingDataNames.Right));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetAppProfileRightsOnlyRow).ExecuteReader(MappingDataNames.ProfileRights));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetAllUserProfileOnlyRow, AppName).ExecuteReader(MappingDataNames.AllUserProfile));
				dataSet.Tables.Add(this.SqlCmdEngine.SetSqlData(SqlKey.GetUserRightsOnlyRow, AppName).ExecuteReader(MappingDataNames.UserRightsRow));

				this.App = new(dataSet.Tables[MappingDataNames.App.ToString()].Rows[0]);
				this.Profiles = dataSet.Tables[MappingDataNames.Profile.ToString()];


				//int idProfileDefault = (int)this.SqlCmdEngine.SetSqlData(SqlKey.GetDefaultProfile, appName).ExecuteScalar();
				//int idProfileCurrentUser = (int)this.SqlCmdEngine.SetSqlData(SqlKey.GetUserProfile, this.App.IdApplication, this.idUser).ExecuteScalar();

				//this.ProfileDefault = this.getProfile(idProfileDefault);
				//this.ProfileCurrentUser = this.GetUserProfile(this.App, this.idUser);
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}



		///// <summary>
		///// Gets the profile.
		///// </summary>
		///// <param name="idProfile">The identifier profile current user.</param>
		///// <returns>Profile.</returns>
		//private Profile getProfile(int idProfile)
		//{
		//    Profile profile = new Profile();
		//    DataRow[] rows = this.Profiles.Select($"{Profile.Properties.IdProfile}={idProfile}");
		//    if (rows.Length == 1)
		//        profile.DataRow = rows[0];
		//    return profile;
		//}
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}
}
