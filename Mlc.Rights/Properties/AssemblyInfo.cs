﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Le informazioni generali relative a un assembly sono controllate dal seguente 
// set di attributi. Modificare i valori di questi attributi per modificare le informazioni
// associate a un assembly.
[assembly: AssemblyTitle("Mlc.Rights")]
[assembly: AssemblyDescription("Personal framework for developing Windows Desktop applications")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MLC")]
[assembly: AssemblyProduct("Mlc.Rights")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("MLC")]
[assembly: AssemblyCulture("")]

// Se si imposta ComVisible su false, i tipi in questo assembly non saranno visibili
// ai componenti COM. Se è necessario accedere a un tipo in questo assembly da
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID seguente verrà utilizzato come ID della libreria dei tipi
[assembly: Guid("a6bd7bf6-c477-4d5f-a8f2-182c6f5cfd6b")]

// Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
//
//      Versione principale
//      Versione secondaria
//      Numero di build
//      Revisione
//
// È possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build
// usando l'asterisco '*' come illustrato di seguito:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("22.4.1.0")]
[assembly: AssemblyFileVersion("22.4.1.0")]