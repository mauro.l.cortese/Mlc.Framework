﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-23-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-24-2019
// ***********************************************************************
// <copyright file="UserRightsMgr.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;

namespace Mlc.Rights
{
    /// <summary>
    /// Class UserRightsMgr.
    /// </summary>
    public class UserRightsMgr
    {
        /// <summary>
        /// The table rights
        /// </summary>
        DataTable tableRights = new();
        /// <summary>
        /// The user rights
        /// </summary>
        private IUserRight userRights = null;
        /// <summary>
        /// The right keys
        /// </summary>
        List<string> rightKeys = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightsMgr"/> class.
        /// </summary>
        public UserRightsMgr(DataTable table)
        {
            rightKeys.Clear();
            this.tableRights = table;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightsMgr"/> class.
        /// </summary>
        public UserRightsMgr()
        {
            rightKeys.Clear();
            tableRights.Columns.Add(new DataColumn("Category", typeof(string)));
            tableRights.Columns.Add(new DataColumn("IdRight", typeof(int)));
            tableRights.Columns.Add(new DataColumn("Right", typeof(string)));
            tableRights.Columns.Add(new DataColumn("Enable", typeof(bool)));

        }

        internal void Initialize(DataTable dataTable)
        {
            this.SetAllRights(false);

            DataRow[] rows = dataTable.Select("Enable=1");
            foreach (DataRow row in rows)
            {
                string key = $"{row["Category"].ToString()}-{row["Right"].ToString()}";
                if (this.valuePairs.ContainsKey(key))
                {
                    ObjRight objRight = this.valuePairs[key];
                    objRight.PropertyInfo.SetValue(objRight.Object, true);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRightsMgr"/> class.
        /// </summary>
        /// <param name="userRights">The user rights.</param>
        public UserRightsMgr(IUserRight userRights)
        {
            this.userRights = userRights;
        }


        private class ObjRight
        {
            public object Object { get; set; }
            public PropertyInfo PropertyInfo { get; set; }
        }


        Dictionary<string, ObjRight> valuePairs = new Dictionary<string, ObjRight>();


        /// <summary>
        /// Sets all rights.
        /// </summary>
        /// <param name="rightValue">if set to <c>true</c> [right value].</param>
        public void SetAllRights(bool rightValue)
        {
            valuePairs.Clear();
            if (this.userRights == null) return;
            List<PropertyInfo> catProperties = getProperties(this.userRights, typeof(CategoryAttribute));

            foreach (PropertyInfo propertyCat in catProperties)
            {
                object category = propertyCat.GetValue(this.userRights);
                List<PropertyInfo> catRights = getProperties(category, typeof(RightAttribute));

                foreach (PropertyInfo propertyRight in catRights)
                {
                    propertyRight.SetValue(category, rightValue);

                    string key = $"{propertyCat.Name}-{propertyRight.Name}";
                    valuePairs.Add(key, new ObjRight() { Object = category, PropertyInfo = propertyRight });
                }
            }
        }

        /// <summary>
        /// Writes the code class.
        /// </summary>
        /// <param name="nameSpace">The name space.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="path">The path.</param>
        public void WriteCodeClass(string nameSpace, string className, string path)
        {
            //object o = this.tableRights;

            StringBuilder code = new StringBuilder(CodeMasterResource.UserRights);

            code.Replace("{NameSpace}", nameSpace);
            code.Replace("{ClassName}", className);

            // set categories
            List<string> categories = this.getCategoriesList();

            code.Replace("{Categories}", this.getCategoryProperty(categories));

            code.Replace("{ClassCategories}", this.getCategoryClass(categories));

            File.WriteAllText(path, code.ToString());
        }

        /// <summary>
        /// Gets the category class.
        /// </summary>
        /// <param name="categories">The categories.</param>
        /// <returns>System.String.</returns>
        private string getCategoryClass(List<string> categories)
        {
            StringBuilder categoriesCode = new();
            foreach (string item in categories)
            {

                string categoryClassCode = CodeMasterResource.UserRights_ClassCategory.Replace("{CategoryName}", item);
                Dictionary<string, int> rights = this.getRights(item);

                StringBuilder rightCode = new();
                foreach (KeyValuePair<string, int> right in rights)
                {
                    rightCode.AppendLine(
                        CodeMasterResource.UserRights_Right.Replace("{Right}", right.Key).Replace("{IdRight}", right.Value.ToString())
                        );
                }

                categoriesCode.AppendLine(categoryClassCode.Replace("{Rights}", rightCode.ToString()));
            }
            return categoriesCode.ToString();
        }

        /// <summary>
        /// Gets the rights.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        private Dictionary<string, int> getRights(string category)
        {
            Dictionary<string, int> rights = new Dictionary<string, int>();
            DataRow[] rows = this.tableRights.Select($"Category='{category}'");
            foreach (DataRow row in rows)
                if (!rights.ContainsKey(row["Right"].ToString()))
                    rights.Add(row["Right"].ToString(), (int)row["IdRight"]);

            return rights;
        }

        /// <summary>
        /// Gets the category property.
        /// </summary>
        /// <param name="categories">The categories.</param>
        /// <returns>System.String.</returns>
        private string getCategoryProperty(List<string> categories)
        {
            StringBuilder categoriesCode = new();
            foreach (string item in categories)
                categoriesCode.AppendLine(CodeMasterResource.UserRights_Category.Replace("{CategoryName}", item));
            return categoriesCode.ToString();
        }

        /// <summary>
        /// Gets the categories list.
        /// </summary>
        /// <returns>List&lt;System.String&gt;.</returns>
        private List<string> getCategoriesList()
        {
            List<string> categories = new List<string>();
            foreach (DataRow row in this.tableRights.Rows)
                if (!categories.Contains(row["Category"].ToString()))
                    categories.Add(row["Category"].ToString());
            return categories;
        }




        /// <summary>
        /// Adds the right.
        /// </summary>
        /// <param name="rightRow">The right row.</param>
        public void AddRight(RightRow rightRow)
        {
            string key = rightRow.ToString();
            if (!this.rightKeys.Contains(key))
            {
                this.rightKeys.Add(key);
                DataRow row = this.tableRights.NewRow();
                row["Category"] = rightRow.Category;
                row["IdRight"] = rightRow.IdRight;
                row["Right"] = rightRow.Right;
                tableRights.Rows.Add(row);
            }
        }

        /// <summary>
        /// Adds the right.
        /// </summary>
        /// <param name="rightRow">The right row.</param>
        public void AddRight(List<RightRow> rightRow)
        {
            foreach (RightRow item in rightRow)
                this.AddRight(item);
        }

        /// <summary>
        /// Class RightRow.
        /// </summary>
        public class RightRow
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="RightRow" /> class.
            /// </summary>
            /// <param name="category">The category.</param>
            /// <param name="idRight">The identifier right.</param>
            /// <param name="right">The right.</param>
            public RightRow(string category, int idRight, string right)
            {
                this.Category = category;
                this.Right = right;
                this.IdRight = idRight;
            }

            /// <summary>
            /// Gets or sets the category.
            /// </summary>
            /// <value>The category.</value>
            public string Category { get; set; }
            /// <summary>
            /// Gets or sets the right.
            /// </summary>
            /// <value>The right.</value>
            public string Right { get; set; }

            /// <summary>
            /// Gets or sets the right.
            /// </summary>
            /// <value>The right.</value>
            public int IdRight { get; set; }

            /// <summary>
            /// Returns a <see cref="System.String" /> that represents this instance.
            /// </summary>
            /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
            public override string ToString()
            {
                return $"{this.Category}-{this.Right}";
            }

        }


        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="attr">The attribute.</param>
        /// <returns>List&lt;PropertyInfo&gt;.</returns>
        private List<PropertyInfo> getProperties(object obj, Type attr)
        {
            List<PropertyInfo> retVal = new List<PropertyInfo>();
            PropertyInfo[] catProperties = obj.GetType().GetProperties();

            foreach (PropertyInfo propertyInfo in catProperties)
            {
                object[] attribCats = propertyInfo.GetCustomAttributes(attr, true);
                if (attribCats.Length == 1)
                    retVal.Add(propertyInfo);
            }
            return retVal;
        }
    }
}
