﻿using System;
using Mlc.Rights;


namespace {NameSpace}
{

    /// <summary>
    /// Class {ClassName}.
    /// </summary>
    /// <seealso cref="Mlc.Rights.UserRightsBase" />
    public class {ClassName} : UserRightsBase
    {
        #region Fields
        /// <summary>
        /// The synchronize root
        /// </summary>
        private static object syncRoot = new Object();
        /// <summary>
        /// The instance
        /// </summary>
        private static volatile {ClassName} instance = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="{ClassName}" /> class from being created.
        /// </summary>
        /// <param name="ioRights">The io rights.</param>
        private {ClassName}(IORights ioRights)
        {
            base.IORights = ioRights;
        }
        #endregion

        #region Properties
{Categories}        #endregion

        #region Method Static
        /// <summary>
        /// Gets always the same instance.
        /// </summary>
        /// <returns>UserRights.</returns>
        public static UserRights GetInstance()
        {
            if (instance != null)
                return instance;
            else throw new Exception("Instance NOT INITIALIZED");
        }		
        /// <summary>
        /// Gets always the same instance.
        /// </summary>
        /// <param name="ioRights">The io rights.</param>
        /// <returns>UserRights.</returns>
        public static {ClassName} GetInstance(IORights ioRights)
        {
            if (instance == null)
                lock (syncRoot)
                    if (instance == null)
                        instance = new {ClassName}(ioRights);
            return instance;
        }
        #endregion

        #region Embedded Types
{ClassCategories}        #endregion
    }
}
