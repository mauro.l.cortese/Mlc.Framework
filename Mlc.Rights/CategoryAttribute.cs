﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-24-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-24-2019
// ***********************************************************************
// <copyright file="CategoryAttribute.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Rights
{
    /// <summary>
    /// Class CategoryAttribute.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class CategoryAttribute : Attribute { }
}
