﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-24-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-25-2019
// ***********************************************************************
// <copyright file="UserRightsBase.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;

namespace Mlc.Rights
{

    /// <summary>
    /// Class UserRightsBase.
    /// </summary>
    /// <seealso cref="Mlc.Rights.IUserRight" />
    public class UserRightsBase : IUserRight
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="UserRights"/> class from being created.
        /// </summary>
        protected UserRightsBase()
        { }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            new UserRightsMgr(this).SetAllRights(false);
        }

        /// <summary>
        /// Initializes the specified data table.
        /// </summary>
        public void Initialize()
        {
            if (this.IORights != null)
                this.Initialize(this.IORights.GetUserRights());
        }

        /// <summary>
        /// Gets the io rights.
        /// </summary>
        /// <value>The io rights.</value>
        public IORights IORights { get; set; }

        /// <summary>
        /// Initializes the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        public void Initialize(DataTable dataTable)
        {
            new UserRightsMgr(this).Initialize(dataTable);
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="attr">The attribute.</param>
        /// <returns>List&lt;PropertyInfo&gt;.</returns>
        private List<PropertyInfo> getProperties(object obj, Type attr)
        {
            List<PropertyInfo> retVal = new List<PropertyInfo>();
            PropertyInfo[] catProperties = obj.GetType().GetProperties();

            foreach (PropertyInfo propertyInfo in catProperties)
            {
                object[] attribCats = propertyInfo.GetCustomAttributes(attr, true);
                if (attribCats.Length == 1)
                    retVal.Add(propertyInfo);
            }
            return retVal;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            PropertyInfo[] catProperties = this.GetType().GetProperties();
            StringBuilder sb = new();
            sb.AppendLine("Rights result:");
            foreach (PropertyInfo propertyInfo in catProperties)
                if (propertyInfo.PropertyType.Name.EndsWith("Category"))
                {
                    sb.AppendLine($"[{propertyInfo.Name.ToUpper()}]");
                    object Catval = propertyInfo.GetValue(this);
                    PropertyInfo[] rightsProp = Catval.GetType().GetProperties();
                    foreach (PropertyInfo rightProp in rightsProp)
                        try
                        {
                            if ((bool)rightProp.GetValue(Catval))
                                sb.AppendLine($"  ● {rightProp.Name}");
                            else
                                sb.AppendLine($"  ○ {rightProp.Name}");
                        }
                        catch { sb.AppendLine($"  ! {rightProp.Name}"); }
                }
            sb.AppendLine("------------------");
            return sb.ToString();
        }

    }
}
