﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-24-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-24-2019
// ***********************************************************************
// <copyright file="CategoryAttribute - Copia.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Rights
{
    /// <summary>
    /// Class RightAttribute.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class RightAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the identifier right.
        /// </summary>
        /// <value>The identifier right.</value>
        public int IdRight { get; set; }
    }
}
