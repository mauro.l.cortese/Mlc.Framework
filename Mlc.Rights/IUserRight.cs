﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-24-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-24-2019
// ***********************************************************************
// <copyright file="IUserRight.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Rights
{
    /// <summary>
    /// Interface IUserRight
    /// </summary>
    public interface IUserRight { }
}
