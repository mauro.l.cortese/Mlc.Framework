
using System;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IAllUserProfile : IDataRowMapping
	{
		#region Propriet�
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdUser
        /// </summary>    
        Int32 IdUser { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo FullName
        /// </summary>    
        String FullName { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Profile
        /// </summary>    
        String Profile { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdProfile
        /// </summary>    
        Int32 IdProfile { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdUserProfile
        /// </summary>    
        Int32 IdUserProfile { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>    
        Boolean Enable { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}