
using System;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface ICategory : IDataRowMapping
	{
		#region Propriet�
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdCategory
        /// </summary>    
        Int32 IdCategory { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}