
using System;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IUserRightsRow : IDataRowMapping
	{
		#region Propriet�
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Category
        /// </summary>    
        String Category { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdRight
        /// </summary>    
        Int32 IdRight { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Right
        /// </summary>    
        String Right { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>    
        Boolean Enable { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}