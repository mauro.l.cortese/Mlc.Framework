
using System;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IProfile : IDataRowMapping
	{
		#region Propriet�
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdProfile
        /// </summary>    
        Int32 IdProfile { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}