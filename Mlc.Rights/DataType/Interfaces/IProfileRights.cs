
using System;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico NON MODIFICARE MANUALMENTE
	/// </summary>
	public interface IProfileRights : IDataRowMapping
	{
		#region Propriet�
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>    
        String Name { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>    
        Boolean Enable { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>    
        String Description { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdApplication
        /// </summary>    
        Int32 IdApplication { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdProfile
        /// </summary>    
        Int32 IdProfile { get ; set; }

        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdRight
        /// </summary>    
        Int32 IdRight { get ; set; }

        #endregion

		#region Metodi 
		#endregion
	}
}