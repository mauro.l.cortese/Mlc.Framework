
using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
    /// <summary>
    /// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
    /// modificare solo questa classe per aggiungere funzionalità
    /// </summary>
    public class Profile : ProfileBase, IProfile
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        #endregion

        #region Campi
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        public Profile()
        { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="dataRow">DataRow su cui mappare l'istanza</param>
        public Profile(DataRow dataRow)
            : base(dataRow)
        {
        }
        #endregion

        #region Propriet�        
        ///// <summary>
        ///// Gets a value indicating whether this <see cref="Profile"/> is enable.
        ///// </summary>
        ///// <value><c>true</c> if enable; otherwise, <c>false</c>.</value>
        //public bool Enable
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (this.DataRow.Table.Columns.Contains("Enable"))
        //                return (bool)this.DataRow["Enable"];
        //        }
        //        catch { }
        //        return false;
        //    }
        //}

        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Restituisce una nuova istanza ottenuta dalla copia di quella corrente
        /// </summary>
        /// <returns>Nuova istanza ottenuta dalla copia di quella corrente</returns>
        public new Profile Clone()
        {
            return (Profile)base.Clone();
        }
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion
    }
}
