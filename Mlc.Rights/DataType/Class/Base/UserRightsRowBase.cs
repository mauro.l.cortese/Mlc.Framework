using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class UserRightsRowBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "UserRightsRow";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public UserRightsRowBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public UserRightsRowBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Propriet�
        /// <summary>Valore mappato sul campo Category</summary>
        protected String category = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Category
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Category { get { return this.category; } set { this.category = value; } }

        /// <summary>Valore mappato sul campo IdRight</summary>
        protected Int32 idright = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdRight
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdRight { get { return this.idright; } set { this.idright = value; } }

        /// <summary>Valore mappato sul campo Right</summary>
        protected String right = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Right
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Right { get { return this.right; } set { this.right = value; } }

        /// <summary>Valore mappato sul campo Enable</summary>
        protected Boolean enable = default(Boolean);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Boolean Enable { get { return this.enable; } set { this.enable = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "Category".</summary>
            public const string Category = "Category";
            /// <summary>Nome campo "IdRight".</summary>
            public const string IdRight = "IdRight";
            /// <summary>Nome campo "Right".</summary>
            public const string Right = "Right";
            /// <summary>Nome campo "Enable".</summary>
            public const string Enable = "Enable";

		}
		#endregion
	}
}
