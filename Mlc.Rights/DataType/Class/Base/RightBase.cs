using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class RightBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "Right";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public RightBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public RightBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Propriet�
        /// <summary>Valore mappato sul campo IdRight</summary>
        protected Int32 idright = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdRight
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdRight { get { return this.idright; } set { this.idright = value; } }

        /// <summary>Valore mappato sul campo Name</summary>
        protected String name = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Valore mappato sul campo Description</summary>
        protected String description = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Description { get { return this.description; } set { this.description = value; } }

        /// <summary>Valore mappato sul campo IdCategory</summary>
        protected Int32 idcategory = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdCategory
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdCategory { get { return this.idcategory; } set { this.idcategory = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "IdRight".</summary>
            public const string IdRight = "IdRight";
            /// <summary>Nome campo "Name".</summary>
            public const string Name = "Name";
            /// <summary>Nome campo "Description".</summary>
            public const string Description = "Description";
            /// <summary>Nome campo "IdCategory".</summary>
            public const string IdCategory = "IdCategory";

		}
		#endregion
	}
}
