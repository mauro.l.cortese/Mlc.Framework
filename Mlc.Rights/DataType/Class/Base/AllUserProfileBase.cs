using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class AllUserProfileBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "AllUserProfile";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public AllUserProfileBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public AllUserProfileBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Propriet�
        /// <summary>Valore mappato sul campo IdUser</summary>
        protected Int32 iduser = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdUser
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdUser { get { return this.iduser; } set { this.iduser = value; } }

        /// <summary>Valore mappato sul campo FullName</summary>
        protected String fullname = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo FullName
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String FullName { get { return this.fullname; } set { this.fullname = value; } }

        /// <summary>Valore mappato sul campo Profile</summary>
        protected String profile = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Profile
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Profile { get { return this.profile; } set { this.profile = value; } }

        /// <summary>Valore mappato sul campo Description</summary>
        protected String description = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Description { get { return this.description; } set { this.description = value; } }

        /// <summary>Valore mappato sul campo IdProfile</summary>
        protected Int32 idprofile = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdProfile
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdProfile { get { return this.idprofile; } set { this.idprofile = value; } }

        /// <summary>Valore mappato sul campo IdUserProfile</summary>
        protected Int32 iduserprofile = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdUserProfile
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdUserProfile { get { return this.iduserprofile; } set { this.iduserprofile = value; } }

        /// <summary>Valore mappato sul campo Enable</summary>
        protected Boolean enable = default(Boolean);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Boolean Enable { get { return this.enable; } set { this.enable = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "IdUser".</summary>
            public const string IdUser = "IdUser";
            /// <summary>Nome campo "FullName".</summary>
            public const string FullName = "FullName";
            /// <summary>Nome campo "Profile".</summary>
            public const string Profile = "Profile";
            /// <summary>Nome campo "Description".</summary>
            public const string Description = "Description";
            /// <summary>Nome campo "IdProfile".</summary>
            public const string IdProfile = "IdProfile";
            /// <summary>Nome campo "IdUserProfile".</summary>
            public const string IdUserProfile = "IdUserProfile";
            /// <summary>Nome campo "Enable".</summary>
            public const string Enable = "Enable";

		}
		#endregion
	}
}
