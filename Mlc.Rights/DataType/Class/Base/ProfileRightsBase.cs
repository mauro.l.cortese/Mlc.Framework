using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// NON MODIFICARE MANUALMENTE
	/// </summary>
	public abstract class ProfileRightsBase : DsRowMapBase
	{
		#region Costanti
		/// <summary>Nome della tabella in DB</summary>
		public const string DbTableName = "";
		/// <summary>Nome della tabella nel DataSet</summary>
		public const string DsTableName = "ProfileRights";
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public ProfileRightsBase()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public ProfileRightsBase(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion

		#region Propriet�
        /// <summary>Valore mappato sul campo Name</summary>
        protected String name = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Name
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Valore mappato sul campo Enable</summary>
        protected Boolean enable = default(Boolean);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Enable
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Boolean Enable { get { return this.enable; } set { this.enable = value; } }

        /// <summary>Valore mappato sul campo Description</summary>
        protected String description = default(String);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo Description
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual String Description { get { return this.description; } set { this.description = value; } }

        /// <summary>Valore mappato sul campo IdApplication</summary>
        protected Int32 idapplication = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdApplication
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdApplication { get { return this.idapplication; } set { this.idapplication = value; } }

        /// <summary>Valore mappato sul campo IdProfile</summary>
        protected Int32 idprofile = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdProfile
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdProfile { get { return this.idprofile; } set { this.idprofile = value; } }

        /// <summary>Valore mappato sul campo IdRight</summary>
        protected Int32 idright = default(Int32);
        /// <summary>
        /// Restituisce o imposta il valore mappato sul campo IdRight
        /// </summary>  
		[Field(Visible = true, HAlign = HorzAlignment.Left)]  
        public virtual Int32 IdRight { get { return this.idright; } set { this.idright = value; } }

        #endregion

		#region Metodi pubblici
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>Costanti pubbliche nomi prorietà</summary>
		public class Properties
		{
            /// <summary>Nome campo "Name".</summary>
            public const string Name = "Name";
            /// <summary>Nome campo "Enable".</summary>
            public const string Enable = "Enable";
            /// <summary>Nome campo "Description".</summary>
            public const string Description = "Description";
            /// <summary>Nome campo "IdApplication".</summary>
            public const string IdApplication = "IdApplication";
            /// <summary>Nome campo "IdProfile".</summary>
            public const string IdProfile = "IdProfile";
            /// <summary>Nome campo "IdRight".</summary>
            public const string IdRight = "IdRight";

		}
		#endregion
	}
}
