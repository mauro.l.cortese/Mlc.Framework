
using System;
using System.Data;
using Mlc.Data;

namespace Mlc.Rights
{
	/// <summary>
	/// Codice generato in automatico da <see cref="Mx.Core.Data.DsTypeCodeCreator"/>
	/// modificare solo questa classe per aggiungere funzionalità
	/// </summary>
	public class AllUserProfile : AllUserProfileBase, IAllUserProfile 
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		#endregion

		#region Costruttori
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public AllUserProfile()
		{ }

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="dataRow">DataRow su cui mappare l'istanza</param>
		public AllUserProfile(DataRow dataRow)
			:base(dataRow)
		{
		}
		#endregion
		
		#region Propriet�
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// Restituisce una nuova istanza ottenuta dalla copia di quella corrente
		/// </summary>
		/// <returns>Nuova istanza ottenuta dalla copia di quella corrente</returns>
		public new AllUserProfile Clone()
		{
			return (AllUserProfile)base.Clone();
		}
		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}
}
