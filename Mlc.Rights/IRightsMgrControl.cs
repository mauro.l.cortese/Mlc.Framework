﻿// ***********************************************************************
// Assembly         : Mlc.Rights
// Author           : mauro.luigi.cortese
// Created          : 06-24-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 06-24-2019
// ***********************************************************************
// <copyright file="IUserRight.cs" company="">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Mlc.Rights
{
    /// <summary>
    /// Interface IUserRight
    /// </summary>
    public interface IRightsMgrControl
    {
        IORights IORights { get; set; }
        void RefreshData();
        string NameSpace { get; set; }
        string ClassName { get; set; }
        string PathClassRight { get; set; }
        string NameSpaceBin { get; set; }
        string NameRootFolderBin { get; set; }
    }
}
