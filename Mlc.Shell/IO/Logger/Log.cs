﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Logger.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class Log.
    /// </summary>
    public class Log : ICloneable
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Log" /> class.
        /// </summary>
        public Log(LogLevel level, LogType type)
        {
            this.Level = level;
            this.Type = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log" /> class.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        public Log(LogLevel level, string message)
            : this(level, string.Empty, message, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log" /> class.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="cmdName">The name.</param>
        /// <param name="message">The message.</param>
        public Log(LogLevel level, string cmdName, string message)
            : this(level, cmdName, message, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log" /> class.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="cmdName">Name of the command.</param>
        /// <param name="message">The message.</param>
        /// <param name="comment">The comment.</param>
        public Log(LogLevel level, string cmdName, string message, string comment)
        {
            this.Level = level;
            this.CmdName = cmdName;
            this.Message = message;
            this.Description = comment;
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>The index.</value>
        public int Index { get; set; }

        /// <summary>
        /// Gets the level.
        /// </summary>
        /// <value>The level.</value>
        public LogLevel Level { get; set; }

        /// <summary>
        /// Gets the log type.
        /// </summary>
        /// <value>The key.</value>
        public LogType Type { get; set; } = LogType.Global;

        /// <summary>
        /// Gets or sets the name of the command.
        /// </summary>
        /// <value>The name of the command.</value>
        public string CmdName { get; set; }

        /// <summary>
        /// Gets or sets the command data.
        /// </summary>
        /// <value>The command data.</value>
        public string CmdData { get; set; }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>The comment.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets the start time.
        /// </summary>
        /// <value>The start time.</value>
        public DateTime StartTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Gets the end time.
        /// </summary>
        /// <value>The end time.</value>
        public DateTime EndTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Gets the spent time.
        /// </summary>
        /// <value>The spent time.</value>
        public TimeSpan SpentTime { get { return this.EndTime - this.StartTime; } }

        /// <summary>
        /// Crea un nuovo oggetto che è una copia dell'istanza corrente.
        /// </summary>
        /// <returns>Nuovo oggetto che è una copia dell'istanza corrente.</returns>
        public object Clone()
        {
            return new Log(this.Level, this.Type)
            {
                CmdName = this.CmdName,
                Description = this.Description,
                CmdData = this.CmdData,
                Message = this.Message,
                StartTime = this.StartTime,
                EndTime = this.EndTime,
            };
        }
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine($"CmdName: {this.CmdName} - Executed at: {this.StartTime}");
            sb.AppendLine();
            sb.AppendLine("Comment:");
            sb.AppendLine($"{this.Description}");
            sb.AppendLine();
            sb.AppendLine("Message:");
            sb.AppendLine($"{this.Message}");
            sb.AppendLine();
            sb.AppendLine("CmdData:");
            sb.AppendLine($"{this.CmdData}");
            return sb.ToString();

            //return this.Time != null ? this.Time.ToShortTimeString() : string.Empty;
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion

        #region Event Handlers - CommandsEngine
        #endregion

        #region Event Handlers
        #endregion
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
