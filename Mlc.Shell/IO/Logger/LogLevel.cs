﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Enumeration of the importance level of log messages
    /// </summary>
    public enum LogLevel
    {
        /// <summary>Messages useful for debugging </summary>
        Debug,
        /// <summary>Normal but significant conditions </summary>
        Notice,
        /// <summary>Notices to pay attention to </summary>
        Warning,
        /// <summary>General errors </summary>
        Error,
        /// <summary>Critical conditions </summary>
        Critical,
        /// <summary>Provide immediately </summary>
        Alert,
        /// <summary>The system is unusable </summary>
        Emergency,
        /// <summary>No log message </summary>
        None,
    }


    /// <summary>
    /// Enum LogType
    /// </summary>
    public enum LogType
    {
        /// <summary>The global</summary>
        Global,
        /// <summary>The Sql</summary>
        Sql,
    }
}
