﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="IViewLogger.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Interface IViewLogger
    /// </summary>
    public interface IViewLogger
    {
        /// <summary>
        /// Gets or sets the logger list.
        /// </summary>
        /// <value>The logger list.</value>
        Logger Logger { get; set; }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        void DataRefresh();
    }
}