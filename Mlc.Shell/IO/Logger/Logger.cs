﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 04-09-2019
// ***********************************************************************
// <copyright file="Logger.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class Logger (Singleton Pattern)
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List{Mlc.Shell.IO.Log}" />
    public class Logger : List<Log>
    {
        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        public Logger()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        /// <param name="logType">The key.</param>
        public Logger(LogType logType)
        {
            this.LogType = logType;
        }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="Logger" /> class.
        ///// </summary>
        ///// <param name="level">The level.</param>
        //public Logger(LogLevel level)
        //{
        //    this.LevelLog = level;
        //}

        ///// <summary>
        ///// Initializes a new instance of the <see cref="Logger" /> class.
        ///// </summary>
        ///// <param name="logType">The key.</param>
        ///// <param name="level">The level.</param>
        //public Logger(LogType logType, LogLevel level)
        //{
        //    this.LogType = logType;
        //    this.LevelLog = level;
        //}

        #endregion

        #region Public
        #region Properties
        ///// <summary>
        ///// Gets the level.
        ///// </summary>
        ///// <value>The level.</value>
        //public LogLevel LevelLog { get; set; } = LogLevel.Warning;

        /// <summary>
        /// The i viewer
        /// </summary>
        private IViewLogger iViewer = null;
        /// <summary>
        /// Gets or sets the i viewer.
        /// </summary>
        /// <value>The i viewer.</value>
        public IViewLogger IViewer
        {
            get { return this.iViewer; }
            set
            {
                this.iViewer = value;
                if (this.iViewer != null)
                    this.iViewer.Logger = this;
            }
        }

        /// <summary>
        /// Gets the log type.
        /// </summary>
        /// <value>The key.</value>
        public LogType LogType { get; set; } = LogType.Global;
        #endregion

        #region Method Static
        #endregion



        #region Method

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        public new void Add(Log log)
        {
            this.addLog((Log)log.Clone());
        }

        /// <summary>
        /// Adds the log.
        /// </summary>
        /// <param name="log">The log.</param>
        private void addLog(Log log)
        {
            log.Index = base.Count + 1;
            base.Add(log);

            if (this.IViewer != null)
                this.IViewer.DataRefresh();
        }

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        public void Add(LogLevel level, string message)
        {
            this.Add(level, message, null, null);
        }

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        /// <param name="name">The name.</param>
        public void Add(LogLevel level, string message, string name)
        {
            this.Add(level, message, name, null);
        }

        /// <summary>
        /// Adds the specified message.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        /// <param name="cmdName">The name.</param>
        /// <param name="pars">The pars.</param>
        public void Add(LogLevel level, string message, string cmdName, params string[] pars)
        {
            string msg;
            if (pars != null)
                msg = string.Format(message, pars);
            else
                msg = message;

            this.addLog(new Log(level, cmdName, msg));
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion

        #region Event Handlers - CommandsEngine
        #endregion

        #region Event Handlers
        #endregion
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
