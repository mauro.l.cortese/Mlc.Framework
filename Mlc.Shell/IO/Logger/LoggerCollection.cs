﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 11-23-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 04-09-2019
// ***********************************************************************
// <copyright file="Logger.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class Logger (Singleton Pattern)
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List{Mlc.Shell.IO.Log}" />
    public class LoggerCollection : Dictionary<LogType, Logger>
    {
        /// <summary>
        /// Gets or sets the log level.
        /// </summary>
        /// <value>The log level.</value>
        public LogLevel LogLevel { get; set; } = LogLevel.None;

        /// <summary>
        /// Adds the specified logger.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public void Add(Logger logger)
        {
            base.Add(logger.LogType, logger);
        }

        /// <summary>
        /// Writes the specified log.
        /// </summary>
        /// <param name="log">The log.</param>
        public void Write(Log log)
        {
            if (this.LogLevel != LogLevel.None)
                if (log.Level >= this.LogLevel)
                    if (base.ContainsKey(log.Type))
                        base[log.Type].Add(log);
        }
    }
}
