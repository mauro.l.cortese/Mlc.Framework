﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-16-2020
// ***********************************************************************
// <copyright file="Logger.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class LogRow.
    /// </summary>
    public class LogRow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogRow"/> class.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The message.</param>
        public LogRow(Logger20.Level level, object logObject)
        {
            this.Level = level;
            this.LogObject = logObject;
            this.TimeStamp = Logger20.TimeStamp;
            this.ElapsedTime = Logger20.ElapsedTime;
            this.LevelTag = level.ToString()[0].ToString().ToUpper();

        }

        /// <summary>
        /// Gets the spent time.
        /// </summary>
        /// <value>The spent time.</value>
        public DateTime TimeStamp { get; }

        /// <summary>
        /// Gets the spent time.
        /// </summary>
        /// <value>The spent time.</value>
        public TimeSpan ElapsedTime { get; }

        /// <summary>
        /// Gets the level.
        /// </summary>
        /// <value>The level.</value>
        public Logger20.Level Level { get; }

        /// <summary>
        /// Gets the log object.
        /// </summary>
        /// <value>The log object.</value>
        public object LogObject { get; set; }

        /// <summary>
        /// Gets the spent time.
        /// </summary>
        /// <value>The spent time.</value>
        public string Message { get => this.LogObject.ToString(); }

        /// <summary>
        /// Gets the level tag.
        /// </summary>
        /// <value>The level tag.</value>
        public string LevelTag { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="LogRow"/> is ignore.
        /// </summary>
        /// <value><c>true</c> if ignore; otherwise, <c>false</c>.</value>
        public bool Ignore { get; set; } = false;
	}
}
