﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 04-16-2020
// ***********************************************************************
// <copyright file="LogHandlerFile.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class LogHandlerFile.
	/// </summary>
	/// <seealso cref="Mlc.Shell.IO.LogHandlerBase" />
	public class LogHandlerFile : LogHandlerBase, IDisposable
	{
		#region Enumerations
		/// <summary>
		/// Enum FileRotation
		/// </summary>
		public enum FileRotation
		{
			/// <summary>
			/// The none, change the log file after each application execution
			/// </summary>
			None = 0,
			/// <summary>
			/// The daily, change the log file after the number of days specified by interval, starting at midnight 
			/// </summary>
			Daily = 1,
			/// <summary>
			/// The hourly, change the log file after the number of hours specified by interval, starting at 00 minute
			/// </summary>
			Hourly = 2,
			/// <summary>
			/// The minutes , change the log file after the number of minutes specified by interval, starting at 00 seconds
			/// </summary>
			Minutes = 3,
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="LogHandlerFile" /> class.
		/// </summary>
		/// <param name="level">The level.</param>
		/// <param name="name">The name.</param>
		/// <param name="folder">The folder.</param>
		/// <exception cref="Exception">Folder {this.Folder} doesn't exists!!</exception>
		public LogHandlerFile(Logger20.Level level, string name, string folder = "") : base(name, level)
		{
			this.Folder = string.IsNullOrEmpty(folder) ? new SysInfo().ExecutableFolder : folder;

			if (!string.IsNullOrEmpty(this.Folder) && !this.Folder.Contains("\\"))
				this.Folder = $"{new SysInfo().ExecutableFolder}\\{this.Folder}";

			this.runDeleteOldFile();

			if (!Directory.Exists(this.Folder))
				Directory.CreateDirectory(this.Folder);

			if (!Directory.Exists(this.Folder))
				throw new Exception($"Folder {this.Folder} doesn't exists!!");

			// Defines the formatters for default types
			this.Formatters.Add(
				new List<Type>() {
					typeof(object),
					typeof(string),
					},
				new LogFormatter());
			this.Formatters.Add(new List<Type>() { typeof(CmdLineParser) }, new LogCmdLineFormatter());
		}

		#endregion

		#region Public

		#region Properties
		/// <summary>
		/// Gets or sets the folder.
		/// </summary>
		/// <value>The folder.</value>
		public string Folder { get; }

		/// <summary>
		/// Gets the file log.
		/// </summary>
		/// <value>The file log.</value>
		public string FileLog { get => $"{base.Name}.log"; }

		/// <summary>
		/// Gets or sets the interval.
		/// </summary>
		/// <value>The interval.</value>
		public FileRotation Rotation { get; set; } = FileRotation.None;

		/// <summary>
		/// Gets or sets the interval.
		/// </summary>
		/// <value>The interval.</value>
		public int Interval { get; set; } = 1;

		/// <summary>
		/// Gets or sets the backup files.
		/// </summary>
		/// <value>The backup files.</value>
		public int BackupFiles { get; set; } = 3;

		/// <summary>
		/// Gets the full path.
		/// </summary>
		/// <value>The full path.</value>
		public string FullPath { get => Path.Combine(this.Folder, this.FileLog); }

		/// <summary>
		/// Gets or sets the log row formatter.
		/// </summary>
		/// <value>The log row formatter.</value>
		public LogTypeFormatters Formatters { get; set; } = new LogTypeFormatters();

		/// <summary>
		/// Gets or sets a value indicating whether [file writing].
		/// </summary>
		/// <value><c>true</c> if [file writing]; otherwise, <c>false</c>.</value>
		public bool FileWriting { get; set; }
		#endregion

		#region Method
		/// <summary>
		/// Deletes the file of this instance (if it exists).
		/// </summary>
		public void Delete() { File.Delete(this?.FullPath); }

		/// <summary>
		/// Writes the specified log row.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		public override void Write(LogRow logRow)
		{
			if (this.checkRotation())
			{
				// Determines which formatter instance to use
				LogFormatter logFormatter = getFormatter(logRow);

				if (logFormatter != null && !logRow.Ignore)
					File.AppendAllText(this.FullPath, logFormatter.ToString(), System.Text.Encoding.Unicode); //, System.Text.Encoding.ASCII);
				else
				{ throw new Exception(); }
			}
			else
				throw new Exception();
		}

		/// <summary>
		/// Gets the next time.
		/// </summary>
		/// <param name="start">The start.</param>
		/// <param name="rotation">The rotation.</param>
		/// <param name="interval">The interval.</param>
		/// <returns>DateTime.</returns>
		public DateTime GetNextTime(DateTime start, LogHandlerFile.FileRotation rotation, int interval)
		{
			DateTime next = default;
			switch (rotation)
			{
				case LogHandlerFile.FileRotation.Daily:
					int day = start.Hour < 12 ? start.Day : (start + new TimeSpan(1, 0, 0, 0)).Day;
					next = new DateTime(start.Year, start.Month, day, 0, 0, 0) + new TimeSpan(interval, 0, 0, 0);
					break;
				case LogHandlerFile.FileRotation.Hourly:
					int hour = start.Minute < 30 ? start.Hour : start.Hour + 1;
					next = new DateTime(start.Year, start.Month, start.Day, hour, 0, 0) + new TimeSpan(interval, 0, 0);
					break;
				case LogHandlerFile.FileRotation.Minutes:
					int minute = start.Second < 30 ? start.Minute : start.Minute + 1;
					next = new DateTime(start.Year, start.Month, start.Day, start.Hour, minute, 0) + new TimeSpan(0, interval, 0);
					break;
			}
			return next;
		}
		#endregion

		#endregion

		#region Private
		#region Method

		/// <summary>
		/// Checks the rotation.
		/// </summary>
		private bool checkRotation()
		{
			bool rotate = false;
			FileInfo fi = new FileInfo(this.FullPath);

			if (this.Rotation == FileRotation.None && this.FileWriting)
				return true;
			else if (this.Rotation == FileRotation.None && !this.FileWriting)
			{
				this.FileWriting = true;
				rotate = true;
			}
			else
			{
				// Gets creation time for current log file
				if (fi.Exists)
					rotate = DateTime.UtcNow > this.GetNextTime(fi.CreationTimeUtc, this.Rotation, this.Interval);
			}

			if (rotate && fi.Exists)
			{
				// rename current file
				string backupfile = Path.Combine(this.Folder, $"({DateTime.Now:yyyyMMdd-HHmmss})-{this.FileLog}");
				try
				{
					if (!File.Exists(backupfile))
					{
						File.Move(this.FullPath, backupfile);
						Thread.Sleep(1000);
						bool stop = true;
						int retry = 0;
						while (stop)
						{
							retry++;
							stop = !File.Exists(backupfile) & File.Exists(this.FullPath);
							if (retry == 5)
								throw new Exception();
						}
					}
				}
				catch (Exception) { throw new Exception(); }
				this.runDeleteOldFile();
			}
			return true;
		}

		/// <summary>
		/// Runs the delete old file.
		/// </summary>
		private void runDeleteOldFile()
		{
			Thread th = new Thread(new ParameterizedThreadStart(deleteOldFile));
			th.Start(base.Name);
		}

		/// <summary>
		/// Deletes the old file.
		/// </summary>
		/// <param name="name">The name.</param>
		private void deleteOldFile(object name)
		{
			string[] files = Directory.GetFiles(this.Folder, $"*-{name}.log");
			int delUntil = files.Length - this.BackupFiles;
			for (int i = 0; i < delUntil; i++)
				File.Delete(files[i]);
		}

		/// <summary>
		/// Gets the formatter.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		/// <returns>Mlc.Shell.IO.LogFormatter.</returns>
		private LogFormatter getFormatter(LogRow logRow)
		{
			LogFormatter logFormatter = null;
			if (logRow.LogObject != null)
			{
				Type type = logRow.LogObject.GetType();
				if (this.Formatters.ContainsKey(type))
					logFormatter = this.Formatters[type];
			}
			if (logFormatter == null && logRow.LogObject is object)
				logFormatter = this.Formatters[typeof(object)];

			if (logFormatter != null)
				logFormatter.LogRow = logRow;

			return logFormatter;
		}

		#endregion

		#endregion
	}
}
