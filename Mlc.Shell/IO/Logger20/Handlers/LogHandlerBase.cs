﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-16-2020
// ***********************************************************************
// <copyright file="Logger.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class LogHandlerBase.
    /// </summary>
    public class LogHandlerBase : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogHandlerBase"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="level">The level.</param>
        public LogHandlerBase(string name, Logger20.Level level)
        {
            this.Name = name;
            this.Level = level;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the level.
        /// </summary>
        /// <value>The level.</value>
        public Logger20.Level Level { get; private set; }

        public virtual void Dispose()
        {
            // Nothing to do Here
        }

        /// <summary>
        /// Writes the specified log row.
        /// </summary>
        /// <param name="logRow">The log row.</param>
        public virtual void Write(LogRow logRow)
        {
            // Nothing to do Here
        }
    }
}
