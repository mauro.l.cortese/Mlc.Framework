﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-16-2020
// ***********************************************************************
// <copyright file="Logger.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Linq;
using System.Collections.Generic;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class LogRowFormatter.
	/// </summary>
	public class LogFormatter
	{
		/// <summary>
		/// The log message
		/// </summary>
		private string logMessage = "";

		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>The width.</value>
		public int Width { get; set; } = 200;

		/// <summary>
		/// The log row
		/// </summary>
		private LogRow logRow;

		/// <summary>
		/// The row header lenght
		/// </summary>
		protected int rowHeaderLenght;

		public LogRow LogRow
		{
			get => logRow;
			internal set
			{
				logRow = value;
				this.logMessage = this.FormatLog(ref logRow);
			}
		}

		/// <summary>
		/// Formats the log.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		/// <returns>System.String.</returns>
		public virtual string FormatLog(ref LogRow logRow)
		{
			string logMessage = "";

			if (string.IsNullOrEmpty(logRow.Message))
				logMessage = Environment.NewLine;
			else if (logRow.Message == Logger20.Start)
			{
				logMessage =  $"{Environment.NewLine}{new string('-', this.Width)}{Environment.NewLine}";
				logMessage += $"{this.getTimeStamp(logRow)}START APPLICATION{Environment.NewLine}";
				logMessage += $"{new string('-', this.Width)}{Environment.NewLine}";
			}
			else if (logRow.Message == Logger20.Stop)
			{
				logMessage =  $"{Environment.NewLine}{new string('-', this.Width)}{Environment.NewLine}";
				logMessage += $"{this.getTimeStamp(logRow)}STOP APPLICATION{Environment.NewLine}";
				logMessage += $"{new string('-', this.Width)}{Environment.NewLine}";
			}
			else if (logRow.LogObject is char)
			{
				logMessage = $"{new string((char)logRow.LogObject, this.Width)}{Environment.NewLine}";
			}
			else
			{
				string timeStamp = this.getTimeStamp(logRow);
				this.rowHeaderLenght = timeStamp.Length;
				int wh = this.rowHeaderLenght - 3;

				if (isEnum(logRow))
				{
					Type t = (Type)logRow.LogObject;
					if (t.BaseType.Name == "Enum")
					{
						List<string> items = Enum.GetNames(t).ToList();
						items.Sort(); logMessage = "";
						foreach (string item in items)
							logMessage += $"{new string(' ', wh)} | => {item}{Environment.NewLine}";
					}
				}
				else
				{
					string firstLine = "";
					string[] lines = logRow.Message.Replace("\n", "").Split(new string[] { "\r" }, StringSplitOptions.None);
					if (lines.Length > 0)
						firstLine = lines[0].Trim();

					switch (logRow.Level)

					{
						case Logger20.Level.None:
							break;
						case Logger20.Level.Debug:
						case Logger20.Level.Info:
							if (string.IsNullOrEmpty(firstLine))
								logMessage = Environment.NewLine;
							else
								logMessage = $"{timeStamp}{this.getMessage(firstLine, lines, wh)}{Environment.NewLine}";
							break;
						case Logger20.Level.Warning:
						case Logger20.Level.Error:
						case Logger20.Level.Critical:
							logMessage = $"{Environment.NewLine}{new string('-', this.Width)}{Environment.NewLine}";
							logMessage += $"{timeStamp}{logRow.Level.ToString().ToUpper()}! => {this.getMessage(firstLine, lines, wh)}{Environment.NewLine}";
							logMessage += $"{new string('-', this.Width)}{Environment.NewLine}{Environment.NewLine}";
							break;
					}
				}
			}
			return logMessage;
		}

		private string getTimeStamp(LogRow logRow)
		{
			return $"{logRow.TimeStamp} | {logRow.ElapsedTime:hh\\:mm\\:ss\\.fffff} |{logRow.LevelTag}| ";
		}

		/// <summary>
		/// Gets the message.
		/// </summary>
		/// <param name="firstLine">The first line.</param>
		/// <param name="lines">The lines.</param>
		/// <returns>System.String.</returns>
		private string getMessage(string firstLine, string[] lines, int wh)
		{
			if (lines.Length == 1)
				return firstLine;

			List<string> listLogLine = new List<string>();

			bool inn = false;
			for (int i = lines.Length - 1; i >= 1; i--)
				if (lines[i] != "" || inn)
				{ inn = true; listLogLine.Add(lines[i]); }

			listLogLine.Reverse();

			StringBuilder sb = new();
			sb.AppendLine(firstLine);

			foreach (string line in listLogLine)
			{
				string ln = Logger20.TrimLine ? line.Trim() : line;
				sb.AppendLine($"{new string(' ', wh)} | {ln}");
			}
			sb.Remove(sb.Length - 2, 2);
			Logger20.TrimLine = true;
			return sb.ToString().Trim();
		}

		/// <summary>
		/// Determines whether the specified log row is enum.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		/// <returns>System.Boolean.</returns>
		private bool isEnum(LogRow logRow) { return logRow.Level == Logger20.Level.Debug && logRow.LogObject is Type; }

		/// <summary>
		/// Converts to string.
		/// </summary>
		/// <returns>System.String.</returns>
		public override string ToString() { return this.logMessage; }


		/// <summary>
		/// Gets the sep string.
		/// </summary>
		/// <param name="repeat">The repeat.</param>
		/// <returns>System.String.</returns>
		protected string getSepString(char repeat)
		{
			if (this.rowHeaderLenght == 0)
				this.rowHeaderLenght = this.getTimeStamp(logRow).Length;

			int count = this.Width - this.rowHeaderLenght;
			return new string(repeat, count);
		}
	}
}
