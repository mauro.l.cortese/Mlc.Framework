﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using Mlc.Shell;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class LogRowFormatter.
	/// </summary>
	public class LogCmdLineFormatter : LogFormatter
	{

        /// <summary>
        /// Formats the log.
        /// </summary>
        /// <param name="logRow">The log row.</param>
        /// <returns>System.String.</returns>
        public override string FormatLog(ref LogRow logRow)
        {
            string retVal;
            if (logRow.LogObject is CmdLineParser)
            {
                CmdLineParser clp = (CmdLineParser)logRow.LogObject;
                StringBuilder sb = new();
                sb.AppendLine("Command line application:");
                sb.AppendLine($"File .exe :{clp.ExecuteblePath}");
                sb.AppendLine($"Arguments :{clp.Parameters}");
                logRow.LogObject = sb;
            }
            else
                logRow.Ignore = string.IsNullOrEmpty(logRow.Message) ? false : true;
            retVal = base.FormatLog(ref logRow);
            return retVal;
        }
    }
}
