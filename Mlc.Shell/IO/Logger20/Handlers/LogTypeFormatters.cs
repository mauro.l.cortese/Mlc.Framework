﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Linq;
using System.Collections.Generic;

namespace Mlc.Shell.IO
{

	/// <summary>
	/// Class LogTypeFormatters.
	/// Implements the <see cref="System.Collections.Generic.Dictionary{System.Type, Mlc.Shell.IO.LogFormatter}" />
	/// </summary>
	/// <seealso cref="System.Collections.Generic.Dictionary{System.Type, Mlc.Shell.IO.LogFormatter}" />
	public class LogTypeFormatters : Dictionary<Type, LogFormatter>
	{
		/// <summary>
		/// Adds the formatter for specified list type.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <param name="logRowFormatter">The log row formatter.</param>
		/// <returns>LogTypeFormatters.</returns>
		public LogTypeFormatters Add(List<Type> listType, LogFormatter logRowFormatter)
		{
			foreach (Type type in listType)
				this.Add(type, logRowFormatter);
			return this;
		}
	}
}
