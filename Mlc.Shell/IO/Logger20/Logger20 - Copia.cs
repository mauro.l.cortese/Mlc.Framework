﻿// *********************************************************************** 
// Assembly         : Logger 
// Author           : mauro.luigi.cortese 
// Created          : 03-16-2020 
// 
// Last Modified By : mauro.luigi.cortese 
// Last Modified On : 03-16-2020 
// *********************************************************************** 
// <copyright file="Logger.cs" company=""> 
//     Copyright ©  2020 
// </copyright> 
// <summary></summary> 
// *********************************************************************** 
using System; 
using System.Collections.Generic; 
 
namespace Mlc.Shell.IO 
{ 
	/// <summary> 
	/// Class Logger20  
	/// Implements the <see cref="Mlc.Shell.SingletonBase{Mlc.Shell.IO.Logger20}" /> 
	/// Implements the <see cref="System.IDisposable" /> 
	/// </summary> 
	/// <seealso cref="Mlc.Shell.SingletonBase{Mlc.Shell.IO.Logger20}" /> 
	/// <seealso cref="System.IDisposable" /> 
	public class Logger20 : SingletonBase<Logger20>, IDisposable 
	{ 
		/// <summary> 
		/// Enumeration of the importance level of log messages 
		/// </summary> 
		public enum Level 
		{ 
			/// <summary> 
			/// No log message 
			/// </summary> 
			None = 0, 
			/// <summary> 
			/// Messages useful for debugging 
			/// </summary> 
			Debug = 10, 
			/// <summary> 
			/// Normal but significant conditions 
			/// </summary> 
			Info = 20, 
			/// <summary> 
			/// Notices to pay attention to 
			/// </summary> 
			Warning = 30, 
			/// <summary> 
			/// General errors 
			/// </summary> 
			Error = 40, 
			/// <summary> 
			/// Critical conditions 
			/// </summary> 
			Critical = 50, 
		} 
 
		/// <summary> 
		/// The default name 
		/// </summary> 
		private const string defaultName = "Log"; 
 
		/// <summary> 
		/// The logs 
		/// </summary> 
		private readonly Logs logs = new Logs(); 
 
		/// <summary> 
		/// Gets the <see cref="Log"/> with the specified name. 
		/// </summary> 
		/// <param name="name">The name.</param> 
		/// <returns>Log.</returns> 
		public virtual Log DefaultLog { get { return this.logs.ContainsKey(defaultName) ? this.logs[defaultName] : null; } } 
 
		/// <summary> 
		/// Gets the <see cref="Log"/> with the specified name. 
		/// </summary> 
		/// <param name="name">The name.</param> 
		/// <returns>Log.</returns> 
		public virtual Log this[string name] { get { return this.logs[name]; } } 
 
		/// <summary> 
		/// Gets the instance of this singleton. 
		/// </summary> 
		/// <returns>T.</returns> 
		public new static Logger20 GetInstance() 
		{ 
			Logger20 logger = SingletonBase<Logger20>.GetInstance(); 
			StartLogger = DateTime.Now; 
			return logger; 
		} 
 
		/// <summary> 
		/// Adds the handler. 
		/// <para> 
		/// Any handler added with this static method is placed in the dictionary 
		/// of the instance that will be called with the static method GetInstance () 
		/// </para> 
		/// </summary> 
		/// <param name="logHandlerBase">The log handler base.</param> 
		public static LogHandlerBase AddHandler(LogHandlerBase logHandlerBase) 
		{ 
			return Logger20.AddHandler(null, logHandlerBase); 
		} 
 
		/// <summary> 
		/// Adds the handler. 
		/// </summary> 
		/// <param name="keyLog">The key log.</param> 
		/// <param name="logHandlerBase">The log handler base.</param> 
		public static LogHandlerBase AddHandler(string keyLog, LogHandlerBase logHandlerBase) 
		{ 
			//Logger20.GetInstance().Handlers.Add(logHandlerBase); 
			Logger20 logger = Logger20.GetInstance(); 
 
			if (string.IsNullOrEmpty(keyLog)) 
				keyLog = defaultName; 
 
			Log log; 
 
			if (!logger.logs.ContainsKey(keyLog)) 
			{ 
				log = new Log(keyLog); 
				logger.logs.Add(log.Name, log); 
			} 
			log = logger.logs[keyLog]; 
 
			log.Add(logHandlerBase); 
			return logHandlerBase; 
		} 
 
		/// <summary> 
		/// Debugs the specified log message. 
		/// </summary> 
		/// <param name="logMessage">The log message.</param> 
		public LogRow Debug(string logMessage) { return this.logs.Write(Level.Debug, logMessage); } 
 
		/// <summary> 
		/// Informations the specified log message. 
		/// </summary> 
		/// <param name="logMessage">The log message.</param> 
		public LogRow Info(string logMessage) { return this.logs.Write(Level.Info, logMessage); } 
 
		/// <summary> 
		/// Warnings the specified log message. 
		/// </summary> 
		/// <param name="logMessage">The log message.</param> 
		public LogRow Warning(string logMessage) { return this.logs.Write(Level.Warning, logMessage); } 
 
		/// <summary> 
		/// Errors the specified log message. 
		/// </summary> 
		/// <param name="logMessage">The log message.</param> 
		public LogRow Error(string logMessage) { return this.logs.Write(Level.Error, logMessage); } 
 
		/// <summary> 
		/// Criticals the specified log message. 
		/// </summary> 
		/// <param name="logMessage">The log message.</param> 
		public LogRow Critical(string logMessage) { return this.logs.Write(Level.Critical, logMessage); } 
 
 
		/// <summary> 
		/// Release all log handlers 
		/// </summary> 
		public void Dispose() 
		{ 
			this.logs.Dispose(); 
			Logger20.StartLogger = default; 
		} 
 
		///// <summary> 
		///// The handlers 
		///// </summary> 
		//private static LogHandlers handlers = new LogHandlers(); 
 
		///// <summary> 
		///// Gets the handlers. 
		///// </summary> 
		///// <value>The handlers.</value> 
		//public LogHandlers Handlers { get => handlers; } 
 
		/// <summary> 
		/// Gets the start time. 
		/// </summary> 
		/// <value>The start time.</value> 
		public static DateTime StartLogger { get; private set; } 
 
		/// <summary> 
		/// Gets the time stamp. 
		/// </summary> 
		/// <value>The time stamp.</value> 
		public static DateTime TimeStamp { get => DateTime.Now; } 
 
		/// <summary> 
		/// Gets the elapsed time. 
		/// </summary> 
		/// <value>The elapsed time.</value> 
		public static TimeSpan ElapsedTime { get { return StartLogger != default ? TimeStamp - StartLogger : default; }  } 
 
		/// <summary> 
		/// Class LogHandlers. 
		/// </summary> 
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.LogHandlerBase}" /> 
		public class LogHandlers : Dictionary<string, LogHandlerBase>, IDisposable 
		{ 
			/// <summary> 
			/// The lock 
			/// </summary> 
			private static object _lock = new object(); 
 
			/// <summary> 
			/// Adds the specified log handler. 
			/// </summary> 
			/// <param name="logHandler">The log handler.</param> 
			/// <returns>LogHandlerBase.</returns>C 
			public LogHandlerBase Add(LogHandlerBase logHandler) 
			{ 
				if (!this.ContainsKey(logHandler.Name)) 
					this.Add(logHandler.Name, logHandler); 
 
				return this[logHandler.Name]; 
			} 
 
			/// <summary> 
			/// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite. 
			/// </summary> 
			public void Dispose() 
			{ 
				foreach (LogHandlerBase logHandler in this.Values) 
					lock (_lock) { logHandler.Dispose(); } 
			} 
 
			/// <summary> 
			/// Writes the specified level. 
			/// </summary> 
			/// <param name="level">The level.</param> 
			/// <param name="logMessage">The log message.</param> 
			internal LogRow Write(Logger20.Level level, string logMessage) 
			{ 
				LogRow logRow = null; 
				if (level == Logger20.Level.None) 
					return logRow; 
 
				foreach (LogHandlerBase logHandler in this.Values) 
					if (level >= logHandler.Level) 
					{ 
						lock (_lock) 
						{ 
							logRow = new LogRow(level, logMessage); 
							logHandler.Write(logRow); 
						} 
					} 
 
				return logRow; 
			} 
		} 
 
		/// <summary> 
		/// Class Logs. The dictionary of private log collection 
		/// <para> 
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.Logger20.Log}" /> 
		/// Implements the <see cref="System.IDisposable" /> 
		/// </para> 
		/// </summary> 
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.Logger20.Log}" /> 
		/// <seealso cref="System.IDisposable" /> 
		private class Logs : Dictionary<string, Log>, IDisposable 
		{ 
			public void Dispose() 
			{ 
				foreach (Log log in this.Values) 
					log.Dispose(); 
				this.Clear(); 
			} 
 
			/// <summary> 
			/// Writes the specified level. 
			/// </summary> 
			/// <param name="level">The level.</param> 
			/// <param name="logMessage">The log message.</param> 
			/// <returns>LogRow.</returns> 
			internal LogRow Write(Level level, string logMessage) 
			{ 
				LogRow retRow = null; 
				foreach (Log log in this.Values) 
					retRow = log.Write(level, logMessage); 
				return retRow; 
			} 
		} 
 
		public class Log : LogHandlers 
		{ 
			public Log(string name) 
			{ 
				this.Name = name; 
			} 
 
			public string Name { get; } 
 
			/// <summary> 
			/// Debugs the specified log message. 
			/// </summary> 
			/// <param name="logMessage">The log message.</param> 
			public LogRow Debug(string logMessage) { return base.Write(Level.Debug, logMessage); } 
 
			/// <summary> 
			/// Informations the specified log message. 
			/// </summary> 
			/// <param name="logMessage">The log message.</param> 
			public LogRow Info(string logMessage) { return base.Write(Level.Info, logMessage); } 
 
			/// <summary> 
			/// Warnings the specified log message. 
			/// </summary> 
			/// <param name="logMessage">The log message.</param> 
			public LogRow Warning(string logMessage) { return base.Write(Level.Warning, logMessage); } 
 
			/// <summary> 
			/// Errors the specified log message. 
			/// </summary> 
			/// <param name="logMessage">The log message.</param> 
			public LogRow Error(string logMessage) { return base.Write(Level.Error, logMessage); } 
 
			/// <summary> 
			/// Criticals the specified log message. 
			/// </summary> 
			/// <param name="logMessage">The log message.</param> 
			public LogRow Critical(string logMessage) { return base.Write(Level.Critical, logMessage); } 
 
		} 
 
	} 
} 
