﻿// ***********************************************************************
// Assembly         : Logger
// Author           : mauro.luigi.cortese
// Created          : 03-16-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 03-16-2020
// ***********************************************************************
// <copyright file="Logger.cs" company="">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class Logger20 
	/// Implements the <see cref="Mlc.Shell.SingletonBase{Mlc.Shell.IO.Logger20}" />
	/// Implements the <see cref="System.IDisposable" />
	/// </summary>
	/// <seealso cref="Mlc.Shell.SingletonBase{Mlc.Shell.IO.Logger20}" />
	/// <seealso cref="System.IDisposable" />
	public class Logger20 : SingletonBase<Logger20>, IDisposable
	{

		/// <summary>
		/// The start
		/// </summary>
		public const string Start = "START";

		/// <summary>
		/// The start
		/// </summary>
		public const string Stop = "STOP";

		#region Enumerations
		/// <summary>
		/// Enumeration of the importance level of log messages
		/// </summary>
		public enum Level
		{
			/// <summary>
			/// No log message
			/// </summary>
			None = 0,
			/// <summary>
			/// Messages useful for debugging
			/// </summary>
			Debug = 10,
			/// <summary>
			/// Normal but significant conditions
			/// </summary>
			Info = 20,
			/// <summary>
			/// Notices to pay attention to
			/// </summary>
			Warning = 30,
			/// <summary>
			/// General errors
			/// </summary>
			Error = 40,
			/// <summary>
			/// Critical conditions
			/// </summary>
			Critical = 50,
		}
		#endregion

		#region Fields
		/// <summary>
		/// The logs
		/// </summary>
		private readonly Logs logs = new Logs();
		#endregion

		#region Public

		#region Properties		
		/// <summary>
		/// Gets or sets the default name.
		/// </summary>
		/// <value>The default name.</value>
		public string DefaultName { get; set; }
		/// <summary>
		/// Gets the <see cref="Log"/> with the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>Log.</returns>
		public virtual Log DefaultLog
		{
			get
			{
				// if logger was not initilized
				if (string.IsNullOrEmpty(this.DefaultName))
				{
					this.DefaultName = new Mlc.Shell.SysInfo().ApplicationName;
					if (!this.logs.ContainsKey(this.DefaultName))
					{
						// initializes the dafault logger
						LogHandlerFile logFileHandler = (LogHandlerFile)Logger20.AddHandler(
							new LogHandlerFile(
								Logger20.Level.Info,
								this.DefaultName
								));
					}
				}

				return this.logs.ContainsKey(this.DefaultName) ? this.logs[this.DefaultName] : null;
			}
		}

		/// <summary>
		/// Gets the <see cref="Log"/> with the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>Log.</returns>
		public virtual Log this[string name] { get { return this.logs[name]; } }
		#endregion

		#region Properties static
		/// <summary>
		/// Gets the start time.
		/// </summary>
		/// <value>The start time.</value>
		public static DateTime StartLogger { get; private set; }

		/// <summary>
		/// Gets the time stamp.
		/// </summary>
		/// <value>The time stamp.</value>
		public static DateTime TimeStamp { get => DateTime.Now; }

		/// <summary>
		/// Gets the elapsed time.
		/// </summary>
		/// <value>The elapsed time.</value>
		public static TimeSpan ElapsedTime { get { return StartLogger != default ? TimeStamp - StartLogger : default; } }

		/// <summary>
		/// Gets or sets the current log level.
		/// </summary>
		/// <value>The current log level.</value>
		public static Level CurrentLogLevel { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [trim line].
		/// </summary>
		/// <value><c>true</c> if [trim line]; otherwise, <c>false</c>.</value>
		public static bool TrimLine { get; set; } = true;

		#endregion

		#region Method Static
		/// <summary>
		/// Gets the instance of this singleton.
		/// </summary>
		/// <returns>T.</returns>
		public new static Logger20 GetInstance()
		{
			Logger20 logger = SingletonBase<Logger20>.GetInstance();
			StartLogger = DateTime.Now;
			return logger;
		}

		/// <summary>
		/// Adds the handler.
		/// <para>
		/// Any handler added with this static method is placed in the dictionary
		/// of the instance that will be called with the static method GetInstance ()
		/// </para>
		/// </summary>
		/// <param name="logHandlerBase">The log handler base.</param>
		public static LogHandlerBase AddHandler(LogHandlerBase logHandlerBase)
		{
			return Logger20.AddHandler(null, logHandlerBase);
		}

		/// <summary>
		/// Adds the handler.
		/// </summary>
		/// <param name="keyLog">The key log.</param>
		/// <param name="logHandlerBase">The log handler base.</param>
		public static LogHandlerBase AddHandler(string keyLog, LogHandlerBase logHandlerBase)
		{
			//Logger20.GetInstance().Handlers.Add(logHandlerBase);
			Logger20 logger = Logger20.GetInstance();

			if (string.IsNullOrEmpty(logger.DefaultName))
				logger.DefaultName = logHandlerBase.Name;


			if (string.IsNullOrEmpty(keyLog))
				keyLog = logger.DefaultName;

			Log log;

			if (!logger.logs.ContainsKey(keyLog))
			{
				log = new Log(keyLog);
				logger.logs.Add(log.Name, log);
			}
			log = logger.logs[keyLog];

			log.Add(logHandlerBase);
			return logHandlerBase;
		}
		#endregion

		#region Method
		/// <summary>
		/// Debugs the specified log message.
		/// </summary>
		/// <param name="objectToLog">The object to log.</param>
		/// <returns>LogRow.</returns>
		public LogRow Debug(object objectToLog) { return this.logs.Write(Level.Debug, objectToLog); }

		/// <summary>
		/// Informations the specified log message.
		/// </summary>
		/// <param name="objectToLog">The object to log.</param>
		/// <returns>LogRow.</returns>
		public LogRow Info(object objectToLog) { return this.logs.Write(Level.Info, objectToLog); }

		/// <summary>
		/// Warnings the specified log message.
		/// </summary>
		/// <param name="objectToLog">The object to log.</param>
		/// <returns>LogRow.</returns>
		public LogRow Warning(object objectToLog) { return this.logs.Write(Level.Warning, objectToLog); }

		/// <summary>
		/// Errors the specified log message.
		/// </summary>
		/// <param name="objectToLog">The object to log.</param>
		/// <returns>LogRow.</returns>
		public LogRow Error(object objectToLog) { return this.logs.Write(Level.Error, objectToLog); }

		/// <summary>
		/// Criticals the specified log message.
		/// </summary>
		/// <param name="objectToLog">The object to log.</param>
		/// <returns>LogRow.</returns>
		public LogRow Critical(object objectToLog) { return this.logs.Write(Level.Critical, objectToLog); }


		/// <summary>
		/// Creates new line.
		/// </summary>
		/// <param name="repeat">The repeat.</param>
		public void NewLine(int repeat = 1) { this.logs.NewLine(repeat); }

		/// <summary>
		/// Creates new line of specified char.
		/// </summary>
		public void NewLine(char ch) { this.logs.NewLine(ch); }

		/// <summary>
		/// Release all log handlers
		/// </summary>
		public void Dispose()
		{
			this.logs.Dispose();
			this.DefaultName = "";
			Logger20.StartLogger = default;
		}
		#endregion

		#endregion

		#region Embedded Types

		/// <summary>
		/// Class LogHandlers.
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.LogHandlerBase}" />
		public class LogHandlers : Dictionary<string, LogHandlerBase>, IDisposable
		{

			#region Fields
			/// <summary>
			/// The lock
			/// </summary>
			private static object _lock = new object();

			/// <summary>
			/// The memory level
			/// </summary>
			private Level memLevel = Level.None;
			#endregion

			#region Public

			#region Method
			/// <summary>
			/// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite.
			/// </summary>
			public void Dispose()
			{
				foreach (LogHandlerBase logHandler in this.Values)
					lock (_lock) { logHandler.Dispose(); }
			}
			#endregion

			#endregion

			#region Internal

			#region Method
			/// <summary>
			/// Writes the specified level.
			/// </summary>
			/// <param name="level">The level.</param>
			/// <param name="objectToLog">The object to log.</param>
			/// <returns>LogRow.</returns>
			internal LogRow Write(Logger20.Level level, object objectToLog)
			{
				this.memLevel = level;

				LogRow logRow = null;
				if (level == Logger20.Level.None || objectToLog == null)
					return logRow;

				foreach (LogHandlerBase logHandler in this.Values)
					if (level >= logHandler.Level)
					{
						lock (_lock)
						{
							logRow = new LogRow(level, objectToLog);
							logHandler.Write(logRow);
						}
					}
				return logRow;
			}

			/// <summary>
			/// Writes the specified ch.
			/// </summary>
			/// <param name="ch">The ch.</param>
			internal void Write(char ch)
			{
				if (this.memLevel == Logger20.Level.None)
					return;

				foreach (LogHandlerBase logHandler in this.Values)
					if (memLevel >= logHandler.Level)
						lock (_lock)
							logHandler.Write(new LogRow(memLevel, ch));
			}


			/// <summary>
			/// Writes the specified level.
			/// </summary>
			internal LogRow Write()
			{
				LogRow logRow = null;
				if (this.memLevel == Logger20.Level.None)
					return logRow;

				foreach (LogHandlerBase logHandler in this.Values)
					if (memLevel >= logHandler.Level)
					{
						lock (_lock)
						{
							logRow = new LogRow(memLevel, "");
							logHandler.Write(logRow);
						}
					}
				return logRow;
			}

			/// <summary>
			/// Adds the specified log handler.
			/// </summary>
			/// <param name="logHandler">The log handler.</param>
			/// <returns>LogHandlerBase.</returns>C
			internal LogHandlerBase Add(LogHandlerBase logHandler)
			{
				if (!this.ContainsKey(logHandler.Name))
					this.Add(logHandler.Name, logHandler);

				return this[logHandler.Name];
			}
			#endregion

			#endregion

		}

		/// <summary> 
		/// Class Logs. The dictionary of private log collection 
		/// <para> 
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.Logger20.Log}" /> 
		/// Implements the <see cref="System.IDisposable" /> 
		/// </para> 
		/// </summary> 
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.Logger20.Log}" /> 
		/// <seealso cref="System.IDisposable" /> 
		private class Logs : Dictionary<string, Log>, IDisposable
		{
			#region Public

			#region Method
			/// <summary>
			/// Disposes this instance.
			/// </summary>
			public void Dispose()
			{
				foreach (Log log in this.Values)
					log.Dispose();
				this.Clear();
			}
			#endregion

			#endregion

			#region Internal

			#region Method
			/// <summary>
			/// Writes the specified level.
			/// </summary>
			/// <param name="level">The level.</param>
			/// <param name="objectToLog">The object to log.</param>
			/// <returns>LogRow.</returns>
			internal LogRow Write(Level level, object objectToLog)
			{
				LogRow retRow = null;
				foreach (Log log in this.Values)
					retRow = log.Write(level, objectToLog);
				return retRow;
			}

			/// <summary>
			/// Creates new line.
			/// </summary>
			/// <param name="repeat">The repeat.</param>
			internal void NewLine(int repeat = 1)
			{
				foreach (Log log in this.Values)
					log.NewLine(repeat);
			}

			/// <summary>
			/// Creates new line.
			/// </summary>
			/// <param name="ch">The ch.</param>
			internal void NewLine(char ch)
			{
				foreach (Log log in this.Values)
					log.NewLine(ch);
			}

			#endregion
			#endregion

		}

		/// <summary>
		/// Class Log.
		/// Implements the <see cref="Mlc.Shell.IO.Logger20.LogHandlers" />
		/// </summary>
		/// <seealso cref="Mlc.Shell.IO.Logger20.LogHandlers" />
		public class Log : LogHandlers
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Log"/> class.
			/// </summary>
			/// <param name="name">The name.</param>
			public Log(string name) { this.Name = name; }
			#endregion

			#region Public

			#region Properties
			/// <summary>
			/// Gets the name.
			/// </summary>
			/// <value>The name.</value>
			public string Name { get; }
			#endregion

			#region Method
			/// <summary> 
			/// Debugs the specified log message. 
			/// </summary> 
			/// <param name="logObject">The log message.</param> 
			public LogRow Debug(object logObject) { return base.Write(Level.Debug, logObject); }

			/// <summary> 
			/// Informations the specified log message. 
			/// </summary> 
			/// <param name="logObject">The log message.</param> 
			public LogRow Info(object logObject) { return base.Write(Level.Info, logObject); }

			/// <summary> 
			/// Warnings the specified log message. 
			/// </summary> 
			/// <param name="logObject">The log message.</param> 
			public LogRow Warning(object logObject) { return base.Write(Level.Warning, logObject); }

			/// <summary> 
			/// Errors the specified log message. 
			/// </summary> 
			/// <param name="logObject">The log message.</param> 
			public LogRow Error(object logObject) { return base.Write(Level.Error, logObject); }

			/// <summary> 
			/// Criticals the specified log message. 
			/// </summary> 
			/// <param name="logObject">The log message.</param> 
			public LogRow Critical(object logObject) { return base.Write(Level.Critical, logObject); }

			/// <summary>
			/// Creates new line.
			/// </summary>
			public void NewLine(int repeat = 1)
			{
				for (int i = 0; i < repeat; i++)
					base.Write();
			}

			/// <summary>
			/// Creates new line.
			/// </summary>
			public void NewLine(char ch)
			{
				base.Write(ch);
			}

			#endregion

			#endregion
		}
		#endregion
	}
}
