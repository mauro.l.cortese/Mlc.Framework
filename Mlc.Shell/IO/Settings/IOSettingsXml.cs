// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="IOSettingsXml.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Windows.Forms;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Serialize or deserialize a instance that inherit from <see cref="Mlc.Shell.IO.Settings" />
	/// </summary>
	internal class IOSettingsXml
	{
		#region Constants       
		/// <summary>
		/// The string database connection settings
		/// </summary>
		private const string STR_DbConnectionSettings = "{0}\\DbConnections.xml";
		/// <summary>
		/// The string database connections
		/// </summary>
		private const string STR_DbConnections = "DbConnections";
		/// <summary>
		/// The string database connection set
		/// </summary>
		private const string STR_DbConnectionSet = "DbConnectionSet";
		/// <summary>
		/// The string name
		/// </summary>
		private const string STR_Name = "Name";
		/// <summary>
		/// The string database connection
		/// </summary>
		private const string STR_DbConnection = "DbConnection";
		/// <summary>
		/// The string password
		/// </summary>
		private const string STR_Password = "password";
		/// <summary>
		/// The string key
		/// </summary>
		private const string STR_Key = "Key";
		/// <summary>
		/// The string value
		/// </summary>
		private const string STR_Value = "Value";
		/// <summary>
		/// The string key value
		/// </summary>
		private const string STR_KeyValue = "KeyValue";
		/// <summary>
		/// The size forms
		/// </summary>
		private const string STR_SizeFormsXmlNode = "SizeForms";
		/// <summary>
		/// The size form file XML
		/// </summary>
		private const string STR_SizeFormFileXml = "\\Layout\\SizeForms.xml";
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The client setting family
		/// </summary>
		private Dictionary<string, ListProperty> clientSettingFamily = new Dictionary<string, ListProperty>();
		/// <summary>
		/// The user roaming setting family
		/// </summary>
		private Dictionary<string, ListProperty> userRoamingSettingFamily = new Dictionary<string, ListProperty>();
		/// <summary>
		/// The server setting family
		/// </summary>
		private Dictionary<string, ListProperty> serverSettingFamily = new Dictionary<string, ListProperty>();
		/// <summary>
		/// The settings
		/// </summary>
		private Settings settings = null;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="IOSettingsXml" /> class.
		/// </summary>
		/// <param name="settings">The settings.</param>
		internal IOSettingsXml(Settings settings)
		{
			this.settings = settings;
		}
		#endregion

		#region Public
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Deserializes the specified setting files.
		/// </summary>
		/// <param name="settingFiles">The setting files.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		internal bool Deserialize(SettingFiles settingFiles)
		{
			if (!settingsFoldersTest(settingFiles)) return false;

			EnumOperator<SettingFiles> eo = new EnumOperator<SettingFiles>();

			this.readSettingsProperties();
			Settings.SettingsFolders folders = this.settings.Folders;
			//if (eo.Contains(settingFiles, SettingFiles.Local))
			this.read(clientSettingFamily, folders.FileSettingClient);

			if (this.settings.DefaultSettings != null && !string.IsNullOrEmpty(this.settings.DefaultSettings.RoamingFolder))
			{
				if (!Directory.Exists(this.settings.DefaultSettings.RoamingFolder))
					try
					{
						Directory.CreateDirectory(this.settings.DefaultSettings.RoamingFolder);
					}
					catch (Exception ex) 
					{ 
						throw new IOSettingExceptions.WrongRoamingFolderException(ex,this.settings.DefaultSettings.RoamingFolder);
					}

				if (Directory.Exists(this.settings.DefaultSettings.RoamingFolder))
					settings.SetRoamingFolder(this.settings.DefaultSettings.RoamingFolder);
			}

			if (eo.Contains(settingFiles, SettingFiles.RoamingUser))
				this.read(userRoamingSettingFamily, folders.FileServerDomainUser);

			if (eo.Contains(settingFiles, SettingFiles.Roaming))
				this.read(serverSettingFamily, folders.FileSettingServer);

			this.readDbConnections(folders.FolderServerDomain);
			return true;
		}


		/// <summary>
		/// Serializes the specified setting files.
		/// </summary>
		/// <param name="settingFiles">The setting files.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		internal bool Serialize(SettingFiles settingFiles)
		{
			if (!settingsFoldersTest(settingFiles)) return false;
			EnumOperator<SettingFiles> eo = new EnumOperator<SettingFiles>();
			this.readSettingsProperties();
			Settings.SettingsFolders folders = this.settings.Folders;

			if (eo.Contains(settingFiles, SettingFiles.Local))
				this.write(clientSettingFamily, folders.FileSettingClient);

			if (eo.Contains(settingFiles, SettingFiles.RoamingUser))
				this.write(userRoamingSettingFamily, folders.FileServerDomainUser);

			if (eo.Contains(settingFiles, SettingFiles.Roaming))
				this.write(serverSettingFamily, folders.FileSettingServer);

			this.writeDbConnections(folders.FolderServerDomain);
			return true;
		}

		/// <summary>
		/// Saves the size.
		/// </summary>
		/// <param name="settingsForm">The settings form.</param>
		/// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
		internal bool SerializeFormSize(SettingsForm settingsForm)
		{
			if (settingsForm == null)
				return false;
			try
			{
				XmlDocument XDoc = this.getXmlSizeForms();
				XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", settingsForm.Form.Name));
				bool save = true;
				if (xNode != null)
				{
					save = xNode.Attributes["Size"].Value != settingsForm.Size.ToString();
					xNode.Attributes["Size"].Value = settingsForm.Size.ToString();

					save = save || xNode.Attributes["Location"].Value != settingsForm.Location.ToString();
					xNode.Attributes["Location"].Value = settingsForm.Location.ToString();

					save = save || xNode.Attributes["WindowState"].Value != settingsForm.WindowState.ToString();
					xNode.Attributes["WindowState"].Value = settingsForm.WindowState.ToString();
				}
				else
				{
					save = true;
					XmlNode n = XDoc.CreateNode(XmlNodeType.Element, "Size", "");

					XmlAttribute a = XDoc.CreateAttribute("Key");
					a.Value = settingsForm.Form.Name;
					n.Attributes.Append(a);

					a = XDoc.CreateAttribute("Size");
					a.Value = settingsForm.Size.ToString();
					n.Attributes.Append(a);

					a = XDoc.CreateAttribute("Location");
					a.Value = settingsForm.Location.ToString();
					n.Attributes.Append(a);

					a = XDoc.CreateAttribute("WindowState");
					a.Value = settingsForm.WindowState.ToString();
					n.Attributes.Append(a);

					XDoc.DocumentElement.AppendChild(n);
				}
				if (save)
					XDoc.Save(getFileXmlFormSize());

				return true;
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
				return false;
			}
		}

		/// <summary>
		/// Upload the dimensions of a form (assign it immediately to the form)
		/// </summary>
		/// <param name="settingsForm">The settings form.</param>
		internal bool DeserializeFormSize(SettingsForm settingsForm)
		{
			if (settingsForm == null)
				return false;
			try
			{
				XmlDocument XDoc = this.getXmlSizeForms();
				XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", settingsForm.Form.Name));
				if (xNode != null)
				{
					settingsForm.Size = TypeConvert<Size>.GetValue(xNode.Attributes["Size"].Value, default(Size));
					settingsForm.Location = TypeConvert<Point>.GetValue(xNode.Attributes["Location"].Value, default(Point));
					settingsForm.WindowState = TypeConvert<FormWindowState>.GetValue(xNode.Attributes["WindowState"].Value, default(FormWindowState));
				}
				return true;
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
				return false;
			}
		}


		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method

		/// <summary>
		/// Reads the settings properties.
		/// </summary>
		private void readSettingsProperties()
		{
			// Ottengo tutte le Propriet� dell'istanza dei settaggi
			PropertyInfo[] propertyInfos = this.settings.GetType().GetProperties();

			// ciclo le Propriet� ottenute e le ragguppo nei dizionari client e server per liste di categorie
			foreach (PropertyInfo propertyInfo in propertyInfos)
			{
				// ottengo la categoria
				string category = "";

				object[] attribCats = propertyInfo.GetCustomAttributes(typeof(SubSettingsAttribute), true);
				if (attribCats.Length == 1)
					category = propertyInfo.Name;

				string desc = getDescription(propertyInfo);

				// ottengo le Propriet� della categoria
				foreach (PropertyInfo property in propertyInfo.PropertyType.GetProperties())
				{
					// considero solo le Propriet� che si possono leggere e scrivere
					if (property.CanWrite && property.CanRead)
					{
						bool crypto = checkAttrib(typeof(CryptoSettingsAttribute), property);

						// CLIENT
						if (checkAttrib(typeof(ClientSettingAttribute), property))
							regProperty(clientSettingFamily, propertyInfo, category, desc, property, crypto);

						// USER ROAMING
						if (checkAttrib(typeof(RoamingUserSettingAttribute), property))
							regProperty(userRoamingSettingFamily, propertyInfo, category, desc, property, crypto);

						// SERVER
						if (checkAttrib(typeof(ServerSettingAttribute), property))
							regProperty(serverSettingFamily, propertyInfo, category, desc, property, crypto);
					}
				}
			}
		}

		/// <summary>
		/// Checks the attribute.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="property">The property.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		private bool checkAttrib(Type type, PropertyInfo property)
		{
			return property.GetCustomAttributes(type, true).Length == 1 ? true : false;
		}

		/// <summary>
		/// Regs the property.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="propertyInfo">The property information.</param>
		/// <param name="category">The category.</param>
		/// <param name="desc">The desc.</param>
		/// <param name="property">The property.</param>
		/// <param name="crypto">if set to <c>true</c> [crypto].</param>
		private void regProperty(Dictionary<string, ListProperty> dictionary, PropertyInfo propertyInfo, string category, string desc, PropertyInfo property, bool crypto)
		{
			if (!dictionary.ContainsKey(category))
				dictionary.Add(category, new ListProperty(propertyInfo.GetValue(this.settings, null), desc, crypto));
			dictionary[category].Add(property);
		}

		/// <summary>
		/// Gets the description.
		/// </summary>
		/// <param name="propertyInfo">The property information.</param>
		/// <returns>System.String.</returns>
		private string getDescription(PropertyInfo propertyInfo)
		{
			object[] descs = propertyInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
			if (descs.Length == 1)
				return ((DescriptionAttribute)descs[0]).Description;
			return string.Empty;
		}

		/// <summary>
		/// Writes the specified dic cat properties.
		/// </summary>
		/// <param name="dicCatProperties">The dic cat properties.</param>
		/// <param name="file">The file.</param>
		private void write(Dictionary<string, ListProperty> dicCatProperties, FileSystemMgr file)
		{
			try
			{
				if (dicCatProperties.Count == 0)
					return;

				if (!file.Create(FileSystemTypes.File).Exists) return;

				using (XmlFileWriter writer = new XmlFileWriter(file.Path))
				{
					writer.OpenXML("Settings");
					writer.OpenNode("IDSettings");
					writer.AddAttribute("Guid", this.settings.IdSettings.ToString());
					writer.CloseNode();

					// ciclo per tutte le categorie
					foreach (KeyValuePair<string, ListProperty> key in dicCatProperties)
					{
						writer.WriteCommento(key.Value.Description);
						writer.OpenNode("Category");
						writer.AddAttribute(STR_Name, key.Key);
						foreach (PropertyInfo property in key.Value.Values)
						{
							key.Value.Crypt = checkAttrib(typeof(CryptoSettingsAttribute), property);

							writer.WriteCommento(this.getDescription(property));
							writer.OpenNode("Setting");
							writer.AddAttribute(STR_Name, property.Name);

							object value = null;
							if (key.Value.Instance != null)
								value = property.GetValue(key.Value.Instance, null);

							if (value is DateTime)
								value = ((DateTime)value).Serialize();
							if (value is Font)
								value = ((Font)value).FontToString();

							if (value == null) value = string.Empty;
							if (!key.Value.Crypt || string.IsNullOrEmpty(value.ToString()))
								writer.AddAttribute(STR_Value, value.ToString());
							else
								// writer.AddAttribute(STR_Value, value.ToString().ToByteStringFormat());
								writer.AddAttribute(STR_Value, value.ToString().Encrypt().ToByteStringFormat());
							writer.AddAttribute("Type", property.PropertyType.FullName);
							writer.CloseNode();
						}
						writer.CloseNode();
					}
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

		}

		/// <summary>
		/// Reads the specified dic cat properties.
		/// </summary>
		/// <param name="dicCatProperties">The dic cat properties.</param>
		/// <param name="file">The file.</param>
		/// <exception cref="System.Exception">
		/// </exception>
		/// <exception cref="Exception"></exception>
		private void read(Dictionary<string, ListProperty> dicCatProperties, FileSystemMgr file)
		{
			if (!file.Exists || file.Size == 0) return;

			try
			{
				XmlDocument xdoc = new XmlDocument();

				xdoc.Load(file.Path);

				string ids = xdoc.SelectSingleNode("//Settings/IDSettings").Attributes["Guid"].Value;

				Guid id = new Guid(xdoc.SelectSingleNode("//Settings/IDSettings").Attributes["Guid"].Value);
				if (id != this.settings.IdSettings)
					throw new Exception(string.Format(Resources.Messages.WrongSettingsID, file, id, this.settings.IdSettings.ToString()));

				ReflectionTools rt = new ReflectionTools();

				foreach (KeyValuePair<string, ListProperty> item in dicCatProperties)
				{
					XmlNodeList xNodes = xdoc.SelectNodes(string.Format("//Settings/Category[@Name='{0}']/Setting", item.Key));

					foreach (XmlNode node in xNodes)
					{
						string Name = node.Attributes[STR_Name].Value;

						string Value = string.Empty;

						PropertyInfo pInfo = item.Value[Name];
						bool crypted = checkAttrib(typeof(CryptoSettingsAttribute), pInfo);

						if (!crypted)
							Value = node.Attributes[STR_Value].Value;
						else
							try { Value = node.Attributes[STR_Value].Value.FromBytesStringFormat().Decrypt(); }
							catch
							{
								try { Value = node.Attributes[STR_Value].Value; }
								catch { throw new Exception(string.Format(Resources.Messages.DecryptSettingsError, Name)); }
							}

						string Type = node.Attributes["Type"].Value;

						try
						{
							if (item.Value.ContainsKey(Name))
								rt.SetProperty(item.Value.Instance, item.Value[Name], Value);
						}
						catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
					}
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Reads the database connections.
		/// </summary>
		/// <param name="folderServerDomain">The folder server domain.</param>
		private void readDbConnections(FileSystemMgr folderServerDomain)
		{
			string fileXml = String.Format(STR_DbConnectionSettings, folderServerDomain);
			if (new FileSystemMgr(fileXml).Exists)
			{
				this.settings.DbConns.Clear();
				XmlDocument xdoc = new XmlDocument();

				xdoc.Load(fileXml);
				XmlNodeList nodes = xdoc.SelectNodes("//" + STR_DbConnectionSet);
				foreach (XmlNode node in nodes)
				{
					Settings.DbConnections dbcns = new Settings.DbConnections();
					dbcns.SetConnectionName = node.AttributeValue<string>(STR_Name);


					XmlNodeList nodechilds = node.SelectNodes(STR_DbConnection);
					foreach (XmlNode nodec in nodechilds)
					{
						DbConnectionStringBuilder dbcsb = new DbConnectionStringBuilder();
						string name = nodec.AttributeValue<string>(STR_Name);

						XmlNodeList nodeKs = nodec.SelectNodes(STR_KeyValue);
						foreach (XmlNode nodeK in nodeKs)
						{
							string key = nodeK.AttributeValue<string>(STR_Key);
							object value = nodeK.AttributeValue<object>(STR_Value);

							if (key.ToLower() == STR_Password)
								try
								{
									value = value.ToString().FromBytesStringFormat().Decrypt();
								}
								catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
							dbcsb.Add(key, value);

						}
						dbcns.Add(name, dbcsb);
					}
					this.settings.DbConns.Add(dbcns);
				}
			}
		}

		/// <summary>
		/// Writes the database connections.
		/// </summary>
		/// <param name="folderServerDomain">The folder server.</param>
		private void writeDbConnections(FileSystemMgr folderServerDomain)
		{
			if (this.settings.DbConns.Count != 0)
				using (XmlFileWriter writer = new XmlFileWriter(String.Format(STR_DbConnectionSettings, folderServerDomain)))
				{
					writer.OpenXML(STR_DbConnections);
					foreach (KeyValuePair<string, Settings.DbConnections> setConn in this.settings.DbConns)
					{
						writer.OpenNode(STR_DbConnectionSet);
						writer.AddAttribute(STR_Name, setConn.Key);

						foreach (KeyValuePair<string, DbConnectionStringBuilder> item in setConn.Value)
						{
							writer.OpenNode(STR_DbConnection);
							writer.AddAttribute(STR_Name, item.Key);
							foreach (KeyValuePair<string, object> o in item.Value)
							{
								writer.OpenNode(STR_KeyValue);
								string key = o.Key;
								string value;

								if (key.ToLower() == STR_Password)
									value = o.Value.ToString().Encrypt().ToByteStringFormat();
								else
									value = o.Value.ToString();
								writer.AddAttribute(STR_Key, o.Key);
								writer.AddAttribute(STR_Value, value);

								writer.CloseNode();
							}
							writer.CloseNode();
						}
						writer.CloseNode();
					}
					writer.CloseXML();
				}
		}

		/// <summary>
		/// Settingses the folders test.
		/// </summary>
		/// <param name="settingFiles">The setting files.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		private bool settingsFoldersTest(SettingFiles settingFiles)
		{
			EnumOperator<SettingFiles> eo = new EnumOperator<SettingFiles>();
			Settings.SettingsFolders folders = this.settings.Folders;

			bool instance = this.settings != null;
			bool local = true;
			bool roaming = true;
			bool roamingUser = true;

			if (eo.Contains(settingFiles, SettingFiles.Local))
				local = folders.FileSettingClient.Create(FileSystemTypes.File).Exists;

			if (eo.Contains(settingFiles, SettingFiles.Roaming))
				roaming = folders.FileSettingServer.Create(FileSystemTypes.File).Exists;

			if (eo.Contains(settingFiles, SettingFiles.RoamingUser))
				roamingUser = folders.FileServerDomainUser.Create(FileSystemTypes.File).Exists;

			bool retVal = instance & local & roaming & roamingUser;
			return retVal;
		}

		/// <summary>
		/// Gets the XML size forms.
		/// </summary>
		/// <returns>XmlDocument.</returns>
		private XmlDocument getXmlSizeForms()
		{
			string fileSize = getFileXmlFormSize();
			XmlDocument XDoc = new XmlDocument();
			if (!File.Exists(fileSize))
				using (XmlFileWriter writer = new XmlFileWriter(fileSize))
					writer.OpenXML(STR_SizeFormsXmlNode);
			XDoc.Load(fileSize);
			return XDoc;
		}

		/// <summary>
		/// Gets the size of the file XML form.
		/// </summary>
		/// <returns>System.String.</returns>
		private string getFileXmlFormSize()
		{
			string fileSize = string.Concat(
				this.settings.Folders.FileServerDomainUser.FsoParentFolder.Path,
				string.Format(STR_SizeFormFileXml)
				);
			string folder = Path.GetDirectoryName(fileSize);
			FileSystemMgr fsm = new FileSystemMgr(folder);
			if (!fsm.Exists)
				fsm.Create(FileSystemTypes.Directory);
			return fileSize;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		/// <summary>
		/// Class ListProperty.
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.Reflection.PropertyInfo}" />
		private class ListProperty : Dictionary<string, PropertyInfo>
		{
			/// <summary>
			/// Adds the specified property.
			/// </summary>
			/// <param name="property">The property.</param>
			public void Add(PropertyInfo property)
			{
				base.Add(property.Name, property);
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="ListProperty" /> class.
			/// </summary>
			/// <param name="instance">The instance.</param>
			/// <param name="description">The description.</param>
			/// <param name="crypt">if set to <c>true</c> [crypt].</param>
			public ListProperty(object instance, string description, bool crypt)
			{
				this.Instance = instance;
				this.Description = description;
				this.Crypt = crypt;
			}

			/// <summary>
			/// Gets or sets the instance.
			/// </summary>
			/// <value>The instance.</value>
			public object Instance { get; set; }

			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="ListProperty" /> is crypt.
			/// </summary>
			/// <value><c>true</c> if crypt; otherwise, <c>false</c>.</value>
			public bool Crypt { get; set; }

			/// <summary>
			/// Gets or sets the description.
			/// </summary>
			/// <value>The description.</value>
			public string Description { get; set; }
		}
		#endregion
	}
}
