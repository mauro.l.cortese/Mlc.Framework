
using System.Collections.Generic;
using System.Data.Common;

namespace Mlc.Shell.IO
{
    /// <summary>Inherit from this class to implement types that will have to manage the settings of an application</summary>
    public abstract partial class SettingBase
    {
        /// <summary>
        /// Class DbConnections for one or more DB connection strings  
        /// </summary>
        /// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.Data.Common.DbConnectionStringBuilder}" />
        public class DbConnections : Dictionary<string, DbConnectionStringBuilder>
        {
            /// <summary>
            /// The set connection name
            /// </summary>
            private string setConnectionName = string.Empty;
            /// <summary>
            /// Gets or sets the name of the set connection.
            /// </summary>
            /// <value>The name of the set connection.</value>
            public string SetConnectionName { get { return this.setConnectionName; } set { this.setConnectionName = value; } }
        }
    }
}
