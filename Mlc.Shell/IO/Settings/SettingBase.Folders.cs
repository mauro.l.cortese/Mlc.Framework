using System;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Mlc.Shell.IO
{
    /// <summary>Inherit from this class to implement types that will have to manage the settings of an application</summary>
    public abstract partial class SettingBase
    {
        /// <summary>
        /// Stores the paths of the files containing the application settings
        /// </summary>
        public class SettingsFolders
        {
            #region Constants
            /// <summary>
            /// The local setting suffix
            /// </summary>
            private const string LocalSettingSuffix = "local";
            /// <summary>
            /// The remote setting suffix
            /// </summary>
            private const string RemoteSettingSuffix = "server";
            /// <summary>
            /// The user setting suffix
            /// </summary>
            private const string UserSettingSuffix = "user";
            /// <summary>
            /// The setting ext
            /// </summary>
            private const string SettingExt = "xml";
            #endregion

            #region Enumerations
            #endregion

            #region Fields            
            /// <summary>
            /// The setting base
            /// </summary>
            private SettingBase settingBase;
            #endregion

            #region Constructors
            public SettingsFolders(SettingBase settingBase)
            {
                this.settingBase = settingBase;
                this.Refresh();
            }


            #endregion

            #region Public
            #region Properties
            /// <summary>
            /// Gets the file setting client.
            /// </summary>
            /// <value>The file setting client.</value>
            public FileSystemMgr FileSettingClient { get; private set; } = null;
            /// <summary>
            /// Gets the file setting user roaming.
            /// </summary>
            /// <value>The file setting user roaming.</value>
            public FileSystemMgr FileServerDomainUser { get; private set; } = null;
            /// <summary>
            /// Gets the file setting server.
            /// </summary>
            /// <value>The file setting server.</value>
            public FileSystemMgr FileSettingServer { get; private set; } = null;
            /// <summary>
            /// Gets the folder server.
            /// </summary>
            /// <value>The folder server.</value>
            public FileSystemMgr FolderServer { get; private set; } = null;
            /// <summary>
            /// Gets the folder server domain.
            /// </summary>
            /// <value>The folder server domain.</value>
            public FileSystemMgr FolderServerDomain { get; private set; } = null;
            /// <summary>
            /// Gets the folder server domain user.
            /// </summary>
            /// <value>The folder server domain user.</value>
            public FileSystemMgr FolderServerDomainUser { get; private set; } = null;
            /// <summary>
            /// Gets the folder client.
            /// </summary>
            /// <value>The folder client.</value>
            public FileSystemMgr FolderClient { get; private set; } = null;
            ///// <summary>
            ///// Gets the folder setting user roaming.
            ///// </summary>
            ///// <value>The folder setting user roaming.</value>
            //public string FolderSettingUserRoaming => Path.GetDirectoryName(this.FileServerDomainUser.Path);

            ///// <summary>
            ///// Gets the folder setting server.
            ///// </summary>
            ///// <value>The folder setting server.</value>
            //public string FolderSettingServer => Path.GetDirectoryName(this.FileSettingServer.Path);
            #endregion

            #region Method Static
            #endregion

            #region Method
            /// <summary>
            /// Refreshes this instance.
            /// </summary>
            public void Refresh() => this.SetPaths();

            /// <summary>
            /// Sets the roaming folder.
            /// </summary>
            /// <param name="serverFolderSetting">The server folder setting.</param>
            /// <exception cref="Exception"></exception>
            internal void SetRoamingFolder(string serverFolderSetting)
            {
                if (string.IsNullOrWhiteSpace(serverFolderSetting)) return;
                this.FolderServer = new FileSystemMgr(serverFolderSetting);
                if (!this.FolderServer.Create(FileSystemTypes.Directory).Exists)
                    throw new Exception(string.Format("Impossibile raggiungere o creare la cartella remota {0}", serverFolderSetting));
                this.Refresh();
            }
            #endregion

            #endregion

            #region Internal
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Protected
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method            
            #endregion
            #endregion

            #region Private
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method

            /// <summary>
            /// Initialize the default path where the settings files will be placed
            /// </summary>
            private void SetPaths()
            {
                //SettingsParameters parameters = this.settingBase.Parameters;

                this.FolderClient = new FileSystemMgr(string.Format("{0}\\xml", SettingBase.Parameters.ExecutableFolder));
                this.FileSettingClient = new FileSystemMgr(string.Format("{0}\\{1}.{2}.{3}.{4}", this.FolderClient, SettingBase.Parameters.DomainName, SettingBase.Parameters.SettingsName, LocalSettingSuffix, SettingExt));
                this.FolderServer = new FileSystemMgr(string.Format("{0}\\server", this.FolderClient));
                this.FileSettingServer = new FileSystemMgr(string.Format("{0}\\{1}.{2}.{3}.{4}", this.FolderServer, SettingBase.Parameters.DomainName, SettingBase.Parameters.SettingsName, RemoteSettingSuffix, SettingExt));
                this.FolderServerDomain = new FileSystemMgr(string.Format("{0}\\{1}", this.FolderServer, SettingBase.Parameters.DomainName));
                this.FolderServerDomainUser = new FileSystemMgr(string.Format("{0}\\{1}", this.FolderServerDomain, SettingBase.Parameters.UserName));
                this.FileServerDomainUser = new FileSystemMgr(string.Format("{0}\\{1}.{2}.{3}.{4}", this.FolderServerDomainUser, SettingBase.Parameters.DomainName, SettingBase.Parameters.SettingsName, UserSettingSuffix, SettingExt));


                //this.FolderSettingUserRoaming =new FileSystemMgr(string.Format("{0}\\{1}", this.folderServer, SettingBase.Parameters.DomainName));

                ///SysInfo syf = new SysInfo(SettingBase.Assembly);

                //this.folderClient = string.IsNullOrEmpty(parameters.SettingsName) ? folderClient : new FileSystemMgr(string.Concat(folderClient, "\\", parameters.SettingsName));

                //this.folderClient = new FileSystemMgr(this.parameters.ExecutableFolder);
                //StringBuilder folder = new StringBuilder();

                //folder.Append(folderClient.Path);
                //folder.Append("\\server");
                //if (!string.IsNullOrEmpty(this.parameters.SettingsName))
                //    folder.Append(string.Concat("\\", this.parameters.SettingsName));

                //this.folderServer = new FileSystemMgr(folder.ToString());

                //if (!this.FolderClient.Create(FileSystemTypes.Directory).Exists)
                //    throw new Exception(string.Format(Resources.Messages.RemoteFolderUnreachable, this.folderClient));


                //string app = Path.GetFileNameWithoutExtension(syf.ExecutableFile);
                //string domain = syf.DomainName.Replace(".", " ").GetUpperCamelCase().Replace("Local", "");

                /*
                this.folderServerDomain = new FileSystemMgr(string.Format("{0}\\{1}", this.folderServer.Path, domain));
                if (SettingBase.UsePcName)
                    this.fileSettingClient = new FileSystemMgr(string.Format("{0}\\{1}-{2}.{3}.{4}", this.FolderClient, domain, syf.ComputerName.ToUpper(), SettingSuffix, SettingExt));
                else
                    this.fileSettingClient = new FileSystemMgr(string.Format("{0}\\{1}.{2}.{3}", this.FolderClient, domain, SettingSuffix, SettingExt));
                this.fileSettingUserRoaming = new FileSystemMgr(string.Format("{0}\\{1}\\users\\{2}\\{3}\\Application.{4}.{5}", this.FolderServer, domain, syf.UserName, app, SettingSuffix, SettingExt));
                this.fileSettingServer = new FileSystemMgr(string.Format("{0}\\{1}.{2}.{3}", this.FolderServer, domain, SettingSuffix, SettingExt));
                */


            }
            #endregion

            #region Event Handlers - CommandsEngine
            #endregion

            #region Event Handlers
            #endregion
            #endregion

            #region Event Definitions
            #endregion

            #region Embedded Types
            #endregion
        }
    }
}
