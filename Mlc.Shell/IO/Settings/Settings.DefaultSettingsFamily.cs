// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 12-22-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Settings.DefaultSettingsFamily.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.ComponentModel;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Inherit from this class to implement types that will have to manage the settings of an application
    /// </summary>
    public abstract partial class Settings
    {
        /// <summary>
        /// Class FamilyDefaultSettings.
        /// </summary>
        /// <seealso cref="Mlc.Shell.IO.SettingsFamily" />
        public class DefaultSettingsFamily : SettingsFamily
        {

            #region Constants
            #endregion

            #region Enumerations
            #endregion

            #region Fields
            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="DefaultSettingsFamily" /> class.
            /// </summary>
            /// <param name="settings">The settings.</param>
            public DefaultSettingsFamily(Settings settings)
                : base(settings)
            {
            }

            #endregion

            #region Public
            #region Properties
            /// <summary>
            /// Gets or sets the roaming folder.
            /// Roamin folder is a common remote setting collections for more client application
            /// </summary>
            /// <value>The roaming folder.</value>
            [ClientSetting, Description("Cartella settaggi remoti")]
            public string RoamingFolder { get; set; }
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Internal
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Protected
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Private
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion

            #region Event Handlers - CommandsEngine
            #endregion

            #region Event Handlers
            #endregion
            #endregion

            #region Event Definitions
            #endregion

            #region Embedded Types
            #endregion
        }
    }
}
