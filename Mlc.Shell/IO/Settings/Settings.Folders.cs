// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 12-15-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Settings.Folders.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Inherit from this class to implement types that will have to manage the settings of an application
    /// </summary>
    public abstract partial class Settings
    {
        /// <summary>
        /// Stores the paths of the files containing the application settings
        /// </summary>
        /// <summary>
        /// Class SettingsFolders.
        /// </summary>
        /// <summary>
        /// Class SettingsFolders.
        /// </summary>
        public class SettingsFolders
        {
            #region Constants
            /// <summary>
            /// The local setting suffix
            /// </summary>
            /// <summary>
            /// The local setting suffix
            /// </summary>
            /// <summary>
            /// The local setting suffix
            /// </summary>
            private const string LocalSettingSuffix = "local";
            /// <summary>
            /// The remote setting suffix
            /// </summary>
            /// <summary>
            /// The remote setting suffix
            /// </summary>
            /// <summary>
            /// The remote setting suffix
            /// </summary>
            private const string RemoteSettingSuffix = "server";
            /// <summary>
            /// The user setting suffix
            /// </summary>
            /// <summary>
            /// The user setting suffix
            /// </summary>
            /// <summary>
            /// The user setting suffix
            /// </summary>
            private const string UserSettingSuffix = "user";
            /// <summary>
            /// The setting ext
            /// </summary>
            /// <summary>
            /// The setting ext
            /// </summary>
            /// <summary>
            /// The setting ext
            /// </summary>
            private const string SettingExt = "xml";
            #endregion

            #region Enumerations
            #endregion

            #region Fields            
            /// <summary>
            /// The setting base
            /// </summary>
            /// <summary>
            /// The setting base
            /// </summary>
            private Settings settings;
            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="SettingsFolders"/> class.
            /// </summary>
            /// <param name="settings">The setting base.</param>
            /// <summary>
            /// Initializes a new instance of the <see cref="SettingsFolders"/> class.
            /// </summary>
            /// <param name="settings">The setting base.</param>
            public SettingsFolders(Settings settings)
            {
                this.settings = settings;
                this.Refresh();
            }


            #endregion

            #region Public
            #region Properties
            /// <summary>
            /// Gets the file setting client.
            /// </summary>
            /// <value>The file setting client.</value>
            public FileSystemMgr FileSettingClient { get; private set; } = null;
            /// <summary>
            /// Gets the file setting user roaming.
            /// </summary>
            /// <value>The file setting user roaming.</value>
            public FileSystemMgr FileServerDomainUser { get; private set; } = null;
            /// <summary>
            /// Gets the file setting server.
            /// </summary>
            /// <value>The file setting server.</value>
            public FileSystemMgr FileSettingServer { get; private set; } = null;
            /// <summary>
            /// Gets the folder server.
            /// </summary>
            /// <value>The folder server.</value>
            public FileSystemMgr FolderServer { get; private set; } = null;
            /// <summary>
            /// Gets the folder server domain.
            /// </summary>
            /// <value>The folder server domain.</value>
            public FileSystemMgr FolderServerDomain { get; private set; } = null;
            /// <summary>
            /// Gets the folder server domain user.
            /// </summary>
            /// <value>The folder server domain user.</value>
            public FileSystemMgr FolderServerDomainUser { get; private set; } = null;
            /// <summary>
            /// Gets the folder client.
            /// </summary>
            /// <value>The folder client.</value>
            public FileSystemMgr FolderClient { get; private set; } = null;
            #endregion

            #region Method Static
            #endregion

            #region Method
            /// <summary>
            /// Refreshes this instance.
            /// </summary>
            public void Refresh() => this.setPaths();

            /// <summary>
            /// Sets the roaming folder.
            /// </summary>
            /// <param name="serverFolderSetting">The server folder setting.</param>
            /// <exception cref="System.Exception"></exception>
            /// <exception cref="Exception"></exception>
            internal void SetRoamingFolder(string serverFolderSetting)
            {
                if (string.IsNullOrWhiteSpace(serverFolderSetting)) return;
                this.FolderServer = new FileSystemMgr(serverFolderSetting);
                if (!this.FolderServer.Create(FileSystemTypes.Directory).Exists)
                    throw new Exception(string.Format("Impossibile raggiungere o creare la cartella remota {0}", serverFolderSetting));
                this.Refresh();
            }
            #endregion

            #endregion

            #region Internal
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Protected
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method            
            #endregion
            #endregion

            #region Private
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method

            /// <summary>
            /// Initialize the default path where the settings files will be placed
            /// </summary>
            private void setPaths()
            {
                this.FolderClient = new FileSystemMgr($"{Settings.Parameters.ExecutableFolder}\\xml");
                this.FileSettingClient = new FileSystemMgr($"{this.FolderClient}\\{Settings.Parameters.DomainName}.{Settings.Parameters.SettingsName}.{LocalSettingSuffix}.{SettingExt}");
                if (this.FolderServer == null)
                    this.FolderServer = new FileSystemMgr($"{this.FolderClient}\\server");
                this.FileSettingServer = new FileSystemMgr($"{this.FolderServer}\\{Settings.Parameters.DomainName}.{Settings.Parameters.SettingsName}.{RemoteSettingSuffix}.{SettingExt}");
                this.FolderServerDomain = new FileSystemMgr($"{this.FolderServer}\\{Settings.Parameters.DomainName}");
                this.FolderServerDomainUser = new FileSystemMgr($"{this.FolderServerDomain}\\{Settings.Parameters.UserName}\\{Settings.Parameters.SettingsName}");
                this.FileServerDomainUser = new FileSystemMgr($"{this.FolderServerDomainUser}\\{Settings.Parameters.DomainName}.{Settings.Parameters.SettingsName}.{UserSettingSuffix}.{SettingExt}");
            }
            #endregion

            #region Event Handlers - CommandsEngine
            #endregion

            #region Event Handlers
            #endregion
            #endregion

            #region Event Definitions
            #endregion

            #region Embedded Types
            #endregion
        }
    }
}
