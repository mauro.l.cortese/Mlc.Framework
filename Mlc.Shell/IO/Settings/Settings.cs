// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Settings.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Inherit from this class to implement types that will have to manage the settings of an application
	/// </summary>
	public abstract partial class Settings
	{
		#region Constants        
		/// <summary>
		/// The setting suffix
		/// </summary>
		private const string SettingSuffix = "Settings";
		/// <summary>
		/// The setting ext
		/// </summary>
		private const string SettingExt = "xml";
		/// <summary>
		/// The size forms
		/// </summary>
		private const string SizeFormsXmlNode = "SizeForms";
		/// <summary>
		/// The size form file XML
		/// </summary>
		private const string SizeFormFileXml = "\\LayoutControl\\SizeForms.xml";
		#endregion

		#region Enumerations
		#endregion

		#region Fields        
		/// <summary>
		/// The identifier settings GUID
		/// </summary>
		internal Guid IdSettings = default;
		/// <summary>
		/// The settings form MGR
		/// </summary>
		private SettingsFormMgr settingsFormMgr;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Settings" /> class.
		/// </summary>
		/// <param name="IdSettings">The identifier settings.</param>
		/// <param name="pathLocalSetting">The path local setting.</param>
		protected Settings(Guid IdSettings, string pathLocalSetting)
		{
			this.IdSettings = IdSettings;
			Parameters.LocalFolder = new(pathLocalSetting);

			this.folders = new(this);
			this.settingsFormMgr = new(this);
		}
		#endregion

		#region Public
		#region Properties    
		/// <summary>
		/// The settings folders
		/// </summary>
		private SettingsFolders folders = null;

		/// <summary>
		/// Gets the settings folders.
		/// </summary>
		/// <value>The settings folders.</value>
		[SubSettings, Description("Posizione dei file di settaggi dell'applicazione")]
		public SettingsFolders Folders { get { return this.folders; } }

		/// <summary>
		/// Gets the parameters.
		/// </summary>
		/// <value>The parameters.</value>
		public static SettingsParameters Parameters { get; set; } = new();

		/// <summary>
		/// Gets or sets the assembly.
		/// </summary>
		/// <value>The assembly.</value>
		public static Assembly Assembly { get; set; }

		///// <summary>
		///// Gets or sets a value indicating whether [use pc name].
		///// </summary>
		///// <value><c>true</c> if [use pc name]; otherwise, <c>false</c>.</value>
		//public static bool UsePcName { get; set; }

		/// <summary>
		/// Gets or sets the database connection string.
		/// </summary>
		/// <value>The database connection string.</value>
		public DbConnectionsColl DbConns { get; set; } = new();

		/// <summary>
		/// Gets or sets the default settings.
		/// </summary>
		/// <value>The default settings.</value>
		[SubSettings, Description("Settaggi per i DB")]
		public DefaultSettingsFamily DefaultSettings { get; set; }
		#endregion

		#region Method Static        
		/// <summary>
		/// Sets the domain user.
		/// </summary>
		/// <param name="domain">The domain.</param>
		/// <param name="username">The username.</param>
		public static void SetDomainUser(string domain, string username)
		{

			if (domain != null)
				Parameters.DomainName = domain.Replace(".", " ").GetUpperCamelCase().Replace("Local", "");
			if (username != null)
				Parameters.UserName = username.ToLower();

			//Settings.Domain = domain.ToLower();
			//Settings.Username = username.ToLower();
		}
		#endregion

		#region Method
		/// <summary>
		/// Maps the form.
		/// </summary>
		/// <param name="form">The form.</param>
		public Form FormMapping(Form form)
		{
			if (!this.settingsFormMgr.ContainsKey(form.Name))
				this.settingsFormMgr.Add(new SettingsForm(form)); //this.settingsFormMgr.Add(new SettingsForm(form, this));
			form.FormClosed += (s, e) =>
			{
				Form f = (Form)s;
				if (this.settingsFormMgr.ContainsKey(form.Name))
					this.settingsFormMgr.Remove(form.Name);
			};
			return form;
		}

		///// <summary>
		///// Saves the size of the form.
		///// </summary>
		///// <param name="formName">The key.</param>
		///// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		//public bool SaveFormSize(string formName)
		//{
		//	try
		//	{
		//		if (this.settingsFormMgr != null)
		//			if (this.settingsFormMgr.ContainsKey(formName))
		//			{
		//				this.settingsFormMgr.SaveFormSize(formName);
		//				return true;
		//			}
		//	}
		//	catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		//	return false;
		//}

		///// <summary>
		///// Saves the size of the form.
		///// </summary>
		///// <param name="formName">The key.</param>
		///// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		//public bool LoadFormSize(string formName)
		//{
		//	try
		//	{
		//		if (this.settingsFormMgr != null)
		//			if (this.settingsFormMgr.ContainsKey(formName))
		//			{
		//				this.settingsFormMgr.LoadFormSize(formName);
		//				return true;
		//			}
		//	}
		//	catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		//	return false;
		//}

		/// <summary>
		/// Sets the roaming folder.
		/// </summary>
		/// <param name="serverFolderSetting">The server folder setting.</param>
		public void SetRoamingFolder(string serverFolderSetting) => this.Folders.SetRoamingFolder(string.Concat(serverFolderSetting));

		/// <summary>
		/// Deserializes the specified setting files.
		/// </summary>
		/// <param name="settingFiles">The setting files.</param>
		/// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
		public bool Deserialize(SettingFiles settingFiles = SettingFiles.All) => new IOSettingsXml(this).Deserialize(settingFiles);

		/// <summary>
		/// Serializes the specified setting files.
		/// </summary>
		/// <param name="settingFiles">The setting files.</param>
		/// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
		public bool Serialize(SettingFiles settingFiles = SettingFiles.All) => new IOSettingsXml(this).Serialize(settingFiles);

		/// <summary>
		/// Sets the default values.
		/// </summary>
		public virtual void SetDefaultValues() => this.DefaultSettings = new(this) { RoamingFolder = this.Folders.FolderServer.Path };

		/// <summary>
		/// Gets the file user layout.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>System.String.</returns>
		public string GetFileUserLayout(string name)
		{
			string retVal = $"{this.Folders.FileServerDomainUser.FsoParentFolder.Path}\\Layout\\{name.GetFirstMaius(true)}";
			return retVal;
		}

		/// <summary>
		/// Gets the file user layout.
		/// </summary>
		/// <param name="userControl">The user control.</param>
		/// <returns>System.String.</returns>
		public string GetFileUserLayout(UserControl userControl)
		{
			if (userControl == null) return string.Empty;
			string formName, controlName, retVal;

			formName = userControl.ParentForm?.Name.GetFirstMaius(true);
			controlName = userControl.Name.GetFirstMaius(true);

			if (string.IsNullOrEmpty(formName))
				retVal = $"{this.Folders.FileServerDomainUser.FsoParentFolder.Path}\\Layout\\{controlName}";
			else
				retVal = $"{this.Folders.FileServerDomainUser.FsoParentFolder.Path}\\Layout\\{formName}\\{controlName}";
			return retVal;
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the XML size forms.
		/// </summary>
		/// <returns>XmlDocument.</returns>
		private XmlDocument getXmlSizeForms()
		{
			string fileSize = getFileXmlFormSize();
			XmlDocument XDoc = new XmlDocument();
			if (!File.Exists(fileSize))
				using (XmlFileWriter writer = new XmlFileWriter(fileSize))
					writer.OpenXML(SizeFormsXmlNode);
			XDoc.Load(fileSize);
			return XDoc;
		}

		/// <summary>
		/// Gets the size of the file XML form.
		/// </summary>
		/// <returns>System.String.</returns>
		private string getFileXmlFormSize()
		{
			string fileSize = string.Concat(this.Folders.FileServerDomainUser.FsoParentFolder.Path, SizeFormFileXml);
			string folder = Path.GetDirectoryName(fileSize);
			FileSystemMgr fsm = new FileSystemMgr(folder);
			if (!fsm.Exists)
				fsm.Create(FileSystemTypes.Directory);
			return fileSize;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#region Event SettingsChanged                
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public event EventHandler SettingsChanged;

		/// <summary>
		/// Metodo per il lancio dell'evento.
		/// </summary>
		/// <param name="e">Dati dell'evento</param>
		protected internal virtual void OnSettingsChanged(EventArgs e)
		{
			// Se ci sono ricettori in ascolto ...
			if (SettingsChanged != null)
				// viene lanciato l'evento
				SettingsChanged(this, e);
		}
		#endregion
		#endregion

		#region Embedded Types
		#endregion

		/// <summary>
		/// The database connection string
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.Settings.DbConnections}" />
		public class DbConnectionsColl : Dictionary<string, DbConnections>
		{
			/// <summary>
			/// Adds the specified database connections.
			/// </summary>
			/// <param name="dbConnections">The database connections.</param>
			public void Add(DbConnections dbConnections)
			{
				base.Add(dbConnections.SetConnectionName, dbConnections);
			}
		}
	}
}
