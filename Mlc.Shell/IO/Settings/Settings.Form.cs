﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="FormMap.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class SettingsForm.
	/// </summary>
	public class SettingsForm
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The setting base
		/// </summary>
		//private readonly Settings settings;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SettingsForm" /> class.
		/// </summary>
		/// <param name="form">The form.</param>
		public SettingsForm(Form form) //, Settings settings)
		{
			this.Form = form;
			this.Form.Load += (s,e) => {this.SettingsFormMgr.LoadFormSize(this.getFormName(s));};
			this.Form.Closing += (s, e) => { this.SettingsFormMgr.SaveFormSize(this.getFormName(s)); };
			this.size = this.Form.Size;
			this.location = this.Form.Location;
			//this.settings = settings;
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the form.
		/// </summary>
		/// <value>The form.</value>
		public Form Form { get; set; }

		/// <summary>
		/// The size
		/// </summary>
		private Size size = default(Size);
		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		/// <value>The size.</value>
		public Size Size
		{
			get
			{
				if (this.Form != null)
					this.size = this.Form.WindowState != FormWindowState.Normal ? this.Form.RestoreBounds.Size : this.Form.Size;
				return this.size;
			}
			set
			{
				this.size = value;
				if (this.Form != null)
					this.Form.Size = this.size;
			}
		}

		/// <summary>
		/// Gets the form MGR.
		/// </summary>
		/// <value>The form MGR.</value>
		public Settings.SettingsFormMgr SettingsFormMgr { get; internal set; }

		/// <summary>
		/// The location
		/// </summary>
		private Point location = default(Point);
		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		/// <value>The location.</value>
		public Point Location
		{
			get
			{
				if (this.Form != null)
					this.location = this.Form.WindowState != FormWindowState.Normal ? this.Form.RestoreBounds.Location : this.Form.Location;
				return this.location;
			}
			set
			{
				this.location = value;
				if (this.Form != null)
					this.Form.Location = this.location;
			}
		}

		/// <summary>
		/// The window state
		/// </summary>
		private FormWindowState windowState = FormWindowState.Normal;
		/// <summary>
		/// Gets the state.
		/// </summary>
		/// <value>The state.</value>
		public FormWindowState WindowState
		{
			get
			{
				if (this.Form != null)
					this.windowState = this.Form.WindowState;
				return this.windowState;
			}
			set
			{
				this.windowState = value;
				if (this.Form != null)
					this.Form.WindowState = this.windowState;
			}
		}
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Initializes the form.
		/// </summary>
		/// <param name="form">The form.</param>
		public void InitForm(Form form)
		{
			this.Form = form;
			this.Form.Size = this.size;
			this.Form.Location = this.location;
		}

		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the settings form.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <returns>System.String.</returns>
		private string getFormName(object sender)
		{
			string retVal = null;
			Form form = (Form)sender;
			if (form != null)
				retVal = form.Name;
			return retVal;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}
}
