// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 12-22-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="FamilySettingBase.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Inherit from this class to implement the types that must contain an application settings family
    /// </summary>
    public abstract class SettingsFamily
    {
        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// The settings manager instance.
        /// </summary>
        private Settings settings = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsFamily" /> class.
        /// </summary>
        /// <param name="settings">The settings manager instance.</param>
        public SettingsFamily(Settings settings)
        {
            this.settings = settings;
        }
        #endregion

        #region Public
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Raises the event settings changed.
        /// </summary>
        public void RaiseEventSettingsChanged()
        {
            this.settings.OnSettingsChanged(new EventArgs());
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion

        #region Event Handlers - CommandsEngine
        #endregion

        #region Event Handlers
        #endregion
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion
    }
}
