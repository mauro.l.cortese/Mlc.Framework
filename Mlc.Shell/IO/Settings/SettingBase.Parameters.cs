using System;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Mlc.Shell.IO
{
    /// <summary>Inherit from this class to implement types that will have to manage the settings of an application</summary>
    public abstract partial class SettingBase
    {
        /// <summary>
        /// Stores the paths of the files containing the application settings
        /// </summary>
        public class SettingsParameters
        {
            #region Constants
            /// <summary>
            /// The setting suffix
            /// </summary>
            private const string SettingSuffix = "Settings";

            /// <summary>
            /// The setting ext
            /// </summary>
            private const string SettingExt = "xml";

            public FileSystemMgr LocalFolder { get; internal set; }
            #endregion

            #region Enumerations
            #endregion

            #region Fields
            private SysInfo sysInfo = new SysInfo(SettingBase.Assembly);

            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="SettingsParameters"/> class.
            /// </summary>
            public SettingsParameters()
            {
                this.domainName = sysInfo.DomainName.Replace(".", " ").GetUpperCamelCase().Replace("Local", "");
                this.settingsName = Path.GetFileNameWithoutExtension(sysInfo.ExecutableFile);
                this.userName = sysInfo.UserName.ToLower();
                this.executableFolder = new FileSystemMgr(sysInfo.ExecutableFolder);

            }

            /// <summary>Initializes a new instance of the <see cref="T:Mlc.Shell.IO.SettingBase.Parameters"/> class.</summary>
            /// <param name="domainName">Name of the domain.</param>
            /// <param name="applictionName">Name of the appliction.</param>
            /// <param name="userName">Name of the user.</param>
            public SettingsParameters(string domainName, string applictionName, string userName, string executableFolder)
            {
                this.domainName = domainName;
                this.settingsName = applictionName;
                this.userName = userName;
                this.executableFolder = new FileSystemMgr(executableFolder);
            }
            #endregion

            #region Public
            #region Properties
            /// <summary>
            /// The domain name
            /// </summary>
            private string domainName;
            /// <summary>
            /// Gets or sets the name of the domain.
            /// </summary>
            /// <value>The name of the domain.</value>
            public string DomainName => this.domainName.ToLower();

            /// <summary>
            /// The settings name
            /// </summary>
            private string settingsName;
            /// <summary>
            /// Gets the name of the settings.
            /// </summary>
            /// <value>The name of the settings.</value>
            public string SettingsName => this.settingsName.ToLower();

            /// <summary>
            /// The user name
            /// </summary>
            private string userName;
            /// <summary>
            /// Gets the name of the user.
            /// </summary>
            /// <value>The name of the user.</value>
            public string UserName => this.userName.ToLower();

            /// <summary>
            /// The executable folder
            /// </summary>
            public FileSystemMgr executableFolder;
            /// <summary>
            /// Gets the executable folder.
            /// </summary>
            /// <value>The executable folder.</value>
            public FileSystemMgr ExecutableFolder => this.executableFolder;
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion

            #endregion

            #region Internal
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Protected
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion
            #endregion

            #region Private
            #region Properties
            #endregion

            #region Method Static
            #endregion

            #region Method
            #endregion

            #region Event Handlers - CommandsEngine
            #endregion

            #region Event Handlers
            #endregion
            #endregion

            #region Event Definitions
            #endregion

            #region Embedded Types
            #endregion
        }
    }
}
