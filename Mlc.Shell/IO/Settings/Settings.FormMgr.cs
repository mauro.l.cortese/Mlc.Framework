// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 12-15-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Settings.Folders.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Inherit from this class to implement types that will have to manage the settings of an application
	/// </summary>
	public abstract partial class Settings
	{
		/// <summary>
		/// Class SettingsFormMgr.
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.IO.SettingsForm}" />
		public class SettingsFormMgr : Dictionary<string, SettingsForm>
		{
			#region Constants
			#endregion

			#region Enumerations
			#endregion

			#region Fields            
			/// <summary>
			/// The setting base
			/// </summary>
			private readonly Settings settings;
			#endregion

			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="SettingsFormMgr" /> class.
			/// </summary>
			/// <param name="setting">The setting.</param>
			public SettingsFormMgr(Settings setting)
			{
				this.settings = setting;
			}
			#endregion

			#region Public
			#region Properties
			#endregion

			#region Method Static
			#endregion

			#region Method
			/// <summary>
			/// Adds the specified key.
			/// </summary>
			/// <param name="key">The key.</param>
			/// <param name="settingsForm">The settings form.</param>
			public new void Add(string key, SettingsForm settingsForm)
			{
				settingsForm.SettingsFormMgr = this;
				base.Add(key, settingsForm);
			}

			#endregion

			#endregion

			#region Internal
			#region Properties
			#endregion

			#region Method Static
			#endregion

			#region Method
			/// <summary>
			/// Adds the specified form.
			/// </summary>
			/// <param name="settingsForm">The settings form.</param>
			internal void Add(SettingsForm settingsForm)
			{
				if (string.IsNullOrEmpty(settingsForm.Form.Name))
					settingsForm.Form.Name = settingsForm.Form.Text;
				this.Add(settingsForm.Form.Name, settingsForm);
			}

			/// <summary>
			/// Saves the size of the form.
			/// </summary>
			/// <param name="key">The key.</param>
			/// <returns>System.Boolean.</returns>
			internal bool SaveFormSize(string key)
			{
				bool retVal = false;
				if (this.ContainsKey(key))
					retVal = new IOSettingsXml(this.settings).SerializeFormSize(this[key]);
				return retVal;
			}

			/// <summary>
			/// Loads the size of the form.
			/// </summary>
			/// <param name="key">The key.</param>
			/// <returns>System.Boolean.</returns>
			internal bool LoadFormSize(string key)
			{
				bool retVal = false;
				if (this.ContainsKey(key))
					retVal = new IOSettingsXml(this.settings).DeserializeFormSize(this[key]);
				return retVal;
			}
			#endregion
			#endregion

			#region Protected
			#region Properties
			#endregion

			#region Method Static
			#endregion

			#region Method            
			#endregion
			#endregion

			#region Private
			#region Properties
			#endregion

			#region Method Static
			#endregion

			#region Method
			#endregion

			#region Event Handlers - CommandsEngine
			#endregion

			#region Event Handlers
			#endregion
			#endregion

			#region Event Definitions
			#endregion

			#region Embedded Types
			#endregion
		}
	}
}
