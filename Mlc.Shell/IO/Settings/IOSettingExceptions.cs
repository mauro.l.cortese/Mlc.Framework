﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class IOSettingExceptions./
	/// </summary>
	public class IOSettingExceptions
	{


		/// <summary>
		/// Class IOSettingExceptionsBase.
		/// Implements the <see cref="System.Exception" />
		/// </summary>
		/// <seealso cref="System.Exception" />
		public class IOSettingExceptionsBase : Exception
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="IOSettingExceptionsBase"/> class.
			/// </summary>
			/// <param name="ex">The ex.</param>
			public IOSettingExceptionsBase(Exception ex)
			{ this.BaseException = ex; }

			/// <summary>
			/// Gets the base exception.
			/// </summary>
			/// <value>The base exception.</value>
			public Exception BaseException { get; }
		}



		/// <summary>
		/// Class RoamingFolderMissingExceptions.
		/// </summary>
		public class WrongRoamingFolderException : IOSettingExceptionsBase
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="WrongRoamingFolderException"/> class.
			/// </summary>
			/// <param name="roamingFolder">The roaming folder.</param>
			public WrongRoamingFolderException(Exception ex, string roamingFolder)
				: base(ex)
			{
				this.RoamingFolder = roamingFolder;
			}

			/// <summary>
			/// Gets the roaming folder.
			/// </summary>
			/// <value>The roaming folder.</value>
			public string RoamingFolder { get; }

		}

	}
}
