using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Xml;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Inherit from this class to implement types that will have to manage the settings of an application
    /// </summary>
    public abstract partial class SettingBase
    {

        #region Constants        
        /// <summary>
        /// The setting suffix
        /// </summary>
        private const string SettingSuffix = "Settings";
        /// <summary>
        /// The setting ext
        /// </summary>
        private const string SettingExt = "xml";
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// The identifier settings GUID
        /// </summary>
        internal Guid IdSettings = default(Guid);
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingBase"/> class.
        /// </summary>
        /// <param name="IdSettings">The identifier settings.</param>
        /// <param name="pathLocalSetting">The path local setting.</param>
        protected SettingBase(Guid IdSettings, string pathLocalSetting)
        {
            this.IdSettings = IdSettings;
            Parameters.LocalFolder = new FileSystemMgr(pathLocalSetting);
            this.folders = new SettingsFolders(this);
        }
        #endregion

        #region Public
        #region Properties      
        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public static SettingsParameters Parameters { get; set; } = new SettingsParameters();

        /// <summary>
        /// Gets or sets the assembly.
        /// </summary>
        /// <value>The assembly.</value>
        public static Assembly Assembly { get; set; }

        ///// <summary>
        ///// Gets or sets a value indicating whether [use pc name].
        ///// </summary>
        ///// <value><c>true</c> if [use pc name]; otherwise, <c>false</c>.</value>
        //public static bool UsePcName { get; set; }

        /// <summary>
        /// Gets or sets the database connection string.
        /// </summary>
        /// <value>The database connection string.</value>
        public DbConnectionsColl DbConns { get; set; } = new DbConnectionsColl();

        /// <summary>
        /// Gets or sets the default settings.
        /// </summary>
        /// <value>The default settings.</value>
        [SubSettings, Description("Settaggi per i DB")]
        public FamilyDefaultSettings DefaultSettings { get; set; }
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Sets the roaming folder.
        /// </summary>
        /// <param name="serverFolderSetting">The server folder setting.</param>
        public void SetRoamingFolder(string serverFolderSetting) => this.Folders.SetRoamingFolder(string.Concat(serverFolderSetting));

        /// <summary>
        /// The settings folders
        /// </summary>
        private SettingsFolders folders = null;
        /// <summary>
        /// Gets the settings folders.
        /// </summary>
        /// <value>The settings folders.</value>
        [SubSettings, Description("Posizione dei file di settaggi dell'applicazione")]
        public SettingsFolders Folders { get { return this.folders; } }

        /// <summary>
        /// Deserializes the specified setting files.
        /// </summary>
        /// <param name="settingFiles">The setting files.</param>
        /// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
        public bool Deserialize(SettingFiles settingFiles)
        {
            return new IOSettingsXml(this).Deserialize(settingFiles);
        }

        /// <summary>
        /// Serializes the specified setting files.
        /// </summary>
        /// <param name="settingFiles">The setting files.</param>
        /// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
        public bool Serialize(SettingFiles settingFiles)
        {
            return new IOSettingsXml(this).Serialize(settingFiles);
        }

        /// <summary>
        /// Save the size of the specified form
        /// </summary>
        /// <param name="formMap">The form map.</param>
        /// <param name="key">The key.</param>
        public void SaveSize(FormMap formMap, string key)
        {
            XmlDocument XDoc = this.getXmlSizeForms();
            XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", key));
            bool save = true;
            if (xNode != null)
            {
                save = xNode.Attributes["Size"].Value != formMap.Size.ToString();
                xNode.Attributes["Size"].Value = formMap.Size.ToString();

                save = save || xNode.Attributes["Location"].Value != formMap.Location.ToString();
                xNode.Attributes["Location"].Value = formMap.Location.ToString();

                save = save || xNode.Attributes["ScreenName"].Value != formMap.ScreenName;
                xNode.Attributes["ScreenName"].Value = formMap.ScreenName;
            }
            else
            {
                save = true;
                XmlNode n = XDoc.CreateNode(XmlNodeType.Element, "Size", "");

                XmlAttribute a = XDoc.CreateAttribute("Key");
                a.Value = key;
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("Size");
                a.Value = formMap.Size.ToString();
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("Location");
                a.Value = formMap.Location.ToString();
                n.Attributes.Append(a);

                a = XDoc.CreateAttribute("ScreenName");
                a.Value = formMap.ScreenName;
                n.Attributes.Append(a);

                XDoc.DocumentElement.AppendChild(n);
            }
            if (save)
                XDoc.Save(getFileXmlFormSize());
        }

        /// <summary>
        /// Upload the dimensions of a form (assign it immediately to the form)
        /// </summary>
        /// <param name="key">Key of the element to be obtained</param>
        /// <returns>Dimensions obtained</returns>
        public FormMap LoadSize(string key)
        {
            FormMap retFormMap = new FormMap();
            XmlDocument XDoc = this.getXmlSizeForms();
            XmlNode xNode = XDoc.SelectSingleNode(string.Format("//SizeForms/Size[@Key='{0}']", key));
            if (xNode != null)
            {
                retFormMap.Size = TypeConvert<Size>.GetValue(xNode.Attributes["Size"].Value, default(Size));
                retFormMap.Location = TypeConvert<Point>.GetValue(xNode.Attributes["Location"].Value, default(Point));
                retFormMap.ScreenName = xNode.Attributes["ScreenName"].Value;
            }
            return retFormMap;
        }

        /// <summary>
        /// Sets the default values.
        /// </summary>
        public virtual void SetDefaultValues()
        {
            this.DefaultSettings = new FamilyDefaultSettings(this)
            {
                RoamingFolder = this.Folders.FolderServer.Path
            };
        }
        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Gets the XML size forms.
        /// </summary>
        /// <returns>XmlDocument.</returns>
        private XmlDocument getXmlSizeForms()
        {
            string fileSize = getFileXmlFormSize();
            XmlDocument XDoc = new XmlDocument();
            if (!File.Exists(fileSize))
                using (XmlFileWriter writer = new XmlFileWriter(fileSize))
                    writer.OpenXML("SizeForms");
            XDoc.Load(fileSize);
            return XDoc;
        }

        /// <summary>
        /// Gets the size of the file XML form.
        /// </summary>
        /// <returns>System.String.</returns>
        private string getFileXmlFormSize()
        {
            string fileSize = string.Concat(this.Folders.FileServerDomainUser.FsoParentFolder.Path, "\\LayoutControl\\SizeForms.xml");
            string folder = Path.GetDirectoryName(fileSize);
            FileSystemMgr fsm = new FileSystemMgr(folder);
            if (!fsm.Exists)
                fsm.Create(FileSystemTypes.Directory);
            return fileSize;
        }
        #endregion

        #region Event Handlers - CommandsEngine
        #endregion

        #region Event Handlers
        #endregion
        #endregion

        #region Event Definitions
        #region Event SettingsChanged                
        /// <summary>
        /// Delegato per la gestione dell'evento.
        /// </summary>
        public event EventHandler SettingsChanged;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected internal virtual void OnSettingsChanged(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (SettingsChanged != null)
                // viene lanciato l'evento
                SettingsChanged(this, e);
        }
        #endregion
        #endregion

        #region Embedded Types
        #endregion

        /// <summary>
        /// The database connection string
        /// </summary>
        public class DbConnectionsColl : Dictionary<string, DbConnections>
        {

            public void Add(DbConnections dbConnections)
            {

                base.Add(dbConnections.SetConnectionName, dbConnections);
            }

        }

    }
}
