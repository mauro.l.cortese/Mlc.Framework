﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="LoadFileInStreaming.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class LoadFileInStreaming.
    /// </summary>
    public class LoadFileInStreaming
    {

        /// <summary>
        /// The full path file
        /// </summary>
        private string fullPathFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadFileInStreaming" /> class.
        /// </summary>
        /// <param name="fullPathFile">The full path file.</param>
        public LoadFileInStreaming(string fullPathFile)
        {
            this.fullPathFile = fullPathFile;
        }

        /// <summary>
        /// Gets the stream.
        /// </summary>
        /// <returns>MemoryStream.</returns>
        public MemoryStream GetStream()
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(this.fullPathFile, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
            }
            return ms;
        }
    }
}
