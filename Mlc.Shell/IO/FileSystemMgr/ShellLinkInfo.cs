// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ShellLinkInfo.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Shell32;

using Mlc.Shell;


namespace Mlc.Shell.IO
{
    /// <summary>
    /// Definisce le informazioni di un link della shell
    /// </summary>
    /// <seealso cref="Mlc.Shell.IO.FileSystemMgr" />
    /// <seealso cref="Mlc.Shell.IO.ShellLink" />
    public class ShellLinkInfo
    {
        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        public ShellLinkInfo() { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="fileLink">Fullpath del file link</param>
        public ShellLinkInfo(string fileLink)
        {
            this.PathFileLink = fileLink;
            this.getLinkInfo(fileLink);
        }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="folderLink">Fullpath della cartella in cui si trova il link</param>
        /// <param name="linkFileName">Nome del file link (Senza estensione .lnk)</param>
        public ShellLinkInfo(string folderLink, string linkFileName)
            : this(string.Concat(folderLink, Chars.BackSlash, linkFileName, FsoExt.Lnk))
        { }

        #endregion

        #region Propriet�
        /// <summary>
        /// The is link
        /// </summary>
        private bool isLink = false;
        /// <summary>
        /// Restiruisce true se l'istanza punta ad un link della shell, altrimenti false
        /// </summary>
        /// <value><c>true</c> if this instance is link; otherwise, <c>false</c>.</value>
        public bool IsLink { get { return this.isLink; } }

        /// <summary>
        /// Restituisce o imposta il fullpath del file puntato dal link
        /// </summary>
        /// <value>The path target.</value>
        public string PathTarget { get; set; }

        /// <summary>
        /// Restituisce o imposta gli argomenti della linea di comando per l'esecuzione del file puntato dal link
        /// </summary>
        /// <value>The arguments.</value>
        public string Arguments { get; set; }

        /// <summary>
        /// Restituisce o imposta la cartella di lavoro
        /// </summary>
        /// <value>The working directory.</value>
        public string WorkingDirectory { get; set; }

        /// <summary>
        /// Restituisce o imposta la descrizione del link
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Restituisce o imposta il path del file per l'icona del link
        /// </summary>
        /// <value>The icon location.</value>
        public string IconLocation { get; set; }

        /// <summary>
        /// The file link
        /// </summary>
        private string fileLink = null;
        /// <summary>
        /// Restituisce o imposta il path del file link
        /// </summary>
        /// <value>The path file link.</value>
        public string PathFileLink
        {
            get { return this.fileLink; }
            set
            {
                this.fileLink = value;
                if (!this.fileLink.EndsWith(FsoExt.Lnk))
                    this.fileLink += FsoExt.Lnk;
            }
        }
        #endregion

        #region Metodi privati
        ///// <summary>
        ///// Popola l'istanza con le info del link
        ///// </summary>
        ///// <param name="fileLink">The file link.</param>
        //private void getLinkInfo(string fileLink)
        //{
        //    FileSystemMgr fLink = new FileSystemMgr(fileLink);
        //    if (!fLink.Exists || !fLink.IsFile)
        //        return;

        //    Shell32.Folder folder = new Shell32.Shell().NameSpace(System.IO.Path.GetDirectoryName(fLink.Path));
        //    FolderItems fi = folder.Items();
        //    Shell32.FolderItem file = fi.Item(System.IO.Path.GetFileName(fLink.Path));
        //    this.isLink = file.IsLink;
        //    if (this.isLink)
        //    {
        //        ShellLinkObject linkObj = (ShellLinkObject)(object)file.GetLink;
        //        this.PathTarget = linkObj.Path;
        //        this.Arguments = linkObj.Arguments;
        //        this.WorkingDirectory = linkObj.WorkingDirectory;
        //        if (string.IsNullOrEmpty(this.WorkingDirectory))
        //            this.WorkingDirectory = System.IO.Path.GetDirectoryName(this.PathTarget);
        //        this.Description = linkObj.Description;
        //        string iconLocation = null;
        //        linkObj.GetIconLocation(out iconLocation);
        //        this.IconLocation = iconLocation;
        //    }
        //}
        #endregion
    }
}
