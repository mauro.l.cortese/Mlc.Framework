﻿// ***********************************************************************
// Assembly         : Mlc.Tools
// Author           : Cortese Mauro Luigi
// Created          : 09-22-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 04-20-2022
// ***********************************************************************
// <copyright file="SizeConverter.cs" company="Microsoft">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.IO;
using System.Xml;
using Mlc.Shell;

namespace Mlc.Shell.IO
{
	/// <summary>
	/// Class TdlWriter.
	/// </summary>
	public class SizeConverter
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SizeConverter" /> class.
		/// </summary>
		/// <param name="byteDimension">The byte dimension.</param>
		public SizeConverter(long byteDimension)
		{
			this.Byte = byteDimension;
			this.Kilobyte = (float)this.Byte / 1024;
			this.Megabyte = this.Kilobyte / 1024;
			this.Gigabyte = this.Megabyte / 1024;
		}

		/// <summary>
		/// Gets the byte.
		/// </summary>
		/// <value>The byte.</value>
		public long Byte { get; private set; }
		/// <summary>
		/// Gets the kilobyte.
		/// </summary>
		/// <value>The kilobyte.</value>
		public float Kilobyte { get; private set; }
		/// <summary>
		/// Gets the megabyte.
		/// </summary>
		/// <value>The megabyte.</value>
		public float Megabyte { get; private set; }
		/// <summary>
		/// Gets the gigabyte.
		/// </summary>
		/// <value>The gigabyte.</value>
		public float Gigabyte { get; private set; }

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>A <see cref="System.String" /> that represents this instance.</returns>
		public override string ToString()
		{
			return $"{this.Byte} byte ({this.Kilobyte:0.000} kB - {this.Megabyte:0.000} MB - {this.Gigabyte:0.000} GB)";
		}
	}
}
