// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="IObj.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mlc.Shell;
using Mlc.Shell.IO;

//using Mx.Core.Constants;
//using Mx.Core.Enumerations;
//using Mx.Core.Crypto;
//using Mx.Core;

//using SevenZip;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe astratta per operazioni comuni sugli elementi del file system
    /// </summary>
    internal abstract class IObj
    {
        #region Campi
        /// <summary>
        /// Fullpath dell'elemento del file system
        /// </summary>
        protected string path = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="path">Fullpath dell'elemento del file system</param>
        public IObj(string path)
        {
            this.path = path;
        }
        #endregion

        #region Propriet�
        /// <summary>
        /// Restituisce la dimensione dell'elemento in byte
        /// </summary>
        /// <value>The size.</value>
        internal abstract long Size { get; }
        #endregion

        #region Metodi interni
        /// <summary>
        /// Copying the file system object at the specified location
        /// </summary>
        /// <param name="pathDest">Path to the copy destination</param>
        /// <param name="actionsCopy">Determines the action to be taken on new files generated, the possible actions are in the enumeration <see cref="Mlc.Shell.IO.ActionsCopy" /></param>
        /// <param name="resulCopyCompress">Determines the possible results of the copy of a file system object for <see cref="Mlc.Shell.IO.ActionsCopy.Compress" /></param>
        /// <param name="password">Password that wil be use for data encryption, if value is null will be used the password of the constant <see cref="Mlc.Shell.Constants.DefaultPwd" /></param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Copy(ref string pathDest, ActionsCopy actionsCopy, ResultCopyCompress resulCopyCompress = ResultCopyCompress.SingleObject, string password = Constants.DefaultPwd);

        /// <summary>
        /// Esegue la cifratura dell'elemento del file system
        /// </summary>
        /// <param name="password">Password per la cifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Encrypt(string password);

        /// <summary>
        /// Esegue la decifratura dell'elemento del file system
        /// </summary>
        /// <param name="password">Password per la decifratura</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Decrypt(string password);

        /// <summary>
        /// Elimina l'elemento del file system
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Delete();

        /// <summary>
        /// Comprime i dati dell'elemento del file system nel file specificato
        /// </summary>
        /// <param name="fileDest">Fullpath del file di destinazione dei dati compressi (formato .7z)</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Compress(string fileDest);

		/// <summary>
		/// Compresses the specified file dest.
		/// </summary>
		/// <param name="fileDest">The file dest.</param>
		/// <param name="resultFiles">The result files.</param>
		/// <param name="volumeSize">Size of the volume.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		internal abstract bool Compress(string fileDest, out List<string> resultFiles, int volumeSize = 1024 * 10000);

		/// <summary>
		/// Estrae i dati compressi dal file puntato dall'istanza nella directory specificata
		/// </summary>
		/// <param name="folderDest">Fullpath della cartella in cui estrarre i dati compressi (formato .7z)</param>
		/// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
		internal abstract bool Extract(string folderDest);

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <param name="toDoListFilePath">To do list file path.</param>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool WriteTreeInToDoList(string toDoListFilePath);

        /// <summary>
        /// Writes the tree in to do list.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool WriteTreeInToDoList();

        /// <summary>
        /// Lock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool Lock();

        /// <summary>
        /// Unlock browsing.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        internal abstract bool UnLock();

		/// <summary>
		/// Moves the specified folder dest.
		/// </summary>
		/// <param name="folderDest">The folder dest.</param>
		/// <param name="pathDest">The path dest.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		internal abstract bool Move(ref string folderDest, out string pathDest);
		#endregion
	}
}
