﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.IO
{
	public class CheckFolderData
	{

		public CheckFolderData(string folderPath)
		{
			this.OriginalFolderPath = folderPath;

			this.FolderPath = this.OriginalFolderPath.EndsWith("\\") ?
				  this.OriginalFolderPath.Substring(0, this.OriginalFolderPath.Length - 1)
				: this.OriginalFolderPath;
		}


		public string OriginalFolderPath { get; private set; } = string.Empty;

		public string FolderPath { get; private set; } = string.Empty;
		public bool Exists { get; internal set; }
	}
}
