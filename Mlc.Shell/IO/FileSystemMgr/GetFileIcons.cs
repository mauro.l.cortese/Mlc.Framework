﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="GetFileIcons.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Class GetFileIcons for get the icons that are associated to the file types
    /// </summary>
    public class GetFileIcons
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// The synchronize root
        /// </summary>
        private static object syncRoot = new Object();

        /// <summary>
        /// The instance
        /// </summary>
        private static volatile GetFileIcons instance = null;

        /// <summary>
        /// The temporary folder
        /// </summary>
        string tmpFolder = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Private constructor does not allow the direct instantiation of this class.
        /// </summary>
        private GetFileIcons()
        {
            this.tmpFolder = string.Concat(Path.GetTempPath(), ".~$mlctmp");
            if (!Directory.Exists(this.tmpFolder))
                Directory.CreateDirectory(this.tmpFolder);

            //this.lockFolder(this.tmpFolder);
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// The icons repository
        /// </summary>
        private RepositoryIcons icons = new RepositoryIcons();
        /// <summary>
        /// Gets the loaded icons list.
        /// </summary>
        /// <value>The icons.</value>
        public List<Icon> Icons { get { return this.icons.Values.ToList(); } }
        #endregion

        #region Method Static
        /// <summary>
        /// Always returns the same instance of the class.
        /// </summary>
        /// <returns>GetFileIcons.</returns>
        public static GetFileIcons GetInstance()
        {
            // if the instance has not yet been allocated
            if (instance == null)
            {
                lock (syncRoot)
                {
                    // gets a new instance
                    if (instance == null)
                        instance = new GetFileIcons();
                }
            }
            // returns the same instance of the class.
            return instance;
        }
        #endregion

        #region Method
        /// <summary>
        /// Loads the icons.
        /// </summary>
        /// <param name="extensions">The vector with the extensions of which need to load icons.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool LoadIcons(string[] extensions)
        {
            try
            {
                foreach (string extension in extensions)
                    this.GetIconByExtension(extension);
                return true;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex);}
            return false;
        }

        /// <summary>
        /// Gets the icon that is associated to the specified file type
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>Icon associated with the specified file type.</returns>
        public Icon GetIconByFile(string file)
        {
            return this.GetIconByExtension(Path.GetExtension(file));
        }

        /// <summary>
        /// Gets the icon that is associated to the specified file type
        /// </summary>
        /// <param name="extension">the extension of the file type</param>
        /// <returns>Icon associated with the specified extension.</returns>
        public Icon GetIconByExtension(string extension)
        {
            return this.GetIconByExtension(extension, false);
        }

        /// <summary>
        /// Gets the icon that is associated to the specified file type
        /// </summary>
        /// <param name="extension">the extension of the file type</param>
        /// <param name="reload">reload icon</param>
        /// <returns>Icon associated with the specified extension.</returns>
        public Icon GetIconByExtension(string extension, bool reload)
        {
            if (reload)
                return this.addIconAnyway(extension);           
            else
                return this.addIconIfNotExists(extension);
        }

        /// <summary>
        /// Refreshes all loaded icons.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Refresh()
        {
            List<string> keys = this.icons.Keys.ToList();
            
            try
            {
                foreach (string key in keys)
                    this.GetIconByExtension(Path.GetExtension(key), true);
                return true;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex);}
            return false;
        }

        /// <summary>
        /// Reset all loaded icons.
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Clear()
        {
            try
            {
                this.icons.Clear();
            }
            catch (Exception ex) {ExceptionsRegistry.GetInstance().Add(ex);}
            return false;
        }

        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Checkings the extension.
        /// </summary>
        /// <param name="extension">The extension to validate.</param>
        /// <returns>The extension validated.</returns>
        private string validateExtension(string extension)
        {
            if (!extension.StartsWith("."))
                extension = string.Concat(".", extension);

            return extension.ToLower();
        }

        /// <summary>
        /// Adds the icon anyway, refreshes all the icons that already exists
        /// </summary>
        /// <param name="extension">the extension of the file type</param>
        /// <returns>Icon associated with the specified extension.</returns>
        private Icon addIconAnyway(string extension)
        {
            string validatedExt = this.validateExtension(extension);
            string tmpFileName = string.Concat("~$_", validatedExt);
            try
            {
                string tmpFile = string.Concat(tmpFolder, "\\", tmpFileName);
                if (!File.Exists(tmpFile))
                    File.Create(tmpFile);

                if (!this.icons.ContainsKey(tmpFileName))
                    this.icons.Add(tmpFileName, Icon.ExtractAssociatedIcon(tmpFile));
                else
                    this.icons[tmpFileName] = Icon.ExtractAssociatedIcon(tmpFile);
            }
            catch (Exception)
            {
                return null;
            }
            return this.icons[tmpFileName];
        }

        /// <summary>
        /// Adds the icon if not exists.
        /// </summary>
        /// <param name="extension">the extension of the file type</param>
        /// <returns>Icon associated with the specified extension.</returns>
        private Icon addIconIfNotExists(string extension)
        {
            string validatedExt = this.validateExtension(extension);
            string tmpFileName = string.Concat("~$_", validatedExt);
            try
            {
                if (!this.icons.ContainsKey(tmpFileName))
                {
                    string tmpFile = string.Concat(tmpFolder, "\\", tmpFileName);
                    if (!File.Exists(tmpFile))
                        File.Create(tmpFile);
                    this.icons.Add(tmpFileName, Icon.ExtractAssociatedIcon(tmpFile));
                }
            }
            catch (Exception)
            {
                return null;
            }
            return this.icons[tmpFileName];
        }
        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        /// <summary>
        /// Class RepositoryIcons.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.Drawing.Icon}" />
        private class RepositoryIcons : Dictionary<string, Icon>
        { }
        #endregion

    }
}
