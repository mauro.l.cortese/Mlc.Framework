// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlFileReader.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using Mlc.Shell;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe per lettura semplificata di un file XML.
    /// </summary>
    /// <seealso cref="Mlc.Shell.IO.XmlReader" />
    public class XmlFileReader : XmlReader
    {
        #region Campi
        /// <summary>
        /// Determina se il file esiste
        /// </summary>
        private bool fileExists = false;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="fileXml">Percorso completo del file xml</param>
        public XmlFileReader(string fileXml)
        {
            this.fileXml = fileXml;
            this.fileExists = File.Exists(this.fileXml);
        }
        #endregion

        #region Propriet�
        /// <summary>
        /// Percorso completo del file xml
        /// </summary>
        private string fileXml;
        /// <summary>
        /// Restituisce o imposta il percorso completo del file xml
        /// </summary>
        /// <value>The file XML.</value>
        public string FileXml { get { return this.fileXml; } set { this.fileXml = value; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Determina se il file o il flusso in esame costituiscono un documento XML valido
        /// </summary>
        /// <returns>true se il documento � valido, altrimenti false</returns>
        public override bool IsValidXml()
        {
            if (base.xDoc == null)
            {
                try { base.xDoc = new XPathDocument(this.fileXml); }
                catch (Exception ex)
                {
                    ExceptionsRegistry.GetInstance().Add(ex);
                    base.xDoc = null; 
                }
            }
            return (base.xDoc == null) ? false : true;
        }

        /// <summary>
        /// Ottiene l'insieme di nodi dalla query XPath
        /// </summary>
        /// <param name="xPath">Query XPath</param>
        /// <returns>Istanza di XPathNodeIterator</returns>
        public override XPathNodeIterator GetXPathNodeIterator(string xPath)
        {
            XPathNodeIterator retVal = null;
            if (!fileExists) return retVal;
            try
            {
                if (base.xDoc == null)
                    if (fileExists)
                        base.xDoc = new XPathDocument(this.fileXml);

                if (base.xNav == null)

                    base.xNav = base.xDoc.CreateNavigator();

                base.nodeIterator = null;
                // ... ottengo i nodi del file 
                base.nodeIterator = base.xNav.Select(xPath);

                retVal = base.nodeIterator;
                return retVal;
            }
            catch (Exception ex) 
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                base.exception = (XmlException)ex;
                return retVal;
            }
        }
        #endregion

    }
}
