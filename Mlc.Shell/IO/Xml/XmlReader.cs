// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlReader.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

using Mlc.Shell;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe base da cui ereditare per la lettura di dati in formato XML.
    /// </summary>
    public abstract class XmlReader
    {
        #region Campi
        /// <summary>
        /// Documento xpath
        /// </summary>
        protected XPathDocument xDoc = null;
        /// <summary>
        /// Xpath navigator
        /// </summary>
        protected XPathNavigator xNav = null;
        #endregion

        #region Propriet�
        /// <summary>
        /// Iteratore sui nodi
        /// </summary>
        protected XPathNodeIterator nodeIterator;
        /// <summary>
        /// Restituisce l'iteratore sui nodi
        /// </summary>
        /// <value>The node iterator.</value>
        public XPathNodeIterator NodeIterator { get { return nodeIterator; } }

        /// <summary>
        /// Restituisce il numero di nodi trovati
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                if (nodeIterator == null) return 0;
                return nodeIterator.Count;
            }
        }

        /// <summary>
        /// Eccezione nei dati XML
        /// </summary>
        protected XmlException exception = null;
        /// <summary>
        /// Restituisce l'eccezione nei dati XML
        /// </summary>
        /// <value>The exception.</value>
        public XmlException Exception { get { return this.exception; } }
        #endregion

        #region Metodi pubblici statici
        /// <summary>
        /// Restituisce un istanza che legge i dati costruendo uno stream in base al testo XML passato
        /// </summary>
        /// <param name="textXml">Testo XML</param>
        /// <returns>Istanza basata su stream</returns>
        public static XmlReader GetReaderByText(string textXml)
        {
            // scrivo in uno stream il testo XML per accedervi con il parser
            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream, Encoding.Unicode);
            streamWriter.Write(textXml);
            streamWriter.Flush();
            return new XmlStreamReader(stream);
        }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Determina se il file o il flusso in esame costituiscono un documento XML valido
        /// </summary>
        /// <returns>True se il documento � valido, altrimenti false</returns>
        public abstract bool IsValidXml();

        /// <summary>
        /// Ottiene l'insieme di nodi dalla query XPath
        /// </summary>
        /// <param name="xPath">Query XPath</param>
        /// <returns>Istanza di XPathNodeIterator</returns>
        public abstract XPathNodeIterator GetXPathNodeIterator(string xPath);

        /// <summary>
        /// Restituisce il valore di un attributo dell'elemento corrente
        /// </summary>
        /// <param name="key">Chiave dell'attributo</param>
        /// <param name="valoreDefault">Valore di default per l'attributo</param>
        /// <returns>Valore letto se esiste, altrimenti quello di default</returns>
        public virtual string GetAttribute(string key, string valoreDefault)
        {
            string valore = valoreDefault;
            try
            {
                valore = this.nodeIterator.Current.GetAttribute(key, string.Empty);
                if (valore.Trim() == string.Empty) valore = valoreDefault;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            return valore;
        }

        /// <summary>
        /// Restituisce il valore d'elemento corrente
        /// </summary>
        /// <returns>il valore letto se esiste, altrimenti quello di default</returns>
        public virtual string Value()
        {
            string valore = string.Empty;
            try
            {
                valore = nodeIterator.Current.Value;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            return valore;
        }

        /// <summary>
        /// Si sposta sul nodo successivo
        /// </summary>
        /// <returns>True se ha trovato un nodo, altrimenti false</returns>
        public bool Read()
        {
            if (nodeIterator == null) return false;
            return nodeIterator.MoveNext();
        }

        /// <summary>
        /// Retituisce l'immagine serializzata nel nodo corrente
        /// </summary>
        /// <returns>Restituisce l'immagine ottenuta, null se non � statao trova un'immagine</returns>
        public Image LoadImage()
        {
            Image retImage = null;
            try
            {
                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(byte[]));
                System.Xml.XmlReader xr = System.Xml.XmlReader.Create(new StringReader(this.NodeIterator.Current.OuterXml));
                byte[] byteImage = (byte[])xs.Deserialize(xr);
                retImage = byteImage.ByteArrayToImage();
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            return retImage;
        }
        #endregion





    }
}
