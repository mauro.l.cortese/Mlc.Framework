// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlStreamReader.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Xml.XPath;

using Mlc.Shell.Crypto;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe base per scrittura semplificata di file XML.
    /// </summary>
    /// <seealso cref="Mlc.Shell.IO.XmlReader" />
    /// <seealso cref="System.IDisposable" />
    public class XmlStreamReader : XmlReader, IDisposable
    {

        #region Campi
        /// <summary>
        /// Memorizza lo strem
        /// </summary>
        private Stream stream = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="stream">Memory stream da cui leggere</param>
        public XmlStreamReader(Stream stream)
        {
            this.stream = stream;
        }

        #endregion

        #region Propriet�
        #endregion

        #region Metodi pubblici statici
        /// <summary>
        /// Restituisce un MemoryStream dal file su disco, sia che il file sia criptato o in chiaro
        /// </summary>
        /// <param name="file">Fullpath del file dal quale ottenere lo stream</param>
        /// <returns>MemoryStream ottenuto</returns>
        public static MemoryStream GetStreamByFile(string file)
        {
            byte[] buffer = File.ReadAllBytes(file);
            RijndaelCryptoByte cryptoByte = new RijndaelCryptoByte(buffer);

            if (cryptoByte.Decrypt())
                return new MemoryStream(cryptoByte.Data);
            else
                return new MemoryStream(buffer);
        }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Determina se il file o il flusso in esame costituiscono un documento XML valido
        /// </summary>
        /// <returns>true se il documento � valido, altrimenti false</returns>
        public override bool IsValidXml()
        {
            if (base.xDoc == null)
            {
                try
                {
                    this.stream.Seek(0, SeekOrigin.Begin);
                    base.xDoc = new XPathDocument(this.stream);
                }
                catch (Exception ex)
                {
                    ExceptionsRegistry.GetInstance().Add(ex);
                    base.xDoc = null;
                }
            }
            return (base.xDoc == null) ? false : true;
        }

        /// <summary>
        /// Ottiene l'insieme di nodi dalla query XPath
        /// </summary>
        /// <param name="xPath">Query XPath</param>
        /// <returns>Istanza di XPathNodeIterator</returns>
        public override XPathNodeIterator GetXPathNodeIterator(string xPath)
        {
            XPathNodeIterator retVal = null;
            try
            {
                if (base.xDoc == null)
                {
                    try
                    {
                        this.stream.Seek(0, SeekOrigin.Begin);
                        base.xDoc = new XPathDocument(this.stream);
                    }
                    catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                }
                if (base.xNav == null)
                    base.xNav = base.xDoc.CreateNavigator();

                this.nodeIterator = null;
                // ... ottengo i nodi del file 
                this.nodeIterator = base.xNav.Select(xPath);

                retVal = this.nodeIterator;

                return retVal;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return retVal;
            }

        }

        /// <summary>
        /// Libera le risorse
        /// </summary>
        public void Dispose()
        {
            if (this.stream != null)
                this.stream.Close();
        }
        #endregion
    }
}
