// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlSerializer.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Text;
using sx = System.Xml;
using System.Xml.Serialization;


namespace Mlc.Shell.IO
{
    /// <summary>
    /// Permette la serializzazione e deserializzazione di istanze in formato Xml
    /// </summary>
    /// <typeparam name="T">Type to serialize</typeparam>
    public class XmlSerializer<T>
    {
        #region Campi
        /// <summary>
        /// File in cui salvare i dati
        /// </summary>
        private FileSystemMgr fileXml = null;
        /// <summary>
        /// Cartella in cui si trova il file
        /// </summary>
        private FileSystemMgr folder = null;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="fileXml">Fullpath del file .xml</param>
        public XmlSerializer(string fileXml)
        {
            this.fileXml = new FileSystemMgr(fileXml);
            this.folder = (FileSystemMgr)this.fileXml.FsoParentFolder;
        }
        #endregion

        #region Propriet�
        /// <summary>
        /// Determina se il file .xml esiste
        /// </summary>
        /// <value><c>true</c> if [file exist]; otherwise, <c>false</c>.</value>
        public bool FileExist { get { return this.fileXml.Exists; } }

        /// <summary>
        /// Determina se la cartella contenente il file .xml esiste
        /// </summary>
        /// <value><c>true</c> if [folder exist]; otherwise, <c>false</c>.</value>
        public bool FolderExist { get { return this.folder.Exists; } }

        /// <summary>
        /// The instance
        /// </summary>
        T instance = default(T);
        /// <summary>
        /// Restituisce l'istanza deserializzata
        /// </summary>
        /// <value>The instance.</value>
        public T Instance { get { return this.instance; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Serializza l'istanza corrente
        /// </summary>
        /// <returns>True se l'operazione riesce senza errori. Altrimenti false</returns>
        public bool Serialize() { return this.Serialize(this.instance); }

        /// <summary>
        /// Serializza l'istanza passata nel file .xml specificato
        /// </summary>
        /// <param name="obj">Istanza da serializzare</param>
        /// <returns>True se l'operazione riesce senza errori. Altrimenti false</returns>
        public bool Serialize(T obj)
        {
            try
            {
                if (obj == null) return false;
                StringBuilder xml = new();
                using (sx.XmlWriter w = sx.XmlTextWriter.Create(xml))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(w, obj);
                }
                sx.XmlDocument xd = new sx.XmlDocument();
                xd.LoadXml(xml.ToString());
                xd.Save(this.fileXml.Path);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// Deserializza l'istanza
        /// </summary>
        /// <returns>True se l'operazione riesce senza errori. Altrimenti false</returns>
        public bool Deserialize()
        {
            bool retVal = false;
            try
            {
                if (!this.FileExist)
                {
                    if (!this.FolderExist)
                    {
                        this.folder.Create(FileSystemTypes.Directory);
                        this.folder.Refresh();
                    }
                    if (this.FolderExist)
                        this.Serialize();
                    this.fileXml.Refresh();
                }
                if (FileExist)
                {
                    using (sx.XmlReader reader = sx.XmlReader.Create(this.fileXml.Path))
                    {
                        XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                        this.instance = (T)xmlSerializer.Deserialize(reader);
                    }
                    retVal = true;
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }
        #endregion
    }
}
