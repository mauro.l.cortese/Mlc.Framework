// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlWriter.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Drawing;
using System.IO;
using System.Xml;
using Mlc.Shell;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Classe base da cui ereditare per la scrittura di dati in formato XML.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public abstract class XmlWriter : IDisposable
    {
        /// <summary>
        /// Writer XML
        /// </summary>
        public XmlTextWriter XmlTextWriter = null;

        /// <summary>
        /// Apre e inizializza il file xml, eseguire l'override di questo metodo istanziando base.xmlTextWriter nel modo opportuno.
        /// </summary>
        /// <param name="rootName">Nome del nodo root</param>
        public virtual void OpenXML(string rootName)
        {
            // inizializzo il parser
            this.XmlTextWriter.Formatting = Formatting.Indented;
            this.XmlTextWriter.Indentation = 4;
            this.XmlTextWriter.IndentChar = ' ';

            // creo il file
            this.XmlTextWriter.WriteStartDocument();
            this.OpenNode(rootName);  // apro la root
        }

        /// <summary>
        /// Chiude il file xml.
        /// </summary>
        public void CloseXML()
        {
            this.XmlTextWriter.Close();
            this.XmlTextWriter = null;
        }

        /// <summary>
        /// Libera le risorse.
        /// </summary>
        public void Dispose()
        {
            if (this.XmlTextWriter != null)
                this.CloseXML();
        }

        /// <summary>
        /// Apre un nodo nel file XML.
        /// </summary>
        /// <param name="nome">Nome del nodo</param>
        public void OpenNode(string nome)
        {
            XmlTextWriter.WriteStartElement(nome);
        }

        /// <summary>
        /// Scrive una stringa per l'ultimo elemento aperto
        /// </summary>
        /// <param name="valore">Testo da scrivere nell'elemento</param>
        public void WriteValue(string valore)
        {
            XmlTextWriter.WriteString(valore);
        }

        /// <summary>
        /// Scrive un commento
        /// </summary>
        /// <param name="valore">Commento da scrivere</param>
        public void WriteCommento(string valore)
        {
            XmlTextWriter.WriteComment(valore);
        }

        /// <summary>
        /// Scrive un elemento CDATA
        /// </summary>
        /// <param name="valore">Elemento CDATA da scrivere</param>
        public void WriteData(string valore)
        {
            XmlTextWriter.WriteCData(valore);
        }

        /// <summary>
        /// Chiude l'ultimo elemento nodo aperto
        /// </summary>
        public void CloseNode()
        {
            XmlTextWriter.WriteEndElement();
            XmlTextWriter.Flush();
        }

        /// <summary>
        /// Chiude il numero (numberNode) di elementi nodo aperti
        /// </summary>
        /// <param name="numberNode">The number node.</param>
        public void CloseNode(int numberNode)
        {
            for (int i = 0; i < numberNode; i++)
                this.CloseNode();
        }

        /// <summary>
        /// Aggiunge un atributo all'elemento aperto
        /// </summary>
        /// <param name="nome">Nome dell'attributo</param>
        /// <param name="valore">Valore dell'attributo</param>
        public void AddAttribute(string nome, string valore)
        {
            XmlTextWriter.WriteAttributeString(nome, valore);
        }

        /// <summary>
        /// Salva l'immagine nel nodo corrente
        /// </summary>
        /// <param name="image">immagine da salvare</param>
        public void SaveImage(Image image)
        {
            byte[] byteImage = image.ImageToByteArray();
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(byte[]));
            TextWriter ts = new StringWriter();
            xs.Serialize(this.XmlTextWriter, byteImage);
        }
    }
}
