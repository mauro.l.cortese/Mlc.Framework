﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Enumerations.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Enumerating all the types of elements of the file system.
    /// </summary>
    public enum FileSystemTypes
    {
        /// <summary>
        /// Specifies that the element is undefined
        /// </summary>
        UnDefine,
        /// <summary>
        /// Specifies that the element is a file
        /// </summary>
        File,
        /// <summary>
        /// Specifies that the element is a folder (directory)
        /// </summary>
        Directory,
    }
}