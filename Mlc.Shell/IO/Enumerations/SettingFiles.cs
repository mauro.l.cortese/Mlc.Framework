﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Enumerations.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.IO
{
    /// <summary>
    /// Enumerating all the possible places where a setting parameter can be saved
    /// </summary>
    [Flags]
    public enum SettingFiles
    {
        /// <summary>
        /// No setting.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies a client setting.
        /// </summary>
        Local = 2,
        /// <summary>
        /// Specifies a user roaming setting.
        /// </summary>
        RoamingUser = 4,
        /// <summary>
        /// Specifies a server setting.
        /// </summary>
        Roaming = 8,
        /// <summary>
        /// Specifies all settings.
        /// </summary>
        All = SettingFiles.Local | SettingFiles.RoamingUser | SettingFiles.Roaming,
    }
}