﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="SingletonBase.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell
{
	/// <summary>
	/// A base class for the singleton design pattern.
	/// </summary>
	/// <typeparam name="T">Class type of the singleton</typeparam>
	public abstract class SingletonBase<T> where T : class
	{
		#region Members
		/// <summary>
		/// Static instance. Needs to use lambda expression
		/// to construct an instance (since constructor is private).
		/// </summary>
		protected readonly static Lazy<T> instance = new Lazy<T>(() => getInstance());
		///protected static Lazy<T> instance;
		#endregion

		#region Methods
		/// <summary>
		/// Gets the instance of this singleton.
		/// </summary>
		/// <returns>T.</returns>
		public static T GetInstance()
		{
			return instance.Value;
		}

		/// <summary>
		/// Creates an instance of T via reflection since T's constructor is expected to be private.
		/// </summary>
		/// <returns>T.</returns>
		private static T getInstance()
		{
			return Activator.CreateInstance(typeof(T), true) as T;
		}

		#endregion
	}
}
