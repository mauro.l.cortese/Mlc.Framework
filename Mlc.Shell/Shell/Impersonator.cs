﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : mauro.luigi.cortese
// Created          : 12-13-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-13-2019
// ***********************************************************************
// <copyright file="Impersonator.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace Mlc.Shell
{

	/// <summary>
	/// Class Impersonator.
	/// Implements the <see cref="System.IDisposable" />
	/// </summary>
	/// <seealso cref="System.IDisposable" />
	public class Impersonator : IDisposable
	{
		const int LOGON32_PROVIDER_DEFAULT = 0;
		//This parameter causes LogonUser to create a primary token.
		const int LOGON32_LOGON_INTERACTIVE = 2;

		/// <summary>
		/// Logons the user.
		/// </summary>
		/// <param name="lpszUsername">The LPSZ username.</param>
		/// <param name="lpszDomain">The LPSZ domain.</param>
		/// <param name="lpszPassword">The LPSZ password.</param>
		/// <param name="dwLogonType">Type of the dw logon.</param>
		/// <param name="dwLogonProvider">The dw logon provider.</param>
		/// <param name="phToken">The ph token.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		[DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
			int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

		/// <summary>
		/// Closes the handle.
		/// </summary>
		/// <param name="handle">The handle.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public extern static bool CloseHandle(IntPtr handle);


		/// <summary>
		/// The safe token handle
		/// </summary>
		private SafeTokenHandle safeTokenHandle;
		/// <summary>
		/// The new identifier
		/// </summary>
		private WindowsIdentity newId;
		/// <summary>
		/// The impersonated user
		/// </summary>
		private WindowsImpersonationContext impersonatedUser;


		/// <summary>
		/// Initializes a new instance of the <see cref="Impersonator"/> class.
		/// </summary>
		/// <param name="userName">Name of the user (mauro.cortese).</param>
		/// <param name="domainName">Name of the domain (LIVETECH.local).</param>
		/// <param name="password">The password.</param>
		public Impersonator(string userName, string domainName, string password)
		{
			this.impersonateValidUser(userName, domainName, password);
		}

		/// <summary>
		/// The user before impersonate
		/// </summary>
		public string UserBeforeImpersonate { get; } = WindowsIdentity.GetCurrent().Name;

		/// <summary>
		/// The user before impersonate
		/// </summary>
		public string UserAfterImpersonate { get; private set; } = string.Empty;

		/// <summary>
		/// Initializes a new instance of the <see cref="Impersonator"/> class.
		/// </summary>
		/// <param name="domainUserName">Name of the domain (mauro.cortese@livetech.local oppure LIVETECH\mauro.cortese).</param>
		/// <param name="password">The password.</param>
		public Impersonator(string domainUserName, string password)
		{
			string userName = null, domainName = null;
			if (domainUserName.Contains("@"))
			{
				string[] strs = domainUserName.Split('@');
				if (strs.Length == 2)
				{
					userName = strs[0];
					domainName = strs[1];
				}
			}
			else if (domainUserName.Contains("\\"))
			{
				string[] strs = domainUserName.Split('\\');
				if (strs.Length == 2)
				{
					userName = strs[1];
					domainName = strs[0];
				}
			}

			if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(domainName))
				throw new Exception("ERROR!! Invalid username or domain");

			this.impersonateValidUser(userName, domainName, password);
		}




		/// <summary>
		/// Impersonates the valid user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="domainName">Name of the domain.</param>
		/// <param name="password">The password.</param>
		[PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
		private void impersonateValidUser(string userName, string domainName, string password)
		{

			try
			{
				// Call LogonUser to obtain a handle to an access token.
				bool returnValue = LogonUser(userName, domainName, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out this.safeTokenHandle);

				if (false == returnValue)
				{
					int ret = Marshal.GetLastWin32Error();
					Console.WriteLine("LogonUser failed with error code : {0}", ret);
					throw new System.ComponentModel.Win32Exception(ret);
				}

				this.newId = new WindowsIdentity(this.safeTokenHandle.DangerousGetHandle());
				this.impersonatedUser = newId.Impersonate();
				this.UserAfterImpersonate = WindowsIdentity.GetCurrent().Name;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception occurred. " + ex.Message);
			}
		}


		/// <summary>
		/// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite.
		/// </summary>
		public void Dispose()
		{
			try
			{
				this.impersonatedUser.Dispose();
				this.newId.Dispose();
				this.safeTokenHandle.Dispose();
			}
			catch (Exception) { throw; }
		}


		/// <summary>
		/// Class SafeTokenHandle. This class cannot be inherited.
		/// Implements the <see cref="Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid" />
		/// </summary>
		/// <seealso cref="Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid" />
		public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
		{
			/// <summary>
			/// Prevents a default instance of the <see cref="SafeTokenHandle"/> class from being created.
			/// </summary>
			private SafeTokenHandle()
				: base(true)
			{
			}

			/// <summary>
			/// Closes the handle.
			/// </summary>
			/// <param name="handle">The handle.</param>
			/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
			[DllImport("kernel32.dll")]
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			[SuppressUnmanagedCodeSecurity]
			[return: MarshalAs(UnmanagedType.Bool)]
			private static extern bool CloseHandle(IntPtr handle);

			/// <summary>
			/// Quando ne viene eseguito l'override in una classe derivata, esegue il codice necessario per liberare l'handle.
			/// </summary>
			/// <returns><see langword="true" /> se l'handle viene rilasciato correttamente; in caso contrario, se si verifica un errore irreversibile, <see langword=" false" />.
			/// In questo caso, genera un assistente al debug gestito releaseHandleFailed MDA.</returns>
			protected override bool ReleaseHandle() => CloseHandle(handle);
		}

	}

}
