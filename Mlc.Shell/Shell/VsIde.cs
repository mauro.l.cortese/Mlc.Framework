﻿using System.Diagnostics;

namespace Mlc.Shell
{
    /// <summary>
    /// Determina se è in esecuzione l'ambiente di sviluppo
    /// </summary>
    public class VsIde
    {
        /// <summary>
        /// Determina se è in esecuzione l'ambiente devenv
        /// </summary>
        public static bool DesignMode { get { return (Process.GetCurrentProcess().ProcessName == "devenv"); } }
    }
}
