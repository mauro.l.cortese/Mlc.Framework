// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ExceptionsRegistry.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Shell.IO;
using System;
using System.Text;
using System.Collections.Generic;

namespace Mlc.Shell
{
	/// <summary>
	/// Class ExceptionsRegistry.
	/// </summary>
	/// <seealso cref="System.Collections.Generic.List{Mlc.Shell.ExceptionsRegistry.RowException}" />
	public class ExceptionsRegistry : List<ExceptionsRegistry.RowException>
	{
		#region Fields
		/// <summary>
		/// Variable for thread safe
		/// </summary>
		private static object syncRoot = new Object();
		/// <summary>
		/// Stores the instance of the class
		/// </summary>
		private static volatile ExceptionsRegistry instance = null;

		/// <summary>
		/// The log
		/// </summary>
		private Logger20.Log log;
		#endregion

		#region Constructors
		/// <summary>
		/// Prevents a default instance of the <see cref="ExceptionsRegistry" /> class from being created.
		/// </summary>
		/// <remarks>The private constructor does not allow the direct instantiation of this class</remarks>
		private ExceptionsRegistry()
		{
			this.Clear();
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// [ERROR: invalid expression FieldName.Words.TheAndAllAsSentence]
		/// </summary>
		private int maxRow = 0;
		/// <summary>
		/// Gets or sets the maximum row.
		/// </summary>
		/// <value>The maximum row.</value>
		/// <remarks>0 to store all exceptions</remarks>
		public int MaxRow { get { return this.maxRow; } set { this.maxRow = value; } }
		#endregion

		#region Method Static
		/// <summary>
		/// Always returns the same instance of the class.
		/// </summary>
		/// <returns>The instance of the class <see cref="ExceptionsRegistry" /></returns>
		public static ExceptionsRegistry GetInstance()
		{
			// if the instance was not yet allocated
			if (instance == null)
			{
				lock (syncRoot)
				{
					// it gets a new instance
					if (instance == null)
						instance = new ExceptionsRegistry();

				}
			}
			else if (instance.log == null)
				instance.log = Logger20.GetInstance()?.DefaultLog;

			// it always returns the same instance
			return instance;
		}
		#endregion

		#region Method
		/// <summary>
		/// It adds a new instance of <see cref="Exception" /> to the registry
		/// </summary>
		/// <param name="exception">Exception to add</param>
		public void Add(Exception exception)
		{

			if (this.log != null)
			{
				StringBuilder sb = new();
				sb.AppendLine("[EXCEPTION]");
				sb.AppendLine();
				sb.AppendLine("Message:");
				sb.AppendLine(exception.Message);
				sb.AppendLine();
				sb.AppendLine("StackTrace:");
				sb.AppendLine(exception.StackTrace);

				this.log?.Error(sb);
			}
			this.Add(new RowException(exception));

			// if you set the maximum number of exceptions to keep in the database and this number is exceeded ...
			if (this.maxRow != 0 && this.Count > this.maxRow)
				// it deletes the exception oldest
				this.RemoveAt(0);

			this.OnCapturedException(new EventArgs());
		}
		#endregion
		#endregion

		#region Event Handlers
		#region CapturedException Event        
		/// <summary>
		/// Event that occurs when captured exception.
		/// </summary>
		public event EventHandler CapturedException;

		/// <summary>
		/// Handles the <see cref="E:CapturedException" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected virtual void OnCapturedException(EventArgs e)
		{
			// If there are receptors listening, the event is raised
			this.CapturedException?.Invoke(this, e);
		}
		#endregion
		#endregion

		#region Embedded Types
		/// <summary>
		/// This class is for Encapsulates the data of an exception, it is a row of <see cref="ExceptionsRegistry" />
		/// </summary>
		public class RowException
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="RowException" /> class.
			/// </summary>
			/// <param name="exception">The exception.</param>
			public RowException(Exception exception)

			{
				this.time = DateTime.Now;
				this.exception = exception;

				string[] stackLines = Environment.StackTrace.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

				string[] stackLines2 = new string[stackLines.Length - 4];
				Array.Copy(stackLines, 4, stackLines2, 0, stackLines2.Length);

				this.stackTrace = string.Join(Environment.NewLine, stackLines2);
			}
			#endregion

			#region Public
			#region Properties
			/// <summary>
			/// The time at which the exception occurred
			/// </summary>
			private DateTime time;
			/// <summary>
			/// Gets the time at which the exception occurred.
			/// </summary>
			/// <value>The time.</value>
			public DateTime Time { get { return this.time; } }

			/// <summary>
			/// Instance of the exception
			/// </summary>
			private Exception exception;
			/// <summary>
			/// Gets the instance of the exception.
			/// </summary>
			/// <value>The instance of the exception.</value>
			public Exception Exception { get { return this.exception; } }

			/// <summary>
			/// The call stack report
			/// </summary>
			private string stackTrace;
			/// <summary>
			/// Gets the call stack report
			/// </summary>
			/// <value>The stack trace.</value>
			public string StackTrace { get { return this.stackTrace; } }
			#endregion
			#endregion
		}
		#endregion
	}
}
