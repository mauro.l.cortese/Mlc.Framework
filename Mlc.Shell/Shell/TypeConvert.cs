﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="TypeConvert.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Drawing;
using System.Reflection;
using System.Xml;

namespace Mlc.Shell
{
    /// <summary>
    /// Class TypeConvert. Tool for converting values from string format to the required type
    /// </summary>
    /// <typeparam name="ResultType">The type of the result type.</typeparam>
    public class TypeConvert<ResultType>
    {
        #region Metodi pubblici statici
        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <returns>Value obtained</returns>
        public static ResultType GetValue(object value)
        {
            ResultType retVal = default(ResultType);
            try
            {
                if (value is DateTime && typeof(ResultType) == typeof(string))
                    retVal = (ResultType)Convert.ChangeType(((DateTime)value).Serialize(), typeof(ResultType));
                else if (value is string && typeof(ResultType) == typeof(DateTime))
                    retVal = (ResultType)Convert.ChangeType(((string)value).ToDateTime(), typeof(ResultType));
                else if (value is IConvertible)
                    retVal = (ResultType)Convert.ChangeType(value, typeof(ResultType));
                else
                    if (value == null)
                        retVal = default(ResultType);
                    else
                        retVal = TypeConvert<ResultType>.GetValue(value.ToString());
            }
            catch { }
            return retVal;
        }

        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <returns>Value obtained</returns>
        public static ResultType GetValue(string value)
        {
            return GetValue(value, default(ResultType));
        }

        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="defaultValue">Value to return as default</param>
        /// <returns>Value obtained</returns>
        /// <exception cref="Exception">Type conversion error</exception>
        public static ResultType GetValue(string value, ResultType defaultValue)
        {
            ResultType retval = defaultValue;
            if (defaultValue == null)
                defaultValue = default(ResultType);
            try
            {
                if (retval is Size) //"{Width=10, Height=20}" 
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 2)
                    {
                        Size size = new Size(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(size, typeof(ResultType));
                    }
                }
                else if (retval is Point) //"{X=15,Y=15}" 
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 2)
                    {
                        Point point = new Point(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(point, typeof(ResultType));
                    }
                }
                else if (retval is Rectangle) //"{X=10,Y=10,Width=200,Height=100}"
                {
                    string[] vals = value.Normalize("", "", "{} ", null).Split(',');
                    if (vals.Length == 4)
                    {
                        Rectangle rectangle = new Rectangle(
                            int.Parse(vals[0].Split('=')[1]),
                            int.Parse(vals[1].Split('=')[1]),
                            int.Parse(vals[2].Split('=')[1]),
                            int.Parse(vals[3].Split('=')[1])
                            );
                        retval = (ResultType)Convert.ChangeType(rectangle, typeof(ResultType));
                    }
                }
                else if (retval is Enum)
                {
                    try
                    {
                        retval = (ResultType)Enum.Parse(typeof(ResultType), value, true);
                    }
                    catch (Exception ex)
                    {
                        ExceptionsRegistry.GetInstance().Add(ex);
                        retval = default(ResultType);
                    }
                }
                // TODO: Completare implementazione del tipo FSO
                //else if (retval is FileSystemMgr)
                //{
                //    try
                //    {
                //        retval = (ResultType)Convert.ChangeType(new FileSystemMgr(value), typeof(ResultType));
                //    }
                //    catch { retval = default(ResultType); }
                //}
                else if (retval is Color)
                    retval = (ResultType)Convert.ChangeType(value.ParseColor(), typeof(ResultType));
                else if (retval is Font)
                    retval = (ResultType)Convert.ChangeType(value.StringToFont(), typeof(ResultType));
                else
                    retval = (ResultType)Convert.ChangeType(value, typeof(ResultType));
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                if (!retval.Equals(defaultValue))
                    throw new Exception("Errore di conversione tipo");
            }
            return retval;
        }


        /// <summary>
        /// Gets the value of the type enumerated based on its assembly qualified name
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="assemblyQualifiedName">Assembly qualified name of the type of the enumerated to obtain</param>
        /// <returns>Value obtained</returns>
        public static Enum GetValue(string value, string assemblyQualifiedName)
        {
            try
            {
                Type type = Type.GetType(assemblyQualifiedName);
                return (Enum)Enum.Parse(type, value);
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return null;
            }
        }

        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <param name="xmlNode">XML node from which to read an attribute</param>
        /// <param name="nameAttribute">Attribute name</param>
        /// <returns>Value obtained</returns>
        public static ResultType GetValue(XmlNode xmlNode, string nameAttribute)
        {
            return GetValue(xmlNode, nameAttribute, default(ResultType));
        }

        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <param name="xmlNode">XML node from which to read an attribute</param>
        /// <param name="nameAttribute">Attribute name</param>
        /// <param name="defaultValue">Value to return as default</param>
        /// <returns>Value obtained</returns>
        public static ResultType GetValue(XmlNode xmlNode, string nameAttribute, ResultType defaultValue)
        {
            try { return GetValue(xmlNode.Attributes[nameAttribute].Value, defaultValue); }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return defaultValue;
            }
        }

        /// <summary>
        /// Set the value for the passed property by converting it from the string type to the correct type
        /// </summary>
        /// <param name="obj">Instance to set the property value</param>
        /// <param name="propertyName">Property to set</param>
        /// <returns>Property value</returns>
        public static ResultType GetProperty(object obj, string propertyName)
        {
            ResultType retVal = default(ResultType);

            PropertyInfo property = obj.GetType().GetProperty(propertyName);
            if (property != null)
                retVal = (ResultType)property.GetValue(obj, null);

            return retVal;
        }

        #endregion
    }
}
