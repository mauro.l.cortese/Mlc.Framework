﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ByteExtensions.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Text;

namespace Mlc.Shell
{

	/// <summary>
	/// Class ListStringExtensions.
	/// </summary>
	public static class ListStringExtensions
	{
		/// <summary>
		/// Splits the elements of the list into sublists with specified maximum character length
		/// </summary>
		/// <param name="listTxt">The list.</param>
		/// <param name="maxLenght">The maximum lenght.</param>
		/// <returns>List&lt;System.String&gt;.</returns>
		public static List<string> Split(this List<string> listTxt, int maxLenght)
		{
			List<string> retList = new List<string>();
			StringBuilder sb = new();
			foreach (string txt in listTxt)
			{
				string itemTxt = $",{txt}";
				if (sb.Length-1 + itemTxt.Length > maxLenght)
				{
					sb.Remove(0, 1);
					retList.Add(sb.ToString());
					sb.Clear();
				}
				sb.Append(itemTxt);
			}

			if (sb.Length > 1)
			{
				sb.Remove(0, 1);
				retList.Add(sb.ToString());
			}

			return retList;
		}
	}
}
