﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ByteExtensions.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Text;

namespace Mlc.Shell
{
	/// <summary>
	/// Class StringExtension. Provides extension methods for fast operations on <see cref="Byte" /> type.
	/// </summary>
	public static class ArrayExtensions
	{
		/// <summary>
		/// It returns a new array consisting of the elements of the first to which
		/// the elements of the second will be appended
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		/// <returns>A new array</returns>
		/// <exception cref="ArgumentNullException">y</exception>
		/// <example>
		/// In the below sample you can see how to get a text string from a byte array
		/// <code>
		/// using System;
		/// using Mlc.Shell;
		/// namespace Sample
		/// {
		///		class Program
		///		{
		///			static void Main(string[] args)
		///			{
		///				int[] x = {1,2,3}, y = {4,5};
		///				int[] z = x.Concat(y);
		///				// now z = {1,2,3,4,5}
		///			}
		///		}
		///	}
		/// </code></example>
		public static T[] Concat<T>(this T[] x, T[] y)
		{
			if (x == null) throw new ArgumentNullException("x");
			if (y == null) throw new ArgumentNullException("y");
			int oldLen = x.Length;
			Array.Resize<T>(ref x, x.Length + y.Length);
			Array.Copy(y, 0, x, oldLen, y.Length);
			return x;
		}
	}
}
