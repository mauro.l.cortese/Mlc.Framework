// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="CriptoStringExtension.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mlc.Shell.Crypto;

namespace Mlc.Shell
{
    /// <summary>
    /// Extensions for string encryption and decryption
    /// </summary>
    /// <example>
    /// Using the static methods of this class, it is possible to perform string encryption and decryption
    /// operations very quickly. To use the extended methods add the following using directive
    /// <code>
    /// using Mlc.Shell.Crypto;
    /// </code>
    /// <para>This example shows how to encrypt and decrypt a string using the default constant password <see cref="Mx.Core.Constants.Strings.DefaultPwd" /></para>
    /// <code>
    /// string cryptoValue = "Text to hide".Encrypt();
    /// string value = cryptoValue.Decrypt();
    /// </code>
    /// Here is how to encrypt and decrypt a string using a given password instead
    /// <code>
    /// string cryptoValue = "Text to hide".Encrypt("a1bd4@32");
    /// string value = cryptoValue.Decrypt("a1bd4@32");
    /// </code>
    /// </example>
    public static class CriptoStringExtension
    {
        /// <summary>
        /// Encrypt the contents of the string passed using the constant password <see cref="Mx.Core.Constants.Strings.DefaultPwd" />
        /// </summary>
        /// <param name="value">String to be encrypted</param>
        /// <returns>Encrypted string</returns>
        public static string Encrypt(this string value)
        {
            return value.Encrypt(string.Empty);
        }

        /// <summary>
        /// Encrypt the contents of the passed string
        /// </summary>
        /// <param name="value">String to be encrypted</param>
        /// <param name="password">Password for string encryption</param>
        /// <returns>Encrypted string</returns>
        public static string Encrypt(this string value, string password)
        {
            RijndaelCryptoByte cb = new RijndaelCryptoByte(value.GetBytes());
            if (!string.IsNullOrEmpty(password))
                cb.Password = password;
            cb.Encrypt();
            return cb.Data.GetString();
        }

        /// <summary>
        /// Decrypt the contents of the string passed using the constant password <see cref="Mx.Core.Constants.Strings.DefaultPwd" />
        /// </summary>
        /// <param name="value">String to be decrypted</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(this string value)
        {
            return value.Decrypt(string.Empty);
        }

        /// <summary>
        /// Decrypt the contents of the passed string
        /// </summary>
        /// <param name="value">String to be decrypted</param>
        /// <param name="password">Password for string decryption</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(this string value, string password)
        {
            RijndaelCryptoByte cb = new RijndaelCryptoByte(value.GetBytes());
            if (!string.IsNullOrEmpty(password))
                cb.Password = password;
            cb.Decrypt();
            return cb.Data.GetString();
        }
    }
}
