// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-29-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="StringExtension.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Shell
{
    /// <summary>
    /// Class StringExtension. Provides extension methods for fast operations on <see cref="String" /> type.
    /// </summary>
    public static class StringArraySqlExtension
    {
        /// <summary>
        /// Adds the apex.
        /// </summary>
        /// <param name="strItems">The text.</param>
        /// <returns>System.String.</returns>
        public static string[] CheckForSql (this string[] strItems)
        {
            for (int i = 0; i < strItems.Length; i++)
                strItems[i] = strItems[i].CheckForSql();

            return strItems;
        }

    }
}
