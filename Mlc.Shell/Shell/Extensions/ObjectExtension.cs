// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ObjectExtension.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Mlc.Shell
{
	/// <summary>
	/// Extensions for object types.
	/// </summary>
	public static class ObjectExtension
	{
		/// <summary>
		/// Returns a dictionary with the properties of the past instance
		/// </summary>
		/// <param name="obj">Instance to get properties</param>
		/// <returns>Properties dictionary</returns>
		public static Dictionary<string, PropertyInfo> GetProperties(this object obj)
		{
			return GetProperties(obj, null);
		}

		/// <summary>
		/// Returns a dictionary with the properties of the past instance
		/// </summary>
		/// <param name="obj">Instance to get properties</param>
		/// <param name="attributeType">Type of the attribute that the properties must contain</param>
		/// <returns>Properties dictionary</returns>
		public static Dictionary<string, PropertyInfo> GetProperties(this object obj, Type attributeType)
		{
			Dictionary<string, PropertyInfo> dicProperties = new Dictionary<string, PropertyInfo>();
			if (obj == null)
				return dicProperties;

			PropertyInfo[] properties = obj.GetType().GetProperties();
			foreach (PropertyInfo pInfo in properties)
			{
				if (attributeType == null)
					dicProperties.Add(pInfo.Name, pInfo);
				else
				{
					if (pInfo.GetCustomAttributes(attributeType, true).Length == 1)
						dicProperties.Add(pInfo.Name, pInfo);
				}
			}
			return dicProperties;
		}

		/// <summary>
		/// Returns a dictionary with the properties of the past instance
		/// </summary>
		/// <param name="type">Instance to get properties</param>
		/// <param name="attributeType">Type of the attribute that the properties must contain</param>
		/// <returns>Properties dictionary</returns>
		public static Dictionary<string, PropertyInfo> GetTypeProperties(this Type type, Type attributeType)
		{
			PropertyInfo[] properties = type.GetProperties();
			Dictionary<string, PropertyInfo> dicProperties = new Dictionary<string, PropertyInfo>();
			foreach (PropertyInfo pInfo in properties)
			{
				if (attributeType == null)
					dicProperties.Add(pInfo.Name, pInfo);
				else
				{
					if (pInfo.GetCustomAttributes(attributeType, true).Length == 1)
						dicProperties.Add(pInfo.Name, pInfo);
				}
			}
			return dicProperties;
		}



		/// <summary>
		/// Enhance the properties of the instance with the value of the properties of another instance
		/// </summary>
		/// <param name="obj">Instance to set properties</param>
		/// <param name="sourceObject">Instance to get property values</param>
		public static void SetProperties(this object obj, object sourceObject)
		{
			Dictionary<string, PropertyInfo> thisProperties = obj.GetProperties();
			Dictionary<string, PropertyInfo> sourceProperties = sourceObject.GetProperties();

			// ciclo per le Propriet� dell'oggetto corrente
			foreach (string key in thisProperties.Keys)
				if (sourceProperties.ContainsKey(key))
					if (thisProperties[key].PropertyType != typeof(System.Data.DataRow) &&
						thisProperties[key].PropertyType != typeof(System.Data.DataTable) &&
						thisProperties[key].PropertyType != typeof(Object)
						)
						if (thisProperties[key].PropertyType == sourceProperties[key].PropertyType)
							if (thisProperties[key].CanWrite)
								try
								{
									thisProperties[key].SetValue(obj, sourceProperties[key].GetValue(sourceObject, null), null);
								}
								catch { }
		}
	}
}
