﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="SafeDBNull.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell
{
    /// <summary>
    /// Class db.
    /// </summary>
    public static class db
    {
        #region Public

        #region Method Static
        /// <summary>
        /// Safes the database null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>T.</returns>
        public static T SafeDBNull<T>(this object value, T defaultValue)
        {
            if (value == null)
                return default(T);

            if (value is string)
                return (T)Convert.ChangeType(value, typeof(T));


            if (value == DBNull.Value)
                return defaultValue;
            else if (value.GetType() == typeof(T))
                return (T)value;
            else
                return defaultValue;
        }

        /// <summary>
        /// Safes the database null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>T.</returns>
        public static T SafeDBNull<T>(this object value)
        {
            return value.SafeDBNull(default(T));
        }
        #endregion

        #endregion
    }

}
