// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="DateTimeEstension.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Globalization;

namespace Mlc.Shell
{
	/// <summary>
	/// Extensions for the DateTime class.
	/// </summary>
	/// <example>
	/// Using the static methods of this class it is possible to perform quick operations on 
	/// the instances of DateTime. To use the extended methods add the following directive using
	/// <code>
	/// using Mlc.Shell;
	/// </code>
	/// </example>
	public static class DateTimeExtension
	{

		/// <summary>
		/// Returns the number of the week for the past DateTime instance
		/// </summary>
		/// <param name="dateTime">DateTime instance</param>
		/// <returns>Week number</returns>
		public static int GetNumberOfWeek(this DateTime dateTime)
		{
			// ottengo il numero del giorno della settimana del primo giorno dell'anno
			int dayOfWeekFirstDayOfYear = (int)new DateTime(dateTime.Year, 1, 1).DayOfWeek;
			// calcolo il numero della settimana
			int numberWeek = dateTime.DayOfYear / 7;
			if (dayOfWeekFirstDayOfYear <= 4 && dayOfWeekFirstDayOfYear > 0)
				numberWeek += 1;

			// se il numero della settimana vale 0 
			if (numberWeek == 0)
			{
				//ricavo il numero della settimana del giorno prima del giorno prima
				numberWeek = GetNumberOfWeek(dateTime - new TimeSpan(1, 0, 0, 0));
			}

			return numberWeek;
		}

		/// <summary>
		///   <para>
		/// Returns a string representing the dateTime value in ISO 8601 notation</para>
		///   <para>See: "<a href="https://it.wikipedia.org/wiki/ISO_8601">Data elements and interchange formats</a>"</para>
		/// </summary>
		/// <param name="dateTime">DateTime instance</param>
		/// <returns>Date and time string in ISO 8601 notation</returns>
		/// <example>
		/// Create an instance of DateTime and serialize it into ISO 8601 text format
		/// <code>DateTime date1 = new(2022, 11, 05, 10, 15, 32, DateTimeKind.Utc);
		/// string strDate = date1.Serialize();
		/// // now strDate value is: "2022-11-05T10:15:32.000"
		/// Console.WriteLine(strDate);</code></example>
		public static string Serialize(this DateTime dateTime)
		{
			try
			{
				string retVal = dateTime.ToString(Constants.FormatISO8601, CultureInfo.InvariantCulture);
//				retVal = dateTime.ToString("yyyy-MM-dd", new CultureInfo("EN-en"));
				return retVal;
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
				return default(DateTime).Serialize();
			}
		}
	}
}
