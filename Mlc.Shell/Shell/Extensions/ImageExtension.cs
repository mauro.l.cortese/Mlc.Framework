// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ImageExtension.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Mlc.Shell
{
    /// <summary>
    /// Extensions for the Image class.
    /// </summary>
    public static class ImageExtension
    {
        /// <summary>
        /// Convert the image into a byte array
        /// </summary>
        /// <param name="image">Image to convert to byte array</param>
        /// <returns>Byte array</returns>
        public static byte[] ImageToByteArray(this Image image)
        {
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);
            return memoryStream.ToArray();
        }

        /// <summary>
        /// Convert an array of bytes into an image
        /// </summary>
        /// <param name="byteImage">Byte array to convert to image</param>
        /// <returns>Image obtained</returns>
        public static Image ByteArrayToImage(this byte[] byteImage)
        {
            ImageConverter imageConverter = new ImageConverter();
            return (Image)imageConverter.ConvertFrom(byteImage);
        }
    }
}
