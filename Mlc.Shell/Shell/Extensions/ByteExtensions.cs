﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ByteExtensions.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Text;

namespace Mlc.Shell
{
	/// <summary>
	/// Class StringExtension. Provides extension methods for fast operations on <see cref="Byte" /> type.
	/// </summary>
	public static class ByteExtensions
	{
		/// <summary>
		/// It returns a text string obtained from the array of bytes.
		/// </summary>
		/// <param name="bytes">Byte array from which to obtain the text string</param>
		/// <returns>Text string obtained from the array</returns>
		/// <example>
		/// In the below sample you can see how to get a text string from a byte array
		///<code>
		///using System;
		///using Mlc.Shell;
		///namespace Sample
		///{
		///	class Program
		///	{
		///		static void Main(string[] args)
		///		{
		///			byte[] bytes = new byte[] { 65, 66, 67, 68, 69 };
		///			string text = bytes.GetString(); // text = "ABCDE"
		///		}
		///	}
		///}
		///</code></example>
		public static string GetString(this byte[] bytes)
		{
			if (bytes == null) return null;
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		/// <summary>
		/// Restituisce un array di bytes ottenuto dalla stringa di testo.
		/// </summary>
		/// <param name="text">Stringa di testo da cui ottenere i bytes</param>
		/// <returns>Array di bytes ottenuto dalla stringa di testo</returns>
		/// <example>
		/// In the below sample you can see how to get an array of bytes from a text string
		///<code>
		///using System;
		///using Mlc.Shell;
		///
		///namespace Sample
		///{
		///	class Program
		///	{
		///		static void Main(string[] args)
		///		{
		///			string text = "ABCDE";
		///			byte[] bytes = text.GetBytes();
		///		}
		///	}
		///}
		///</code></example>
		public static byte[] GetBytes(this string text)
		{
			if (text == null) return null;
			byte[] bytes = new byte[text.Length * sizeof(char)];
			System.Buffer.BlockCopy(text.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		/// <summary>
		/// It Gets a string formed by the values of the bytes of all the characters in it.
		/// (The text -&gt; 84,0,104,0,101,0,32,0,116,0,101,0,120,0,116,0)
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns>System.String.</returns>
		public static string ToByteStringFormat(this string text)
		{
			StringBuilder sb = new();
			byte[] bytes = text.GetBytes();
			foreach (byte bt in bytes)
			{
				sb.Append(bt.ToString());
				sb.Append(Chars.Comma);
			}
			sb.Remove(sb.Length - 1, 1);
			return sb.ToString();
		}

		/// <summary>
		/// Gets a string froms the bytes in string format (84,0,104,0,101,0,32,0,116,0,101,0,120,0,116,0 -&gt; The text).
		/// </summary>
		/// <param name="text">The text (for example 84,0,104,0,101,0,32,0,116,0,101,0,120,0,116,0).</param>
		/// <returns>System.String</returns>
		public static string FromBytesStringFormat(this string text)
		{
			string[] chars = text.Split(Chars.Comma);
			byte[] bytes = new byte[chars.Length];

			for (int i = 0; i < chars.Length; i++)
			{
				bytes[i] = Convert.ToByte(int.Parse(chars[i]));
			}
			return bytes.GetString();
		}

	}
}
