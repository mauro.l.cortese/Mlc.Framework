// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="DateTimeEstension.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Globalization;

namespace Mlc.Shell
{
    /// <summary>
    /// Estensioni per la classe DateTime.
    /// </summary>
    /// <example>
    /// Tramite i metodi statici di questa classe è possibile compiere operazioni rapide sulle istanze di DateTime
    /// Per utilizzare i metodi estesi aggiungere la seguente direttiva using
    ///<code>
    ///using Mx.Core.Extensions;
    ///</code></example>
    public static class EnumEstension<T> //where T:Enum
    {
        /// <summary>
        /// Restituisce il numero della settimana per l'istanza DateTime passata
        /// </summary>
        /// <param name="dateTime">Istanza DateTime</param>
        /// <returns>Numero della settimana</returns>
        public static int[] GetItems(this Enum enumer)
        {


        }
    }
}
