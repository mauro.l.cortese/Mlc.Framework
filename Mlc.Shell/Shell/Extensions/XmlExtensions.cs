// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="XmlExtensions.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Xml;

namespace Mlc.Shell
{
    /// <summary>
    /// Extends the XmlNode class
    /// </summary>
    public static class XmlEstensions
    {
        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <typeparam name="TypeValue">The type of the type value.</typeparam>
        /// <param name="node">The node.</param>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns>Value obtained</returns>
        public static TypeValue AttributeValue<TypeValue>(this XmlNode node, string attributeName)
        {
            return TypeConvert<TypeValue>.GetValue(node, attributeName, default(TypeValue));
        }

        /// <summary>
        /// Gets the value of the attribute casted to the requested type
        /// </summary>
        /// <typeparam name="TypeValue">The type of the type value.</typeparam>
        /// <param name="node">The node.</param>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>Value obtained</returns>
        public static TypeValue AttributeValue<TypeValue>(this XmlNode node, string attributeName, TypeValue defaultValue)
        {
            return TypeConvert<TypeValue>.GetValue(node, attributeName, defaultValue);
        }

        /// <summary>
        /// Determines whether the specified attribute name contains attribute.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns><c>true</c> if the specified attribute name contains attribute; otherwise, <c>false</c>.</returns>
        public static bool ContainsAttribute(this XmlNode node, string attributeName)
        {
            bool retVal = false;
            if (node.Attributes.Count == 0)
                return retVal;

            foreach (XmlAttribute xmlAttr in node.Attributes)
            {
                if (xmlAttr.Name == attributeName)
                {
                    retVal = true;
                    break;
                }
            }
            return retVal;
        }
    }
}
