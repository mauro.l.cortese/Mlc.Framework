// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="TypeExtension.cs" company="">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell
{
    /// <summary>
    /// Provides extended methods for the class <see cref="System.Type" />
    /// </summary>
    public static class TypeExtension
    {
        /// <summary>
        /// Returns an enumerated by instance <see cref="System.Type" /> passata
        /// </summary>
        /// <param name="type">Type to evaluate</param>
        /// <returns>System type enumerated</returns>
        public static SysTypeName GetTypeName(this Type type)
        {
            SysTypeName sysTypeName;
            if (type.BaseType == typeof(Enum))
                sysTypeName = SysTypeName.Enum;
            else
                sysTypeName = TypeConvert<SysTypeName>.GetValue(type.Name);
            return sysTypeName;
        }


		/// <summary>
		/// Gets the default value.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>System.Object.</returns>
		public static object GetDefaultValue(this Type type)
		{
			object value = null;
			switch (type.Name)
			{
				case "String":
					value = string.Empty;
					break;

				case "Int16":
				case "Int32":
				case "Int64":
				case "UInt16":
				case "UInt32":
				case "UInt64":
				case "Single":
				case "Decimal":
					value = Convert.ChangeType(0, type);
					break;

				case "Boolean":
					value = false;
					break;

				case "DateTime":
					value = default(DateTime);
					break;

				default:
					break;
			}
			return value;

		}
	}
}
