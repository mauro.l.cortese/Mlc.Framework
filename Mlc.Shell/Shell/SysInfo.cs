// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="SysInfo.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Reflection;
using System.ServiceProcess;
using Mlc.Shell.IO;

namespace Mlc.Shell
{

	/// <summary>
	/// Class SysInfo, returns system information
	/// </summary>
	public class SysInfo
	{

		#region Constants
		/// <summary>
		/// The name of the development environment process
		/// </summary>
		private const string DevEnv = "devenv";
		#endregion

		#region Fields
		/// <summary>
		/// The assembly
		/// </summary>
		private Assembly assembly = null;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SysInfo" /> class.
		/// </summary>
		public SysInfo()
			: this(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SysInfo" /> class.
		/// </summary>
		/// <param name="assembly">The assembly.</param>
		public SysInfo(Assembly assembly)
		{
			this.assembly = assembly;
			if (this.assembly == null)
				this.assembly = Assembly.GetEntryAssembly();
			if (this.assembly == null)
				this.assembly = Assembly.GetExecutingAssembly();
			if (this.assembly == null)
				this.assembly = Assembly.GetCallingAssembly();
		}
		#endregion

		#region Public

		#region Properties
		/// <summary>
		/// Returns true if the process is 64-bit running, otherwise false
		/// </summary>
		/// <value><c>true</c> if the process is 64-bit running; otherwise, <c>false</c>.</value>
		public bool Is64BitProcess { get { return Environment.Is64BitProcess; } }

		/// <summary>
		/// Returns true if the operating system is running 64-bit, otherwise false
		/// </summary>
		/// <value><c>true</c> if the operating system is running 64-bit; otherwise, <c>false</c>.</value>
		public bool Is64BitOperatingSystem { get { return Environment.Is64BitOperatingSystem; } }

		/// <summary>
		/// Returns the fullpath of the folder where the executable of the current application is located
		/// </summary>
		/// <value>The executable folder.</value>
		public string ExecutableFolder
		{
			get { return Path.GetDirectoryName(this.ExecutableFile); }
		}

		/// <summary>
		/// Returns the path of the temporary folder for the current application
		/// </summary>
		/// <value>The executable temporary folder.</value>
		public string ExecutableTempFolder { get { return string.Concat(this.ExecutableFolder, "\\temp"); } }

		/// <summary>
		/// Returns the fullpath of the executable of the current application
		/// </summary>
		/// <value>The executable file.</value>
		public string ExecutableFile
		{
			get
			{
				return this.assembly.Location;
			}
		}

		public string ApplicationName { get { return this.assembly.GetName().Name; } }

		/// <summary>
		/// Gets the name of the assembly.
		/// </summary>
		/// <value>The name of the assembly.</value>
		public string AssemblyName { get { return this.assembly.GetName().Name; } }

		/// <summary>
		/// Returns the NetBIOS name of the local computer
		/// </summary>
		/// <value>The name of the computer.</value>
		public string ComputerName { get { return Environment.MachineName; } }

		/// <summary>
		/// Returns the name of the domain to which the computer belongs or the name of the workgroup
		/// </summary>
		/// <value>The name of the domain.</value>
		public string DomainName
		{
			get
			{
				string domainName = "";
				try
				{
					domainName = Environment.UserDomainName;
					SelectQuery query = new SelectQuery("Select * from Win32_ComputerSystem");
					ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
					foreach (ManagementObject mo in searcher.Get())
					{
						if (mo["workgroup"] != null)
							domainName = mo["workgroup"].ToString();
						else
							if (mo["domain"] != null)
							domainName = mo["domain"].ToString();
					}
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				return domainName;
			}
		}


		/// <summary>
		/// Returns the name of the domain to which the computer belongs or the name of the workgroup
		/// </summary>
		/// <value>The name of the domain.</value>
		public string RootDomain
		{
			get
			{
				string rootDomain = this.DomainName;
				try
				{
					if (rootDomain.Contains("."))
						rootDomain = rootDomain.Split('.')[0];
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				return rootDomain;
			}
		}

		/// <summary>
		/// Returns the name of the user logged in to the local computer
		/// </summary>
		/// <value>The name of the user.</value>
		public string UserName { get { return Environment.UserName; } }

		/// <summary>
		/// Determines whether execution is taking place in the graphical editor environment of the VisualStudio IDE
		/// </summary>
		/// <value><c>true</c> if this instance is execute in vs IDE design mode; otherwise, <c>false</c>.</value>
		public static bool IsExecuteInVsIdeDesignMode { get { return (Process.GetCurrentProcess().ProcessName.ToLower() == SysInfo.DevEnv); } }
		#endregion

		#region Method
		/// <summary>
		/// Clears the temporary.
		/// </summary>
		public void ClearTemp()
		{
			FileSystemMgr fsm = new(Path.Combine(Path.GetTempPath(), this.ApplicationName));
			if (fsm.Exists)
				fsm.Delete();
		}


		/// <summary>
		/// Returns the path of the first file that is found in the executable folder or in one of its subfolders given its name
		/// </summary>
		/// <param name="fileName">Name of the file to be found</param>
		/// <returns>Fullpath of the file obtained</returns>
		public string GetFileOnExecutableFolder(string fileName)
		{
			string[] files = Directory.GetFiles(this.ExecutableFolder, fileName, SearchOption.AllDirectories);
			if (files.Length > 0)
				return files[0];
			else
				return null;
		}

		/// <summary>
		/// Returns the full path of a temporary file with a specified name
		/// </summary>
		/// <param name="fileName">Temporary file name</param>
		/// <returns>Full path of the temporary file obtained</returns>
		public string GetFileTmp(string fileName = null, bool createFile = false)
		{
			FileSystemMgr fsm = new(Path.Combine(Path.GetTempPath(), this.ApplicationName));

			if (!fsm.Exists)
				fsm = fsm.Create(FileSystemTypes.Directory);

			if (fsm.Exists)
				if (string.IsNullOrEmpty(fileName))
					fsm = new(Path.Combine(fsm.Path, Path.GetFileName(Path.GetTempFileName())));
				else
					fsm = new(Path.Combine(fsm.Path, fileName));

			if (!fsm.Exists && createFile)
				fsm.Create(FileSystemTypes.File);

			return fsm.Path;
		}

		/// <summary>
		/// Gets the ip address.
		/// </summary>
		/// <returns>The local IP</returns>
		public string GetIPAddress()
		{
			string localIP = "";
			IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (IPAddress ip in host.AddressList)
				if (ip.AddressFamily == AddressFamily.InterNetwork)
					localIP = ip.ToString();
			return localIP;
		}

		#endregion

		#endregion

		#region Tipi nidificati
		/// <summary>
		/// Provides methods and properties for operating with the services of a computer on the network
		/// </summary>
		public class ServiceInfo
		{
			#region Fields
			/// <summary>
			/// Represents a Windows service
			/// </summary>
			private ServiceController serviceController = null;

			/// <summary>
			/// The service name
			/// </summary>
			private string serviceName = null;
			#endregion

			#region Constructors
			/// <summary>
			/// Initializes a new instance for a service on the localhost
			/// </summary>
			/// <param name="serviceName">The service name</param>
			public ServiceInfo(string serviceName)
				: this(serviceName, "localhost")
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="ServiceInfo"/> class.
			/// </summary>
			/// <param name="serviceName">Name of the service.</param>
			/// <param name="machineName">Name of the machine.</param>
			public ServiceInfo(string serviceName, string machineName)
			{
				this.serviceName = serviceName;
				this.serviceController = new ServiceController(serviceName, machineName);
				try
				{
					string name = this.serviceController.ServiceName;
					this.isInstalled = true;
				}
				catch { this.isInstalled = false; }
			}
			#endregion

			#region Propriet�
			/// <summary>
			/// The name of the service
			/// </summary>
			/// <value>The name.</value>
			public string Name { get { return this.serviceName; } }

			/// <summary>
			/// Determines whether the service is installed on the computer
			/// </summary>
			public bool isInstalled = false;

			/// <summary>
			/// Returns true if the service is installed on the computer, otherwise false
			/// </summary>
			/// <value><c>true</c> if this instance is installed; otherwise, <c>false</c>.</value>
			public bool IsInstalled { get { return this.isInstalled; } }

			/// <summary>
			/// Returns true if the service is running, otherwise false
			/// </summary>
			/// <value><c>true</c> if this instance is running; otherwise, <c>false</c>.</value>
			public bool IsRunning
			{
				get
				{
					return this.IsInstalled ? this.serviceController.Status == ServiceControllerStatus.Running : false;
				}
			}
			#endregion

			#region Metodi pubblici
			/// <summary>
			/// Stops the service if it is running and wait for the its status to be Stopped.
			/// </summary>
			public void Stop()
			{
				if (this.IsInstalled && this.IsRunning)
				{
					this.serviceController.Stop();
					this.serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
				}
			}

			/// <summary>
			/// Starts the service if it is not running and wait for the its status to be Running.
			/// </summary>
			public void Start()
			{
				if (this.IsInstalled && !this.IsRunning)
				{
					this.serviceController.Start();
					this.serviceController.WaitForStatus(ServiceControllerStatus.Running);
				}
			}
			#endregion
		}
		#endregion

	}
}
