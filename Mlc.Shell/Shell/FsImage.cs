﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Mlc.Shell.IO;

namespace Mlc.Shell
{
    /// <summary>
    /// Class FsImage.
    /// </summary>
    public class FsImage
    {
        #region Fields
        /// <summary>
        /// The filter
        /// </summary>
        private string filter = string.Format("*{0}", FsoExt.Jpg);
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="FsImage"/> class.
        /// </summary>
        public FsImage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FsImage"/> class.
        /// </summary>
        /// <param name="defaultImage">The default image.</param>
        public FsImage(Image defaultImage)
            : this(defaultImage, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FsImage"/> class.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        public FsImage(FileSystemMgr sourceImage)
            : this(null, sourceImage, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FsImage"/> class.
        /// </summary>
        /// <param name="defaultImage">The default image.</param>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="filter">The filter.</param>
        public FsImage(Image defaultImage, FileSystemMgr sourceImage, string filter)
        {
            if (!string.IsNullOrEmpty(filter))
                this.filter = filter;
            this.DefaultImage = defaultImage;
            this.SourceImage = sourceImage;

        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// The source image
        /// </summary>
        private FileSystemMgr sourceImage;
        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source image.</value>
        public FileSystemMgr SourceImage
        {
            get { return this.sourceImage; }
            set
            {
                this.sourceImage = value;
                if (this.sourceImage == null) return;
                loadImage();
            }
        }

        /// <summary>
        /// Gets or sets the default image.
        /// </summary>
        /// <value>The default image.</value>
        public Image DefaultImage { get; set; } = null;

        /// <summary>
        /// Gets the images.
        /// </summary>
        /// <value>The images.</value>
        public ImagesCollection Images { get; } = new ImagesCollection();

        /// <summary>
        /// Gets the thumbnails.
        /// </summary>
        /// <value>The thumbnails.</value>
        public ImagesCollection Thumbnails { get; } = new ImagesCollection();


        /// <summary>
        /// The image/
        /// </summary>
        private Image image = null;

        /// <summary>
        /// Gets the image.
        /// </summary>
        /// <value>The image.</value>
        public Image Image
        {
            get
            {
                if (this.image == null)
                    return this.DefaultImage;
                else
                    return this.image;
            }
        }


        /// <summary>
        /// Gets the thumbnail.
        /// </summary>
        /// <value>The thumbnail.</value>
        public Image Thumbnail { get; private set; } = null;
        #endregion

        #region Method
        /// <summary>
        /// Saves the preview.
        /// </summary>
        public void SavePreview()
        {
            try
            {
                if (this.sourceImage.IsFile && this.Thumbnail != this.image)
                    this.Thumbnail.Save(sourceImage.Path);
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion
        #endregion

        #region Private
        #region Method

        /// <summary>
        /// Loads the image.
        /// </summary>
        private void loadImage()
        {
            if (sourceImage.FsoType == FileSystemTypes.UnDefine) return;
            if (sourceImage.IsFile)
                loadFromFile(sourceImage.Path);
            else
                loadFromFolder();
        }


        /// <summary>
        /// Loads from folder.
        /// </summary>
        private void loadFromFolder()
        {
            try
            {
                foreach (string file in Directory.GetFiles(sourceImage.Path, filter))
                {
                    this.loadFromFile(file);
                    string key = Path.GetFileNameWithoutExtension(file);
                    if (!this.Images.ContainsKey(key) && !this.Thumbnails.ContainsKey(key))
                        this.Images.Add(new FsImage.ImageItem(key, this.image));
                    this.Thumbnails.Add(new FsImage.ImageItem(key, this.Thumbnail));
                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }


        /// <summary>
        /// Loads from file.
        /// </summary>
        /// <param name="file">The file.</param>
        private void loadFromFile(string file)
        {
            try
            {
                if (File.Exists(file))
                {
                    FileStream streamFileIn = File.Open(file, FileMode.Open, FileAccess.Read);
                    byte[] fileBuffer = new byte[streamFileIn.Length];
                    streamFileIn.Read(fileBuffer, 0, (int)streamFileIn.Length);
                    this.image = Image.FromStream(streamFileIn);
                    // rilascio la risorsa sul file
                    streamFileIn.Dispose();

                    // anteprima dell'immagine
                    //calcolo il fattore di proporzione 
                    double propFact = (double)this.image.Height / (double)this.image.Width;

                    // calcolo l'anteprima solo se la dimensione dell'immagine � superiore del 20% 
                    // rispetto alle dimensioni dell'anteprima.
                    if (this.image.Width > (448 * 1.2))
                    {
                        //calcolo un anteprima con dimensione width in pixel di circa 448 formato immagine web =(448*336)
                        int widthPreview = (int)((1d / (double)((double)(this.image.Width / 448d)) * this.image.Width));
                        int heightPreview = (int)(widthPreview * propFact);
                        this.Thumbnail = this.image.GetThumbnailImage(widthPreview, heightPreview, null, new IntPtr());
                    }
                    else // altrimenti l'anteprima � uguale all'immagine
                    {
                        this.Thumbnail = this.image;
                    }


                }
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion
        #endregion

        #region Embedded Types
        /// <summary>
        /// Class ImagesCollection.
        /// </summary>
        /// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Shell.FsImage.ImageItem}" />
        public class ImagesCollection : Dictionary<string, ImageItem>
        {
            /// <summary>
            /// Adds the specified item.
            /// </summary>
            /// <param name="item">The item.</param>
            public void Add(ImageItem item)
            {
                if (!this.ContainsKey(item.Key))
                    this.Add(item.Key, item);
            }
        }

        /// <summary>
        /// Class ImageItem.
        /// </summary>
        public class ImageItem
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="ImageItem"/> class.
            /// </summary>
            /// <param name="key">The key.</param>
            /// <param name="Image">The image.</param>
            public ImageItem(string key, Image Image)
            {
                this.Key = key;
                this.Image = Image;
            }
            /// <summary>
            /// Gets or sets the key.
            /// </summary>
            /// <value>The key.</value>
            public string Key { get; set; }
            /// <summary>
            /// Gets or sets the image.
            /// </summary>
            /// <value>The image.</value>
            public Image Image { get; set; }
        }

        #endregion
    }
}
