﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="ICryptoByte.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell.Crypto
{
    /// <summary>
    /// Espone i membri per la cifratura o decifratura di un vettore di byte
    /// </summary>
    public interface ICryptoByte
    {
        /// <summary>
        /// Restituisce o imposta il vettore di bytes cifrato o decifrato
        /// </summary>
        /// <value>The data.</value>
        byte[] Data { get; set; }

        /// <summary>
        /// Esegue la decifratura dei bytes contenuti in <see cref="Mlc.Shell.Crypto.ICryptoByte.Data" />
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        bool Decrypt();

        /// <summary>
        /// Esegue la cifratura dei bytes contenuti in <see cref="Mlc.Shell.Crypto.ICryptoByte.Data" />
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        bool Encrypt();

        /// <summary>
        /// Restituisce o imposta la password di cifratura
        /// </summary>
        /// <value>The password.</value>
        string Password { get; set; }
    }
}
