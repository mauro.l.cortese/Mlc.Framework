// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="RijndaelCryptoByte.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Mlc.Shell.Crypto
{
    /// <summary>
    /// Encrypts or decrypts a byte vector with the Rijndael algorithm
    /// </summary>
    /// <seealso cref="Mlc.Shell.Crypto.ICryptoByte" />
    /// <example>
    /// Encryption of a byte array is very simple.
    /// <code>
    /// CryptoByte cb = new CryptoByte(new byte[] { 126, 111, 12, 24, 1 });
    /// </code>
    /// Otherwise:
    /// <code>
    /// CryptoByte cb = new CryptoByte();
    /// cb.Data = new byte[] { 126, 111, 12, 24, 1 };
    /// </code>
    /// Then, the method <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Encrypt()"/> is called to encrypt the bytes of the vector
    /// <code>
    /// cb.Encrypt();
    /// </code>
    /// The property <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Data" /> returns the encrypted data, that is, a byte vector 
    /// obtained by the encryption of the vector passed to the constructor.
    /// <para>
    /// The method <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Decrypt()" /> decrypts the bytes of the vector 
    /// assigned to the property <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Data" />.
    /// </para>
    /// <code>
    /// cb.Decrypt();
    /// </code>
    /// In the proposed examples, the data is encrypted using the constant <see cref="Mlc.Shell.Constants.DefaultPwd"/> as the password.
    /// To use a different password, assign it to the property<see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Password"/> 
    /// before calling the methods<see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Encrypt()" /> or<see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Decrypt()"/>
    /// <code>
    /// CryptoByte cb = new CryptoByte(new byte[] { 126, 111, 12, 24, 1 });
    /// cb.Password = "12dtrhs@435";
    /// cb.Encrypt();
    /// </code>
    /// The following example shows how to encrypt a file
    /// <code>
    /// CryptoByte cb = new CryptoByte();
    /// cb.Data = File.ReadAllBytes(@"C:\Prova.xml");
    /// cb.Encrypt();
    /// File.WriteAllBytes(@"C:\Prova.xml.cry", cb.Data);
    /// </code>
    /// and finally how to decrypt it
    /// <code>
    /// CryptoByte cb = new CryptoByte();
    /// cb.Data = File.ReadAllBytes(@"C:\Prova.xml.cry");
    /// cb.Decrypt();
    /// File.WriteAllBytes(@"C:\Prova.xml.new", cb.Data);
    /// </code>
    /// <para>
    /// The extended methods of <see cref="Mx.Core.Extensions.CriptoStringExtension"/> can be used to encrypt and decrypt text strings
    /// </para>   
    /// <para>
    /// The extended methods of <see cref="Mx.Core.IO.FileSystemMgr"/> can be used to encrypt and decrypt flders and files
    /// </para>   
    /// </example>
    public class RijndaelCryptoByte : ICryptoByte
    {
        #region Campi
        /// <summary>
        /// Vector of encryption key
        /// </summary>
        private byte[] Key;
        /// <summary>
        /// Initialization vector
        /// </summary>
        private byte[] IV;
        #endregion

        #region Costruttori
        /// <summary>
        /// Initializes a new instance of the <see cref="RijndaelCryptoByte"/> class.
        /// </summary>
        public RijndaelCryptoByte()
        {
            this.Password = Constants.DefaultPwd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RijndaelCryptoByte"/> class.
        /// </summary>
        /// <param name="bytesData">The bytes data.</param>
        public RijndaelCryptoByte(byte[] bytesData)
            : this()
        {
            this.bytesData = bytesData;
        }
        #endregion

        #region Propriet�
        /// <summary>
        /// The password
        /// </summary>
        private string password = null;
        /// <summary>
        /// Gets or sets the encryption password
        /// </summary>
        /// <value>The password.</value>
        public string Password
        {
            get { return password; }
            set
            {
                this.password = value;
                this.Key = getBytesKey(this.password, 32, false);
                this.IV = getBytesKey(this.password, 16, true);
            }
        }

        /// <summary>
        /// Vector of encripted or decripted bytes
        /// </summary>
        private byte[] bytesData;
        /// <summary>
        /// Returns or sets the encripted or decripted bytes vector
        /// </summary>
        /// <value>The data.</value>
        public byte[] Data { get { return bytesData; } set { bytesData = value; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// It encrypts the bytes contained in <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Data" />
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Encrypt()
        {
            try
            {
                RijndaelManaged rijndaelMan = new RijndaelManaged();
                rijndaelMan.Key = this.Key;
                rijndaelMan.IV = this.IV;
                dataProcess(rijndaelMan.CreateEncryptor(Key, IV));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }

        /// <summary>
        /// It decrypts the bytes contained in <see cref="Mlc.Shell.Crypto.RijndaelCryptoByte.Data" />
        /// </summary>
        /// <returns><c>true</c> on success, <c>false</c> otherwise.</returns>
        public bool Decrypt()
        {
            try
            {
                RijndaelManaged rijndaelMan = new RijndaelManaged();
                rijndaelMan.Key = this.Key;
                rijndaelMan.IV = this.IV;
                dataProcess(rijndaelMan.CreateDecryptor(Key, IV));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionsRegistry.GetInstance().Add(ex);
                return false;
            }
        }
        #endregion

        #region Metodi privati
        /// <summary>
        /// Process the data with the transformation request
        /// </summary>
        /// <param name="iCryptoTransform">Request for data transformation</param>
        private void dataProcess(ICryptoTransform iCryptoTransform)
        {
            byte[] bytesDecrypt;
            MemoryStream ms = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(ms, iCryptoTransform, CryptoStreamMode.Write); ;
            cryptoStream.Write(this.bytesData, 0, this.bytesData.Length);
            cryptoStream.FlushFinalBlock();
            bytesDecrypt = ms.ToArray();
            cryptoStream.Close();
            ms.Close();
            this.bytesData = bytesDecrypt;
        }

        /// <summary>
        /// Returns a vector of bytes of the required length obtained from the password string
        /// </summary>
        /// <param name="password">Password string</param>
        /// <param name="lenght">Length of carrier required</param>
        /// <param name="reverse">Determines whether or not to invert the bytes vector</param>
        /// <returns>Byte vector obtained</returns>
        private byte[] getBytesKey(string password, int lenght, bool reverse)
        {
            if (password.Length > lenght)
                password = password.Substring(0, lenght);

            byte[] retVal = new byte[lenght];
            byte[] pwd = ASCIIEncoding.ASCII.GetBytes(password);

            if (reverse)
            {
                byte[] buffer = new byte[pwd.Length];
                int n = 0;
                for (int i = pwd.Length - 1; i >= 0; i--)
                {
                    buffer[n] = pwd[i];
                    n++;
                }
                pwd = buffer;
            }

            pwd.CopyTo(retVal, 0);

            return retVal;
        }
        #endregion
    }
}
