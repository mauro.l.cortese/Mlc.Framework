﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 09-30-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="WrongParameterSet.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;


namespace Mlc.Shell
{
    /// <summary>
    /// The WrongParameterSet class can be use when is need to check if a generic type inherits from the <see cref="System.Enum" /> type.
    /// In case that checked type isn't inherits from the <see cref="System.Enum" /> type, raise this exception.
    /// </summary>
    /// <seealso cref="System.Exception" />
    /// <remarks>The text of <see cref="System.Exception.Message" /> property is localized in italian and english
    /// This type is also used in <see cref="EnumOperator{T}" /></remarks>
    public class WrongParameterSet : Exception
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="WrongParameterSet" /> class.
        ///// </summary>
        //public WrongParameterSet()
        //    : base(Resources.TextResource.WrongParameterSetMessage)
        //{ }
    }
}
