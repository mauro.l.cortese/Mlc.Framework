﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-29-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="NotAllowedMethod.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace Mlc.Shell
{
    /// <summary>
    /// Class LengthOfVectorsDifferentException for two arrays which don't have the same number of elements
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class NotAllowedMethod  : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotAllowedMethod" /> class.
        /// </summary>
        public NotAllowedMethod()
            : base(Resources.Messages.NotAllowedMethod)
        { }
    }
}
