﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : mauro.luigi.cortese
// Created          : 12-13-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-13-2019
// ***********************************************************************
// <copyright file="Impersonator.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Mlc.Shell
{
    /// <summary>
    /// Class Impersonator.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class Impersonator : IDisposable
    {
        #region P/Invoche

        #region Constants
        /// <summary>
        /// The logon32 interactive
        /// </summary>
        private const int Logon32_Interactive = 2;
        /// <summary>
        /// The logon32 provider default
        /// </summary>
        private const int Logon32_Provider_Default = 0;
        #endregion

        /// <summary>
        /// Logons the user.
        /// </summary>
        /// <param name="lpszUserName">Name of the LPSZ user.</param>
        /// <param name="lpszDomain">The LPSZ domain.</param>
        /// <param name="lpszPassword">The LPSZ password.</param>
        /// <param name="dwLogonType">Type of the dw logon.</param>
        /// <param name="dwLogonProvider">The dw logon provider.</param>
        /// <param name="phToken">The ph token.</param>
        /// <returns>System.Int32.</returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern int logonUser(string lpszUserName, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        /// <summary>
        /// Duplicates the token.
        /// </summary>
        /// <param name="hToken">The h token.</param>
        /// <param name="impersonationLevel">The impersonation level.</param>
        /// <param name="hNewToken">The h new token.</param>
        /// <returns>System.Int32.</returns>
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int duplicateToken(IntPtr hToken, int impersonationLevel, ref IntPtr hNewToken);

        /// <summary>
        /// Reverts to self.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool revertToSelf();

        /// <summary>
        /// Closes the handle.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern bool closeHandle(IntPtr handle);
        #endregion

        #region Fields
        /// <summary>
        /// The impersonation context
        /// </summary>
        private WindowsImpersonationContext impersonationContext = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Impersonator"/> class.
        /// </summary>
        /// <param name="userName">Name of the user (mauro.cortese).</param>
        /// <param name="domainName">Name of the domain (LIVETECH.local).</param>
        /// <param name="password">The password.</param>
        public Impersonator(string userName, string domainName, string password)
        {
            this.impersonateValidUser(userName, domainName, password);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Impersonator"/> class.
        /// </summary>
        /// <param name="domainUserName">Name of the domain (mauro.cortese@livetech.local oppure LIVETECH\mauro.cortese).</param>
        /// <param name="password">The password.</param>
        public Impersonator(string domainUserName, string password)
        {
            string userName = null, domainName = null;
            if (domainUserName.Contains("@"))
            {
                string[] strs = domainUserName.Split('@');
                if (strs.Length == 2)
                {
                    userName = strs[0];
                    domainName = strs[1];
                }
            }
            else if (domainUserName.Contains("\\"))
            {
                string[] strs = domainUserName.Split('\\');
                if (strs.Length == 2)
                {
                    userName = strs[1];
                    domainName = strs[0];
                }
            }

            if(string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(domainName))
                throw new Exception("ERROR!! Invalid username or domain");

            this.impersonateValidUser(userName, domainName, password);
        }

        #endregion

        #region Public
        
        #region Method
        /// <summary>
        /// Esegue attività definite dall'applicazione, come rilasciare o reimpostare risorse non gestite.
        /// </summary>
        public void Dispose()
        {
            this.undoImpersonation();
        }
        #endregion
        
        #endregion

        #region Private

        #region Method
        /// <summary>
        /// Impersonates the valid user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="Win32Exception">
        /// </exception>
        private void impersonateValidUser(string userName, string domain, string password)
        {
            WindowsIdentity tempWindowsIdentity = null;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            try
            {
                if (revertToSelf())
                {
                    if (logonUser(userName, domain, password, Logon32_Interactive, Logon32_Provider_Default, ref token) != 0)
                    {
                        if (duplicateToken(token, 2, ref tokenDuplicate) != 0)
                        {
                            tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                            impersonationContext = tempWindowsIdentity.Impersonate();
                        }
                        else
                            throw new Win32Exception(Marshal.GetLastWin32Error());
                    }
                    else
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                else
                    throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            finally
            {
                if (token != IntPtr.Zero)
                    closeHandle(token);
                if (tokenDuplicate != IntPtr.Zero)
                    closeHandle(tokenDuplicate);
            }

        }

        /// <summary>
        /// Undoes the impersonation.
        /// </summary>
        private void undoImpersonation()
        {
            if (impersonationContext != null)
                impersonationContext.Undo();
        }

        #endregion

        #endregion
    }
}
