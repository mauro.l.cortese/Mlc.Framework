// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 05-03-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="CmdLineParser.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Mlc.Shell
{
    /// <summary>
    /// Provides a wrapper for command line interpretation used to launch the application
    /// </summary>
    /// <seealso cref="Mlc.Shell.LineParser" />
    /// <remarks>
    /// <para>This class is an implementation of the singleton pattern</para>
    /// <para>The returned instance is always the same and is obtained by calling the static method:
    /// <see cref="Mx.Core.CmdLineParser.GetInstance()"/></para>
    /// </remarks>
    public class CmdLineParser : LineParser
    {
        #region Fields
        /// <summary>
        /// The static instance
        /// </summary>
        private static volatile CmdLineParser instance = null;
        #endregion 

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="CmdLineParser"/> class from being created.
        /// The private constructor does not allow direct instancing of this class.
        /// </summary>
        private CmdLineParser()
        {
            base.args = System.Environment.GetCommandLineArgs();
            base.parseParameters();
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// Returns the fullpath of the executable file to which the command line belongs.
        /// </summary>
        /// <value>The executeble path.</value>
        public string ExecuteblePath { get { return base.FirstArgument; } }
        #endregion

        #region Method Static
        /// <summary>
        /// Gets always the same instance.
        /// </summary>
        /// <returns>CmdLineParser.</returns>
        public static CmdLineParser GetInstance()
        {
            // se l'istanza non � ancora stata allocata
            if (instance == null)
            {
                // ottengo una nuova istanza
                if (instance == null)
                    instance = new CmdLineParser();
            }
            // restituisco sempre la stessa istanza 
            return instance;
        }
        #endregion
        #endregion
    }
}
