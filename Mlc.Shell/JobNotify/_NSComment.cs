﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Cortese Mauro Luigi
// Created          : 07-21-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-30-2017
// ***********************************************************************
// <copyright file="_NSComment.cs" company="Personale">
//     Copyright © Microsoft 2015
// </copyright>
// <summary></summary>
// ***********************************************************************

/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img>
/// <para>The namespace Mlc.Shell.JobNotify contains all the base types that can be used for notifications on the progress of a running process</para>
/// </summary>
namespace Mlc.Shell.JobNotify
{

}
