﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 04-23-2019
//
// Last Modified By : Mauro
// Last Modified On : 04-24-2019
// ***********************************************************************
// <copyright file="MefComposer.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;

namespace Mlc.Shell
{
    /// <summary>
    /// Class MefComposer.
    /// </summary>
    /// <seealso cref="Mlc.Shell.SingletonBase{Mlc.Shell.MefComposer}" />
    public class MefComposer : SingletonBase<MefComposer>
    {
        /// <summary>
        /// The composition container
        /// </summary>
        private CompositionContainer compositionContainer = null;

        /// <summary>
        /// Gets or sets the assembly paths.
        /// </summary>
        /// <value>The assembly paths.</value>
        public List<string> AssemblyPaths { get; set; } = new List<string>(); //{ Assembly.GetExecutingAssembly().Location};
        
        /// <summary>
        /// Gets or sets the directory paths.
        /// </summary>
        /// <value>The directory paths.</value>
        public List<string> DirectoryPaths { get; set; } = new List<string>() {Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) };

        /// <summary>
        /// Initializes the specified object.
        /// </summary>
        public void Initialize()
        {
            AggregateCatalog catalog = new();

            if (this.AssemblyPaths != null)
                foreach (string item in this.AssemblyPaths)
                    catalog.Catalogs.Add(new AssemblyCatalog(item));

            if (this.DirectoryPaths != null)
                foreach (string item in this.DirectoryPaths)
                    catalog.Catalogs.Add(new DirectoryCatalog(item));

            this.compositionContainer = new CompositionContainer(catalog);
        }

        /// <summary>
        /// Composes the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Compose(params object[] obj)
        {
            compositionContainer.ComposeParts(obj);
        }
    }
}
