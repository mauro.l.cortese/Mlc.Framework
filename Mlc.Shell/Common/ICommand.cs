﻿namespace Mlc.Shell
{
	/// <summary>
	/// Interface ICommand
	/// </summary>
	public interface ICommand
	{
		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		string Key { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The description.</value>
		string Description { get; set; }

	}
}