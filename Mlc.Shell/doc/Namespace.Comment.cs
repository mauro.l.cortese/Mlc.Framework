﻿// ***********************************************************************
// Assembly         : Mlc.Shell
// Author           : Mauro
// Created          : 12-30-2018
//
// Last Modified By : Mauro
// Last Modified On : 12-30-2018
// ***********************************************************************
// <copyright file="Namespace.Comment.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************


/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img>
/// <para>The Mlc.Shell namespace contains all the base types that can be used for the development of applications</para>
/// </summary>
namespace Mlc.Shell
{

}


/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img>
/// <para>The Mlc.Shell.IO namespace contains all the base types that can be used for data IO (files and folder)</para>
/// </summary>
namespace Mlc.Shell.IO
{

}


/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img>
/// <para>The namespace Mlc.Shell.JobNotify contains all the base types that can be used for notifications on the progress of a running process</para>
/// </summary>
namespace Mlc.Shell.JobNotify
{

}


/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img> 
/// <para>The Mlc.Shell.Crypto namespace contains all the basic types that can be used for encrypting and decrypting some data in your applications</para>
/// </summary>
namespace Mlc.Shell.Crypto
{

}


/// <summary>
/// <img src="{ImageFolder}/32/MLC-2015.png"></img> 
/// <para>The Mlc.Shell.Resources namespace contiene risorse fortemente tipizzate per la ricerca di stringhe localizzate e così via.</para>
/// </summary>
namespace Mlc.Shell.Resources
{

}