using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

//using Mx.Core;

namespace Mlc.Shell.Gui
{
	/// <summary>
	/// Muove un form selezionandolo con il puntatore del mouse in un punto qualsiasi
	/// </summary>
    public class MoveForm
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		/// <summary>
		/// (10/02/2009)
		/// <para>Enumerazione delle posizioni per un form secondario</para>
		/// </summary>
		public enum FormPosition
		{
			/// <summary>Sopra, allineato a sinistra rispetto al form principale</summary>
			TopLeft,
			/// <summary>Sopra, allineato al centro rispetto al form principale</summary>
			TopCenter,
			/// <summary>Sopra, allineato a destra rispetto al form principale</summary>
			TopRight,

			/// <summary>A sinistra, allineato in alto rispetto al form principale</summary>
			LeftTop,
			/// <summary>A sinistra, allineato al centro rispetto al form principale</summary>
			LeftCenter,
			/// <summary>A sinistra, allineato in basso rispetto al form principale</summary>
			LeftBottom,

			/// <summary>A destra, allineato in alto rispetto al form principale</summary>
			RightTop,
			/// <summary>A destra, allineato al centro rispetto al form principale</summary>
			RightCenter,
			/// <summary>A destra, allineato in basso rispetto al form principale</summary>
			RightBottom,

			/// <summary>Sotto, allineato a sinistra rispetto al form principale</summary>
			BottomLeft,
			/// <summary>Sotto, allineato al centro rispetto al form principale</summary>
			BottomCenter,
			/// <summary>Sotto, allineato a destra rispetto al form principale</summary>
			BottomRight,
		}
		#endregion

		#region Campi
		/// <summary>Form da muovere</summary>
		private Form formMain = null;
		/// <summary>Determina se muovere o meno il form</summary>
		private bool move = false;
		/// <summary>Posizione relativa del puntatore rispetto al form principale</summary>
		private DeltaPos deltaPos;
		/// <summary>Lista di dati per il posizionamento di form satelliti</summary>
		private List<FormPositionData> formDataPositions = new List<FormPositionData>();
		#endregion

		#region Costruttori
		/// <summary>
		/// (08/02/2009)
		/// <para>Inizializza una nuova istanza ...</para>
		/// </summary>
		/// <param name="formMain">Form da controllare</param>
		/// <param name="controls">Vettore di controlli che devono rispondere agli eventi, null se non ci sono</param>
		public MoveForm(Form formMain, Control[] controls)
		{
			this.formMain = formMain;
			this.formMain.MouseMove += new MouseEventHandler(form_MouseMove);
			this.formMain.MouseDown += new MouseEventHandler(form_MouseDown);
			this.formMain.MouseUp += new MouseEventHandler(form_MouseUp);

			if (controls != null)
			{
				foreach (Control control in controls)
				{
					control.MouseMove += new MouseEventHandler(form_MouseMove);
					control.MouseDown += new MouseEventHandler(form_MouseDown);
					control.MouseUp += new MouseEventHandler(form_MouseUp);
				}
			}
		}


		#endregion

		#region Propriet�
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// (10/02/2009)
		/// <para>Aggiunge il form passato alla gestione dello spostamento, muovendo il form principale si muovono anche i secondari.</para>
		/// </summary>
		/// <param name="form">Form da aggiungere a quello principale</param>
		/// <param name="formPosition">Posizione del form rispetto a quello principale</param>
		public void AddForm(Form form, FormPosition formPosition)
		{
			FormPositionData formPositionData = new FormPositionData(this.formMain, form, formPosition);
			formDataPositions.Add(formPositionData);
			formPositionData.SetLocation();
		}
		#endregion

		#region Metodi per eventi
		private void form_MouseUp(object sender, MouseEventArgs e)
		{
			this.move = false;
		}

		private void form_MouseDown(object sender, MouseEventArgs e)
		{
			this.move = true;
			deltaPos = new DeltaPos(e.X, e.Y);
		}

		private void form_MouseMove(object sender, MouseEventArgs e)
		{
			if (move)
			{
				Point p = this.formMain.PointToScreen(new Point(e.X, e.Y));
				this.formMain.Location = new Point(p.X - deltaPos.DeltaX, p.Y - deltaPos.DeltaY);


				foreach (FormPositionData formPositionData in this.formDataPositions)
					formPositionData.SetLocation();
			}
		}
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		/// <summary>
		/// (08/02/2009)
		/// <para>Definisce la posizione relativa del puntatore rispetto all'origine del form</para> 
		/// </summary>
		private struct DeltaPos
		{
			/// <summary>
			/// (08/02/2009)
			/// <para>Delta coordinata X</para>
			/// </summary>
			public int DeltaX;

			/// <summary>
			/// (08/02/2009)
			/// <para>Delta coordinata Y</para>
			/// </summary>
			public int DeltaY;

			/// <summary>
			/// (08/02/2009)
			/// <para>Inizializza una nuova istanza </para>
			/// </summary>
			/// <param name="deltaX">Delta coordinata X</param>
			/// <param name="deltaY">Delta coordinata Y</param>
			public DeltaPos(int deltaX, int deltaY)
			{
				this.DeltaX = deltaX;
				this.DeltaY = deltaY;
			}
		}

		/// <summary>
		/// (08/02/2009)
		/// <para>Definisce la posizione relativa del puntatore rispetto all'origine del form</para> 
		/// </summary>
		private struct FormPositionData
		{
			/// <summary>
			/// (08/02/2009)
			/// <para>Form principale</para>
			/// </summary>
			internal Form FormMain;

			/// <summary>
			/// (08/02/2009)
			/// <para>Form da aggiungere a quello principale</para>
			/// </summary>
			internal Form Form;

			/// <summary>
			/// (08/02/2009)
			/// <para>Posizione del form rispetto a quello principale</para>
			/// </summary>
			internal FormPosition FormPosition;

			/// <summary>
			/// (08/02/2009)
			/// <para>Inizializza una nuova istanza </para>
			/// </summary>
			/// <param name="formMain">Form principale</param>
			/// <param name="form">Form da aggiungere a quello principale</param>
			/// <param name="formPosition">Posizione del form rispetto a quello principale</param>
			internal FormPositionData(Form formMain, Form form, FormPosition formPosition)
			{
				this.FormMain = formMain;
				this.Form = form;
				this.FormPosition = formPosition;

			}

			/// <summary>
			/// (08/02/2009)
			/// <para>Imposta la posizione del form secondario</para> 
			/// </summary>
			internal void SetLocation()
			{
				int pos;
				switch (this.FormPosition)
				{
					case FormPosition.TopLeft:
						this.Form.Location = new Point(this.FormMain.Left, this.FormMain.Top - this.Form.Height);
						break;
					case FormPosition.TopCenter:
						pos = this.FormMain.Left + (this.FormMain.Width / 2) // calcolo della mezzeria del form principale
							  - (this.Form.Width / 2); // sottrae alla mezzeria l'altezza del form secondario
						this.Form.Location = new Point(pos, this.FormMain.Top - this.Form.Height);
						break;
					case FormPosition.TopRight:
						pos = this.FormMain.Left + this.FormMain.Width - this.Form.Width;
						this.Form.Location = new Point(pos, this.FormMain.Top - this.Form.Height);
						break;

					case FormPosition.LeftTop:
						this.Form.Location = new Point(this.FormMain.Left - this.Form.Width, this.FormMain.Top);
						break;
					case FormPosition.LeftCenter:
						pos = this.FormMain.Top + (this.FormMain.Height / 2) // calcolo della mezzeria del form principale
							  - (this.Form.Height / 2); // sottrae alla mezzeria l'altezza del form secondario
						this.Form.Location = new Point(this.FormMain.Left - this.Form.Width, pos);
						break;
					case FormPosition.LeftBottom:
						pos = this.FormMain.Top + this.FormMain.Height - this.Form.Height;
						this.Form.Location = new Point(this.FormMain.Left - this.Form.Width, pos);
						break;

					case FormPosition.RightTop:
						this.Form.Location = new Point(this.FormMain.Left + this.FormMain.Width, this.FormMain.Top);
						break;
					case FormPosition.RightCenter:
						pos = this.FormMain.Top + (this.FormMain.Height / 2) // calcolo della mezzeria del form principale
							  - (this.Form.Height / 2); // sottrae alla mezzeria l'altezza del form secondario
						this.Form.Location = new Point(this.FormMain.Left + this.FormMain.Width, pos);
						break;
					case FormPosition.RightBottom:
						pos = this.FormMain.Top + this.FormMain.Height - this.Form.Height;
						this.Form.Location = new Point(this.FormMain.Left + this.FormMain.Width, pos);
						break;

					case FormPosition.BottomLeft:
						this.Form.Location = new Point(this.FormMain.Left, this.FormMain.Top + this.FormMain.Height);
						break;
					case FormPosition.BottomCenter:
						pos = this.FormMain.Left + (this.FormMain.Width / 2) // calcolo della mezzeria del form principale
							  - (this.Form.Width / 2); // sottrae alla mezzeria l'altezza del form secondario
						this.Form.Location = new Point(pos, this.FormMain.Top + this.FormMain.Height);
						break;
					case FormPosition.BottomRight:
						pos = this.FormMain.Left + this.FormMain.Width - this.Form.Width;
						this.Form.Location = new Point(pos, this.FormMain.Top + this.FormMain.Height);
						break;
				}
			}
		}
		#endregion

	}
}
