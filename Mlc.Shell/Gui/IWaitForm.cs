﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.Gui
{
    public interface IDataTitleForm
    {
        string Caption { get; set; }
        string Description { get; set; }
    }
}
