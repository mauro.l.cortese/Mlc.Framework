using System.Drawing;
using System.Windows.Forms;
using Mlc.Shell;

namespace Mlc.Shell.Gui
{
    /// <summary>
    /// SplashForm animato
    /// </summary>
    public partial class SplashForm : Form
    {
        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        public SplashForm()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            this.InitializeComponent();
        }
        #endregion

        #region Propriet�
        /// <summary>Gestore dello splash form</summary>
        private SplashScreenManager splashScreenManager = null;
        /// <summary>
        /// Restituisce il gestore dello splash form inizializzato con <see cref="Mlc.Resources.Gui.Forms.SplashForm.GetSplashScreenManager(System.Drawing.Image)"/>
        /// </summary>
        public SplashScreenManager SplashScreenManager { get { return this.splashScreenManager; } }

        /// <summary>
        /// Restituisce i parametri per lo style del form
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                if (SysInfo.IsExecuteInVsIdeDesignMode)
                    return base.CreateParams;
                else
                {
                    CreateParams cp = base.CreateParams;
                    cp.ExStyle |= 0x80000;
                    return cp;
                }
            }
        }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Chiude il form
        /// </summary>
        public new void Close()
        {
            this.splashScreenManager.Close();
        }

        /// <summary>
        /// Inizializza e restituisce un gestore dello splash form
        /// </summary>
        /// <param name="backGroundImage">Immagine del form</param>
        /// <returns>Gestore dello splash form</returns>
        public SplashScreenManager GetSplashScreenManager(Image backGroundImage)
        {
            this.splashScreenManager = new SplashScreenManager(this, backGroundImage);
            MoveForm mv = new MoveForm(this, null);
            return this.splashScreenManager;
        }
        #endregion

        protected override void OnPaint(PaintEventArgs e)
        {
           // this.splashScreenManager.DrawSplashForm();
        }
    }
}
