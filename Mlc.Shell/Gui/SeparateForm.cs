﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 05-07-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 05-07-2019
// ***********************************************************************
// <copyright file="SeparateForm.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading;
using System.Windows.Forms;

namespace Mlc.Shell.Gui
{
    /// <summary>
    /// Class SeparateForm.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SeparateForm<T> : IDataTitleForm where T : Form, IDataTitleForm
    {
        /// <summary>
        /// The form
        /// </summary>
        private T form = null;

        public string Caption { get; set; } = "Per favore attendere";
        public string Description { get; set; } = "Elaborazione in corso";

        /// <summary>
        /// Initializes a new instance of the <see cref="SeparateForm{T}"/> class.
        /// </summary>
        /// <param name="mainForm">The main form.</param>
        public SeparateForm(Form mainForm)
        {
            mainForm.FormClosing += mainForm_FormClosing;
        }
        /// <summary>
        /// Handles the FormClosing event of the mainForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs"/> instance containing the event data.</param>
        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Opens this instance.
        /// </summary>
        public void Open()
        {
            Thread thread = new Thread(new ThreadStart(this.progress));
            thread.Start();
            Thread.Sleep(200);
            Application.DoEvents();
        }

        /// <summary>
        /// Progresses this instance.
        /// </summary>
        private void progress()
        {
            form = Activator.CreateInstance(typeof(T), true) as T;
            form.Caption = this.Caption;
            form.Description = this.Description;
            Application.Run(form);
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public void Close()
        {
            if (form == null) return;
            try
            {
                form.Invoke((MethodInvoker)delegate () { form.Close(); });
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            form = null;
        }
    }
}
