﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Shell.Gui
{
    /// <summary>
    /// Class DragDropMgr.
    /// </summary>
    public class DragDropMgr
    {

        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields

        /// <summary>
        /// The controls
        /// </summary>
        private Control[] controls = null;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DragDropMgr"/> class.
        /// </summary>
        /// <param name="controls">The controls.</param>
        public DragDropMgr(params Control[] controls)
        {
            this.controls = controls;
            this.EnableHandlerEvents(controls);
        }
        #endregion

        #region Public
        #region Properties
        private List<string> fileFilters = new List<string>() { ".txt", ".csv" };
        /// <summary>
        /// Gets or sets the file filters.
        /// </summary>
        /// <value>The file filters.</value>
        public List<string> FileFilters
        {
            get => this.fileFilters;
            set
            {
                this.fileFilters = value;
                for (int i = 0; i < this.fileFilters.Count; i++)
                    this.fileFilters[i] = this.fileFilters[i].ToLower();
            }
        }
        #endregion

        #region Method Static
        #endregion

        #region Method

        /// <summary>
        /// Enables the handler events.
        /// </summary>
        /// <param name="controls">The controls.</param>
        public void EnableHandlerEvents(Control[] controls)
        {
            foreach (Control control in controls)
            {
                this.setAllowDropProperty(control, true);
                control.DragEnter += control_DragEnter;
                control.DragDrop += control_DragDrop;
            }
        }

        /// <summary>
        /// Enables the handler events.
        /// </summary>
        /// <param name="controls">The controls.</param>
        public void DisableHandlerEvents(Control[] controls)
        {
            foreach (Control control in controls)
            {
                this.setAllowDropProperty(control, false);
                control.DragEnter -= control_DragEnter;
                control.DragDrop -= control_DragDrop;
            }
        }

        #endregion
        #endregion

        #region Internal
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion

        #region Event Handlers 
        /// <summary>
        /// Handles the DragDrop event of the Control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void control_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                object dropData = e.Data.GetData(DataFormats.FileDrop);
                if (dropData is string[])
                {
                    List<string> droppedFiles = new List<string>();
                    List<string> invalidDroppedFiles = new List<string>();
                    string[] files = (string[])dropData;

                    if (files.Length == 1)
                        if (new IO.FileSystemMgr(files[0]).FsoType == IO.FileSystemTypes.Directory)
                            files = System.IO.Directory.GetFiles(files[0]);


                    bool allFile = this.fileFilters.Contains(".*");
                    foreach (string file in files)
                    {
                        string extension = System.IO.Path.GetExtension(file).ToLower();
                        if (this.fileFilters.Contains(extension) || allFile)
                            //droppedFiles.Add(file.ToLower());
							droppedFiles.Add(file);
                        else
                            //invalidDroppedFiles.Add(file.ToLower());
							invalidDroppedFiles.Add(file);
                    }
                    if (invalidDroppedFiles.Count != 0)
                        this.OnInvalidFileDropped(new EventArgsDropFiles(invalidDroppedFiles));

                    if (droppedFiles.Count != 0)
                        this.OnDropFiles(new EventArgsDropFiles(droppedFiles));
                }
            }
        }

        /// <summary>
        /// Handles the DragEnter event of the Control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void control_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        /// <summary>
        /// Sets the allow drop property.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="allowDrop">if set to <c>true</c> [allow drop].</param>
        private void setAllowDropProperty(Control control, bool allowDrop)
        {
            control.AllowDrop = allowDrop;
            Control parent = control.Parent;
            while (parent != null)
            {
                parent.AllowDrop = allowDrop;
                parent = parent.Parent;
            }
        }

        #endregion
        #endregion

        #region Event Definitions
        #region Dichiarazione Evento DropFiles
        /// <summary>
        /// Definizione dell'evento.
        /// </summary>
        public event EventHandlerDropFiles DropFiles;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnDropFiles(EventArgsDropFiles e)
        {
            // Se ci sono ricettori in ascolto ...
            if (DropFiles != null)
                // viene lanciato l'evento
                DropFiles(this, e);
        }
        #endregion

        #region Definizione Evento DropFiles
        /// <summary>
        /// Delegato per la gestione dell'evento.
        /// </summary>
        public delegate void EventHandlerDropFiles(object sender, EventArgsDropFiles e);

        /// <summary>
        /// Classe per gli argomenti della generazione di un evento.
        /// </summary>
        public class EventArgsDropFiles : System.EventArgs
        {
            #region Costruttori
            /// <summary>
            /// Inizilizza una nuova istanza
            /// </summary>
            /// <param name="droppedFiles"></param>
            public EventArgsDropFiles(List<string> droppedFiles)
            {
                this.droppedFiles = droppedFiles;
            }
            #endregion

            #region Proprietà
            /// <summary></summary>
            private List<string> droppedFiles;
            /// <summary>
            /// Restituisce o imposta il ...
            /// </summary>
            public List<string> DroppedFiles { get { return droppedFiles; } set { droppedFiles = value; } }
            #endregion
        }
        #endregion

        #region Evento InvalidFileDropped                
        /// <summary>
        /// Delegato per la gestione dell'evento.
        /// </summary>
        public event EventHandlerDropFiles InvalidFileDropped;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnInvalidFileDropped(EventArgsDropFiles e)
        {
            // Se ci sono ricettori in ascolto ...
            if (InvalidFileDropped != null)
                // viene lanciato l'evento
                InvalidFileDropped(this, e);
        }
        #endregion
        #endregion

        #region Embedded Types
        #endregion
    }
}
