using System.Collections.Generic;
using System.Drawing;

namespace Mlc.Shell.Gui
{
    /// <summary>
    /// Collezione delle aree cliccabili di uno splash form
    /// </summary>
    public class ClickAreaCollections : Dictionary<string, ClickArea>
    {
        #region Propriet�
        /// <summary>Area corrente</summary>
        private ClickArea currentArea = null;
        /// <summary>
        /// Restituisce o imposta l'area corrente
        /// </summary>    
        public ClickArea CurrentArea { get { return this.currentArea; } set { this.currentArea = value; } }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Aggiunge una nuova area alla collezione
        /// </summary>
        /// <param name="area">Area da aggiungere alla collezione</param>
        public void Add(ClickArea area)
        {
            base.Add(area.Name, area);
        }

        #endregion

        #region Metodi interni
        /// <summary>
        /// Esegue l hit test sulle aree della collezione
        /// </summary>
        /// <param name="click">Punto su cui � avvenuto il click</param>
        /// <returns>true se il click � avvnuto su un area, altrimetni false</returns>
        internal bool HitTest(Point click)
        {
            foreach (ClickArea area in this.Values)
            {
                if
                    (
                        click.X > area.Location.X &&
                        click.X < area.Location.X + area.AreaSize.Width &&
                        click.Y > area.Location.Y &&
                        click.Y < area.Location.Y + area.AreaSize.Height
                    )
                {
                    this.currentArea = area;
                    return true;
                }
            }
            this.currentArea = null;
            return false;
        }
        #endregion

        #region Definizione eventi
        #region Evento Click
        /// <summary>
        /// Delegato per la gestione dell'evento click su un'area.
        /// </summary>
        public delegate void EventHandlerClick(object sender, EventArgsClick e);

        /// <summary>
        /// Definizione dell'evento click su un'area.
        /// </summary>
        public event EventHandlerClick Click;

        /// <summary>
        /// Metodo per il lancio dell'evento click su un'area.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        internal virtual void OnClick(EventArgsClick e)
        {
            // Se ci sono ricettori in ascolto ...
            if (Click != null)
                // viene lanciato l'evento
                Click(this, e);
        }

        /// <summary>
        /// Classe per gli argomenti della generazione dell'evento click su un'area.
        /// </summary>
        public class EventArgsClick : System.EventArgs
        {
            #region Costruttori
            /// <summary>
            /// Inizilizza una nuova istanza
            /// </summary>
            /// <param name="area">Area su cui � avvenuto i click</param>
            public EventArgsClick(ClickArea area)
            {
                this.area = area;
            }
            #endregion

            #region Propriet�
            /// <summary>Area su cui � avvenuto i click</summary>
            private ClickArea area;
            /// <summary>
            /// Restituisce l'area su cui � avvenuto i click
            /// </summary>
            public ClickArea Area { get { return area; } }
            #endregion
        }
        #endregion
        #endregion
    }
}
