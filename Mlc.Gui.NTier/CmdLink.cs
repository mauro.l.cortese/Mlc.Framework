﻿// ***********************************************************************
// Assembly         : CommandsHandler
// Author           : Cortese Mauro Luigi
// Created          : 01-22-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-22-2016
// ***********************************************************************
// <copyright file="CmdDef.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.ComponentModel;


namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Class CmdDef.
    /// </summary>
    internal class CmdLink
    {
        #region Constants
        #endregion

        #region Enumerations
        #endregion

        #region Fields
        /// <summary>
        /// The mnu items
        /// </summary>
        private MnuItems mnuItems = new MnuItems();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdLink" /> class.
        /// </summary>
        /// <param name="index">The index.</param>
        public CmdLink(int index)
        {
            // TODO: Complete member initialization
            this.index = index;
        }
        #endregion

        #region Public
        #region Properties
        /// <summary>
        /// The index of the command
        /// </summary>
        private int index;
        /// <summary>
        /// Gets the index of the command.
        /// </summary>
        /// <value>the index of the command.</value>
        public int Index { get { return this.index; } }
        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <value>The name of the property.</value>
        public List<Component> MnuComponents { get { return this.mnuItems.GetComponents(); } }
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Internal
        #region Properties
        /// <summary>
        /// Gets or sets the command HDLR.
        /// </summary>
        /// <value>The command HDLR.</value>
        internal CmdHdlr CmdHdlr { get; set; }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        /// <value>The form.</value>
        internal Form Form { get; set; }
        #endregion

        #region Method Static
        #endregion

        #region Method
        /// <summary>
        /// Gets or sets the mnu item.
        /// </summary>
        /// <param name="mnuItem">The mnu item.</param>
        /// <value>The mnu item.</value>
        internal void AddMnuItem(MnuItem mnuItem)
        {
            this.mnuItems.Add(mnuItem.Item, mnuItem);
            if (mnuItem.Item is BarItem)
                ((BarItem)mnuItem.Item).ItemClick += this.barItemCommand_ItemClick;
            else if (mnuItem.Item is SimpleButton)
                ((SimpleButton)mnuItem.Item).Click += this.cmdLink_Click;
            else if (mnuItem.Item is Button)
                ((Button)mnuItem.Item).Click += this.cmdLink_Click;
            else if (mnuItem.Item is ToolStripItem)
                ((ToolStripItem)mnuItem.Item).Click += this.cmdLink_Click;
        }

        /// <summary>
        /// Gets the mnu item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>MnuItem.</returns>
        internal MnuItem GetMnuItem(Component item)
        {
            return this.mnuItems[item];
        }

        #endregion
        #endregion

        #region Protected
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Private
        #region Properties
        #endregion

        #region Method Static
        #endregion

        #region Method
        #endregion
        #endregion

        #region Event Handlers
        /// <summary>
        /// Handles the ItemClick event of the barItemCommand control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ItemClickEventArgs"/> instance containing the event data.</param>
        private void barItemCommand_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.CmdHdlr.BarItemCommandClicked(this, sender, e.Item);
        }

        private void cmdLink_Click(object sender, EventArgs e)
        {
            this.CmdHdlr.BarItemCommandClicked(this, sender, (Component)sender);
        }
        #endregion

        #region Event Definitions
        #endregion

        #region Embedded Types
        #endregion


    }
}
