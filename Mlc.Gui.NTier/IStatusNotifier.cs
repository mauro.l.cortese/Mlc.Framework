﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Interface IStatusNotifier
    /// </summary>
    public interface IStatusNotifier
    {
        /// <summary>
        /// Gets or sets the notify.
        /// </summary>
        /// <value>The notify.</value>
        string Notify { get; set; }

        /// <summary>
        /// Occurs when [notification arrived].
        /// </summary>
        event EventHandler NotificationArrived;

        /// <summary>
        /// Sends the specified notify.
        /// </summary>
        /// <param name="notify">The notify.</param>
        void Send(string notify);

        /// <summary>
        /// Clears this instance.
        /// </summary>
        void Clear();
    }
}
