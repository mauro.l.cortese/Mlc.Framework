﻿// ***********************************************************************
// Assembly         : ClassLibrary1
// Author           : Cortese Mauro Luigi
// Created          : 01-23-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-23-2016
// ***********************************************************************
// <copyright file="MnuItem.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Class MnuItem.
    /// </summary>
    public class MnuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MnuItem"/> class.
        /// </summary>
        /// <param name="mnuItem">The mnu item.</param>
        /// <param name="enumCmd">The enum command.</param>
        /// <returns>MnuItem.</returns>
        public MnuItem(Component mnuItem, Enum enumCmd)
        {
            this.key = enumCmd.ToString();
            this.index = Convert.ToInt32(enumCmd);
            this.item = mnuItem;
        }

        private string key;
        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        internal string Key { get { return this.key; } }

        private int index;
        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>The index.</value>
        internal int Index { get { return this.index; } }

        private Component item;
        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>The item.</value>
        internal Component Item { get { return this.item; } }


        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        /// <value>The form.</value>
        internal ICmdHdlrFrm Form { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                if (this.item is BarItem)
                    return ((BarItem)this.item).Name;
                else if (this.item is Button)
                    return ((Button)this.item).Name;
                else if (this.item is SimpleButton)
                    return ((SimpleButton)this.item).Name;
                else if (this.Item is ToolStripItem)
                    return ((ToolStripItem)this.item).Name;
                else
                    return string.Empty;
            }
        }
    }
}
