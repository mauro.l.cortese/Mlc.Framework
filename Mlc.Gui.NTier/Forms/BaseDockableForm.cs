﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.ComponentModel;
using System.Windows.Forms;
using System;
using Mlc.Shell;
using System.Drawing;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class BaseDockableForm.
	/// </summary>
	/// <seealso cref="DevExpress.XtraEditors.XtraForm" />
	public class BaseDockableForm : XtraForm, ICommandHdlrFrm
	{
		#region Constants
		#endregion

		#region Enumerations
		/// <summary>
		/// Enum BarType
		/// </summary>
		protected enum BarType { Main, Status, Other }
		#endregion

		#region Fields
		/// <summary>
		/// The components
		/// </summary>
		protected IContainer components = null;
		/// <summary>
		/// The dock manager
		/// </summary>
		protected DockManager dockManager = null;
		/// <summary>
		/// The document manager
		/// </summary>
		protected DocumentManager documentManager = null;
		/// <summary>
		/// The tabbed view
		/// </summary>
		protected TabbedView tabbedView = null;
		/// <summary>
		/// The bar manager
		/// </summary>
		protected BarManager barManager = null;
		/// <summary>
		/// The bar status
		/// </summary>
		protected Bar barStatus = null;
		/// <summary>
		/// The bar main
		/// </summary>
		protected Bar barMain = null;
		#endregion

		#region Constructors
		#endregion

		#region Event Handlers - CommandsEngine
		public virtual void GetCommandData(ref Command cmd, Component component)
		{
			//Nothing to do here
		}

		public virtual void ProcessCommandResult(ref Command cmd, Component component)
		{
			//Nothing to do here
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the CMDS HDLR.
		/// </summary>
		/// <value>The CMDS HDLR.</value>
		public ICommandsHdlr CmdsHdlr { get; set; } = CommandsHdlr.GetInstance();
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion

		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected

		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method

		/// <summary>
		/// Initializes the document manager.
		/// </summary>
		protected virtual void InitBarManager()
		{
			this.setComponents();
			this.barManager = new(this.components);
			((ISupportInitialize)this.barManager).BeginInit();
			this.barManager.Form = this;
			this.barMain = GetBar("Main", BarCanDockStyle.Top, BarDockStyle.Top, BarType.Main);
			this.barManager.MaxItemId = 0;


			this.barStatus = GetBar("Status", BarCanDockStyle.Bottom, BarDockStyle.Bottom, BarType.Status);

			BarDockControl barDockControlTop = new() { Manager = this.barManager, Dock = DockStyle.Top };
			BarDockControl barDockControlBottom = new() { Manager = this.barManager, Dock = DockStyle.Bottom };
			BarDockControl barDockControlRight = new() { Manager = this.barManager, Dock = DockStyle.Right };
			BarDockControl barDockControlLeft = new() { Manager = this.barManager, Dock = DockStyle.Left };

			this.barManager.DockControls.Add(barDockControlTop);
			this.barManager.DockControls.Add(barDockControlBottom);
			this.barManager.DockControls.Add(barDockControlLeft);
			this.barManager.DockControls.Add(barDockControlRight);

			this.Controls.Add(barDockControlLeft);
			this.Controls.Add(barDockControlRight);
			this.Controls.Add(barDockControlBottom);
			this.Controls.Add(barDockControlTop);

			this.barManager.DockControls.Add(barDockControlTop);
			this.barManager.DockControls.Add(barDockControlBottom);
			this.barManager.DockControls.Add(barDockControlLeft);
			this.barManager.DockControls.Add(barDockControlRight);

			((ISupportInitialize)this.barManager).EndInit();

		}

		/// <summary>
		/// Gets the bar.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="barCanDockStyle">The bar can dock style.</param>
		/// <param name="barDockStyle">The bar dock style.</param>
		/// <param name="barType">Type of the bar.</param>
		/// <returns>Bar.</returns>
		protected Bar GetBar(string name, BarCanDockStyle barCanDockStyle, BarDockStyle barDockStyle, BarType barType = BarType.Other)
		{
			Bar bar = new() { BarName = "Bar" + name, Text = name };
			bar.DockCol = 0;
			bar.DockRow = 0;
			bar.DockStyle = barDockStyle;
			bar.OptionsBar.UseWholeRow = true;
			bar.OptionsBar.AllowQuickCustomization = false;
			bar.OptionsBar.DrawDragBorder = false;

			this.barManager.Bars.Add(bar);
			switch (barType)
			{
				case BarType.Main:
					//this.barManager.MainMenu = bar;
					bar.OptionsBar.MultiLine = true;
					break;
				case BarType.Status:
					this.barManager.StatusBar = bar;
					bar.CanDockStyle = barCanDockStyle;
					break;
				case BarType.Other:
					bar.CanDockStyle = BarCanDockStyle.All;
					break;
			}

			return bar;
		}

		/// <summary>
		/// Initializes the document manager.
		/// </summary>
		protected virtual void InitDockManager()
		{

			this.setComponents();
			this.dockManager = new DockManager(this.components);
			this.documentManager = new DocumentManager(this.components);
			this.tabbedView = new TabbedView(this.components);
			((ISupportInitialize)this.dockManager).BeginInit();
			((ISupportInitialize)this.documentManager).BeginInit();
			((ISupportInitialize)this.tabbedView).BeginInit();


			this.dockManager.Form = this;
			this.dockManager.TopZIndexControls.AddRange(new string[] {
			"DevExpress.XtraBars.BarDockControl",
			"DevExpress.XtraBars.StandaloneBarDockControl",
			"System.Windows.Forms.StatusBar",
			"System.Windows.Forms.MenuStrip",
			"System.Windows.Forms.StatusStrip",
			"DevExpress.XtraBars.Ribbon.RibbonStatusBar",
			"DevExpress.XtraBars.Ribbon.RibbonControl",
			"DevExpress.XtraBars.Navigation.OfficeNavigationBar",
			"DevExpress.XtraBars.Navigation.TileNavPane",
			"DevExpress.XtraBars.TabFormControl",
			});
			//this.dockManager.DockingOptions.ShowCloseButton = false;

			if (this.IsMdiContainer)
				this.documentManager.MdiParent = this;
			else
				this.documentManager.ContainerControl = this;
			this.documentManager.View = this.tabbedView;
			this.documentManager.ViewCollection.AddRange(new BaseView[] { this.tabbedView });
		}

		/// <summary>
		/// Ends the initialize document manager.
		/// </summary>
		protected virtual void EndInitDocManager()
		{
			((ISupportInitialize)this.dockManager).EndInit();
			((ISupportInitialize)this.documentManager).EndInit();
			((ISupportInitialize)this.tabbedView).EndInit();
		}

		/// <summary>
		/// Adds the new panel.
		/// </summary>
		/// <param name="namePanel">The name panel.</param>
		/// <param name="titlePanel">The title panel.</param>
		/// <param name="control">The control.</param>
		/// <param name="dockingStyle">The docking style.</param>
		/// <returns>DockPanel.</returns>
		protected DockPanel AddNewPanel(string namePanel, string titlePanel, DockingStyle dockingStyle, Control control = null)
		{
			if (this.dockManager == null) return null;
			DockPanel panel = this.dockManager.AddPanel(dockingStyle);
			if (control != null)
			{
				control.Visible = true;
				control.Parent = panel.ControlContainer;
				control.Dock = DockStyle.Fill;
			}
			panel.Text = titlePanel;
			panel.Name = namePanel;
			panel.Options.ShowCloseButton = false;
			return panel;
		}

		/// <summary>
		/// Adds the new tab panel.
		/// </summary>
		/// <param name="namePanel">The name panel.</param>
		/// <param name="titlePanel">The title panel.</param>
		/// <param name="control">The control.</param>
		/// <returns>DockPanel.</returns>
		protected DockPanel AddNewTabPanel(string namePanel, string titlePanel, Control control = null)
		{
			DockPanel panel = this.AddNewPanel(namePanel, titlePanel, DockingStyle.Float, control);
			panel.DockedAsTabbedDocument = true;
			panel.Options.ShowCloseButton = true;
			panel.ClosedPanel += (sender, e) => panel.Dispose();
			return panel;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing & components != null && components.Components.Count > 0)
				try
				{
					foreach (var item in components.Components)
						if (item is IDisposable disp)
							try { disp.Dispose(); }
							catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); } //components.Dispose();
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			base.Dispose(disposing);
		}
		#endregion

		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Sets the components.
		/// </summary>
		private void setComponents()
		{
			if (this.components == null)
				this.components = new Container();
		}
		#endregion

		#region Event Handlers
		#endregion

		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion


		//private void InitializeComponent()
		//{
		//	this.SuspendLayout();
		//	// 
		//	// BaseDockableForm
		//	// 
		//	this.ClientSize = new System.Drawing.Size(691, 428);
		//	this.Name = "BaseDockableForm";
		//	this.ResumeLayout(false);

		//}
	}
}
