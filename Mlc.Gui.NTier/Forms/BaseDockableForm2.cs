﻿using DevExpress.Utils;
using DevExpress.Utils.Serializing;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraTreeList;
using Mlc.Gui.DevExpressTools;
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DView = DevExpress.XtraBars.Docking2010.Views;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class BaseDockableForm.
	/// </summary>
	/// <seealso cref="DevExpress.XtraEditors.XtraForm" />
	public class BaseDockableForm2 : XtraForm, ICommandHdlrFrm
	{
		#region Constants
		#endregion

		#region Enumerations
		/// <summary>
		/// Enum BarType
		/// </summary>
		protected enum BarType { Main, Info, Other }

		/// <summary>
		/// Enum LayoutType
		/// </summary>
		[Flags]
		public enum LayoutType
		{
			None = 0,
			BarManager = 2,
			DockManager = 4,
			LayoutControl = 8,
			LayoutForm = DockManager | LayoutControl,
			GridView = 16,
			TreeView = 32,
			GridAndTree = GridView | TreeView,
			All = LayoutControl | DockManager | BarManager | GridView | TreeView
		}
		#endregion

		#region Fields
		/// <summary>
		/// The components
		/// </summary>
		protected IContainer components = new Container();

		/// <summary>
		/// The layout controls
		/// </summary>
		protected LayoutControls layoutControls = new();

		/// <summary>
		/// The grid controls
		/// </summary>
		protected GridControls gridControls = new();

		/// <summary>
		/// The tree control
		/// </summary>
		protected TreeControls treeControls = new();

		/// <summary>
		/// The simple buttons
		/// </summary>
		protected SimpleButtons simpleButtons = new();

		/// <summary>
		/// The bar manager
		/// </summary>
		protected BarManager barManager = null;

		/// <summary>
		/// The dock manager
		/// </summary>
		protected DockManager dockManager = null;

		/// <summary>
		/// The document manager
		/// </summary>
		protected DocumentManager documentManager = null;

		/// <summary>
		/// The tabbed view
		/// </summary>
		protected TabbedView tabbedView = null;

		/// <summary>
		/// The document group
		/// </summary>
		private DocumentGroup documentGroup = null;

		/// <summary>
		/// The bar menu composer
		/// </summary>
		protected ToolBarComposer ToolBarComposer = null;

		/// <summary>
		/// The list dock panel
		/// </summary>
		private List<DockPanel> listDockPanel = new List<DockPanel>();

		/// <summary>
		/// The list document
		/// </summary>
		private List<Document> listDocument = new List<Document>();

		/// <summary>
		/// The is initilized
		/// </summary>
		private bool isInitialized;

		/// <summary>
		/// The folder layout io
		/// </summary>
		private string folderLayoutIO;

		/// <summary>
		/// The accordion control
		/// </summary>
		protected AccordionManager accordionManager;
		#endregion

		#region Constructors
		public BaseDockableForm2() => this.formInitialize();
		#endregion

		#region Event Handlers - CommandsEngine
		public virtual void GetCommandData(ref Command cmd, Component component)
		{
			//Nothing to do here
		}

		public virtual void ProcessCommandResult(ref Command cmd, Component component)
		{
			//Nothing to do here
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the CMDS HDLR.
		/// </summary>
		/// <value>The CMDS HDLR.</value>
		public ICommandsHdlr CmdsHdlr { get; set; } = CommandsHdlr.GetInstance();
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion

		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected

		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Adds the accordion control.
		/// </summary>
		/// <param name="dockStyle">The dock style.</param>
		/// <param name="scrollBarMode">The scroll bar mode.</param>
		/// <returns>AccordionControl.</returns>
		protected AccordionManager InitializeAccordionManager(DockStyle dockStyle = DockStyle.Left, ScrollBarMode scrollBarMode = ScrollBarMode.Auto)
		{
			if (!this.isInitialized && this.accordionManager == null)
				this.accordionManager = new(this, dockStyle, scrollBarMode);
			return this.accordionManager;
		}

		/// <summary>
		/// Adds the bar manager.
		/// </summary>
		protected BarManager InitializeBarManager()
		{
			if (!this.isInitialized && this.barManager == null)
			{
				this.barManager = new(this.components);
				this.barManager.BeginInit();
				this.barManager.Form = this;
				this.barManager.MaxItemId = 0;

				this.barManager.Bars.AddRange(new Bar[] {
					getBar("Main", BarCanDockStyle.Top, BarDockStyle.Top, BarType.Main),
					getBar("Info", BarCanDockStyle.Bottom, BarDockStyle.Bottom, BarType.Info)
				});

				addBarDockControl(DockStyle.Top);
				addBarDockControl(DockStyle.Bottom);
				addBarDockControl(DockStyle.Right);
				addBarDockControl(DockStyle.Left);

				void addBarDockControl(DockStyle dockStyle)
				{
					BarDockControl barDockControl = new() { Manager = this.barManager, Dock = dockStyle };
					this.barManager.DockControls.Add(barDockControl);
					this.Controls.Add(barDockControl);
				}
			}
			return this.barManager;
		}

		/// <summary>
		/// Adds the dock manager.
		/// </summary>
		/// <returns>DockManager.</returns>
		protected DockManager AddDockManager()
		{
			if (!this.isInitialized && this.dockManager == null)
			{
				this.dockManager = new(this.components);
				this.documentManager = new(this.components);
				this.tabbedView = new(this.components);
				this.documentGroup = new(this.components);

				((ISupportInitialize)(this.dockManager)).BeginInit();
				((ISupportInitialize)(this.documentManager)).BeginInit();
				((ISupportInitialize)(this.tabbedView)).BeginInit();
				((ISupportInitialize)(this.documentGroup)).BeginInit();

				// 
				// dockManager1
				// 
				this.dockManager.DockingOptions.ShowCaptionImage = true;
				this.dockManager.Form = this;
				this.dockManager.TopZIndexControls.AddRange(new string[] {
					"DevExpress.XtraBars.BarDockControl",
					"DevExpress.XtraBars.StandaloneBarDockControl",
					"System.Windows.Forms.StatusBar",
					"System.Windows.Forms.MenuStrip",
					"System.Windows.Forms.StatusStrip",
					"DevExpress.XtraBars.Ribbon.RibbonStatusBar",
					"DevExpress.XtraBars.Ribbon.RibbonControl",
					"DevExpress.XtraBars.Navigation.OfficeNavigationBar",
					"DevExpress.XtraBars.Navigation.TileNavPane",
					"DevExpress.XtraBars.TabFormControl",
					"DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl",
					"DevExpress.XtraBars.ToolbarForm.ToolbarFormControl",
					});

				// documentManager1
				this.documentManager.ContainerControl = this;
				this.documentManager.View = this.tabbedView;
				this.documentManager.ViewCollection.AddRange(new DView.BaseView[] { this.tabbedView });

				// tabbedView1
				this.tabbedView.DocumentGroups.AddRange(new DocumentGroup[] { this.documentGroup });
				this.tabbedView.RootContainer.Nodes.AddRange(new DockingContainer[] { new DockingContainer() { Element = this.documentGroup } });

				//this.dockManager = new(this.components);
				//this.documentManager = new();
				//this.tabbedView = new();
				//this.documentGroup = new(this.components);

				//((ISupportInitialize)this.dockManager).BeginInit();
				//((ISupportInitialize)this.documentManager).BeginInit();
				//((ISupportInitialize)this.tabbedView).BeginInit();
				//((ISupportInitialize)this.documentGroup).BeginInit();

				//this.dockManager.Form = this;
				//this.dockManager.TopZIndexControls.AddRange(new string[] {
				//	"DevExpress.XtraBars.BarDockControl",
				//	"DevExpress.XtraBars.StandaloneBarDockControl",
				//	"System.Windows.Forms.StatusBar",
				//	"System.Windows.Forms.MenuStrip",
				//	"System.Windows.Forms.StatusStrip",
				//	"DevExpress.XtraBars.Ribbon.RibbonStatusBar",
				//	"DevExpress.XtraBars.Ribbon.RibbonControl",
				//	"DevExpress.XtraBars.Navigation.OfficeNavigationBar",
				//	"DevExpress.XtraBars.Navigation.TileNavPane",
				//	"DevExpress.XtraBars.TabFormControl",
				//	});

				//this.documentManager.ContainerControl = this;
				//this.documentManager.View = this.tabbedView;
				//this.documentManager.ViewCollection.AddRange(
				//	new DevExpress.XtraBars.Docking2010.Views.BaseView[] { this.tabbedView });

				//this.tabbedView.DocumentGroups.AddRange(new DocumentGroup[] { this.documentGroup });
				//this.tabbedView.RootContainer.Nodes.Add(new DockingContainer() { Element = this.documentGroup });
			}
			return this.dockManager;
		}

		/// <summary>
		/// Adds the new panel.
		/// </summary>
		/// <param name="idPanel">The identifier panel.</param>
		/// <param name="title">The title panel.</param>
		/// <param name="control">The control.</param>
		/// <param name="dockingStyle">The docking style.</param>
		/// <returns>DockPanel.</returns>
		protected DockPanel AddNewPanel(Guid idPanel, Control control, string title = "")
		{

			if (this.dockManager == null || control == null) return null;

			DockPanel dockPanel = new();
			ControlContainer dockPanel_Container = new();
			Document document = new(this.components);

			listDockPanel.Add(dockPanel);
			listDocument.Add(document);

			dockPanel.SuspendLayout();

			//string suffix = $"{dockPanel.Count:00}";
			string suffix = control.Name;

			((ISupportInitialize)(document)).BeginInit();

			this.dockManager.RootPanels.AddRange(new DockPanel[] { dockPanel });
			// 
			// dockPanel
			// 
			dockPanel.Appearance.Options.UseImage = true;
			dockPanel.Controls.Add(dockPanel_Container);
			dockPanel.DockedAsTabbedDocument = true;
			dockPanel.FloatLocation = new Point(0, 0);
			dockPanel.ID = idPanel; // new("7ccc10e2-8775-4019-9876-98021e954cc7");
									//dockPanel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dockPanel2.ImageOptions.Image")));
			dockPanel.Name = $"dockPanel{suffix}";
			dockPanel.Options.AllowDockTop = false;
			dockPanel.OriginalSize = new Size(200, 200);
			dockPanel.SavedDock = DockingStyle.Left;
			dockPanel.SavedIndex = 0;
			dockPanel.Text = string.IsNullOrEmpty(title) ? $"dockPanel{suffix}" : title;
			// 
			// dockPanel2_Container
			// 
			dockPanel_Container.Controls.Add(control);
			dockPanel_Container.Location = new Point(0, 0);
			dockPanel_Container.Name = $"dockPanel_Container{suffix}";
			dockPanel_Container.Size = new Size(200, 200);
			dockPanel_Container.TabIndex = 0;

			// 
			// tabbedView1
			// 
			this.tabbedView.Documents.AddRange(new DView.BaseDocument[] { document });
			// 
			// document1
			// 
			document.Caption = dockPanel.Name;
			document.ControlName = dockPanel.Name;
			document.FloatLocation = new Point(-1548, 318);
			document.FloatSize = new Size(200, 200);
			document.Properties.AllowClose = DefaultBoolean.True;
			document.Properties.AllowFloat = DefaultBoolean.True;
			document.Properties.AllowFloatOnDoubleClick = DefaultBoolean.True;
			// 
			// documentGroup1
			// 
			this.documentGroup.Items.AddRange(new Document[] { document });

			return dockPanel;

			//DockPanel panel = this.dockManager.AddPanel(DockingStyle.Bottom);

			////panel.SuspendLayout();
			////panel.ControlContainer.SuspendLayout();

			//panel.Text = string.IsNullOrEmpty(title) ? panel.Name : title;
			//panel.ID = idPanel;
			//panel.ControlContainer.Controls.Add(control);
			////panel.Visibility = DockVisibility.Hidden;
			////panel.DockedAsTabbedDocument = true;
		}

		/// <summary>
		/// Adds the control layout.
		/// </summary>
		/// <param name="name">The name.</param>
		protected LayoutControl AddLayoutControl(string name, string caption = "")
		{
			LayoutControl layoutControl = null;
			if (!this.isInitialized)
			{
				layoutControl = layoutControls.GetLayout(name, caption);
				layoutControl.BeginInit();
				layoutControl.SuspendLayout();
				LayoutControlGroup root = new();
				root.BeginInit();
				root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
				root.GroupBordersVisible = false;
				root.Name = $"root{name}";
				root.TextVisible = false;
				layoutControl.Root = root;
				layoutControl.Dock = DockStyle.Fill;
				this.Controls.Add(layoutControl);
			}
			return layoutControl;
		}

		/// <summary>
		/// Adds the grid view control.
		/// </summary>
		/// <param name="name">Name of the grid.</param>
		/// <param name="layoutName">Name of the layout.</param>
		protected GridControl AddGridViewControl(string name, string layoutName = "")
		{
			GridControl gridControl = null;
			if (!this.isInitialized)
			{
				layoutName = this.getLayoutName(layoutName);

				if (this.layoutControls.ContainsKey(layoutName))
				{
					gridControl = gridControls.GetGrid(name); ;
					GridView gridView = new();
					gridControl.BeginInit();
					gridView.BeginInit();

					gridControl.MainView = gridView;
					gridControl.Name = $"gridControl{name.GetTrimForChar(' ')}";
					gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { gridView });
					gridView.GridControl = gridControl;
					gridView.Name = $"gridView{name}";
					gridView.OptionsView.ColumnAutoWidth = false;
					addControlToLayout(layoutName, name, gridControl);
				}
			}
			return gridControl;
		}

		/// <summary>
		/// Adds the tree list control.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="layoutName">Name of the layout.</param>
		protected TreeList AddTreeListControl(string name, string layoutName = "")
		{
			TreeList treeList = null;
			if (!this.isInitialized)
			{
				layoutName = this.getLayoutName(layoutName);
				if (this.layoutControls.ContainsKey(layoutName))
				{
					treeList = treeControls.GetTree(name);
					treeList.BeginInit();
					treeList.OptionsView.AutoWidth = false;
					addControlToLayout(layoutName, name, treeList);
				}
			}
			return treeList;
		}

		/// <summary>
		/// Adds the simple button.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="layoutName">Name of the layout.</param>
		protected SimpleButton AddSimpleButton(string name, string layoutName = "")
		{
			SimpleButton simpleButton = null;
			if (!this.isInitialized)
			{
				layoutName = this.getLayoutName(layoutName);
				if (this.layoutControls.ContainsKey(layoutName))
				{
					simpleButton = simpleButtons.GetSimpleButton(name); ;
					simpleButton.Text = name;
					simpleButton.StyleController = this.layoutControls[layoutName];
					addControlToLayout(layoutName, name, simpleButton);
				}
			}
			return simpleButton;
		}

		/// <summary>
		/// Saves the layout.
		/// </summary>
		/// <param name="layoutType">Type of the layout.</param>
		/// <returns>List&lt;System.String&gt;.</returns>
		protected List<string> SaveLayout(LayoutType layoutType = LayoutType.All) => this.layoutIO(layoutType, true);

		/// <summary>
		/// Saves the layout.
		/// </summary>
		/// <param name="layoutType">Type of the layout.</param>
		/// <returns>List&lt;System.String&gt;.</returns>
		protected List<string> LoadLayout(LayoutType layoutType = LayoutType.All) => this.layoutIO(layoutType, false);

		/// <summary>
		/// Initializes the component.
		/// </summary>
		protected virtual void InitializeComponent()
		{
			FileSystemMgr fsm = new(Path.Combine(new SysInfo().ExecutableFolder, "xml\\layout"));
			if (!fsm.Exists)
				fsm.Create(FileSystemTypes.Directory);

			if (!fsm.Exists)
				throw new($"Immpossibile creare o accedere alla cartella: {fsm}");

			EventArgsFolderSettingInitializing eventArgsFolderSettingInitializing = new(fsm.Path);
			this.OnFolderSettingInitializing(eventArgsFolderSettingInitializing);
			this.folderLayoutIO = eventArgsFolderSettingInitializing.FolderLayoutIO;
		}

		/// <summary>Initializes the menu.</summary>
		/// <param name="barManager"></param>
		/// <remarks>This method is called only when the BarManager control has been initialized</remarks>
		protected virtual void InitializeBarMenu(BarManager barManager)
		{

		}

		/// <summary>Initializes the accordion control.</summary>
		/// <param name="accordionManager">The accordion manager.</param>
		/// <remarks>This method is called only when the AccordionManager control has been initialized</remarks>
		protected virtual void InitializeAccordionControl(AccordionManager accordionManager)
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null) && components.Components.Count > 0)
				try { components.Dispose(); }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			base.Dispose(disposing);
		}
		#endregion

		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method

		/// <summary>
		/// Controls the initialize.
		/// </summary>
		private void formInitialize()
		{
			if (SysInfo.IsExecuteInVsIdeDesignMode) return;
			//Console.WriteLine($"IsInitializing:{this.IsInitializing}");
			this.isInitialized = false;
			this.SuspendLayout();

			this.InitializeComponent();

			if (this.barManager != null)
				this.InitializeBarMenu(this.barManager);

			if (this.accordionManager != null)
				this.InitializeAccordionControl(this.accordionManager);

			if (this.dockManager != null)
			{
				foreach (Document document in listDocument)
					((ISupportInitialize)(document)).EndInit();
				foreach (DockPanel dockPanel in listDockPanel)
					dockPanel.ResumeLayout(false);

				((ISupportInitialize)this.dockManager).EndInit();
				if (this.documentManager != null)
					((ISupportInitialize)this.documentManager).EndInit();
				if (this.tabbedView != null)
					((ISupportInitialize)this.tabbedView).EndInit();
				if (this.documentGroup != null)
					((ISupportInitialize)this.documentGroup).EndInit();
			}

			if (this.barManager != null)
				this.barManager.EndInit();

			if (this.accordionManager != null)
				this.accordionManager.EndInit();

			foreach (LayoutControl layoutControl in this.layoutControls.Values)
			{
				layoutControl.EndInit();
				layoutControl.Root.EndInit();
				foreach (LayoutItem item in layoutControl.Root.Items)
					item.EndInit();
				layoutControl.ResumeLayout(false);
			}

			foreach (GridControl grid in this.gridControls.Values)
			{
				grid.EndInit();
				foreach (DevExpress.XtraGrid.Views.Base.BaseView view in grid.Views)
					view.EndInit();
			}

			foreach (TreeList tree in this.treeControls.Values)
				tree.EndInit();

			this.AutoScaleDimensions = new SizeF(6F, 13F);
			this.AutoScaleMode = AutoScaleMode.None;

			this.ResumeLayout(false);
			this.isInitialized = true;

			this.LoadLayout(LayoutType.BarManager);
		}

		/// <summary>
		/// Layouts the io.
		/// </summary>
		/// <param name="layoutType">Type of the layout.</param>
		/// <param name="save">if set to <c>true</c> [save].</param>
		/// <returns>List&lt;System.String&gt;.</returns>
		private List<string> layoutIO(LayoutType layoutType, bool save = true)
		{
			if (string.IsNullOrEmpty(this.folderLayoutIO))
				throw new($"Percorso cartella settaggi non configurato!");
			if (string.IsNullOrEmpty(this.Name))
				throw new($"Nome del Form non impostato o vuoto");

			FileSystemMgr fsm = new(Path.Combine(this.folderLayoutIO, this.Name));
			List<string> files = new();
			if (!fsm.Exists)
				fsm.Create(FileSystemTypes.Directory);

			if (!fsm.Exists)
				throw new($"Impossibile creare o accedere alla cartella :{fsm.Path}");

			string getFileName(string text) => Path.Combine(fsm.Path, $"{string.Join("_", text.GetFirstMaius(true).SplitUpperCamelCase()).ToLower()}.xml");

			EnumOperator<LayoutType> eo = new();

			if (eo.Contains(layoutType, LayoutType.BarManager) && this.barManager != null)
				try { ioXtraSerializer(this.barManager, Path.Combine(fsm.Path, "bar_manager.xml"), save); }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			if (eo.Contains(layoutType, LayoutType.DockManager) && this.dockManager != null)
				try { ioXtraSerializer(dockManager, Path.Combine(fsm.Path, "dock_manager.xml"), save); }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			if (eo.Contains(layoutType, LayoutType.LayoutControl))
				foreach (LayoutControl layout in this.layoutControls.Values)
					try { ioXtraSerializer(layout, Path.Combine(fsm.Path, getFileName(layout.Name)), save); }
					catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			if (eo.Contains(layoutType, LayoutType.GridView))
				foreach (GridControl grid in this.gridControls.Values)
					foreach (DevExpress.XtraGrid.Views.Base.BaseView view in grid.Views)
						try { ioXtraSerializer(view, Path.Combine(fsm.Path, getFileName(view.Name)), save); }
						catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			if (eo.Contains(layoutType, LayoutType.TreeView))
				foreach (TreeList tree in this.treeControls.Values)
					try { ioXtraSerializer(tree, Path.Combine(fsm.Path, getFileName(tree.Name)), save); }
					catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			void ioXtraSerializer(ISupportXtraSerializer iSupportXtraSerializer, string path, bool save)
			{
				try
				{
					FileSystemMgr fsm = new(path);
					if (save)
					{
						iSupportXtraSerializer.SaveLayoutToXml(fsm.Path);
						files.Add(fsm.Path);
					}
					else
					{
						if (fsm.Exists)
						{
							iSupportXtraSerializer.RestoreLayoutFromXml(fsm.Path);
							files.Add(fsm.Path);
						}
					}
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}

			return files;
		}

		/// <summary>
		/// Adds the control to layout.
		/// </summary>
		/// <param name="layoutName">Name of the layout.</param>
		/// <param name="name">The name.</param>
		/// <param name="control">The control.</param>
		private void addControlToLayout(string layoutName, string name, Control control)
		{
			LayoutControl layoutControl = this.layoutControls[layoutName];
			control.Dock = DockStyle.Fill;
			layoutControl.Controls.Add(control);
			LayoutControlItem layoutItem = new();
			layoutItem.BeginInit();
			layoutItem.Control = control;
			layoutItem.Name = $"layoutItem{name.GetTrimForChar(' ')}";
			layoutItem.Text = name;
			layoutItem.TextVisible = true;
			layoutItem.TextLocation = DevExpress.Utils.Locations.Top;
			layoutControl.Root.Items.AddRange(new BaseLayoutItem[] { layoutItem });
		}

		/// <summary>
		/// Gets the name of the layout.
		/// </summary>
		/// <param name="layoutName">Name of the layout.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		private string getLayoutName(string layoutName)
		{
			string retval = layoutName;
			if (!this.layoutControls.ContainsKey(retval))
				retval = this.layoutControls.Keys.Last();
			return retval;
		}


		/// <summary>
		/// Gets the bar.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="barCanDockStyle">The bar can dock style.</param>
		/// <param name="barDockStyle">The bar dock style.</param>
		/// <param name="barType">Type of the bar.</param>
		/// <returns>Bar.</returns>
		private Bar getBar(string name, BarCanDockStyle barCanDockStyle, BarDockStyle barDockStyle, BarType barType = BarType.Other)
		{
			Bar bar = new()
			{
				BarName = "Bar" + name,
				Text = name,
				DockCol = 0,
				DockRow = 0,
				CanDockStyle = barCanDockStyle,
				DockStyle = barDockStyle,
			};

			bar.OptionsBar.UseWholeRow = true;
			bar.OptionsBar.AllowQuickCustomization = false;
			bar.OptionsBar.DrawDragBorder = false;

			switch (barType)
			{
				case BarType.Main:
					bar.OptionsBar.MultiLine = true;
					break;
				case BarType.Info:
					this.barManager.StatusBar = bar;
					break;
				case BarType.Other:
					bar.CanDockStyle = BarCanDockStyle.All;
					break;
			}
			return bar;
		}
		#endregion

		#region Event Handlers
		#endregion

		#endregion

		#region Event Definitions

		#region Event FolderSettingInitializing
		/// <summary>
		/// Event FolderSettingInitializing.
		/// </summary>
		public event EventHandlerFolderSettingInitializing FolderSettingInitializing;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnFolderSettingInitializing(EventArgsFolderSettingInitializing e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.FolderSettingInitializing?.Invoke(this, e);
		}
		#endregion

		#region FolderSettingInitializing event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerFolderSettingInitializing(object sender, EventArgsFolderSettingInitializing e);

		/// <summary>
		/// FolderSettingInitializing arguments.
		/// </summary>
		public class EventArgsFolderSettingInitializing : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initialized a new instance
			/// </summary>
			/// <param name="folderLayoutIO"></param>
			public EventArgsFolderSettingInitializing(string folderLayoutIO) => this.FolderLayoutIO = folderLayoutIO;
			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			public string FolderLayoutIO { get; set; }
			#endregion
		}
		#endregion


		#endregion

		#region Embedded Types
		/// <summary>
		/// Class LayoutControls.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraLayout.LayoutControl}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraLayout.LayoutControl}" />
		protected class LayoutControls : Dictionary<string, LayoutControl>
		{
			internal LayoutControl GetLayout(string name, string caption)
			{
				name = name.GetTrimForChar(' ');
				if (!this.ContainsKey(name))
					this.Add(name, new());
				LayoutControl layoutControl = this[name];
				layoutControl.Name = name;
				layoutControl.Text = string.IsNullOrEmpty(caption) ? name : caption;
				return layoutControl;
			}
		}

		/// <summary>
		/// Class GridControls.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraGrid.GridControl}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraGrid.GridControl}" />
		protected class GridControls : Dictionary<string, GridControl>
		{
			internal GridControl GetGrid(string name)
			{
				name = name.GetTrimForChar(' ');
				if (!this.ContainsKey(name))
					this.Add(name, new());
				GridControl gridControl = this[name];
				gridControl.Name = name;
				return gridControl;
			}
		}


		/// <summary>
		/// Class GridControls.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraGrid.GridControl}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraGrid.GridControl}" />
		protected class TreeControls : Dictionary<string, TreeList>
		{
			internal TreeList GetTree(string name)
			{
				name = name.GetTrimForChar(' ');
				if (!this.ContainsKey(name))
					this.Add(name, new());
				TreeList treeList = this[name];
				treeList.Name = name;
				return treeList;
			}
		}

		/// <summary>
		/// Class SimpleButtons.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraEditors.SimpleButton}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, DevExpress.XtraEditors.SimpleButton}" />
		protected class SimpleButtons : Dictionary<string, SimpleButton>
		{
			internal SimpleButton GetSimpleButton(string name)
			{
				name = name.GetTrimForChar(' ');
				if (!this.ContainsKey(name))
					this.Add(name, new());
				SimpleButton simpleButton = this[name];
				simpleButton.Name = name;
				return simpleButton;
			}
		}

		/// <summary>
		/// Class AccordionManager.
		/// </summary>
		protected class AccordionManager
		{
			/// <summary>
			/// The accordion control
			/// </summary>
			public AccordionControl AccordionControl { get; set; }

			/// <summary>
			/// Initializes a new instance of the <see cref="AccordionManager"/> class.
			/// </summary>
			/// <param name="form">The form.</param>
			/// <param name="dockStyle">The dock style.</param>
			/// <param name="scrollBarMode">The scroll bar mode.</param>
			public AccordionManager(Form form, DockStyle dockStyle, ScrollBarMode scrollBarMode)
			{
				this.AccordionControl = new();
				this.AccordionControl.BeginInit();
				this.AccordionControl.Name = "accordionControl";
				this.AccordionControl.Text = "accordionControl";
				this.DockStyle = dockStyle;
				this.ScrollBarMode = scrollBarMode;
				this.Size = new Size(200, 450);
				this.ExpandElementMode = ExpandElementMode.Single;
				this.AllowMinimizeMode = DefaultBoolean.True;
				this.NormalWidth = 250;
				this.ShowFilterControl = ShowFilterControl.Always;
				form.Controls.Add(this.AccordionControl);
			}

			/// <summary>
			/// Gets or sets the dock style.
			/// </summary>
			/// <value>The dock style.</value>
			public DockStyle DockStyle { get => this.AccordionControl.Dock; set => this.AccordionControl.Dock = value; }

			/// <summary>
			/// Gets or sets the scroll bar mode.
			/// </summary>
			/// <value>The scroll bar mode.</value>
			public ScrollBarMode ScrollBarMode { get => this.AccordionControl.ScrollBarMode; set => this.AccordionControl.ScrollBarMode = value; }

			/// <summary>
			/// Gets or sets the size.
			/// </summary>
			/// <value>The size.</value>
			public Size Size { get => this.AccordionControl.Size; set => this.AccordionControl.Size = value; }

			/// <summary>
			/// Gets or sets the expand element mode.
			/// </summary>
			/// <value>The expand element mode.</value>
			public ExpandElementMode ExpandElementMode { get => this.AccordionControl.ExpandElementMode; set => this.AccordionControl.ExpandElementMode = value; }

			/// <summary>
			/// Gets or sets the show filter control.
			/// </summary>
			/// <value>The show filter control.</value>
			public ShowFilterControl ShowFilterControl { get => this.AccordionControl.ShowFilterControl; set => this.AccordionControl.ShowFilterControl = value; }

			/// <summary>
			/// Gets or sets the options minimizing.
			/// </summary>
			/// <value>The options minimizing.</value>
			public DefaultBoolean AllowMinimizeMode { get => this.AccordionControl.OptionsMinimizing.AllowMinimizeMode; set => this.AccordionControl.OptionsMinimizing.AllowMinimizeMode = value; }

			/// <summary>
			/// Gets or sets the width of the normal.
			/// </summary>
			/// <value>The width of the normal.</value>
			public int NormalWidth { get => this.AccordionControl.OptionsMinimizing.NormalWidth; set => this.AccordionControl.OptionsMinimizing.NormalWidth = value; }

			/// <summary>
			/// Adds the accordion element.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <param name="text">The text.</param>
			/// <returns>AccordionControlElement.</returns>
			public AccordionControlElement AddAccordionElement(string name, string text)
				=> AddAccordionElement(name, text, ElementStyle.Item);


			/// <summary>
			/// Adds the accordion element.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <param name="text">The text.</param>
			/// <param name="accordionElementParent">The accordion element parent.</param>
			/// <returns>AccordionControlElement.</returns>
			public AccordionControlElement AddAccordionElement(string name, string text, AccordionControlElement accordionElementParent)
				=> AddAccordionElement(name, text, ElementStyle.Item, accordionElementParent);

			/// <summary>
			/// Adds the accordion element.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <param name="text">The text.</param>
			/// <param name="accordionElementChildren">The accordion element children.</param>
			/// <returns>AccordionControlElement.</returns>
			public AccordionControlElement AddAccordionElement(string name, string text, AccordionControlElement[] accordionElementChildren)
			{
				AccordionControlElement accordionControlElement = this.AddAccordionElement(name, text, ElementStyle.Group);
				if (accordionElementChildren != null && accordionElementChildren.Length > 0)
					accordionControlElement.Elements.AddRange(accordionElementChildren);
				return accordionControlElement;
			}

			/// <summary>
			/// Adds the accordion element.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <param name="text">The text.</param>
			/// <param name="style">The style.</param>
			/// <param name="accordionElementParent">The accordion element parent.</param>
			/// <returns>AccordionControlElement.</returns>
			public AccordionControlElement AddAccordionElement(string name, string text, ElementStyle style = ElementStyle.Item, AccordionControlElement accordionElementParent = null)
			{
				AccordionControlElement accordionControlElement = new();
				accordionControlElement.Name = name;
				accordionControlElement.Text = text;
				accordionControlElement.Style = style;
				if (accordionElementParent == null)
					this.AccordionControl.Elements.AddRange(new AccordionControlElement[] { accordionControlElement });
				else
					accordionElementParent.Elements.AddRange(new AccordionControlElement[] { accordionControlElement });
				return accordionControlElement;
			}

			/// <summary>
			/// Ends the initialize.
			/// </summary>
			internal void EndInit() => this.AccordionControl.EndInit();
		}


		#endregion

	}
}
