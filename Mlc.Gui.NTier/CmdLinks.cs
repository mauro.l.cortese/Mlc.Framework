﻿// ***********************************************************************
// Assembly         : CommandsHandler
// Author           : Cortese Mauro Luigi
// Created          : 01-23-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-23-2016
// ***********************************************************************
// <copyright file="CmdLinks.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Class CmdLinks.
    /// </summary>
    internal class CmdLinks : Dictionary<int, CmdLink>
    {


    }
}
