﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : Cortese Mauro Luigi
// Created          : 01-26-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-26-2016
// ***********************************************************************
// <copyright file="MnuItems.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;


namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Class MnuItems.
    /// </summary>
    public class MnuItems : Dictionary<Component, MnuItem>
    {
        /// <summary>
        /// Adds the specified mnu item.
        /// </summary>
        /// <param name="mnuItem">The mnu item.</param>
        public void Add(MnuItem mnuItem)
        {
            base.Add(mnuItem.Item, mnuItem);
        }

        /// <summary>
        /// Gets the components.
        /// </summary>
        /// <returns>List&lt;Component&gt;.</returns>
        internal List<Component> GetComponents()
        {
            return this.Keys.ToList();
        }
    }
}
