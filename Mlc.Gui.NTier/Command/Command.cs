﻿// ***********************************************************************
// Assembly         : CommandsHandler
// Author           : Cortese Mauro Luigi
// Created          : 01-22-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-25-2016
// ***********************************************************************
// <copyright file="Cmd.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Shell;
using System;
using System.Text;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class Cmd.
	/// </summary>
	public class Command : ICommand
	{

		#region Constants
		#endregion

		#region Enumerations
		public enum CommandStates
		{
			Ready,
			Running,
			Stopped,
			Completed,
			StoppedForException,
		}
		#endregion

		#region Fields
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Command" /> class.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="index">The index.</param>
		internal Command(string key, int index)
		{
			this.Key = key;
			this.Index = index;
			this.Data.Command = this;
		}

		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets the command list.
		/// </summary>
		/// <value>The command list.</value>
		public Commands CommandCollection { get; internal set; }

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		public string Key { get; set; }


		/// <summary>
		/// Gets or sets the index.
		/// </summary>
		/// <value>The index.</value>
		public int Index { get; set; }

		/// <summary>
		/// Gets or sets the data.
		/// </summary>
		/// <value>The data.</value>
		public CommandData Data { get; set; } = new CommandData();

		/// <summary>
		/// The can be excute
		/// </summary>
		private bool canBeExcute = false;
		/// <summary>
		/// Gets or sets a value indicating whether this instance can be excute.
		/// </summary>
		/// <value><c>true</c> if this instance can be excute; otherwise, <c>false</c>.</value>
		public bool CanBeExecute
		{
			get { return this.canBeExcute; }
			set
			{
				this.canBeExcute = value;
				//this.Data.ResetTime();
			}
		}

		/// <summary>
		/// Gets or sets the section.
		/// </summary>
		/// <value>The section.</value>
		public string Section { get; set; }

		/// <summary>
		/// Gets or sets the caption.
		/// </summary>
		/// <value>The caption.</value>
		public string Caption { get; set; }

		/// <summary>
		/// Gets or sets the command links list.
		/// </summary>
		/// <value>The command links list.</value>
		public CommandLinksList CommandLinksList { get; set; } = new CommandLinksList();

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The description.</value>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the tool tip.
		/// </summary>
		/// <value>The tool tip.</value>
		public string ToolTip { get; set; }
		/// <summary>
		/// Gets or sets the hint.
		/// </summary>
		/// <value>The hint.</value>
		public string Hint { get; set; }
		/// <summary>
		/// Gets or sets the new form.
		/// </summary>
		/// <value>The new form.</value>
		public ICommandHdlrFrm NewForm { get; set; }

		/// <summary>
		/// Gets the command HDLR.
		/// </summary>
		/// <value>The command HDLR.</value>
		public CommandsHdlr CmdHdlr { get; internal set; }

		/// <summary>
		/// Gets the log message.
		/// </summary>
		/// <value>The log message.</value>
		public StringBuilder LogMessage { get; } = new();

		/// <summary>
		/// Gets a value indicating whether [result success].
		/// </summary>
		/// <value><c>true</c> if [result success]; otherwise, <c>false</c>.</value>
		public bool ResultSuccess { get; internal set; } = false;

		/// <summary>
		/// Gets the tag.
		/// </summary>
		/// <value>The tag.</value>
		public Enum Tag { get; internal set; }

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		public CommandStates State { get; set; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>A <see cref="System.String" /> that represents this instance.</returns>
		public override string ToString()
		{
			return this.Caption;
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		internal void Init()
		{
			this.CanBeExecute = true;
			this.ResultSuccess = false;
			this.State = CommandStates.Ready;
			this.Data.BeginExecuteCommand = DateTime.Now;
			this.LogMessage.Clear();
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}
}
