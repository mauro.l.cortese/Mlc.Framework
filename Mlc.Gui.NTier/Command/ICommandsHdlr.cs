﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : mauro.luigi.cortese
// Created          : 04-11-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 04-11-2019
// ***********************************************************************
// <copyright file="ICommandsHdlr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Utils.Menu;
using System;
using System.Collections.Generic;
using System.Data;
using Mlc.Shell.IO;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Interface ICommandsHdlr
	/// </summary>
	public interface ICommandsHdlr
	{
		/// <summary>
		/// Gets the log level.
		/// </summary>
		/// <value>The log level.</value>
		LogLevel LogLevel { get; }

		/// <summary>
		/// Gets or sets a value indicating whether [switch alignment].
		/// </summary>
		/// <value><c>true</c> if [switch alignment]; otherwise, <c>false</c>.</value>
		bool SwitchAlignment { get; set; }

		/// <summary>
		/// Clears all logs.
		/// </summary>
		void ClearAllLogs();

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		void PerformCommand(Command cmd);

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		void PerformCommand(Enum cmd);

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="cmd">The command.</param>
		void PerformCommand(ICommandHdlrFrm form, Command cmd);

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="cmd">The command.</param>
		void PerformCommand(ICommandHdlrFrm form, Enum cmd);

		/// <summary>
		/// Performs the command for all form.
		/// </summary>
		/// <param name="cmd">The command.</param>
		void PerformCommandForAllForm(Command cmd);

		/// <summary>
		/// Adds the mnu items.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="commandLinksList">The command links list.</param>
		void AddMnuItems(ICommandHdlrFrm form, CommandLinksList commandLinksList);

		/// <summary>
		/// Adds the mnu items.
		/// </summary>
		/// <param name="forms">The forms.</param>
		/// <param name="commandLinksList">The command links list.</param>
		void AddMnuItems(List<ICommandHdlrFrm> forms, CommandLinksList commandLinksList);

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		/// <returns>Command.</returns>
		Command GetCommand(Enum cmd);

		/// <summary>
		/// Occurs when [bar edit item value changed].
		/// </summary>
		event EventHandler BarEditItemValueChanged;

		/// <summary>
		/// Adds the tree list menu item.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="commandLinks">The command links lists.</param>
		DXMenuItem AddTreeListMenuItem(ICommandHdlrFrm form, CommandLinks commandLinks);
	}
}