﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Class PaginalDataMgr.
    /// </summary>
    public class SqlPageDataMgr
    {

        #region Private Constants
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        /// <summary>
        /// The maximum rows
        /// </summary>
        private int maxRows = 500;
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlPageDataMgr" /> class.
        /// </summary>
        /// <param name="maxRows">The maximum rows.</param>
        public SqlPageDataMgr(int maxRows)
            : this(maxRows, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlPageDataMgr" /> class.
        /// </summary>
        /// <param name="maxRows">The maximum rows.</param>
        /// <param name="totalRows">The total rows.</param>
        public SqlPageDataMgr(int maxRows, int totalRows)
        {
            this.Fields = new NTier.FieldOnSearchCollection();
            this.Reset(maxRows, totalRows);
            this.maxRows = maxRows;
            this.limit = new Range
            {
                From = 1,
                To = this.maxRows,
                TotalRows = totalRows
            };
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The limit from to
        /// </summary>
        private Range limit;
        /// <summary>
        /// Gets or sets the limit from to.
        /// </summary>
        /// <value>The limit from to.</value>
        public Range Limit { get { return this.limit; } set { this.limit = value; } }

        /// <summary>
        /// Gets or sets the text tosearch.
        /// </summary>
        /// <value>The text tosearch.</value>
        public String TextFilter { get; set; }

        /// <summary>
        /// Gets a value indicating whether [show next page].
        /// </summary>
        /// <value><c>true</c> if [show next page]; otherwise, <c>false</c>.</value>
        public bool ShowNextPage { get { return this.Limit.Until < this.Limit.TotalRows; } }

        /// <summary>
        /// Gets a value indicating whether [show previous page].
        /// </summary>
        /// <value><c>true</c> if [show previous page]; otherwise, <c>false</c>.</value>
        public bool ShowPreviousPage { get { return this.Limit.From > 1; } }

        /// <summary>
        /// The where
        /// </summary>
        private string where = string.Empty;
        /// <summary>
        /// Gets or sets the where.
        /// </summary>
        /// <value>The where.</value>
        public string Where { get { return this.where; } set { this.where = value; } }

        /// <summary>
        /// The select
        /// </summary>
        private string select;
        /// <summary>
        /// Gets or sets the select.
        /// </summary>
        /// <value>The select.</value>
        public string Select
        {
            get
            {
                return select
                  .Replace("<$FROM>", this.Limit.From.ToString())
                  .Replace("<$UNTIL>", this.Limit.Until.ToString())
                  .Replace("<$WHERE>", this.where);
            }
            set
            {
                select = value;
                //this.Reset();
            }
        }

        public FieldOnSearchCollection Fields { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SqlPageDataMgr"/> is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Resets this instance.
        /// </summary>
        /// <param name="maxRows">The maximum rows.</param>
        /// <param name="totalRows">The total rows.</param>
        public void Reset(int maxRows, int totalRows)
        {
            this.maxRows = maxRows;
            this.limit = new Range
            {
                From = 1,
                To = this.maxRows,
                TotalRows = totalRows
            };
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this.Reset(this.Limit.To, 0);
        }

        /// <summary>
        /// Nexts the page.
        /// </summary>
        public bool NextPage() { return this.limit.Next(); }

        /// <summary>
        /// Previouses the page.
        /// </summary>
        public bool PreviousPage() { return this.limit.Previous(); }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return string.Format("From:{0} - To:{1} - Until:{2} - TotalRows:{3}",
                this.Limit.From,
                this.Limit.To,
                this.Limit.Until,
                this.Limit.TotalRows
                );
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        /// <summary>
        /// Class Range.
        /// </summary>
        public class Range
        {
            /// <summary>
            /// From
            /// </summary>
            public int From { get; set; }

            /// <summary>
            /// To
            /// </summary>
            public int To { get; set; }

            /// <summary>
            /// Gets or sets the until.
            /// </summary>
            /// <value>The until.</value>
            public int Until { get { return this.From + this.To - 1; } }

            /// <summary>
            /// The total rows
            /// </summary>
            public int TotalRows { get; set; }

            /// <summary>
            /// Nexts this instance.
            /// </summary>
            internal bool Next()
            {
                bool retVal = false;
                if (this.From + this.To < this.TotalRows)
                {
                    this.From += this.To;
                    retVal = true;
                }
                return retVal;
            }

            /// <summary>
            /// Previouses this instance.
            /// </summary>
            internal bool Previous()
            {
                bool retVal = false;
                if (this.From > 1)
                {
                    this.From -= this.To;
                    if (this.From < 1)
                        this.From = 1;
                    retVal = true;
                }
                return retVal;
            }
        }
        #endregion
    }

    /// <summary>
    /// Class FieldOnSearchCollection.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.NTier.FieldOnSearch}" />
    public class FieldOnSearchCollection : Dictionary<string, FieldOnSearch>
    {
        /// <summary>
        /// Adds the specified field on serach.
        /// </summary>
        /// <param name="fieldOnSerach">The field on serach.</param>
        public void Add(FieldOnSearch fieldOnSerach)
        {
            this.Add(fieldOnSerach.Name, fieldOnSerach);
        }
    }

    /// <summary>
    /// Class FieldOnSearch.
    /// </summary>
    public class FieldOnSearch
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldOnSearch"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public FieldOnSearch(string name, string aliasTable)
        {
            this.Name = name;
            this.Sql = string.Format("{0} {1}.{2} LIKE '{3}'\r\n", "{0}", aliasTable, name, "%{1}%");
        }
        
        /// <summary>
        /// Gets or sets the checked.
        /// </summary>
        /// <value>The checked.</value>
        public CheckState Checked { get; set; }
        
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the SQL.
        /// </summary>
        /// <value>The SQL.</value>
        public string Sql { get; set; }
    }
}