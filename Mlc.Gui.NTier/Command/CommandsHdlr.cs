﻿// ***********************************************************************
// Assembly         : CommandsHandler
// Author           : Cortese Mauro Luigi
// Created          : 01-22-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-22-2016
// ***********************************************************************
// <copyright file="CmdHdlr.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Utils.Menu;
using System.Data;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Classe Singleton pattern
	/// </summary>
	public class CommandsHdlr : SingletonBase<CommandsHdlr>, ICommandsHdlr
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		private List<ICommandHdlrFrm> iCommandHdlrFrmList = new List<ICommandHdlrFrm>();
		#endregion

		#region Constructors
		/// <summary>
		/// Prevents a default instance of the <see cref="CommandsHdlr"/> class from being created.
		/// </summary>
		private CommandsHdlr()
		{
			this.commands.cmdHdlr = this;
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The command dictionary
		/// </summary>
		internal Commands commands = new Commands();

		/// <summary>
		/// The execute command
		/// </summary>
		private ExecuteCommandDelegate commandDelegate = null;

		/// <summary>
		/// Sets the execute command delegate method.
		/// </summary>
		/// <value>The execute command delegate method.</value>
		public ExecuteCommandDelegate CommandDelegate { set { this.commandDelegate = value; } }

		private ICommandHdlrFrm mainForm;
		/// <summary>
		/// Gets or sets the main form.
		/// </summary>
		/// <value>The main form.</value>
		public ICommandHdlrFrm MainForm
		{
			get { return this.mainForm; }
			set
			{
				this.mainForm = value;
				if (this.iCommandHdlrFrmList.Contains(this.mainForm))
					this.iCommandHdlrFrmList.Remove(this.mainForm);
				this.iCommandHdlrFrmList.Add(this.mainForm);
			}
		}

		/// <summary>
		/// Gets the loggers.
		/// </summary>
		/// <value>The loggers.</value>
		public LoggerCollection Loggers { get; set; } = new LoggerCollection();

		/// <summary>
		/// Gets or sets the logger.
		/// </summary>
		/// <value>The logger.</value>
		public Logger20 Logger { get; set; } = Logger20.GetInstance();

		/// <summary>
		/// Gets the log level.
		/// </summary>
		/// <value>The log level.</value>
		public LogLevel LogLevel => this.Loggers.LogLevel;

		/// <summary>
		/// Gets or sets a value indicating whether [switch alignment].
		/// </summary>
		/// <value><c>true</c> if [switch alignment]; otherwise, <c>false</c>.</value>
		public bool SwitchAlignment { get; set; } = false;
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Adds the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		public void AddCommand(Enum cmd)
		{
			this.commands.Add(cmd);
		}

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		public Command GetCommand(Enum cmd)
		{
			Command retVal = null;
			int cmdIndex = Convert.ToInt32(cmd);
			if (this.commands.ContainsKey(cmdIndex))
				retVal = this.commands[cmdIndex];
			return retVal;
		}

		/// <summary>
		/// Adds the mnu items.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="commandLinksList">The command links list.</param>
		public void AddMnuItems(ICommandHdlrFrm form, CommandLinksList commandLinksList)
		{
			this.AddMnuItems(new List<ICommandHdlrFrm>() { form }, commandLinksList);
		}


		/// <summary>
		/// Adds the mnu items.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="commandLinksList">The command links list.</param>
		public void AddMnuItems(List<ICommandHdlrFrm> forms, CommandLinksList commandLinksList)
		{

			foreach (ICommandHdlrFrm form in forms)
				if (!this.iCommandHdlrFrmList.Contains(form))
					this.iCommandHdlrFrmList.Add(form);

			foreach (CommandLinks commandLinks in commandLinksList)
			{
				if (this.commands.ContainsKey(commandLinks.EnumCommandKey))
				{
					commandLinks.Forms = forms;
					Command cmd = this.commands[commandLinks.EnumCommandKey];
					cmd.CommandLinksList.Add(commandLinks);
				}
			}
		}



		/// <summary>
		/// Adds the tree list menu item.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="commandLinks">The command links lists.</param>
		/// <returns>DXMenuItem.</returns>
		public DXMenuItem AddTreeListMenuItem(ICommandHdlrFrm form, CommandLinks commandLinks)
		{
			if (!this.iCommandHdlrFrmList.Contains(form))
				this.iCommandHdlrFrmList.Add(form);

			if (this.commands.ContainsKey(commandLinks.EnumCommandKey))
			{

				//commandLinks.Form = form;
				Command cmd = this.commands[commandLinks.EnumCommandKey];
				cmd.CommandLinksList.Add(commandLinks);
			}
			return commandLinks.DXMenuItem;
		}


		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		public void PerformCommand(Enum cmd)
		{
			this.PerformCommand(this.MainForm, cmd);
		}

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="cmd">The command.</param>
		public void PerformCommand(ICommandHdlrFrm form, Enum cmd)
		{
			try
			{
				ICommandHdlrFrm tmpForm = (form == null) ? this.MainForm : form;
				this.RunCommandJob(this.commands[Convert.ToInt32(cmd)], tmpForm, null);
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		public void PerformCommand(Command cmd)
		{
			this.RunCommandJob(cmd, this.MainForm, null);
		}

		/// <summary>
		/// Performs the command.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="cmd">The command.</param>
		public void PerformCommand(ICommandHdlrFrm form, Command cmd)
		{
			this.RunCommandJob(cmd, form, null);
		}

		/// <summary>
		/// Performs the command for all form.
		/// </summary>
		/// <param name="cmd">The command.</param>
		public void PerformCommandForAllForm(Command cmd)
		{
			try
			{
				foreach (ICommandHdlrFrm form in this.iCommandHdlrFrmList)
				{
					cmd.CanBeExecute = true;
					cmd.Data.BeginExecuteCommand = DateTime.Now;
					cmd.Data.Form = form;
					cmd.Data.Component = null;
					if (form != null)
						form.ProcessCommandResult(ref cmd, null);
					cmd.Data.EndExecuteCommand = DateTime.Now;
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			finally { this.writeLog(cmd); }
		}

		/// <summary>
		/// Clears all logs.
		/// </summary>
		public void ClearAllLogs()
		{
			foreach (Logger logger in this.Loggers.Values)
				logger.Clear();
		}

		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Bars the item command clicked.
		/// </summary>
		/// <param name="cmd">The command.</param>
		/// <param name="form">The form.</param>
		/// <param name="component">The component.</param>
		/// <param name="requestData">if set to <c>true</c> [request data].</param>
		internal void RunCommandJob(Command cmd, ICommandHdlrFrm form, Component component = null)
		{
			try
			{
				cmd.Init();
				cmd.Data.Form = form;
				cmd.Data.Component = component;
				Logger20.Log log = this.Logger.DefaultLog;
				log?.NewLine('=');
				log?.Info(cmd);
				log?.NewLine('=');

				cmd.State = Command.CommandStates.Running;
				if (form != null)
				{
					log?.Info($"Data request for command execution: {cmd.Key} on: {form.Name}");
					form.GetCommandData(ref cmd, component);
					log?.Info("Acquired data");
				}

				if (cmd.State != Command.CommandStates.Completed)
				{
					if (cmd.CanBeExecute && this.commandDelegate != null)
					{
						log?.Info("Transfer of command execution to the delegate method");
						this.commandDelegate(cmd);
						cmd.ResultSuccess = true;
						log?.Info("Executed command");
					}

					if (form != null)
					{
						log?.Info($"Process the result of: {cmd.Key} on: {form.Name}");
						form.ProcessCommandResult(ref cmd, component);
						log?.Info("Elaborate result");
					}

					if (cmd.NewForm != null)
						this.iCommandHdlrFrmList.Add(cmd.NewForm);

				}

				cmd.Data.EndExecuteCommand = DateTime.Now;
				cmd.State = Command.CommandStates.Completed;

			}
			catch (Exception ex)
			{
				cmd.State = Command.CommandStates.StoppedForException;
				ExceptionsRegistry.GetInstance().Add(ex);
			}
			finally { this.writeLog(cmd); }
		}
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Writes the log.
		/// </summary>
		/// <param name="cmd">The command.</param>
		private void writeLog(Command cmd)
		{
			try
			{
				this.Loggers.Write(new Log(LogLevel.Debug, LogType.Global)
				{
					CmdName = cmd != null ? cmd.Key : "",
					Description = cmd.Description,
					StartTime = cmd.Data.BeginExecuteCommand,
					EndTime = cmd.Data.EndExecuteCommand,
					Message = cmd.LogMessage.ToString(),
				});


				Logger20.Log log = this.Logger.DefaultLog;

				if (log != null)
				{
					// TODO: Completare log del comando
					/*
					if (Logger20.CurrentLogLevel == Logger20.Level.Debug)
						log.Debug(cmd);
					else
						log.Info(cmd);
					*/
					log.NewLine('-');
					log.NewLine();
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}
		#endregion
		#endregion

		#region Event Handlers

		#endregion

		#region Event Definitions


		#region Event BarEditItemValueChanged                
		/// <summary>
		/// Event BarEditItemValueChanged.
		/// </summary>
		public event EventHandler BarEditItemValueChanged;

		/// <summary>
		/// Handles the <see cref="E:BarEditItemValueChanged" /> event.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		internal void OnBarEditItemValueChanged(object sender, EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.BarEditItemValueChanged?.Invoke(sender, e);
		}
		#endregion

		#endregion

		#region Embedded Types
		#endregion
	}
}

