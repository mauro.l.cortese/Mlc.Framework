﻿using System;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class CmdEnumAttribute.
	/// Implements the <see cref="System.Attribute" />
	/// </summary>
	/// <seealso cref="System.Attribute" />
	public class CmdPropertiesAttribute : Attribute
	{
		/// <summary>
		/// Gets or sets the caption.
		/// </summary>
		/// <value>The caption.</value>
		public string Caption { get; set; }
		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The description.</value>
		public string Description { get; set; }
		/// <summary>
		/// Gets or sets the tip.
		/// </summary>
		/// <value>The tip.</value>
		public string ToolTip { get; set; }
		/// <summary>
		/// Gets or sets the section.
		/// </summary>
		/// <value>The section.</value>
		public string Section { get; set; }

	}
}
