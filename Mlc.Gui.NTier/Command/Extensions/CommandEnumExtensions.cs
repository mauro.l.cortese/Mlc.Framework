﻿using System;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Form more info https://stackoverflow.com/questions/1799370/getting-attributes-of-enums-value
	/// </summary>
	public static class CommandEnumExtensions
	{
		/// <summary>
		/// This method creates a specific call to the above method, requesting the
		/// Description MetaData attribute.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static CmdPropertiesAttribute CmdProperties(this Enum value)
		{
			var attribute = value.getAttribute<CmdPropertiesAttribute>();
			return attribute;
		}

		/// <summary>
		/// This extension method is broken out so you can use a similar pattern with 
		/// other MetaData elements in the future. This is your base method for each.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>
		private static T getAttribute<T>(this Enum value) where T : CmdPropertiesAttribute
		{
			var type = value.GetType();
			var memberInfo = type.GetMember(value.ToString());
			var attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);
			return attributes.Length > 0 ? (T)attributes[0] : null;
		}
	}
}
