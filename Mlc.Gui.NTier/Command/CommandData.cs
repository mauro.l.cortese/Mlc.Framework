﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : Cortese Mauro Luigi
// Created          : 11-30-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-12-2017
// ***********************************************************************
// <copyright file="CommandData.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class CommandDataExtension.
	/// </summary>
	public static class CommandDataExtension
	{
		/// <summary>
		/// Copies the base data.
		/// </summary>
		/// <param name="cmData">The cm data.</param>
		/// <param name="cmDataSource">The cm data source.</param>
		/// <returns>CommandData.</returns>
		public static CommandData CopyBaseData(this CommandData cmData, CommandData cmDataSource)
		{
			cmData.BeginExecuteCommand = cmDataSource.BeginExecuteCommand;
			cmData.Checked = cmDataSource.Checked;
			cmData.Command = cmDataSource.Command;
			cmData.Component = cmDataSource.Component;
			cmData.DialogMessage = cmDataSource.DialogMessage;
			cmData.EndExecuteCommand = cmDataSource.EndExecuteCommand;
			cmData.Form = cmDataSource.Form;
			cmData.Note = cmDataSource.Note;
			cmData.PathIO = cmDataSource.PathIO;
			cmData.ProgressHandler = cmDataSource.ProgressHandler;
			cmData.ResultTable = cmDataSource.ResultTable;
			cmData.Sender = cmDataSource.Sender;
			cmData.Value = cmDataSource.Value;
			return cmData;
		}
	}

	/// <summary>
	/// Class CommandData.
	/// </summary>
	public class CommandData
	{
		#region Private Constants        
		#endregion

		#region Private Enumerations
		#endregion

		#region Private Static Fields
		#endregion

		#region Private Fields
		#endregion

		#region Public Constructors
		#endregion

		#region Public Properties
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public object Value { get; set; }

		/// <summary>
		/// Gets or sets the note.
		/// </summary>
		/// <value>The note.</value>
		public string Note { get; set; }

		/// <summary>
		/// Gets or sets the result table.
		/// </summary>
		/// <value>The result table.</value>
		public virtual DataTable ResultTable { get; set; }

		/// <summary>
		/// Gets or sets the form.
		/// </summary>
		/// <value>The form.</value>
		public ICommandHdlrFrm Form { get; set; }

		/// <summary>
		/// Gets or sets the component.
		/// </summary>
		/// <value>The component.</value>
		public Component Component { get; set; }

		/// <summary>
		/// Gets the end execute command.
		/// </summary>
		/// <value>The end execute command.</value>
		public DateTime EndExecuteCommand { get; internal set; }

		/// <summary>
		/// Gets the spent time.
		/// </summary>
		/// <value>The spent time.</value>
		public TimeSpan SpentTime { get { return this.EndExecuteCommand - this.BeginExecuteCommand; } }

		/// <summary>
		/// Gets the begin execute command.
		/// </summary>
		/// <value>The begin execute command.</value>
		public DateTime BeginExecuteCommand { get; internal set; }

		/// <summary>
		/// Gets or sets the path io.
		/// </summary>
		/// <value>The path io.</value>
		public string PathIO { get; set; }

		/// <summary>
		/// Gets the sender.
		/// </summary>
		/// <value>The sender.</value>
		public object Sender { get; internal set; }

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <value>The command.</value>
		public Command Command { get; internal set; }

		/// <summary>
		/// Gets or sets the log message.
		/// </summary>
		/// <value>The log message.</value>
		public DialogMessage DialogMessage { get; set; } = new DialogMessage();

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="CommandData"/> is checked.
		/// </summary>
		/// <value><c>true</c> if checked; otherwise, <c>false</c>.</value>
		public bool Checked { get; set; } = false;

		private CommandProgressHandler progressHandler;
		/// <summary>
		/// Gets or sets the progress handler.
		/// </summary>
		/// <value>The progress handler.</value>
		public CommandProgressHandler ProgressHandler
		{
			get => this.progressHandler; set
			{
				this.progressHandler = value;
				if (this.progressHandler != null)
					this.progressHandler.Command = this.Command;
			}
		}

		/// <summary>
		/// Gets the key.
		/// </summary>
		/// <value>The key.</value>
		public string Key
		{
			get
			{
				string retVal = string.Empty;
				if (this.Form != null && this.Form is ContainerControl control)
					retVal = $"{control.ParentForm.Name}\\{this.Form.Name}";
				return retVal;
			}
		}

		#endregion

		#region Public Static Method
		#endregion

		#region Public Method
		#endregion

		#region Public Event Handlers - CommandsEngine
		#endregion

		#region Private Event Handlers
		#endregion

		#region Private Method
		/// <summary>
		/// Resets the time.
		/// </summary>
		internal void ResetTime()
		{
			this.BeginExecuteCommand = default(DateTime);
			this.EndExecuteCommand = default(DateTime);
		}
		#endregion

		#region Event
		#endregion

		#region Nested Types
		#endregion
	}
}
