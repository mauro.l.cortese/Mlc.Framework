﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : Cortese Mauro Luigi
// Created          : 01-26-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-26-2016
// ***********************************************************************
// <copyright file="Class1.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Navigation;
using DevExpress.Utils.Menu;

namespace Mlc.Gui.NTier
{

	/// <summary>
	/// Class CommandLinks.
	/// </summary>
	public class CommandLinks
	{


		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The command HDLR
		/// </summary>
		private CommandsHdlr cmdHdlr = CommandsHdlr.GetInstance();
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CommandLinks"/> class.
		/// </summary>
		/// <param name="enumKey">The enum key.</param>
		/// <param name="mnuComponents">The mnu components.</param>
		public CommandLinks(Enum enumKey, params Component[] mnuComponents)
			: this(true, mnuComponents)
		{
			this.EnumCommandKey = Convert.ToInt32(enumKey);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CommandLinks"/> class.
		/// </summary>
		/// <param name="mnuComponents">The mnu components.</param>
		public CommandLinks(bool withEvent, params Component[] mnuComponents)
		{
			this.Components = mnuComponents;
			if (withEvent)
				foreach (Component comp in this.Components)
				{
					if (comp is BarCheckItem barCheckItem)
						barCheckItem.CheckedChanged += cmdLink_CheckedChanged;
					else if (comp is BarEditItem barEditItem)
						barEditItem.EditValueChanged += cmdLink_EditValueChanged;
					else if (comp is BarItem barItem)
						barItem.ItemClick += this.barItemCommand_ItemClick;
					else if (comp is SimpleButton simpleButton)
						simpleButton.Click += this.cmdLink_Click;
					else if (comp is Button button)
						button.Click += this.cmdLink_Click;
					else if (comp is ToolStripItem toolStripItem)
						toolStripItem.Click += this.cmdLink_Click;
					else if (comp is AccordionControlElement accordionControlElement)
						accordionControlElement.Click += this.cmdLink_Click;
				}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CommandLinks"/> class.
		/// </summary>
		/// <param name="enumKey">The enum key.</param>
		/// <param name="dXMenuItem">The d x menu item.</param>
		public CommandLinks(Enum enumKey, DXMenuItem dXMenuItem)
		{
			this.EnumCommandKey = Convert.ToInt32(enumKey);
			this.DXMenuItem = dXMenuItem;
			this.DXMenuItem.Click -= this.cmdLink_Click;
			this.DXMenuItem.Click += this.cmdLink_Click;
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the enum key.
		/// </summary>
		/// <value>The enum key.</value>
		public int EnumCommandKey { get; set; }

		/// <summary>
		/// Gets the dx menu item.
		/// </summary>
		/// <value>The dx menu item.</value>
		public DXMenuItem DXMenuItem { get; }

		/// <summary>
		/// Gets or sets the form.
		/// </summary>
		/// <value>The form.</value>
		public List<ICommandHdlrFrm> Forms { get; set; }

		/// <summary>
		/// Gets or sets the components.
		/// </summary>
		/// <value>The components.</value>
		public Component[] Components { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [ignore move].
		/// </summary>
		/// <value><c>true</c> if [ignore move]; otherwise, <c>false</c>.</value>
		public bool IgnoreMove { get; set; } = false;
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Event Handlers
		/// <summary>
		/// Handles the ItemClick event of the barItemCommand control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ItemClickEventArgs"/> instance containing the event data.</param>
		private void barItemCommand_ItemClick(object sender, ItemClickEventArgs e)
		{
			if (cmdHdlr.SwitchAlignment && !IgnoreMove)
			{
				if (e.Item != null)
					e.Item.Alignment = e.Item.Alignment == BarItemLinkAlignment.Left ? BarItemLinkAlignment.Right : BarItemLinkAlignment.Left;
			}
			else
			{
				Command cmd = cmdHdlr.commands[this.EnumCommandKey];
				cmd.Data.Sender = sender;
				foreach (ICommandHdlrFrm form in this.Forms)
					cmdHdlr.RunCommandJob(cmdHdlr.commands[this.EnumCommandKey], form, e.Item);
			}
		}

		/// <summary>
		/// Handles the Click event of the cmdLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void cmdLink_Click(object sender, EventArgs e)
		{
			Command cmd = cmdHdlr.commands[this.EnumCommandKey];
			cmd.Data.Sender = sender;

			if (sender is Component component)
				foreach (ICommandHdlrFrm form in this.Forms)

					cmdHdlr.RunCommandJob(cmdHdlr.commands[this.EnumCommandKey], form, component);
			else
				foreach (ICommandHdlrFrm form in this.Forms)
					cmdHdlr.RunCommandJob(cmdHdlr.commands[this.EnumCommandKey], form);
		}

		/// <summary>
		/// Handles the CheckedChanged event of the cmdLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ItemClickEventArgs"/> instance containing the event data.</param>
		/// <exception cref="NotImplementedException"></exception>
		private void cmdLink_CheckedChanged(object sender, ItemClickEventArgs e)
		{
			if (cmdHdlr.SwitchAlignment && !IgnoreMove)
			{

				if (e.Item != null)
					e.Item.Alignment = e.Item.Alignment == BarItemLinkAlignment.Left ? BarItemLinkAlignment.Right : BarItemLinkAlignment.Left;
				((BarCheckItem)sender).Checked = !((BarCheckItem)sender).Checked;
			}
			else
			{
				Command cmd = cmdHdlr.commands[this.EnumCommandKey];
				cmd.Data.Sender = sender;
				cmd.Data.Checked = ((BarCheckItem)sender).Checked;
				if (this.Forms != null)
					foreach (ICommandHdlrFrm form in this.Forms)
						cmdHdlr.RunCommandJob(cmdHdlr.commands[this.EnumCommandKey], form, (Component)sender);
			}
		}

		/// <summary>
		/// Handles the EditValueChanged event of the cmdLinks control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void cmdLink_EditValueChanged(object sender, EventArgs e)
		{


			if (cmdHdlr.SwitchAlignment && !IgnoreMove)
			{
				BarEditItem barItem = (BarEditItem)sender;
				barItem.Alignment = barItem.Alignment == BarItemLinkAlignment.Left ? BarItemLinkAlignment.Right : BarItemLinkAlignment.Left;

				if (barItem.EditValue is bool)
				{
					barItem.EditValueChanged -= cmdLink_EditValueChanged;
					barItem.EditValue = !(bool)barItem.EditValue;
					barItem.EditValueChanged += cmdLink_EditValueChanged;
				}
			}
			else
			{
				this.cmdHdlr.OnBarEditItemValueChanged(sender, e);
			}
		}


		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}

}
