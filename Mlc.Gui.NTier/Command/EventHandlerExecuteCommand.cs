﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Mlc.Shell.IO;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Delegate for event management.
	/// </summary>
	public delegate void EventHandlerExecuteCommand(object sender, EventArgsExecuteCommand e);

	/// <summary>
	/// BeforExecuteCommand arguments.
	/// </summary>
	public class EventArgsExecuteCommand : EventArgs
	{
		#region Constructors
		/// <summary>
		/// Initialized a new instance
		/// </summary>
		/// <param name="cmd"></param>
		public EventArgsExecuteCommand(Command cmd) => this.Cmd = cmd;
		#endregion

		#region Properties
		/// <summary>
		/// Get or set the property
		/// </summary>
		public Command Cmd { get; set; }
		#endregion
	}
}
