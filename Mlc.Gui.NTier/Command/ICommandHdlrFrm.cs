﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Mlc.Shell.IO;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Interface ICmdHdlrFrm
	/// </summary>
	public interface ICommandHdlrFrm
	{
		/// <summary>
		/// Gets the command data.
		/// </summary>
		/// <param name="cmd">The command.</param>
		/// <param name="component">The component.</param>
		void GetCommandData(ref Command cmd, Component component);

		/// <summary>
		/// Performs the command result.
		/// </summary>
		/// <param name="cmd">The command.</param>
		/// <param name="component">The component.</param>
		void ProcessCommandResult(ref Command cmd, Component component);

		/// <summary>
		/// Gets or sets the CMDS HDLR.
		/// </summary>
		/// <value>The CMDS HDLR.</value>
		ICommandsHdlr CmdsHdlr { get; set; }

		/// <summary>
		/// Gets or sets the CMDS HDLR.
		/// </summary>
		/// <value>The CMDS HDLR.</value>
		string Name { get; set; }

	}

}
