﻿// ***********************************************************************
// Assembly         : CommandsHandler
// Author           : Cortese Mauro Luigi
// Created          : 01-23-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-20-2017
// ***********************************************************************
// <copyright file="CmdLinks.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class CmdLinks.
	/// </summary>
	/// <seealso cref="System.Collections.Generic.Dictionary{System.Int32, Command}" />
	public class Commands : Dictionary<int, Command>
	{
		/// <summary>
		/// Adds the specified command.
		/// </summary>
		/// <param name="cmd">The command.</param>
		internal void Add(Enum cmd)
		{
			int cmdIndex = Convert.ToInt32(cmd);
			if (!this.ContainsKey(cmdIndex))
			{
				CmdPropertiesAttribute cpa = cmd.CmdProperties();

				Command newCmd = new Command(cmd.ToString(), cmdIndex)
				{
					Caption = cmd.ToString(),
					Tag = cmd,
					CommandCollection = this,
					CmdHdlr = this.cmdHdlr
				};

				if (cpa != null)
				{
					newCmd.Section = cpa.Section;
					newCmd.Caption = cpa.Caption;
					newCmd.Description = cpa.Description;
					newCmd.ToolTip = cpa.ToolTip;
				}

				this.Add(cmdIndex, newCmd);
			}
		}


		/// <summary>
		/// The command HDLR
		/// </summary>
		internal CommandsHdlr cmdHdlr;

		/// <summary>
		/// Gets the command HDLR.
		/// </summary>
		/// <value>The command HDLR.</value>
		public CommandsHdlr CmdHdlr { get { return cmdHdlr; } }
	}
}
