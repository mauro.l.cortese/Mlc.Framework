﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : Cortese Mauro Luigi
// Created          : 01-26-2016
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 01-26-2016
// ***********************************************************************
// <copyright file="Class1.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace Mlc.Gui.NTier
{
    /// <summary>
    /// For storage list of <see cref="CommandLinks"/>  instances
    /// </summary>
    public class CommandLinksList : List<CommandLinks>
    { }
}
