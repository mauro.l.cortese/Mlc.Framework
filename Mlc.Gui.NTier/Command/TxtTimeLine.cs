﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : mauro.luigi.cortese
// Created          : 04-11-2019
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 04-11-2019
// ***********************************************************************
// <copyright file="ICommandsHdlr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using Mlc.Shell.IO;

namespace Mlc.Gui.NTier
{
    /// <summary>
    /// Interface ICommandsHdlr
    /// </summary>
    public static class TxtTimeLine
    {
        /// <summary>
        /// Logs the line.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String.</returns>
        public static string LogLine(this string text)
        {
            return string.Format("{0} ⇒ {1}", DateTime.Now.ToString("HH:mm:ss.fffffff"), text);
        }

        /// <summary>
        /// Logs the line.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.String.</returns>
        public static string LogLine(this string text, params string[] pars)
        {
            return string.Format(text, pars).LogLine();
        }
    }
}