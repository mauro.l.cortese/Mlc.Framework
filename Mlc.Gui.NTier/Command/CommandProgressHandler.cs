﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : Cortese Mauro Luigi
// Created          : 11-30-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-12-2017
// ***********************************************************************
// <copyright file="CommandData.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraEditors.Repository;
using Mlc.Shell;
using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class CommandData.
	/// </summary>
	public class CommandProgressHandler
	{

		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		#endregion

		#region Constructors
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties

		/// <summary>The total step</summary>
		private int totalStep = 0;
		/// <summary>
		/// Gets the total step.
		/// </summary>
		/// <value>The total step.</value>
		public int TotalStep
		{
			get => this.totalStep; set
			{
				this.totalStep = value;
				this.CurrentStep = 0;
				this.OnTotalStepChanged(new EventArgs());
			}

		}

		/// <summary>
		/// Gets or sets the current step.
		/// </summary>
		/// <value>The current step.</value>
		public int CurrentStep { get; set; }

		/// <summary>
		/// Gets the message.
		/// </summary>
		/// <value>The message.</value>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the progress caption.
		/// </summary>
		/// <value>The progress caption.</value>
		public string ProgressCaption { get; set; } = "Run";

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <value>The command.</value>
		public Command Command { get; internal set; }
		#endregion

		#region Method Static
		#endregion

		#region Method		
		/// <summary>
		/// Resets this instance.
		/// </summary>
		public void Reset()
		{
			this.CurrentStep = 0;
			this.TotalStep = 0;
		}

		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#region Event BeforeProcess                
		/// <summary>
		/// Event BeforeProcess.
		/// </summary>
		public event EventHandler BeforeProcess;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnBeforeProcess(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.BeforeProcess?.Invoke(this, e);
		}

		/// <summary>
		/// Raises the end process.
		/// </summary>
		public void RaiseBeforeProcess() => this.OnBeforeProcess(new EventArgs());
		#endregion

		#region Event EndProcess
		/// <summary>
		/// Event EndProcess.
		/// </summary>
		public event EventHandler EndProcess;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnEndProcess(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.EndProcess?.Invoke(this, e);
		}

		/// <summary>
		/// Raises the end process.
		/// </summary>
		public void RaiseEndProcess()
		{
			this.OnEndProcess(new EventArgs());
			this.Reset();
		}
		#endregion

		#region Event TotalStepChanged
		/// <summary>
		/// Event OnTotalStepChanged.
		/// </summary>
		public event EventHandler TotalStepChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnTotalStepChanged(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.TotalStepChanged?.Invoke(this, e);
		}
		#endregion


		#region Event Step
		#region Before Step
		/// <summary>
		/// Event StepCompleted.
		/// </summary>
		public event EventHandlerStepData BeforeStep;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnBeforeStep(EventArgsStepData e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.BeforeStep?.Invoke(this, e);
		}

		/// <summary>
		/// Nexts the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void RaiseBeforeStep(string message = "", params object[] pars)
		{
			this.Message = message;
			this.OnBeforeStep(new EventArgsStepData(message, pars));
		}
		#endregion

		#region After Step
		/// <summary>
		/// Event StepCompleted.
		/// </summary>
		public event EventHandlerStepData AfterStep;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnAfterStep(EventArgsStepData e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.AfterStep?.Invoke(this, e);
		}

		/// <summary>
		/// Nexts the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void RaiseAfterStep(string message = "", params object[] pars)
		{
			this.CurrentStep++;
			this.Message = message;
			this.OnAfterStep(new EventArgsStepData(message, pars));
		}
		#endregion

		#region Run Step
		/// <summary>
		/// Event StepCompleted.
		/// </summary>
		public event EventHandlerStepData RunMessage;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnRunMessage(EventArgsStepData e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.RunMessage?.Invoke(this, e);
		}

		/// <summary>
		/// Nexts the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void RaiseRunMessage(string message = "", params object[] pars)
		{
			this.Message = message;
			this.OnRunMessage(new EventArgsStepData(message, pars));
		}
		#endregion
		#endregion

		#region StepCompleted event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerStepData(object sender, EventArgsStepData e);

		/// <summary>
		/// StepCompleted arguments.
		/// </summary>
		public class EventArgsStepData : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initialized a new instance
			/// </summary>
			/// <param name="pars"></param>
			public EventArgsStepData(string message, object pars)
			{
				this.Message = message;
				this.Pars = pars;
			}

			public string Message { get; set; }
			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			public object Pars { get; set; }
			#endregion
		}
		#endregion

		#endregion

		#region Embedded Types
		#endregion
	}
}
