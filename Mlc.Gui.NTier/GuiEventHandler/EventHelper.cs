﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Mlc.Shell;
using Mlc.Shell.IO;
using DevExpress.XtraEditors;

namespace Mlc.Gui.NTier
{
    public class EventHelper
    {
        private Dictionary<object, string> controlsForDragAndDrop = new Dictionary<object, string>();
        private Dictionary<object, FileSystemTypes> controlsForOpenFolder = new Dictionary<object, FileSystemTypes>();

        public void AddControlForDragAndDrop(Control control, string dataFormats)
        {
            if (!this.controlsForDragAndDrop.ContainsKey(control))
            {
                this.controlsForDragAndDrop.Add(control, dataFormats);
                control.AllowDrop = true;
                control.DragEnter += control_DragEnter;
                control.DragDrop += control_DragDrop;
            }
        }

        /// <summary>
        /// Adds the control for open folder.
        /// </summary>
        /// <param name="control">The control.</param>
        public void AddControlForOpenFolder(Control control)
        {
            if (!this.controlsForOpenFolder.ContainsKey(control))
            {
                this.controlsForOpenFolder.Add(control, FileSystemTypes.Directory);
                control.DoubleClick += control_DoubleClick;
                control.Enter += controlPathFolder_CheckPath;

                if (control is TextBox)
                    ((TextBox)control).TextChanged += controlPathFolder_CheckPath;
                else if (control is TextEdit)
                    ((TextEdit)control).TextChanged += controlPathFolder_CheckPath;
            }
        }

        private void controlPathFolder_CheckPath(object sender, EventArgs e)
        {
            string value = this.tryGetPropertiesFor(sender);

            if (value != null)
                this.setFsoElementExists(sender, new FileSystemMgr(value).Exists);
        }

        private void setFsoElementExists(object sender, bool exists)
        {
            if (sender is TextBox)
                ((TextBox)sender).ForeColor = exists ? Color.Black : Color.Red;
            else if (sender is TextEdit)
                ((TextEdit)sender).ForeColor = exists ? Color.Black : Color.Red; 
        }

        private void control_DoubleClick(object sender, EventArgs e)
        {
            bool okToOpen = false;
            string value = this.tryGetPropertiesFor(sender);
            if (!string.IsNullOrEmpty(value))
            {
                FileSystemMgr fsm = new FileSystemMgr(value);
                okToOpen = fsm.Exists;
                if (!okToOpen)
                    okToOpen = fsm.Create(this.controlsForOpenFolder[sender]).Exists;
            }
            if (okToOpen)
                try
                {
                    this.setFsoElementExists(sender, okToOpen);
                    Process.Start("explorer.exe", value);
                }
                catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        private string tryGetPropertiesFor(object sender)
        {
            string retVal = null;

            if (sender is TextBox)
                retVal = ((TextBox)sender)?.Text;
            else if (sender is TextEdit)
                retVal = ((TextEdit)sender)?.Text;
            return retVal;
        }

        private void control_DragDrop(object control, DragEventArgs e)
        {
            if (this.checkDataFormat(control, e))
            {
                string dataFormat = this.controlsForDragAndDrop[control];
                switch (dataFormat)
                {
                    case "Bitmap":
                        break;
                    case "CommaSeparatedValue":
                        break;
                    case "Dib":
                        break;
                    case "Dif":
                        break;
                    case "EnhancedMetafile":
                        break;
                    case "FileDrop":
                        this.FileDropped(control, new EventArgsFileDropped((string[])e.Data.GetData(DataFormats.FileDrop), e));
                        break;
                    case "Html":
                        break;
                    case "Locale":
                        break;
                    case "MetafilePict":
                        break;
                    case "OemText":
                        break;
                    case "Palette":
                        break;
                    case "PenData":
                        break;
                    case "Riff":
                        break;
                    case "Rtf":
                        break;
                    case "Serializable":
                        break;
                    case "StringFormat":
                        break;
                    case "SymbolicLink":
                        break;
                    case "Text":
                        break;
                    case "Tiff":
                        break;
                    case "UnicodeText":
                        break;
                    case "WaveAudio":
                        break;
                    default:
                        break;
                }
            }

            List<string> files = new List<string>();
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] fsItems = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string fsItem in fsItems)
                {
                    FileAttributes attr = File.GetAttributes(fsItem);
                    if (attr.HasFlag(FileAttributes.Directory))
                    {
                        string[] fils = Directory.GetFiles(fsItem, "*.smg");
                        foreach (string file in fils)
                            files.Add(file);
                    }
                    else
                        if (Path.GetExtension(fsItem).ToLower() == ".smg")
                        files.Add(fsItem);
                }
            }

            //this.processfiles(files);
        }

        private void control_DragEnter(object control, DragEventArgs e)
        {
            e.Effect = this.checkDataFormat(control, e) ? DragDropEffects.All : DragDropEffects.None;
        }

        private bool checkDataFormat(object control, DragEventArgs e)
        {
            bool retVal = false;
            if (this.controlsForDragAndDrop.ContainsKey(control))
            {
                string dataFormat = this.controlsForDragAndDrop[control].ToString();
                retVal = e.Data.GetDataPresent(dataFormat.ToString());
            }
            return retVal;
        }


        #region Event declaration FileDropped
        /// <summary>
        /// Event definition.
        /// </summary>
        public event EventHandlerFileDropped FileDropped;

        /// <summary>
        /// Method to launch the event.
        /// </summary>  
        /// <param name="e">Event data</param>
        protected virtual void OnFileDropped(EventArgsFileDropped e)
        {
            // If there are receptors in listening ...
            if (FileDropped != null)
                // ... the event is launched
                FileDropped(this, e);
        }
        #endregion

        #region Event definition FileDropped
        /// <summary>
        /// Delegate for handling event.
        /// </summary>
        public delegate void EventHandlerFileDropped(object sender, EventArgsFileDropped e);

        /// <summary>
        /// Class for the definition of the data to be transmitted with the event.
        /// </summary>
        public class EventArgsFileDropped : System.EventArgs
        {
            #region Constructors
            /// <summary>
            /// Initializes a new instance
            /// </summary>
            /// <param name="fsItems"></param>
            /// <param name="e"></param>
            public EventArgsFileDropped(string[] fsItems, DragEventArgs e)
            {
                this.fsItems = fsItems;
                this.e = e;
            }
            #endregion

            #region Properties
            /// <summary></summary>
            private string[] fsItems;
            /// <summary>
            /// Returns or sets ....
            /// </summary>
            public string[] FsItems { get { return fsItems; } set { fsItems = value; } }


            private DragEventArgs e = null;
            /// <summary>
            /// Returns or sets ....
            /// </summary>
            public DragEventArgs EventArgs { get { return e; } set { e = value; } }
            #endregion
        }
        #endregion



    }
}
