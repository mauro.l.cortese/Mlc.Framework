﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Mlc.Gui.NTier
{
    public partial class FormDialog : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// The HTML message
        /// </summary>
        private string htmlMessage;
        /// <summary>
        /// Gets or sets the HTML message.
        /// </summary>
        /// <value>The HTML message.</value>
        public string HtmlMessage
        {
            get { return this.htmlMessage; }
            set
            {
                this.htmlMessage = value;
                if (!string.IsNullOrEmpty(this.HtmlMessage))
                    this.richEditControl1.HtmlText = this.HtmlMessage;
            }
        }


		/// <summary>
		/// The message
		/// </summary>
		private string message;

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>The message.</value>
		public string Message
		{
			get { return this.message; }
			set
			{
				this.message = value;
				if (!string.IsNullOrEmpty(this.message))
					this.richEditControl1.Text = this.message;
			}
		}



		/// <summary>
		/// Initializes a new instance of the <see cref="FormDialog"/> class.
		/// </summary>
		public FormDialog()
        {
            InitializeComponent();
        }

        private void formDialog_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            this.simpleButton1.Focus();
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void simpleButtonContinue_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
