﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mlc.Gui.NTier
{
    public class DialogMessage
    {
        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Message { get; set; }

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		/// <value>The size.</value>
		public DialogSettings Settings { get; set; } = new();

		public class DialogSettings
		{
			/// <summary>
			/// Gets or sets the size.
			/// </summary>
			/// <value>The size.</value>
			public Size FormSize { get; set; } = new Size(600, 400);
			/// <summary>
			/// Gets or sets a value indicating whether [word wrap].
			/// </summary>
			/// <value><c>true</c> if [word wrap]; otherwise, <c>false</c>.</value>
			public bool WordWrap { get; set; } = false;
			/// <summary>
			/// Gets or sets the scroll bars.
			/// </summary>
			/// <value>The scroll bars.</value>
			public ScrollBars ScrollBars { get; set; } = ScrollBars.Vertical;
		}

	}

}
