﻿// ***********************************************************************
// Assembly         : Mlc.Gui.NTier
// Author           : mauro.luigi.cortese
// Created          : 05-06-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 07-28-2020
// ***********************************************************************
// <copyright file="Dialog.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;
using System.Windows.Forms;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class Dialog.
	/// </summary>
	public class Dialog
    {

		/// <summary>
		/// Initializes a new instance of the <see cref="Dialog"/> class.
		/// </summary>
		public Dialog()
        {
            this.FormSize = new System.Drawing.Size(600, 400);
        }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>The message.</value>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the size of the form.
		/// </summary>
		/// <value>The size of the form.</value>
		public Size FormSize { get; set; }

		/// <summary>
		/// Shows the alert.
		/// </summary>
		/// <returns>DialogResult.</returns>
		public DialogResult ShowAlert(bool useHtml = true)
        {
				FormDialog fd = new FormDialog();
			if (useHtml)
				fd.HtmlMessage = this.Message;
			else
				fd.Message = this.Message;
			fd.Size = this.FormSize;
			return fd.ShowDialog();
        }
    }
}
