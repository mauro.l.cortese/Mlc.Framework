﻿using Mlc.Shell.IO;
using System;
using System.Text;


namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class LogRowFormatter.
	/// </summary>
	public class LogCommandFormatter : LogFormatter
	{
		/// <summary>
		/// Formats the log.
		/// </summary>
		/// <param name="logRow">The log row.</param>
		/// <returns>System.String.</returns>
		public override string FormatLog(ref LogRow logRow)
		{

			string newLine = string.Empty;
			if (logRow.LogObject is Command)
			{
				Command cmd = (Command)logRow.LogObject;
				string cmdDescription = string.IsNullOrEmpty(cmd.Description) ? "[-]" : cmd.Description;
				string cmdSection = string.IsNullOrEmpty(cmd.Section) ? "[-]" : cmd.Section;
				StringBuilder sb = new();

				switch (cmd.State)
				{
					case Command.CommandStates.Ready:
						sb.AppendLine($"Beging Run Command   : {cmd.Tag}  =>  {cmd.Caption}  =>  {cmd.Section}");
						sb.AppendLine($"Description          : {cmdDescription}");
						break;
					case Command.CommandStates.Running:
					case Command.CommandStates.Stopped:
					case Command.CommandStates.Completed:
						sb.AppendLine($"End Executed Command : {cmd.Tag}  =>  {cmd.Caption}  =>  {cmd.Section}");
						sb.AppendLine($"Description          : {cmdDescription}");

						if (logRow.Level == Logger20.Level.Debug)
						{
							sb.AppendLine($"Can Be Excute        : {cmd.CanBeExecute}");
							sb.AppendLine($"Begin at             : {cmd.Data.BeginExecuteCommand:dd/MM/yyyy hh:mm:ss.fffff}");
							sb.AppendLine($"End at               : {cmd.Data.EndExecuteCommand:dd/MM/yyyy hh:mm:ss.fffff}");
							sb.AppendLine($"Spent Time           :            {cmd.Data.SpentTime:hh\\:mm\\:ss\\.fffff}");
							sb.AppendLine($"Result Success       : {cmd.ResultSuccess}");
							sb.AppendLine($"State                : {cmd.State}");
						}
						break;
				}
				logRow.LogObject = sb;
			}
			return base.FormatLog(ref logRow);
		}
	}
}
