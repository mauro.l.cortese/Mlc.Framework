﻿using Mlc.Shell.IO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mlc.Gui.NTier
{
	/// <summary>
	/// Class LogHandlerCommandFile.
	/// Implements the <see cref="Mlc.Shell.IO.LogHandlerFile" />
	/// </summary>
	/// <seealso cref="Mlc.Shell.IO.LogHandlerFile" />
	public class LogHandlerCommandFile : LogHandlerFile
	{
		public LogHandlerCommandFile(Logger20.Level level, string name, string folder = "")
			: base(level, name, folder)
		{
			// this.Formatter = new LogCommandFormatter();

			// Defines the formatters for command types
			base.Formatters.Add(
				new List<Type>() { 
					typeof(Command) 
					}, 
				new LogCommandFormatter());
		}
	}
}
