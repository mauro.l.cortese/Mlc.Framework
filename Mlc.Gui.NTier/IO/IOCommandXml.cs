﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using Mlc.Shell;
using Mlc.Shell.IO;
using System.ComponentModel;
using System.Windows.Forms;


namespace Mlc.Gui.NTier
{
    public class IOCommandXml
    {
        /// <summary>
        /// The command handler instance
        /// </summary>
        private CommandsHdlr cmdsHdlr = CommandsHdlr.GetInstance();

        /// <summary>
        /// The file XML
        /// </summary>
        private string fileXml = "CommandsHdlr.xml";

        /// <summary>
        /// Initializes a new instance of the <see cref="IOCommandXml"/> class.
        /// </summary>
        public IOCommandXml()
            :this(string.Empty)
        { }
        /// <summary>
        /// Initializes a new instance of the <see cref="IOCommandXml"/> class.
        /// </summary>
        /// <param name="fileXml">The file XML.</param>
        public IOCommandXml (string fileXml)
        {
            if(!string.IsNullOrEmpty(fileXml))
            this.fileXml = fileXml;
        }
        /// <summary>
        /// Reads the XML data.
        /// </summary>
        /// <returns><c>true</c> if operation succeeded, <c>false</c> otherwise.</returns>

        public bool LoadXml()
        {
            bool retVal = false;
            try
            {
                XmlFileReader reader = new XmlFileReader(this.getXmlFile());

                // it traverses the objects structure
                foreach (Command cmd in this.cmdsHdlr.commands.Values)
                {
                    string defCaption = string.Empty;
                    string defDescription = string.Empty;
                    string defToooltip = string.Empty;
                    string defHint = string.Empty;
                    string caption = string.Empty;
                    string description = string.Empty;
                    string toooltip = string.Empty;
                    string hint = string.Empty;
                    
                    // gets default command data
                    string xPath = string.Format("/CommandHdlr/Commands/Command[@CommandKey='{0}']", cmd.Key);
                    reader.GetXPathNodeIterator(xPath);
                    if (reader.Read())
                    {
                        defCaption = reader.GetAttribute("Caption", string.Empty);
                        defDescription = reader.GetAttribute("Description", string.Empty);
                        defToooltip = reader.GetAttribute("ToolTip", string.Empty);
                        defHint = reader.GetAttribute("Hint", string.Empty);
                    }
                    cmd.Description = defDescription;

                    foreach (CommandLinks commandLinks in cmd.CommandLinksList)
                    {
                        string formName = ((Form)commandLinks.Form).Name;
                        foreach (Component component in commandLinks.Components)
                        {
                            string name = string.Empty;
                            if (component is BarItem)
                                name = ((BarItem)component).Name;
                            else if (component is Button)
                                name = ((Button)component).Name;
                            else if (component is SimpleButton)
                                name = ((SimpleButton)component).Name;
                            else if (component is ToolStripItem)
                                name = ((ToolStripItem)component).Name;


                            // gets default command data
                            xPath = string.Format("/CommandHdlr/Commands/Command[@CommandKey='{0}']", cmd.Key);
                            reader.GetXPathNodeIterator(xPath);
                            if (reader.Read())
                            {
                                defCaption = reader.GetAttribute("Caption", string.Empty);
                                defDescription = reader.GetAttribute("Description", string.Empty);
                                defToooltip = reader.GetAttribute("ToolTip", string.Empty);
                                defHint = reader.GetAttribute("Hint", string.Empty);
                            }

                            xPath = string.Format("/CommandHdlr/Commands/Command[@CommandKey='{0}']/Forms/Form[@Name='{1}']/Components/Component[@Name='{2}']", cmd.Key, formName, name);
                            reader.GetXPathNodeIterator(xPath);
                            if (reader.Read())
                            {
                                caption = reader.GetAttribute("Caption", string.Empty);
                                description = reader.GetAttribute("Description", string.Empty);
                                toooltip = reader.GetAttribute("ToolTip", string.Empty);
                                hint = reader.GetAttribute("Hint", string.Empty);
                            }

                            if (component is BarItem)
                            {
                                ((BarItem)component).Caption = string.IsNullOrEmpty(caption) ? defCaption : caption;
                                ((BarItem)component).Description = string.IsNullOrEmpty(description) ? defDescription : description;
                                ((BarItem)component).Hint = string.IsNullOrEmpty(hint) ? defHint : hint;

                                // string toolTipText = string.IsNullOrEmpty(toooltip) ? defToooltip : toooltip;
                                ((BarItem)component).SuperTip = this.getSuperToolTip(string.IsNullOrEmpty(toooltip) ? defToooltip : toooltip);
                            }
                            else if (component is Button)
                            {
                                ((Button)component).Text = string.IsNullOrEmpty(caption) ? defCaption : caption;
                            }
                            else if (component is SimpleButton)
                            {
                                ((SimpleButton)component).Text = string.IsNullOrEmpty(caption) ? defCaption : caption;
                            }
                            else if (component is ToolStripItem)
                            {
                                ((ToolStripItem)component).Text = string.IsNullOrEmpty(caption) ? defCaption : caption;
                                ((ToolStripItem)component).ToolTipText = string.IsNullOrEmpty(toooltip) ? defToooltip : toooltip;
                            }

                            if (string.IsNullOrEmpty(description)) description = caption;
                            if (string.IsNullOrEmpty(hint)) hint = description;
                        }
                    }
                }
                retVal = true;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

            return retVal;

        }
        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <returns><c>true</c> if operation succeeded, <c>false</c> otherwise.</returns>
        public bool SaveXml()
      
        {
            bool retVal = false;
            try
            {
                using (XmlFileWriter writer = new XmlFileWriter(this.getXmlFile()))
                {
                    writer.OpenXML("CommandHdlr");
                    writer.OpenNode("Commands");
                    foreach (Command cmd in this.cmdsHdlr.commands.Values)
                    {
                        writer.OpenNode("Command");
                        writer.AddAttribute("CommandKey", cmd.Key);
                        writer.AddAttribute("Caption", cmd.Caption);
                        writer.AddAttribute("Description", cmd.Description);
                        writer.AddAttribute("ToolTip", cmd.ToolTip);
                        writer.AddAttribute("Hint", cmd.ToolTip);
                        writer.OpenNode("Forms");
                        foreach (CommandLinks commandLinks in cmd.CommandLinksList)
                        {
                            writer.OpenNode("Form");
                            writer.AddAttribute("Name", ((Form)commandLinks.Form).Name);
                            writer.OpenNode("Components");
                            foreach (Component component in commandLinks.Components)
                            {
                                writer.OpenNode("Component");
                                writer.AddAttribute("Type", component.GetType().Name);
                                string name = string.Empty;
                                string caption = string.Empty;
                                string description = string.Empty;
                                string toooltip = string.Empty;
                                string hint = string.Empty;

                                if (component is BarItem)
                                {
                                    name = ((BarItem)component).Name;
                                    caption = ((BarItem)component).Caption;
                                    description = ((BarItem)component).Description;
                                    if (((BarItem)component).SuperTip != null)
                                        toooltip = ((BarItem)component).SuperTip.ToString();
                                    hint = ((BarItem)component).Hint;
                                }
                                else if (component is Button)
                                {
                                    name = ((Button)component).Name;
                                    caption = ((Button)component).Text;
                                }
                                else if (component is SimpleButton)
                                {
                                    name = ((SimpleButton)component).Name;
                                    caption = ((SimpleButton)component).Text;
                                    if (((SimpleButton)component).SuperTip != null)
                                        toooltip = ((SimpleButton)component).SuperTip.ToString();
                                }
                                else if (component is ToolStripItem)
                                {
                                    name = ((ToolStripItem)component).Name;
                                    caption = ((ToolStripItem)component).Text;
                                    toooltip = ((ToolStripItem)component).ToolTipText;
                                }

                                writer.AddAttribute("Name", name);
                                writer.AddAttribute("Caption", caption);
                                writer.AddAttribute("Description", description);
                                writer.AddAttribute("ToolTip", toooltip);
                                writer.AddAttribute("Hint", hint);
                                writer.CloseNode();
                            }
                            writer.CloseNode();
                            writer.CloseNode();
                        }
                        writer.CloseNode();
                        writer.CloseNode();
                    }
                    writer.CloseNode();
                    writer.CloseXML();
                }
                retVal = true;
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
            return retVal;
        }

        /// <summary>
        /// Gets the XML file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>System.String.</returns>
        private string getXmlFile()
        {
            string file = string.Concat(FileSystemMgr.AppSubFolder("data", true).Path, Chars.BackSlash, this.fileXml);
            return file;
        }

        /// <summary>
        /// Gets the super tool tip.
        /// </summary>
        /// <param name="toolTipText">The tool tip text.</param>
        /// <returns>SuperToolTip.</returns>
        private SuperToolTip getSuperToolTip(string toolTipText)
        {
            SuperToolTip superToolTip = null;
            // Method 2
            if (!string.IsNullOrEmpty(toolTipText))
            {
                string[] txts = toolTipText.Split('/');
                superToolTip = new SuperToolTip();
                // Create an object to initialize the SuperToolTip.
                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs();

                if (txts.Length > 0)
                    args.Title.Text = txts[0];
                if (txts.Length > 1)
                    args.Contents.Text = txts[1];
                if (txts.Length > 2)
                    args.Footer.Text = txts[2];

                superToolTip.Setup(args);
            }
            return superToolTip;
        }

    }
}
