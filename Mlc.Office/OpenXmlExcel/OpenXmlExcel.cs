﻿using Mlc.Shell;
using Mlc.Shell.Resources;
using Mlc.Data;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using OfficeOpenXml.Style.Dxf;
using OfficeOpenXml.Style;
using OfficeOpenXml.Export.ToDataTable;

namespace Mlc.Office.Excel
{
	/// <summary>
	/// Class OpenXmlExcel.
	/// </summary>
	public class OpenXmlExcel
	{


		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>The value cell exceptions</summary>
		private ValueCellExceptionList valueCellExceptions = new ValueCellExceptionList();
		/// <summary>The excel</summary>
		private ExcelPackage excel = null;
		/// <summary>The working file</summary>
		private string workingFile;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="OpenXmlExcel"/> class.
		/// </summary>
		/// <param name="fileExcel">The file excel.</param>
		public OpenXmlExcel(string fileExcel)
		{
			// If you use EPPlus in a noncommercial context
			// according to the Polyform Noncommercial license:
			ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
			this.FileExcel = fileExcel;
		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public

		#region Properties
		/// <summary>
		/// Gets the file excel.
		/// </summary>
		/// <value>The file excel.</value>
		public string FileExcel { get; private set; }

		/// <summary>
		/// Gets the loaded rows.
		/// </summary>
		/// <value>The loaded rows.</value>
		public int LoadedRows { get; private set; }

		/// <summary>
		/// Gets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public Exception Exception { get; private set; }
		#endregion

		#region Method Static

		/// <summary>
		/// Defaults the value.
		/// </summary>
		/// <param name="maybeNullable">The maybe nullable.</param>
		/// <returns>System.Object.</returns>
		public static object DefaultValue(Type maybeNullable)
		{
			Type underlying = Nullable.GetUnderlyingType(maybeNullable);
			if (underlying != null)
				return Activator.CreateInstance(underlying);

			if (maybeNullable == typeof(string))
				return string.Empty;

			return Activator.CreateInstance(maybeNullable);
		}
		#endregion

		#region Method
		/// <summary>
		/// Gets all table names.
		/// </summary>
		/// <returns>List&lt;System.String&gt;.</returns>
		public Tables GetAllTableNames()
		{
			this.Exception = null;
			Tables tables = new();
			try
			{
				if (this.openFile())
				{
					ExcelWorkbook workBook = this.excel.Workbook;
					foreach (ExcelWorksheet worksheet in workBook.Worksheets)
						if (worksheet.Tables.Count != 0)
							foreach (ExcelTable table in worksheet.Tables)
								tables.Add(new Table(worksheet.Name, table.Name));
				}
				else
				{
					this.Exception = new Exception(string.Format(Messages.ExcelOpenFileError, this.FileExcel));
					throw this.Exception;
				}
			}
			catch (Exception ex)
			{
				ExceptionsRegistry.GetInstance().Add(ex);
				this.Exception = ex;
			}
			finally
			{
				if (!this.close(false))
				{
					this.Exception = new Exception(string.Format(Messages.ExcelCloseFileError, this.FileExcel));
					throw this.Exception;
				}
			}

			return tables;
		}


		/// <summary>
		/// Writes the table.
		/// </summary>
		/// <param name="workSheetName">Name of the work sheet.</param>
		/// <param name="iRows">The i rows.</param>
		public void WriteTable(string workSheetName, IRows iRows)
		{
			this.WriteTable(workSheetName, iRows.GetTable());
		}


		/// <summary>
		/// Writes the table.
		/// </summary>
		/// <param name="workSheetName">Name of the work sheet.</param>
		/// <param name="table">The table.</param>
		public void WriteTable(string workSheetName, DataTable table)
		{
			using (ExcelPackage excel = new ExcelPackage())
			{
				ExcelWorkbook workBook = excel.Workbook;

				workBook.Worksheets.Add(workSheetName);

				List<string> fields = new List<string>();
				// write title
				foreach (DataColumn column in table.Columns)
					fields.Add(column.Caption);


				var headerRow = new List<string[]>() { fields.ToArray() };

				// Determine the header range (e.g. A1:D1)
				string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

				// Target a worksheet
				var worksheet = excel.Workbook.Worksheets[workSheetName];

				// Popular header row data
				worksheet.Cells.Style.Font.Size = 10;
				worksheet.Cells[headerRange].LoadFromArrays(headerRow);
				worksheet.Cells[headerRange].Style.Font.Bold = true;
				worksheet.Cells[headerRange].Style.Font.Size = 12;
				// worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

				List<object[]> rows = new List<object[]>();
				foreach (DataRow row in table.Rows)
				{
					List<object> cols = new List<object>();
					foreach (DataColumn column in table.Columns)
						cols.Add(row[column]);
					rows.Add(cols.ToArray());
				}

				worksheet.Cells[2, 1].LoadFromArrays(rows);

				for (int i = 1; i <= table.Columns.Count; i++)
					worksheet.Column(i).AutoFit();

				FileInfo excelFile = new FileInfo(this.FileExcel);
				excel.SaveAs(excelFile);
			}
		}


		/// <summary>
		/// Writes the table.
		/// </summary>
		/// <param name="tables">The tables.</param>
		public void UpdateTables(Tables tables, TableStyles tableStyles = TableStyles.Light18)
		{
			if (tables != null && tables.Count > 0)
			{
				if (File.Exists(this.FileExcel))
				{
					try
					{
						if (this.openFile())
						{
							ExcelWorkbook workBook = this.excel.Workbook;
							foreach (Table table in tables)
							{
								if (!string.IsNullOrEmpty(table.SheetName))
								{
									ExcelWorksheet worksheet = workBook.Worksheets[table.SheetName];
									if (worksheet == null)
									{
										this.Exception = new Exception(string.Format(Messages.ExcelMissingWorksheet, table.SheetName));
										throw this.Exception;
									}
									else
									{
										ExcelTable excelTable = worksheet.Tables[table.TableName];

										//ExcelNamedRange namedRange = worksheet.Names[0];

										//namedRange.NameComment = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc";
										

										if (excelTable != null)
										{
											ColStyleCollection colStyleCollection = new(excelTable);
											ExcelRangeBase range = excelTable.Range;
											worksheet.Tables.Delete(table.TableName, true);
											range.LoadFromDataTable(table.DataTable, true, tableStyles);
											colStyleCollection.SetStyle(worksheet.Tables[table.TableName]);
										}
										else
										{
											this.Exception = new Exception(string.Format(Messages.ExcelMissingTable, table.TableName));
											throw this.Exception;
										}

									}
								}
							}
						}
						else
						{
							this.Exception = new Exception(string.Format(Messages.ExcelOpenFileError, this.FileExcel));
							throw this.Exception;
						}
					}
					catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
					finally
					{
						if (!this.close())
						{
							this.Exception = new Exception(string.Format(Messages.ExcelCloseFileError, this.FileExcel));
							throw this.Exception;
						}
					}
				}
			}
		}

		/// <summary>
		/// Reads the table.
		/// </summary>
		/// <param name="workSheetName">Name of the work sheet.</param>
		/// <param name="tableName">Name of the excel table.</param>
		/// <param name="rowMapType">Type of the row map.</param>
		/// <returns>DataTable.</returns>
		/// <exception cref="System.Exception">Foglio {workSheetName} non trovato!</exception>
		public DataTable ReadTable(string workSheetName, string tableName, Type rowMapType)
		{
			if (
				!File.Exists(this.FileExcel) ||
				string.IsNullOrEmpty(workSheetName) ||
				string.IsNullOrEmpty(tableName) ||
				rowMapType is null
				)
				return null;

			DataTable table = null;
			if (rowMapType.GetInterface(nameof(IDataRowWrapper)) != null)
			{
				try { table = ((IDataRowWrapper)Activator.CreateInstance(rowMapType)).Table; }
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}
			else
				table = DataTableTools.GetTableFromType(rowMapType);

			if (table != null)
			{
				foreach (DataColumn col in table.Columns)
					col.ReadOnly = false;

				try
				{
					using (FileStream fileStream = new FileStream(this.FileExcel, FileMode.Open))
					{
						using (ExcelPackage package = new ExcelPackage(fileStream))
						{
							ExcelWorkbook workBook = package.Workbook;
							ExcelWorksheet worksheet = workBook.Worksheets[workSheetName];
							if (worksheet == null)
								throw new Exception($"Foglio {workSheetName} non trovato!");
							else
							{
								ExcelTable excelTable = worksheet.Tables[tableName];
								if (excelTable != null)
								{
									ExcelRangeBase range = excelTable.Range;
									ExcelCellAddress cellAddress = range.Start;

									int currRow = cellAddress.Row, c = cellAddress.Column;
									//this.LoadedRows = this.readData(worksheet, table, range.Start.Row, range.Start.Column, range.Columns, range.Rows);

									table.TableName = excelTable.Name;
									//try
									//{
									//	DataTable xlstable = excelTable.ToDataTable(opt =>
									//	{
									//		opt.DataTableName = excelTable.Name;
									//		opt.ExcelErrorParsingStrategy = ExcelErrorParsingStrategy.IgnoreRowWithErrors;
									//	});

									//	this.mergeTable(table, xlstable);
									//}
									//catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

									try
									{
										this.LoadedRows = this.readData(worksheet, table, excelTable);
									}
									catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }




								}
								else
									throw new Exception($"Tabella {tableName} non trovata!");

							}
						}
					}
				}
				catch (Exception ex)
				{
					if (ex is IOException)
					{
						this.switchToFileTmp();
						return ReadTable(workSheetName, tableName, rowMapType);
					}
					else
						ExceptionsRegistry.GetInstance().Add(ex);
				}
				if (this.valueCellExceptions.Count != 0)
				{
					this.valueCellExceptions.FileExcel = this.FileExcel;
					this.OnWrongDataFounded(new EventArgsWrongDataFounded(this.valueCellExceptions, this.FileExcel));
				}
			}
			return table;
		}

		/// <summary>
		/// Reads the table.
		/// </summary>
		/// <param name="workSheetName">Name of the work sheet.</param>
		/// <param name="rowMapType">Type of the row map.</param>
		/// <returns>DataTable.</returns>
		/// <exception cref="Exception">Foglio {workSheetName}</exception>
		public DataTable ReadTable(string workSheetName, Type rowMapType)
		{
			if (!File.Exists(this.FileExcel))
				return null;

			//  DataTable table = this.getTable(rowMapType);
			DataTable table = DataTableTools.GetTableFromType(rowMapType);

			if (table != null)
			{

				try
				{
					using (var fs = new FileStream(this.FileExcel, FileMode.Open))
					{
						using (ExcelPackage package = new ExcelPackage(fs))
						{
							ExcelWorkbook workBook = package.Workbook;
							ExcelWorksheet worksheet = workBook.Worksheets[workSheetName];
							if (worksheet == null)
								throw new Exception($"Foglio {workSheetName} non trovato!");
							else
							{
								int currRow = 1, c = 1;
								Dictionary<string, int> titles = new Dictionary<string, int>();
								try
								{
									// Read titles
									foreach (DataColumn col in table.Columns)
									{
										string title = worksheet.Cells[currRow, c].GetValue<string>();
										if (!string.IsNullOrWhiteSpace(title))
											titles.Add(title, c);
										c++;
									}
								}
								catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

								//Read values
								bool exit = false;
								currRow++;
								while (!exit)
								{
									object value = null;
									int nCol = 0;
									string colName = null;
									try
									{
										DataRow row = table.NewRow();
										bool rowOk = true;

										foreach (string title in titles.Keys)
										{

											if (table.Columns.Contains(title))
											{
												DataColumn col = table.Columns[title];
												value = worksheet.Cells[currRow, titles[col.ColumnName]].GetValue<object>();
												nCol++;

												if (nCol == 1 && value == null)
												{
													exit = true;
													break;
												}
												else
												{
													if (value is null)
														value = DefaultValue(col.DataType);


													try
													{
														row[col.ColumnName] = Convert.ChangeType(value, col.DataType);
													}
													catch (Exception ex)
													{
														ExceptionsRegistry.GetInstance().Add(ex);
														valueCellExceptions.Add(ex, currRow, nCol, colName, col, value);
														rowOk = false;
													}
												}
											}
										}
										if (!exit)
										{
											if (rowOk)
												table.Rows.Add(row);
											currRow++;
										}
									}
									catch (Exception ex)
									{
										ExceptionsRegistry.GetInstance().Add(ex);
										exit = true;
									}
								}

								this.LoadedRows = currRow - 2;
							}
						}
					}
				}
				catch (Exception ex)
				{
					if (ex is IOException)
					{
						this.switchToFileTmp();
						return ReadTable(workSheetName, rowMapType);
					}
					else
						ExceptionsRegistry.GetInstance().Add(ex);
				}

				if (this.valueCellExceptions.Count != 0)
				{
					this.valueCellExceptions.FileExcel = this.FileExcel;
					this.OnWrongDataFounded(new EventArgsWrongDataFounded(this.valueCellExceptions, this.FileExcel));
				}
			}
			return table;

		}
		#endregion

		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Closes the specified save.
		/// </summary>
		/// <param name="save">if set to <c>true</c> [save].</param>
		private bool close(bool save = true)
		{
			if (this.excel != null)
				try
				{
					if (save)
					{
						//this.excel.SaveAs(this.FileExcel);
						this.excel.Save();
					}
					if (this.excel != null)
					{
						if (this.excel.Stream != null)
							this.excel.Stream.Dispose();
						this.excel.Dispose();
					}
					try
					{
						if (File.Exists(workingFile))
							File.Delete(workingFile);
					}
					catch (Exception) { }
					return true;
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return false;
		}

		/// <summary>
		/// Gets the default value.
		/// </summary>
		/// <param name="dataType">Type of the data.</param>
		/// <returns>System.Object.</returns>
		private object getDefaultValue(Type dataType)
		{
			object retVal = null;
			switch (dataType.FullName)
			{
				case "System.String":
					retVal = string.Empty;
					break;
				case "System.Boolean":
					retVal = default(bool);
					break;
				case "System.DateTime":
					retVal = default(DateTime);
					break;
				case "System.Int16":
				case "System.Int32":
				case "System.Int64":
				case "System.Float":
				case "System.Double":
				case "System.Decimal":
				case "System.Single":
					retVal = 0;
					break;
				default:
					retVal = null;
					break;
			}
			return retVal;
		}

		/// <summary>
		/// Parses the table.
		/// </summary>
		/// <param name="toTable">To table.</param>
		/// <param name="fromExcelTable">From excel table.</param>
		private void mergeTable(DataTable toTable, DataTable fromExcelTable)
		{
			toTable.Clear();
			toTable.TableName = fromExcelTable.TableName;
			try
			{
				foreach (DataRow row in fromExcelTable.Rows)
				{
					DataRow nRow = toTable.NewRow();
					foreach (DataColumn toCol in toTable.Columns)
						if (fromExcelTable.Columns.Contains(toCol.ColumnName))
						{
							DataColumn fromCol = fromExcelTable.Columns[toCol.ColumnName];
							object value = row[fromCol];
							if (fromCol.DataType != toCol.DataType)
							{
								EventArgsMissmatchType eamt = new(fromCol.DataType, toCol.DataType, value);
								this.OnMismatchType(eamt);
								value = eamt.ToValue;
							}
							try
							{
								nRow[toCol] = value;
							}
							catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
						}
					toTable.Rows.Add(nRow);
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Opens the file.
		/// </summary>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		private bool openFile()
		{
			if (File.Exists(this.FileExcel))
				try
				{
					// copy the file
					this.workingFile = new SysInfo().GetFileTmp();
					File.Copy(this.FileExcel, workingFile, true);
					File.Exists(workingFile);
					//this.excel = new ExcelPackage(new FileStream(workingFile, FileMode.Open));
					this.excel = new ExcelPackage(this.FileExcel);
					return true;
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			return false;
		}

		/// <summary>
		/// Reads the data.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="table">The table.</param>
		/// <param name="excelTable">The excel table.</param>
		/// <returns>System.Int32.</returns>
		private int readData(ExcelWorksheet worksheet, DataTable table, ExcelTable excelTable) =>
			this.readData(worksheet, table, excelTable.Range);

		/// <summary>
		/// Reads the data.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="table">The table.</param>
		/// <param name="range">The range.</param>
		/// <returns>System.Int32.</returns>
		private int readData(ExcelWorksheet worksheet, DataTable table, ExcelRangeBase range)
		{
			Dictionary<string, int> titles = new Dictionary<string, int>();
			int currRow = range.Start.Row;
			try
			{
				// Read titles from table
				for (int col = 0; col < range.Columns; col++)
				{
					int currCol = range.Start.Column + col;
					string title = worksheet.Cells[currRow, currCol].GetValue<string>().Trim();

					if (!string.IsNullOrWhiteSpace(title))
						titles.Add(title.Normalize("", "", ". ", null).ToLower(), currCol);
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			//Read values
			bool exit = false;
			currRow++;
			while (!exit)
			{
				if (range.Rows != 0 && currRow >= (range.Start.Row + range.Rows))
					break;

				object cellValue = null;
				int nCol = 0;
				string colName = null;
				try
				{
					DataRow dataRow = table.NewRow();
					bool rowOk = true;

					foreach (string title in titles.Keys)
					{
						if (table.Columns.Contains(title))
						{
							DataColumn col = table.Columns[title];
							cellValue = worksheet.Cells[currRow, titles[col.ColumnName.ToLower()]].GetValue<object>();
							nCol++;

							if (nCol == 1 && cellValue == null && range.Rows == 0)
							{
								exit = true;
								break;
							}
							else
							{
								if (cellValue is null)
									//cellValue = DefaultValue(col.DataType);
									cellValue = this.getDefaultValue(col.DataType);

								Exception ex = null;
								dataRow[col.ColumnName] = this.setValue(table.Columns[col.ColumnName], cellValue, ref ex);

								if (ex != null)
								{
									ExceptionsRegistry.GetInstance().Add(ex);
									valueCellExceptions.Add(ex, currRow, nCol, colName, col, cellValue);
									rowOk = false;
								}
							}
						}
					}
					if (!exit)
					{
						if (rowOk)
						{
							foreach (DataColumn col in table.Columns)
								if (dataRow[col.ColumnName] == DBNull.Value)
									dataRow[col.ColumnName] = this.getDefaultValue(col.DataType);
							table.Rows.Add(dataRow);
						}
						currRow++;
					}
				}
				catch (Exception ex)
				{
					ExceptionsRegistry.GetInstance().Add(ex);
					exit = true;
				}
			}
			return currRow - 2;
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="cellValue">The cell value.</param>
		/// <param name="exception">The ex.</param>
		/// <returns>System.Object.</returns>
		private object setValue(DataColumn col, object cellValue, ref Exception exception)
		{
			Type fromType = cellValue.GetType();
			Type toType = col.DataType;

			object value = cellValue;

			if (fromType != toType)
			{
				EventArgsMissmatchType eamt = new(fromType, toType, value);
				this.OnMismatchType(eamt);
				value = eamt.ToValue;
			}

			if (value is string str)
				value = str.Trim();

			try { value = Convert.ChangeType(value, toType); }
			catch (Exception ex)
			{
				exception = ex;
				value = Activator.CreateInstance(toType);
			}

			return value;
		}

		/// <summary>
		/// Gets the file temporary.
		/// </summary>
		/// <returns>System.String.</returns>
		private void switchToFileTmp()
		{
			string fileTmp = Path.GetTempFileName();
			File.Delete(fileTmp);
			File.Copy(this.FileExcel, fileTmp);
			this.FileExcel = fileTmp;
		}
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions

		#region Event MissmatchDataType
		/// <summary>
		/// Event MissmatchDataType.
		/// </summary>
		public event EventHandlerMismatchType MismatchType;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnMismatchType(EventArgsMissmatchType e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.MismatchType?.Invoke(this, e);
		}
		#endregion

		#region MissmatchDataType event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerMismatchType(object sender, EventArgsMissmatchType e);

		/// <summary>
		/// MissmatchDataType arguments.
		/// </summary>
		public class EventArgsMissmatchType : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initialized a new instance
			/// </summary>
			/// <param name="campo"></param>

			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsMissmatchType"/> class.
			/// </summary>
			/// <param name="campo">The campo.</param>
			/// <param name="dataType">Type of the data.</param>
			/// <param name="value">The value.</param>
			public EventArgsMissmatchType(Type fromType, Type toType, object value)
			{
				this.FromType = fromType;
				this.ToType = toType;
				this.FromValue = value;
				this.ToValue = this.FromValue;
			}

			#endregion

			#region Properties
			/// <summary>
			/// Gets from type.
			/// </summary>
			/// <value>From type.</value>
			public Type FromType { get; }
			/// <summary>
			/// Converts to type.
			/// </summary>
			/// <value>To type.</value>
			public Type ToType { get; }
			/// <summary>
			/// Get or set the property
			/// </summary>
			public object FromValue { get; set; }
			/// <summary>
			/// Get or set the property
			/// </summary>
			public object ToValue { get; set; } = null;
			#endregion
		}
		#endregion



		#region Event WrongDataFounded
		/// <summary>
		/// Event WrongDataFounded.
		/// </summary>
		public event EventHandlerWrongDataFounded WrongDataFounded;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnWrongDataFounded(EventArgsWrongDataFounded e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.WrongDataFounded?.Invoke(this, e);
		}
		#endregion

		#region WrongDataFounded event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerWrongDataFounded(object sender, EventArgsWrongDataFounded e);

		/// <summary>
		/// WrongDataFounded arguments.
		/// </summary>
		public class EventArgsWrongDataFounded : System.EventArgs
		{
			#region Constructors			
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsWrongDataFounded"/> class.
			/// </summary>
			/// <param name="valueCellExceptions">The value cell exceptions.</param>
			public EventArgsWrongDataFounded(ValueCellExceptionList valueCellExceptions, string fileExcel)
			{
				this.ValueCellExceptions = valueCellExceptions;
				this.FileExcel = fileExcel;
			}

			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			public ValueCellExceptionList ValueCellExceptions { get; private set; }

			/// <summary>
			/// Gets or sets the file excel.
			/// </summary>
			/// <value>The file excel.</value>
			public string FileExcel { get; private set; }
			#endregion
		}
		#endregion

		#endregion

		#region Embedded Types
		/// <summary>
		/// Class ColStyleCollection.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Office.Excel.OpenXmlExcel.ColStyle}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Office.Excel.OpenXmlExcel.ColStyle}" />
		public class ColStyleCollection : Dictionary<string, ColStyle>
		{
			/// <summary>
			/// The first col
			/// </summary>
			private int firstCol;


			/// <summary>
			/// Initializes a new instance of the <see cref="ColStyleCollection"/> class.
			/// </summary>
			/// <param name="excelTable">The excel table.</param>
			public ColStyleCollection(ExcelTable excelTable)
			{
				this.ExcelTable = excelTable;
				this.firstCol = this.ExcelTable.Range.EntireColumn.StartColumn;

				foreach (ExcelTableColumn etc in excelTable.Columns)
					this.Add(etc.Name, new ColStyle(etc, firstCol));
			}
			/// <summary>
			/// Gets the excel table.
			/// </summary>
			/// <value>The excel table.</value>
			public ExcelTable ExcelTable { get; }

			/// <summary>
			/// Sets the style.
			/// </summary>
			/// <param name="excelTable">The excel table.</param>
			internal void SetStyle(ExcelTable excelTable)
			{
				try
				{
					excelTable.Range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
					foreach (ExcelTableColumn etc in excelTable.Columns)
						if (this.ContainsKey(etc.Name))
						{
							etc.DataStyle.SetProperties(this[etc.Name].DataStyle);
							this[etc.Name].WorkSheetColumn.Style.SetProperties(this[etc.Name].ColumnStyle);
							this[etc.Name].WorkSheetColumn.Style.Numberformat.Format = etc.DataStyle.NumberFormat.Format;
						}

					int i = excelTable.Range.EntireRow.StartRow;
					excelTable.WorkSheet.Row(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}
		}

		/// <summary>
		/// Class ColStyle.
		/// </summary>
		public class ColStyle
		{
			/// <summary>
			/// Gets the excel table column.
			/// </summary>
			/// <value>The excel table column.</value>
			private ExcelTableColumn ExcelTableColumn { get; }

			/// <summary>
			/// Gets the work sheet column.
			/// </summary>
			/// <value>The work sheet column.</value>
			public ExcelColumn WorkSheetColumn { get; }
			/// <summary>
			/// Gets the data style.
			/// </summary>
			/// <value>The data style.</value>
			public ExcelDxfStyle DataStyle { get; }
			/// <summary>
			/// Gets the column style.
			/// </summary>
			/// <value>The column style.</value>
			public ExcelStyle ColumnStyle { get; }

			/// <summary>
			/// Initializes a new instance of the <see cref="ColStyle"/> class.
			/// </summary>
			/// <param name="excelTableColumn">The excel table column.</param>
			/// <param name="firstCol">The first col.</param>
			public ColStyle(ExcelTableColumn excelTableColumn, int firstCol)
			{
				this.ExcelTableColumn = excelTableColumn;
				this.WorkSheetColumn = this.ExcelTableColumn.Table.WorkSheet.Column(this.ExcelTableColumn.Position + firstCol);
				this.DataStyle = this.ExcelTableColumn.DataStyle;
				this.ColumnStyle = this.WorkSheetColumn.Style;
			}
		}

		/// <summary>
		/// Class Tables.
		/// Implements the <see cref="System.Collections.Generic.List{Mlc.Office.Excel.OpenXmlExcel.Table}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.List{Mlc.Office.Excel.OpenXmlExcel.Table}" />
		public class Tables : List<Table> { }

		/// <summary>
		/// Class Table.
		/// </summary>
		public class Table
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="Table"/> class.
			/// </summary>
			/// <param name="sheetName">Name of the sheet.</param>
			/// <param name="tableName">Name of the table.</param>
			public Table(string sheetName, string tableName)
				: this(sheetName, tableName, null)
			{ }


			/// <summary>
			/// Initializes a new instance of the <see cref="Table" /> class.
			/// </summary>
			/// <param name="sheetName">Name of the sheet.</param>
			/// <param name="table">The table.</param>
			public Table(string sheetName, DataTable table)
				: this(sheetName, table.TableName, table)
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="Table" /> class.
			/// </summary>
			/// <param name="sheetName">Name of the sheet.</param>
			/// <param name="tableName">Name of the table.</param>
			/// <param name="table">The table.</param>
			public Table(string sheetName, string tableName, DataTable table)
			{
				this.SheetName = sheetName;
				this.TableName = tableName;
				this.DataTable = table;
			}


			/// <summary>
			/// Gets or sets the name of the sheet.
			/// </summary>
			/// <value>The name of the sheet.</value>
			public string SheetName { get; set; }
			/// <summary>
			/// Gets or sets the name of the table.
			/// </summary>
			/// <value>The name of the table.</value>
			public string TableName { get; set; }

			/// <summary>
			/// Gets or sets the data table.
			/// </summary>
			/// <value>The data table.</value>
			public DataTable DataTable { get; set; }

			/// <summary>
			/// Returns a <see cref="String" /> that represents this instance.
			/// </summary>
			/// <returns>A <see cref="String" /> that represents this instance.</returns>
			public override string ToString() => $"{SheetName}\\{TableName}";
		}

		/// <summary>
		/// Class ValueCellException.
		/// </summary>
		public class ValueCellException
		{

			#region Fields
			/// <summary>
			/// The ex
			/// </summary>
			private Exception ex;

			/// <summary>
			/// The value
			/// </summary>
			private object value;

			/// <summary>
			/// The data column
			/// </summary>
			private DataColumn dataColumn;

			#endregion

			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="ValueCellException"/> class.
			/// </summary>
			/// <param name="ex">The ex.</param>
			/// <param name="row">The row.</param>
			/// <param name="col">The col.</param>
			/// <param name="colName">Name of the col.</param>
			/// <param name="dataColumn">The data column.</param>
			/// <param name="value">The value.</param>
			public ValueCellException(Exception ex, int row, int col, string colName, DataColumn dataColumn, object value)
			{
				this.ex = ex;
				this.NumRow = row;
				this.NumCol = col;
				this.ColName = colName;
				this.dataColumn = dataColumn;
				this.value = value;
			}

			#endregion

			#region Public

			#region Properties
			/// <summary>
			/// Gets or sets the number row.
			/// </summary>
			/// <value>The number row.</value>
			public int NumRow { get; set; } = 0;

			/// <summary>
			/// Gets or sets the number col.
			/// </summary>
			/// <value>The number col.</value>
			public int NumCol { get; set; } = 0;

			/// <summary>
			/// Gets or sets the name of the col.
			/// </summary>
			/// <value>The name of the col.</value>
			public string ColName { get; set; } = "";

			/// <summary>
			/// Gets the error.
			/// </summary>
			/// <value>The error.</value>
			public string Error { get => this.ex != null ? this.ex.Message : ""; }

			/// <summary>
			/// Gets the value.
			/// </summary>
			/// <value>The value.</value>
			public string Value { get => this.value != null ? this.value.ToString() : ""; }

			#endregion

			#endregion

		}

		/// <summary>
		/// Class ValueCellExceptionList.//
		/// </summary>
		/// <seealso cref="System.Collections.Generic.List{Mlc.Office.Excel.OpenXmlExcel.ValueCellException}" />
		public class ValueCellExceptionList : List<ValueCellException>
		{

			#region Fields
			/// <summary>
			/// Gets or sets the file excel.
			/// </summary>
			/// <value>The file excel.</value>
			public string FileExcel { get; set; }
			#endregion

			#region Internal

			#region Method
			/// <summary>
			/// Adds the specified ex.
			/// </summary>
			/// <param name="ex">The ex.</param>
			/// <param name="row">The row.</param>
			/// <param name="col">The col.</param>
			/// <param name="colName">Name of the col.</param>
			/// <param name="dataColumn">The data column.</param>
			/// <param name="value">The value.</param>
			internal void Add(Exception ex, int row, int col, string colName, DataColumn dataColumn, object value)
			{
				this.Add(new ValueCellException(ex, row, col, colName, dataColumn, value));
			}
			#endregion

			#endregion

		}
		#endregion
	}
}
