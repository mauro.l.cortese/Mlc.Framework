﻿using System;
using Mlc.Shell.IO;
using System.IO;
using System.Diagnostics;

namespace Mlc.Office.Excel
{
    public class ExcelApplication
    {
        /// <summary>
        /// Starts the specified excel file.
        /// </summary>
        /// <param name="excelFile">The excel file.</param>
        public static Process Start(string excelFile)
        {
			Process process = null;

			FileSystemMgr fileSystemMgr = new FileSystemMgr(excelFile);
            bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;
            if (isExcelInstalled && fileSystemMgr.Exists && fileSystemMgr.IsFile)
                process = fileSystemMgr.Run();

            return process;
        }
    }
}
