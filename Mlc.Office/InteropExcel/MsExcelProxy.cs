using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

using E = Microsoft.Office.Interop.Excel;


namespace Mlc.Office.Excel
{

    /// <summary>
    /// Fornisce i membri per interagire con l'applicazione Excel
    /// </summary>
    /// <remarks>Implementazione del pattern singleton</remarks>
    public class MsExcelProxy
    {
        #region Campi
        /// <summary>Variabile per safe thread</summary>
        private static object syncRoot = new Object();
        /// <summary>Istanza della classe</summary>
        private static volatile MsExcelProxy instance = null;
        /// <summary>Memorizza la cultura corrente</summary>
        private CultureInfo memCultureInfo = null;
        /// <summary>Memorizza lo stato di visibilità dell'applicazione</summary>
        private bool applicationVisible = false;
        #endregion

        #region Costruttori
        /// <summary>
        /// Costruttore privato non permette l'istanziamento diretto di questa classe.
        /// </summary>
        private MsExcelProxy()
        { }
        #endregion

        #region Proprietà
        /// <summary>Applicazione Excel</summary>
        private E.Application excelApplication = null;
        /// <summary>
        /// Restituisce l'applicazione Excel
        /// </summary>
        public E.Application ExcelApplication { get { return this.excelApplication; } }
        #endregion

        #region Metodi pubblici statici
        /// <summary>
        /// Restituisce sempre la stessa istanza della classe.
        /// </summary>
        public static MsExcelProxy GetInstance()
        {
            // se l'istanza non è ancora stata allocata
            if (instance == null)
            {
                lock (syncRoot)
                {
                    // ottengo una nuova istanza
                    if (instance == null)
                        instance = new MsExcelProxy();
                }
            }
            // restituisco sempre la stessa istanza 
            return instance;
        }
        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Aggancia l'applicazione 3DViaComposer
        /// </summary>
        /// <param name="applicationVisible">Determina se l'applicazione è visibile meno</param>
        /// <returns>True se l'applicazione era già aperta, altrimenti false</returns>
        public bool AttachApp(bool applicationVisible)
        {
            bool retVal = this.AttachApp();
            this.applicationVisible = applicationVisible;
            if (retVal)
                // imposto la visibilità dell'applicazione excel.
                this.excelApplication.Visible = applicationVisible;
            return retVal;
        }


        /// <summary>
        /// Aggancia l'applicazione 3DViaComposer
        /// </summary>
        /// <returns>True se l'applicazione era già aperta, altrimenti false</returns>
        public bool AttachApp()
        {
            bool retVal = false;
            if (excelApplication == null)
                try
                {
                    excelApplication = (E.Application)Marshal.GetActiveObject("Excel.Application");
                    retVal = true;
                }
                catch { }

            if (excelApplication == null)
                excelApplication = new E.Application();

            // messaggio di errore se l'esecuzione di excel fallisce
            if (excelApplication == null)
                throw new Exception("ERROR - Impossibile avviare Microsoft Excel");

            return retVal;
        }

        /// <summary>
        /// Sgancia l'applicazione 3DViaComposer
        /// </summary>
        /// <param name="quit">Determina se chiudere o meno l'applicazione</param>
        /// <param name="save">Determina se salvare o meno le modifiche</param>
        public void DetachApp(bool quit, bool save)
        {
            try
            {
                if (this.excelApplication == null) return;
                if (save)
                    foreach (E.Workbook workbook in this.excelApplication.Workbooks)
                    {
                        if (!workbook.Saved)
                            workbook.Save();
                        this.Close(workbook);
                    }
                if (quit)
                    this.excelApplication.Quit();

                if (this.memCultureInfo != null)
                    Thread.CurrentThread.CurrentCulture = this.memCultureInfo;
                this.excelApplication = null;
            }
            catch { }
        }

        /// <summary>
        /// <para>Metodo per aprire un file esistente di Excel</para>
        /// </summary>
        /// <param name="pathFile">Full path del file .xls da aprire</param>
        /// <returns>True se il file esiste e viene aperto con successo</returns>
        public E.Workbook OpenXLS(string pathFile)
        {
            E.Workbook retVal = null;
            if (!File.Exists(pathFile)) return retVal;
            try
            {
                retVal = this.excelApplication.Workbooks.Open(pathFile, true, false, 1, "", "", false, E.XlPlatform.xlWindows, "", false, false, 0, false, false, E.XlCorruptLoad.xlNormalLoad);
            }
            catch
            {
                this.memCultureInfo = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                retVal = this.excelApplication.Workbooks.Open(pathFile, true, false, 1, "", "", false, E.XlPlatform.xlWindows, "", false, false, 0, false, false, E.XlCorruptLoad.xlNormalLoad);
            }
            this.excelApplication.Visible = this.applicationVisible;
            return retVal;
        }

        /// <summary>
        /// Seleziona un range di celle contenenti valori partendo da una cella specificata
        /// </summary>
        /// <param name="workbook">Documento excel</param>
        /// <param name="sheetIndex">Foglio di lavoro</param>
        /// <param name="row">Numero riga cella di partenza</param>
        /// <param name="column">Numero colonna cella di partenza</param>
        /// <returns>Range ottenuto</returns>
        public E.Range SelectRangeWithValues(E.Workbook workbook, int sheetIndex, int row, int column)
        {
            E.Worksheet workSheet = (E.Worksheet)workbook.Worksheets[sheetIndex];

            int r = row;
            int c = column;

            // conto le colonne
            while (this.GetValueCell(workbook, sheetIndex, row, c) != null)
                c++;
            c--;

            // conto le righe
            while (this.GetValueCell(workbook, sheetIndex, r, column) != null)
                r++;
            r--;

            E.Range range = (E.Range)workSheet.Range[workSheet.Cells[row, column], workSheet.Cells[r, c]];
            return range;
        }

        /// <summary>
        /// Seleziona un range di celle contenenti valori partendo da una cella specificata
        /// </summary>
        /// <param name="workbook">Documento excel</param>
        /// <param name="sheetIndex">Foglio di lavoro</param>
        /// <param name="row">Numero riga cella di partenza</param>
        /// <param name="column">Numero colonna cella di partenza</param>
        /// <returns>Range ottenuto</returns>
        public E.Range SelectRangeUntilToEND(E.Workbook workbook, int sheetIndex, int row, int column, string endtag)
        {
            E.Worksheet workSheet = (E.Worksheet)workbook.Worksheets[sheetIndex];

            int r = row;
            int c = column;

            // conto le colonne
            while (this.GetValueCell(workbook, sheetIndex, row, c) != null)
                c++;
            c--;

            // conto le righe
            bool exit = false;
            while (!exit)
            {
                object val = this.GetValueCell(workbook, sheetIndex, r, column);
                if (val != null)
                {
                    string text = val.ToString();
                    if (text.ToUpper() == endtag)
                        exit = true;
                }
                r++;
            }
            r-=2;

            E.Range range = (E.Range)workSheet.Range[workSheet.Cells[row, column], workSheet.Cells[r, c]];
            return range;
        }

        /// <summary>
        /// Restituisce il valore di una cella
        /// </summary>
        /// <param name="workbook">Documento excel</param>
        /// <param name="sheetIndex">Foglio di lavoro</param>
        /// <param name="row">Numero riga</param>
        /// <param name="column">Numero colonna</param>
        /// <returns>Valore della cella</returns>
        private object GetValueCell(E.Workbook workbook, int sheetIndex, int row, int column)
        {
            E.Worksheet workSheet = (E.Worksheet)workbook.Worksheets[sheetIndex];
            return ((E.Range)workSheet.Cells[row, column]).Value2;
        }

        /// <summary>
        /// Imposta la larghezza alle colonne in base al vettore passato
        /// </summary>
        /// <param name="workbook">Documento excel</param>
        /// <param name="sheetIndex">Foglio di lavoro</param>
        /// <param name="column">Numero della colonna da cui iniziare</param>
        /// <param name="colsWidths">Vettore con il valori delle larghezze</param>
        public void SetColumsWidth(E.Workbook workbook, int sheetIndex, int column, double[] colsWidths)
        {
            E.Worksheet workSheet = (E.Worksheet)workbook.Worksheets[sheetIndex];
            for (int i = 0; i < colsWidths.Length; i++)
                ((E.Range)workSheet.Columns[column + i]).ColumnWidth = colsWidths[i];
        }

        /// <summary>
        /// Scrive una riga di valori partendo dalla cella specificata
        /// </summary>
        /// <param name="workbook">Documento excel</param>
        /// <param name="sheetIndex">Foglio di lavoro</param>
        /// <param name="row">Numero riga</param>
        /// <param name="column">Numero colonna</param>
        /// <param name="values">Vettore di valori da scrivere</param>
        public void WriteRow(E.Workbook workbook, int sheetIndex, int row, int column, string[] values)
        {
            E.Worksheet workSheet = (E.Worksheet)workbook.Worksheets[sheetIndex];
            for (int i = 0; i < values.Length; i++)
                workSheet.Cells[row, i + column] = values[i];
        }

        /// <summary>
        /// Chiude il documento di excel
        /// </summary>
        /// <param name="workbook">Documento da chiudere</param>
        public void Close(E.Workbook workbook)
        {
            if (workbook != null)
            {
                this.excelApplication.Range["A1"].Copy();
                workbook.Saved = true;
                workbook.Close(false);
            }
        }
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion
    }
}
