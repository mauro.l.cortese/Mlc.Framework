﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Office.Excel
{    /// <summary>
     /// Enumerazione dei modi di allineamento verticale del testo di una cella Excel
     /// </summary>
    public enum CellVerticalAlignment
    {
        /// <summary>Testo allineato in alto</summary>
        Top,
        /// <summary>Testo giustificato</summary>
        Justify,
        /// <summary>Testo distribuito</summary>
        Distributed,
        /// <summary>Testo centrato</summary>
        Center,
        /// <summary>Testo in basso</summary>
        Bottom,
    }

    /// <summary>
    /// Enumerazione dei modi di allineamento verticale del testo di una cella Excel
    /// </summary>
    public enum CellHorizontalAlignment
    {
        /// <summary>Testo allineato a destra</summary>
        Right,
        /// <summary>Testo giustificato</summary>
        Justify,
        /// <summary>Testo distribuito</summary>
        Distributed,
        /// <summary>Testo centrato</summary>
        Center,
        /// <summary>Testo a sinistra</summary>
        Left,
    }
}
