﻿using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class RowStateHelper.
	/// Implements the <see cref="Component" />
	/// </summary>
	/// <seealso cref="Component" />
	[DesignerCategory("")]
	[Designer("")]
	public class GridViewRowStateHelper : Component
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GridViewRowStateHelper"/> class.
		/// </summary>
		public GridViewRowStateHelper() => this.AppearanceDisabledRow.Changed += appearanceDisabledRow_Changed;
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets the appearance disabled row.
		/// </summary>
		/// <value>The appearance disabled row.</value>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public AppearanceObject AppearanceDisabledRow { get; private set; } = new();

		/// <summary>
		/// The grid view
		/// </summary>
		private GridView gridView;
		/// <summary>
		/// Gets or sets the grid view.
		/// </summary>
		/// <value>The grid view.</value>
		public GridView GridView
		{
			get => this.gridView;
			set
			{
				detachEventHandler(this.gridView);
				this.gridView = value;
				attachEventHandler(value);
			}
		}

		/// <summary>
		/// Gets the disabled rows.
		/// </summary>
		/// <value>The disabled rows.</value>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public List<int> DisabledRows { get; private set; } = new();

		/// <summary>
		/// The expression
		/// </summary>
		private string expression;
		/// <summary>
		/// Gets or sets the expression.
		/// </summary>
		/// <value>The expression.</value>
		public string Expression
		{
			get => this.expression;
			set
			{
				if (this.expression != value)
				{
					this.expression = value;

					gridView.FormatConditions.Clear();
					StyleFormatCondition condition = new();
					condition.Condition = FormatConditionEnum.Expression;
					condition.Appearance.Assign(this.AppearanceDisabledRow);
					condition.Expression = this.Expression;
					condition.ApplyToRow = true;
					condition.Appearance.Options.HighPriority = true;
					condition.Appearance.Options.UseBackColor = true;
					condition.Appearance.Options.UseFont = true;
					condition.Appearance.Options.UseForeColor = true;
					gridView.FormatConditions.Add(condition);
					gridView.LayoutChanged();
				}
			}
		}

		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Disables the row.
		/// </summary>
		/// <param name="dataSourceRowIndex">Index of the data source row.</param>
		public void DisableRow(int dataSourceRowIndex) => DisabledRows.Add(dataSourceRowIndex);


		/// <summary>
		/// Determines whether [is row disabled] [the specified data source row index].
		/// </summary>
		/// <param name="dataSourceRowIndex">Index of the data source row.</param>
		/// <returns><c>true</c> if [is row disabled] [the specified data source row index]; otherwise, <c>false</c>.</returns>
		public bool IsRowDisabled(int dataSourceRowIndex) => DisabledRows.Contains(dataSourceRowIndex);

		/// <summary>
		/// Enables the row.
		/// </summary>
		/// <param name="dataSourceRowIndex">Index of the data source row.</param>
		public void EnableRow(int dataSourceRowIndex)
		{
			while (IsRowDisabled(dataSourceRowIndex))
				DisabledRows.Remove(dataSourceRowIndex);
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Detaches the event handler.
		/// </summary>
		/// <param name="view">The view.</param>
		private void detachEventHandler(GridView view)
		{
			if (view != null)
			{
				view.RowCellStyle -= view_RowCellStyle;
				view.ShowingEditor -= view_ShowingEditor;
			}

		}

		/// <summary>
		/// Attaches the event handler.
		/// </summary>
		/// <param name="view">The view.</param>
		private void attachEventHandler(GridView view)
		{
			if (view != null)
			{
				view.RowCellStyle += view_RowCellStyle;
				view.ShowingEditor += view_ShowingEditor;
			}
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Handles the Changed event of the appearanceDisabledRow control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void appearanceDisabledRow_Changed(object sender, EventArgs e)
		{
			if (this.GridView == null) return;
			for (int i = 0; i < DisabledRows.Count; i++)
				this.GridView.RefreshRow(this.GridView.GetRowHandle(DisabledRows[i]));

		}

		/// <summary>
		/// Handles the ShowingEditor event of the view control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
		private void view_ShowingEditor(object sender, CancelEventArgs e)
		{
			int index = gridView.GetDataSourceRowIndex(gridView.FocusedRowHandle);
			if (gridView.FormatConditions.Count > 0)
				e.Cancel = IsRowDisabled(GridView.GetDataSourceRowIndex(GridView.FocusedRowHandle)) || gridView.FormatConditions[0].CheckValue(null, null, index);
		}

		/// <summary>
		/// Handles the RowCellStyle event of the view control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RowCellStyleEventArgs"/> instance containing the event data.</param>
		private void view_RowCellStyle(object sender, RowCellStyleEventArgs e)
		{
			Console.WriteLine(DateTime.Now.Millisecond);
			if (IsRowDisabled(GridView.GetDataSourceRowIndex(e.RowHandle)))
				e.Appearance.Assign(AppearanceDisabledRow);
		}

		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion
	}
}
