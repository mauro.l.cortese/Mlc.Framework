﻿using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Mlc.Data;
using System.Reflection;
using System;
using System.Data;
using Mlc.Shell;
namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class GridTools.
	/// </summary>
	public static class GridTools
    {
		/// <summary>
		/// Combines the data tables.
		/// ASSUMPTION: All tables must have the same columns
		/// </summary>
		/// <param name="tables">The tables.</param>
		/// <returns>DataTable.</returns>
		public static DataTable GetTableFromSelection(this GridView gridView)
		{
			if (gridView.GridControl.DataSource is DataTable dataTable)
			{
				DataTable retTable = dataTable.Clone();
				try
				{
					int[] selectedRows = gridView.GetSelectedRows();
					for (int i = 0; i < selectedRows.Length; i++)
						retTable.ImportRow(gridView.GetDataRow(selectedRows[i]));
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				return retTable;
			}
			else
				return null;
		}

	}
}