﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-24-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-24-2017
// ***********************************************************************
// <copyright file="GridPopUpMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraBars;
namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class GridPopUpMgr.
    /// </summary>
    public class GridPopUpMgr
    {
        /// <summary>
        /// The popup menu
        /// </summary>
        private PopupMenu popupMenu;

        /// <summary>
        /// Initializes a new instance of the <see cref="GridPopUpMgr"/> class.
        /// </summary>
        /// <param name="gridView">The grid view.</param>
        /// <param name="popupMenu">The popup menu.</param>
        public GridPopUpMgr(GridView gridView, PopupMenu popupMenu)
        {
            gridView.PopupMenuShowing += gridView_PopupMenuShowing;
            this.popupMenu = popupMenu;
        }

        /// <summary>
        /// Handles the PopupMenuShowing event of the GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PopupMenuShowingEventArgs"/> instance containing the event data.</param>
        private void gridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.HitInfo.InDataRow)
            {
                GridView gridView = (GridView)sender;
                this.OnSetContextMenuRequest(new EventArgsSetContextMenuRequest(gridView.GetDataRow(gridView.FocusedRowHandle)));
                this.popupMenu.ShowPopup(this.popupMenu.Manager, gridView.GridControl.PointToScreen(e.Point));
            }
        }

        #region Dichiarazione Evento SetContextMenuRequest
        /// <summary>
        /// Definizione dell'evento.
        /// </summary>
        public event EventHandlerSetContextMenuRequest SetContextMenuRequest;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnSetContextMenuRequest(EventArgsSetContextMenuRequest e)
        {
            // Se ci sono ricettori in ascolto ...
            if (SetContextMenuRequest != null)
                // viene lanciato l'evento
                SetContextMenuRequest(this, e);
        }
        #endregion

    }
}
