﻿using System.IO;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid;

using Mlc.Shell.IO;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Gestisce l'aspetto dei controlli GridView
    /// </summary>
    public sealed class GridAppearance
    {
        /// <summary>GridView da gestire</summary>
        private GridView gridView = null;

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="gridView">Istanza GridView da gestire</param>
        public GridAppearance(GridView gridView)
        {
            this.gridView = gridView;
        }

        /// <summary>
        /// Imposta i font
        /// </summary>
        public void SetFonts()
        {
            SetFonts(new Font("Arial", 9F));
        }

        /// <summary>
        /// Imposta i font
        /// </summary>
        public void SetFonts(Font font)
        {
            GridViewAppearances app = gridView.Appearance;
            app.ColumnFilterButton.Font = font;
            app.FilterPanel.Font = font;
            app.FocusedRow.Font = font;
            app.FooterPanel.Font = font;
            app.GroupFooter.Font = font;
            app.GroupPanel.Font = font;
            app.GroupRow.Font = new Font(font, FontStyle.Bold);
            app.HeaderPanel.Font = font;
            app.Preview.Font = font;
            app.Row.Font = font;
            app.TopNewRow.Font = font;

            app.ColumnFilterButton.Options.UseFont = true;
            app.FilterPanel.Options.UseFont = true;
            app.FocusedRow.Options.UseFont = true;
            app.FooterPanel.Options.UseFont = true;
            app.GroupFooter.Options.UseFont = true;
            app.GroupPanel.Options.UseFont = true;
            app.GroupRow.Options.UseFont = true;
            app.HeaderPanel.Options.UseFont = true;
            app.Preview.Options.UseFont = true;
            app.Row.Options.UseFont = true;
            app.TopNewRow.Options.UseFont = true;
        }

        /// <summary>
        /// Carica il layout del controllo GridView
        /// </summary>
        /// <param name="file">Fullpath del file da cui caricare il layout</param>
        public void LoadLayout(string file)
        {
            if (File.Exists(file))
                this.gridView.RestoreLayoutFromXml(file);
        }

        /// <summary>
        /// Salva il layout del controllo GridView
        /// </summary>
        /// <param name="file">Fullpath del file in cui salvare il layout</param>
        public void SaveLayout(string file)
        {
            FileSystemMgr fso = new FileSystemMgr(file);
            if (!fso.Exists)
                fso.Create(FileSystemTypes.File);
            this.gridView.SaveLayoutToXml(fso.Path);
        }

        /// <summary>
        /// Restituisce l'accesso alle opzioni di visualizzazione
        /// </summary>
        public GridOptionsView OptionsView { get { return this.gridView.OptionsView; } }

        /// <summary>
        /// Imposta l'altezza delle righe
        /// </summary>
        public int RowHeight
        {
            get { return this.gridView.RowHeight; }
            set
            {
                this.gridView.RowHeight = value;
                this.gridView.OptionsView.RowAutoHeight = value == -1 ? true : false;
            }
        }
    }
}
