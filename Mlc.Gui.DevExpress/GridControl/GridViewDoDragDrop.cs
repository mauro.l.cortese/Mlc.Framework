﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 08-19-2021
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 08-19-2021
// ***********************************************************************
// <copyright file="GridViewDoDragDrop.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;


namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class GridTools.
	/// </summary>
	public class GridViewDoDragDrop
	{

		private GridHitInfo downHitInfo = null;

		/// <summary>
		/// Gets the grid view.
		/// </summary>
		/// <value>The grid view.</value>
		public GridView GridView { get; private set; } = null;

		/// <summary>
		/// Gets a value indicating whether this <see cref="GridViewDoDragDrop"/> is enable.
		/// </summary>
		/// <value><c>true</c> if enable; otherwise, <c>false</c>.</value>
		public bool Enable { get; private set; } = false;

		/// <summary>
		/// Gets a value indicating whether this <see cref="GridViewDoDragDrop"/> is enable.
		/// </summary>
		/// <value><c>true</c> if enable; otherwise, <c>false</c>.</value>
		public bool MultiSelection { get; set; } = true;

		/// <summary>
		/// Initializes a new instance of the <see cref="GridViewDoDragDrop"/> class.
		/// </summary>
		/// <param name="gridView">The grid view.</param>
		public GridViewDoDragDrop(GridView gridView)
		{
			this.GridView = gridView;
		}

		/// <summary>
		/// Enables the do drag.
		/// </summary>
		public void EnableDoDrag()
		{
			if (this.GridView != null)
			{
				this.GridView.MouseDown += gridView_MouseDown;
				this.GridView.MouseMove += gridView_MouseMove;
				this.Enable = true;
			}
			else
				this.Enable = false;
		}


		/// <summary>
		/// Disables the do drag.
		/// </summary>
		public void DisableDoDrag()
		{
			if (this.GridView != null)
			{
				this.GridView.MouseDown += gridView_MouseDown;
				this.GridView.MouseMove += gridView_MouseMove;
			}
			this.Enable = false;
		}

		/// <summary>
		/// Handles the MouseDown event of the gridView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		private void gridView_MouseDown(object sender, MouseEventArgs e)
		{
			GridView view = sender as GridView;
			this.downHitInfo = null;
			GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
			if (Control.ModifierKeys != Keys.None) return;
			if (e.Button == MouseButtons.Left && hitInfo.RowHandle >= 0)
				this.downHitInfo = hitInfo;
		}

		/// <summary>
		/// Handles the MouseMove event of the gridView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		private void gridView_MouseMove(object sender, MouseEventArgs e)
		{
			GridView gridView = sender as GridView;
			if (e.Button == MouseButtons.Left && this.downHitInfo != null)
			{
				Size dragSize = SystemInformation.DragSize;
				Rectangle dragRect = new Rectangle(
					new Point(
						this.downHitInfo.HitPoint.X - dragSize.Width / 2,  // X
						this.downHitInfo.HitPoint.Y - dragSize.Height / 2) // Y
					, dragSize);

				if (!dragRect.Contains(new Point(e.X, e.Y)))
				{
					DataRow currentRow = gridView.GetDataRow(this.downHitInfo.RowHandle);
					EventArgsBeforStartDrag eventArgsBeforStartDrag = new EventArgsBeforStartDrag(gridView, currentRow, this.MultiSelection);
					this.OnBeforStartDrag(eventArgsBeforStartDrag);

					if (eventArgsBeforStartDrag.Paths != null && eventArgsBeforStartDrag.Paths.Length != 0)
						gridView.GridControl.DoDragDrop(new DataObject(DataFormats.FileDrop, eventArgsBeforStartDrag.Paths), DragDropEffects.Copy);
					else
						gridView.GridControl.DoDragDrop(currentRow, DragDropEffects.Copy);
					this.downHitInfo = null;
					DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
					this.OnAfterDragDrop(new EventArgs());
				}
			}
		}

		#region Event BeforStartDrag
		/// <summary>
		/// Event BeforStartDrag.
		/// </summary>
		public event EventHandlerBeforStartDrag BeforStartDrag;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnBeforStartDrag(EventArgsBeforStartDrag e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.BeforStartDrag?.Invoke(this, e);
		}
		#endregion



		#region Event AfterDragDrop
		/// <summary>
		/// Event AfterDragDrop.
		/// </summary>
		public event EventHandler AfterDragDrop;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnAfterDragDrop(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.AfterDragDrop?.Invoke(this, e);
		}
		#endregion



		#region BeforStartDrag event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerBeforStartDrag(object sender, EventArgsBeforStartDrag e);

		/// <summary>
		/// BeforStartDrag arguments.
		/// </summary>
		public class EventArgsBeforStartDrag : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsBeforStartDrag"/> class.
			/// </summary>
			/// <param name="gridView">The grid view.</param>
			/// <param name="currentRow">The current row.</param>
			public EventArgsBeforStartDrag(GridView gridView, DataRow currentRow, bool multiSelection)
			{
				this.GridView = gridView;
				this.CurrentRow = currentRow;
				this.MultiSelection = multiSelection;
			}
			#endregion

			#region Properties
			/// <summary>
			/// Gets the grid view.
			/// </summary>
			/// <value>The grid view.</value>
			public GridView GridView { get; }

			/// <summary>
			/// Gets the current row.
			/// </summary>
			/// <value>The current row.</value>
			public DataRow CurrentRow { get; }

			/// <summary>
			/// Gets or sets the path.
			/// </summary>
			/// <value>The path.</value>
			public String[] Paths { get; set; }

			/// <summary>
			/// Gets a value indicating whether [multi selection].
			/// </summary>
			/// <value><c>true</c> if [multi selection]; otherwise, <c>false</c>.</value>
			public bool MultiSelection { get; }
			#endregion
		}
		#endregion
	}
}