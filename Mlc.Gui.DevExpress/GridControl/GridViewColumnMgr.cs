﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 05-06-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 12-09-2021
// ***********************************************************************
// <copyright file="GridViewColumnMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList.Columns;
using Mlc.Shell;
using Mlc.Data;
using System;
using System.Data;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Classe Singleton pattern
	/// </summary>
	public class GridViewColumnMgr : InPlaceRepository
	{
		#region Costanti
		#endregion

		#region Enumerazioni
		#endregion

		#region Campi
		/// <summary>
		/// Variabile per safe thread
		/// </summary>
		private static object syncRoot = new Object();
		/// <summary>
		/// Istanza della classe
		/// </summary>
		private static volatile GridViewColumnMgr instance = null;
		#endregion

		#region Costruttori
		/// <summary>
		/// Costruttore privato non permette l'istanziamento diretto di questa classe.
		/// </summary>
		private GridViewColumnMgr()
		{ }
		#endregion

		#region Proprietà
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// Restituisce sempre la stessa istanza della classe.
		/// </summary>
		/// <returns>GridViewColumnMgr.</returns>
		public static GridViewColumnMgr GetInstance()
		{
			// se l'istanza non è ancora stata allocata
			if (instance == null)
			{
				lock (syncRoot)
				{
					// ottengo una nuova istanza
					if (instance == null)
						instance = new GridViewColumnMgr();
				}
			}
			// restituisco sempre la stessa istanza 
			return instance;
		}

		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">if set to <c>true</c> [allow focus].</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		public static void FormatColumn(GridColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment)
		{
			FormatColumn(column, allowFocus, horzAlignment, 0);
		}

		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">The allow focus.</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		/// <param name="width">The width.</param>
		public static void FormatColumn(GridColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment, int width)
		{
			FormatColumn(column, allowFocus, horzAlignment, width, false, true);
		}

		/// <summary>
		/// Formats the specified column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">if set to <c>true</c> [allow focus].</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public static void FormatColumn(GridColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment, int width, bool fixedWidth, bool allowSize)
		{
			if (column == null) return;
			column.OptionsColumn.AllowFocus = allowFocus;
			column.AppearanceCell.TextOptions.HAlignment = horzAlignment;
			column.AppearanceHeader.TextOptions.HAlignment = horzAlignment;
			if (width != 0)
				column.Width = width;
			column.OptionsColumn.FixedWidth = fixedWidth;
			column.OptionsColumn.AllowSize = allowSize;
		}

		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="formatColumnData">The format column data.</param>
		public static void FormatColumn(GridViewColumnFormatData formatColumnData)
		{
			if (formatColumnData.Column != null)
			{
				formatColumnData.Column.OptionsColumn.AllowFocus = formatColumnData.AllowFocus;
				formatColumnData.Column.OptionsColumn.ReadOnly = formatColumnData.ReadOnly;
				formatColumnData.Column.AppearanceCell.TextOptions.HAlignment = formatColumnData.HorzAlignment;
				formatColumnData.Column.AppearanceHeader.TextOptions.HAlignment = formatColumnData.HorzAlignment;
				formatColumnData.Column.Fixed = formatColumnData.Fixed;
				formatColumnData.Column.OptionsColumn.AllowSize = formatColumnData.AllowSize;
				formatColumnData.Column.OptionsColumn.FixedWidth = formatColumnData.FixedWidth;
				formatColumnData.Column.Visible = formatColumnData.Visible;
				if (formatColumnData.Width != 0)
					formatColumnData.Column.Width = formatColumnData.Width;
				if (formatColumnData.Format!= Format.None)
				{
					switch (formatColumnData.Format)
					{
						case Format.Currency:
							formatColumnData.Column.DisplayFormat.FormatType = FormatType.Numeric;
							formatColumnData.Column.DisplayFormat.FormatString = "C5";
							break;
						case Format.Percent:
							formatColumnData.Column.DisplayFormat.FormatType = FormatType.Numeric;
							formatColumnData.Column.DisplayFormat.FormatString = "P";
							break;
						case Format.Date:
							break;
						default:
							break;
					}
				}
			}
		}


		/// <summary>
		/// Sets the column for icon.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForIcon(GridColumn column, string keyRepository, int width, bool fixedWidth, bool allowSize)
		{
			try
			{
				column.ColumnEdit = base.GetRepository(keyRepository, typeof(RepositoryItemImageComboBox));
				column.Width = width;
				column.OptionsColumn.FixedWidth = fixedWidth;
				column.OptionsColumn.AllowSize = allowSize;
				column.OptionsColumn.AllowFocus = false;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}


		/// <summary>
		/// Sets the column forlook up.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="table">The table.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForlookUp(GridColumn column, DataTable table, bool fixedWidth, bool allowSize)
		{
			this.SetColumnForlookUp(column, column.FieldName, table, column.Width, fixedWidth, allowSize);
		}

		/// <summary>
		/// Sets the column for combo.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="table">The table.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForlookUp(GridColumn column, string keyRepository, DataTable table, int width, bool fixedWidth, bool allowSize)
		{
			try
			{
				column.ColumnEdit = base.GetRepository(keyRepository, typeof(RepositoryItemLookUpEdit), table);
				column.Width = width;
				column.OptionsColumn.FixedWidth = fixedWidth;
				column.OptionsColumn.AllowSize = allowSize;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

		}

		/// <summary>
		/// Sets the column for currency.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="editMask">The edit mask.</param>
		public void SetColumnForCurrency(GridColumn column, string editMask)
		{
			this.SetColumnForCurrency(column, column.FieldName, editMask);
		}

		/// <summary>
		/// Sets the column for currency.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="editMask">The edit mask.</param>
		public void SetColumnForCurrency(GridColumn column, string keyRepository, string editMask)
		{
			column.ColumnEdit = base.GetRepository(keyRepository, typeof(RepositoryItemTextEdit), editMask);
		}



		//    /// <summary>
		//    /// Gets the repository.
		//    /// </summary>
		//    /// <param name="keyRepository">The key repository.</param>
		//    /// <returns>RepositoryItem.</returns>
		//    /// <exception cref="System.NotImplementedException"></exception>
		//    public RepositoryItem GetRepository(string keyRepository, Type typeRepository, DataTable table)
		//    {
		//        RepositoryItem retVal = null;

		//        if (!repCollection.ContainsKey(keyRepository))
		//        {
		//            switch (typeRepository.Name)
		//            {
		//                case "RepositoryItemImageComboBox":
		//                    RepositoryItemImageComboBox riicb = new RepositoryItemImageComboBox();
		//                    riicb.AutoHeight = false;
		//                    riicb.Name = keyRepository;
		//                    repCollection.Add(keyRepository, riicb);
		//                    retVal = riicb;

		//                    string exeFolder = string.Format("{0}\\Resources\\Repositories\\{1}", Environment.CurrentDirectory, keyRepository);
		//                    ImageCollectionMgr icm = new ImageCollectionMgr();

		//                    this.listImagesSmallCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(20, 20), exeFolder + "\\Small"));
		//                    riicb.SmallImages = this.listImagesSmallCollection[keyRepository];

		//                    this.listImagesLargeCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(32, 32), exeFolder + "\\Large"));
		//                    riicb.LargeImages = this.listImagesLargeCollection[keyRepository];

		//                    foreach (ImageCollectionMgr.IconNameParser inp in icm.IconParserList)
		//                        riicb.Items.Add(new ImageComboBoxItem(inp.name, inp.value, inp.index));
		//                    break;
		//	case "RepositoryItemLookUpEdit":
		//		RepositoryItemLookUpEdit rilue = new RepositoryItemLookUpEdit();
		//		rilue.Name = keyRepository;
		//		rilue.AutoHeight = false;
		//		rilue.DataSource = table;
		//		rilue.DisplayMember = "Value";
		//		rilue.ValueMember = "Key";
		//		rilue.PopulateColumns();
		//		repCollection.Add(keyRepository, rilue);
		//		retVal = rilue;
		//		break;
		//}
		//        }

		//        retVal = repCollection[keyRepository];
		//        return retVal;
		//    }


		#endregion

		#region Handlers eventi
		#endregion

		#region Metodi privati
		#endregion

		#region Definizione eventi
		#endregion

		#region Tipi nidificati
		#endregion
	}


	/// <summary>
	/// Class GridViewColumnFormatData.
	/// Implements the <see cref="Mlc.Gui.DevExpressTools.ColumnFormatDataBase" />
	/// </summary>
	/// <seealso cref="Mlc.Gui.DevExpressTools.ColumnFormatDataBase" />
	public class GridViewColumnFormatData : ColumnFormatDataBase
	{
		/// <summary>
		/// Gets or sets the column.
		/// </summary>
		/// <value>The column.</value>
		public GridColumn Column { get; set; }
		/// <summary>
		/// Gets or sets the fixed.
		/// </summary>
		/// <value>The fixed.</value>
		public DevExpress.XtraGrid.Columns.FixedStyle Fixed { get; set; } = DevExpress.XtraGrid.Columns.FixedStyle.None;
	}



	/// <summary>
	/// Class TreeListColumnFormatData.
	/// Implements the <see cref="Mlc.Gui.DevExpressTools.ColumnFormatDataBase" />
	/// </summary>
	/// <seealso cref="Mlc.Gui.DevExpressTools.ColumnFormatDataBase" />
	public class TreeListColumnFormatData: ColumnFormatDataBase
	{
		/// <summary>
		/// Gets or sets the column.
		/// </summary>
		/// <value>The column.</value>
		public TreeListColumn Column { get; set; }
		/// <summary>
		/// Gets or sets the fixed.
		/// </summary>
		/// <value>The fixed.</value>
		public DevExpress.XtraTreeList.Columns.FixedStyle Fixed { get; set; } = DevExpress.XtraTreeList.Columns.FixedStyle.None;
	}


	/// <summary>
	/// Class ColumnFormatDataBase.
	/// </summary>
	public class ColumnFormatDataBase
	{
		/// <summary>
		/// Gets or sets a value indicating whether [allow focus].
		/// </summary>
		/// <value><c>true</c> if [allow focus]; otherwise, <c>false</c>.</value>
		public bool AllowFocus { get; set; } = false;
		/// <summary>
		/// Gets or sets the horz alignment.
		/// </summary>
		/// <value>The horz alignment.</value>
		public DevExpress.Utils.HorzAlignment HorzAlignment { get; set; } = DevExpress.Utils.HorzAlignment.Default;
		/// <summary>
		/// Gets or sets a value indicating whether [allow size].
		/// </summary>
		/// <value><c>true</c> if [allow size]; otherwise, <c>false</c>.</value>
		public bool AllowSize { get; set; } = true;
		/// <summary>
		/// Gets or sets a value indicating whether [fixed width].
		/// </summary>
		/// <value><c>true</c> if [fixed width]; otherwise, <c>false</c>.</value>
		public bool FixedWidth { get;  set; } = false;
		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>The width.</value>
		public int Width { get;  set; } = 0;
		/// <summary>
		/// Gets or sets a value indicating whether [read only].
		/// </summary>
		/// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
		public bool ReadOnly { get; set; } = true;
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ColumnFormatDataBase"/> is visible.
		/// </summary>
		/// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
		public bool Visible { get; set; } = true;

		/// <summary>
		/// Gets or sets the format.
		/// </summary>
		/// <value>The format.</value>
		public Format Format { get; set; } = Format.None;

	}
}
