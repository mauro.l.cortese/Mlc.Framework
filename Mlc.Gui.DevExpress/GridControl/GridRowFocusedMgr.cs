﻿using DevExpress.XtraGrid.Views.Grid;
using Mlc.Shell;
using System;
using System.Data;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Gestisce l'aspetto dei controlli GridView
	/// </summary>
	public class GridRowEventsMgr
	{

		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The memory row
		/// </summary>
		private int memRow = -1;
		#endregion

		#region Constructors
		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		public GridRowEventsMgr()
		{
		}

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="gridView">Istanza GridView da gestire</param>
		public GridRowEventsMgr(GridView gridView)
		{
			this.GridView = gridView;
		}

		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The reise event
		/// </summary>
		public bool RaiseEvent { get; set; } = true;

		/// <summary>
		/// The grid view
		/// </summary>
		private GridView gridView;
		/// <summary>
		/// Gets or sets the grid view.
		/// </summary>
		/// <value>The grid view.</value>
		public GridView GridView
		{
			get { return this.gridView; }
			set
			{
				this.gridView = value;
				if (this.gridView != null)
				{
					this.gridView.FocusedRowChanged += (sender, e) =>
					{
						if (!RaiseEvent || this.memRow == e.FocusedRowHandle) return;
						this.memRow = e.FocusedRowHandle;
						this.checkIfFocusedRowIsChanged();
					};
					this.gridView.MouseDown += (sender, e) =>
					{
						if (!RaiseEvent) return;
						if (memRow == -1 && this.gridView.RowCount != 0)
							this.checkIfFocusedRowIsChanged(this.gridView.CalcHitInfo(e.Location).RowHandle);
						this.OnViewFocused(new EventArgsFocusedRow(this.memRow));
					};
					this.gridView.DoubleClick += (sender, e) =>
					{
						if (!RaiseEvent) return;
						this.OnDoubleClick(new EventArgsFocusedRow(this.memRow));
					};
					this.gridView.RowCountChanged += (sender, e) =>
					{
						if (!RaiseEvent) return;
						this.OnRowCountChanged(new EventArgs());
					};
					this.gridView.SelectionChanged += (sender, e) =>
					{
						if (!RaiseEvent) return;
						this.OnRowSelectedChanged(new EventArgs());
					};
				}
			}
		}


		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Resets this instance.
		/// </summary>
		/// <exception cref="NotImplementedException"></exception>
		public void Reset() => this.memRow = -1;
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Checks if focused row is changed.
		/// </summary>
		/// <param name="focusedRowHandle">The focused row handle.</param>
		private void checkIfFocusedRowIsChanged(int rowHandle = -1)
		{
			int tmpRow = rowHandle;
			if (tmpRow > -1)
				memRow = tmpRow;
			else
				memRow = this.gridView.FocusedRowHandle > -1 ? this.gridView.FocusedRowHandle : -1;

			if (this.memRow < 0) return;
			this.OnCurrentDataRowChange(new EventArgsFocusedRow(this.memRow));
		}
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions

		#region Event ViewFocused                
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public event EventHandlerFocusedRow ViewFocused;

		/// <summary>
		/// Metodo per il lancio dell'evento.
		/// </summary>  
		/// <param name="e">Dati dell'evento</param>
		protected void OnViewFocused(EventArgsFocusedRow e)
		{
			// Se ci sono ricettori in ascolto ...
			// viene lanciato l'evento
			ViewFocused?.Invoke(this.gridView, e);
		}
		#endregion

		#region Event CurrentDataRowChange
		/// <summary>
		/// Definizione dell'evento.
		/// </summary>
		public event EventHandlerFocusedRow CurrentDataRowChange;

		/// <summary>
		/// Metodo per il lancio dell'evento.
		/// </summary>  
		/// <param name="e">Dati dell'evento</param>
		protected void OnCurrentDataRowChange(EventArgsFocusedRow e)
		{
			// Se ci sono ricettori in ascolto ...
			// viene lanciato l'evento
			CurrentDataRowChange?.Invoke(this.gridView, e);
		}
		#endregion

		#region Event DoubleClick                
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public event EventHandlerFocusedRow DoubleClick;

		/// <summary>
		/// Metodo per il lancio dell'evento.
		/// </summary>  
		/// <param name="e">Dati dell'evento</param>
		protected void OnDoubleClick(EventArgsFocusedRow e)
		{
			// Se ci sono ricettori in ascolto ...
			// viene lanciato l'evento
			DoubleClick?.Invoke(this.gridView, e);
		}
		#endregion

		#region Event ArgsFocusedRow
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public delegate void EventHandlerFocusedRow(object sender, EventArgsFocusedRow e);

		/// <summary>
		/// Classe per gli argomenti della generazione di un evento.
		/// </summary>
		public class EventArgsFocusedRow : System.EventArgs
		{
			#region Costruttori
			/// <summary>
			/// Inizilizza una nuova istanza
			/// </summary>
			/// <param name="campo"></param>
			public EventArgsFocusedRow(int focusedRowHandle) => this.FocusedRowHandle = focusedRowHandle;
			#endregion

			#region Proprietà
			/// <summary>
			/// Restituisce o imposta il ...
			/// </summary>
			public int FocusedRowHandle { get; set; }
			#endregion
		}
		#endregion


		#region Event RowCountChanged
		/// <summary>
		/// Event RowCountChanged.
		/// </summary>
		public event EventHandler RowCountChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnRowCountChanged(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.RowCountChanged?.Invoke(this, e);
		}
		#endregion


		#region Event RowCountChanged
		/// <summary>
		/// Event RowCountChanged.
		/// </summary>
		public event EventHandler RowSelectedChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnRowSelectedChanged(EventArgs e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.RowSelectedChanged?.Invoke(this, e);
		}
		#endregion

		#endregion

		#region Embedded Types
		#endregion
	}
}