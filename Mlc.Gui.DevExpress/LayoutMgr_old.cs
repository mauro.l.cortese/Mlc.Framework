﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-22-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-22-2017
// ***********************************************************************
// <copyright file="LayoutMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mlc.Shell;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;

using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using DevExpress.XtraBars.Docking;
using Mlc.Gui.DevExpressTools;
using DevExpress.XtraBars;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class LayoutMgr.
    /// </summary>
    public class LayoutMgr
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        #endregion

        #region Public Properties
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Saves the layout.
        /// </summary>
        /// <param name="devControls">The dev controls.</param>
        public void SaveLayout(List<object> devControls, string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (Directory.Exists(folder))
                foreach (object obj in devControls)
                    if (obj is GridView)
                        new GridAppearance((GridView)obj).SaveLayout(string.Format("{0}\\{1}.layout.settings", folder, ((GridView)obj).Name));
                    else if (obj is TreeList)
                        ((TreeList)obj).SaveLayoutToXml(string.Format("{0}\\{1}.layout.settings", folder, ((TreeList)obj).Name));
                    else if (obj is LayoutControl)
                        ((LayoutControl)obj).SaveLayoutToXml(string.Format("{0}\\{1}.layout.settings", folder, ((LayoutControl)obj).Name));
                    else if (obj is DockManager)
                        ((DockManager)obj).SaveLayoutToXml(string.Format("{0}\\{1}.layout.settings", folder, "DockManager"));
                    else if (obj is BarManager)
                        ((BarManager)obj).SaveLayoutToXml(string.Format("{0}\\{1}.layout.settings", folder, "BarManager"));
        }

        /// <summary>
        /// Loads the layout.
        /// </summary>
        /// <param name="devControls">The dev controls.</param>
        /// <param name="folder">The folder.</param>
        public void LoadLayout(List<object> devControls, string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (Directory.Exists(folder))
                foreach (object obj in devControls)
                    if (obj is GridView)
                        try { new GridAppearance((GridView)obj).LoadLayout(string.Format("{0}\\{1}.layout.settings", folder, ((GridView)obj).Name)); }
                        catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                    else if (obj is TreeList)
                        try { ((TreeList)obj).RestoreLayoutFromXml(string.Format("{0}\\{1}.layout.settings", folder, ((TreeList)obj).Name)); }
                        catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                    else if (obj is LayoutControl)
                        try { ((LayoutControl)obj).RestoreLayoutFromXml(string.Format("{0}\\{1}.layout.settings", folder, ((LayoutControl)obj).Name)); }
                        catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                    else if (obj is DockManager)
                        try { ((DockManager)obj).RestoreLayoutFromXml(string.Format("{0}\\{1}.layout.settings", folder, "DockManager")); }
                        catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                    else if (obj is BarManager)
                        try {
                            BarManager bar = (BarManager)obj;
                            bar.RestoreLayoutFromXml(string.Format("{0}\\{1}.layout.settings", folder, "BarManager")); }
                        catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }
}
