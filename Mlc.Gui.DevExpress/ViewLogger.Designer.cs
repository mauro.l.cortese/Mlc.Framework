﻿namespace Mlc.Gui.DevExpressTools
{
    partial class ViewLogger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logBindingSource = new System.Windows.Forms.BindingSource();
            this.loggerBindingSource = new System.Windows.Forms.BindingSource();
            this.ViewLoggerlayoutControl1ConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.richEditControlMessage = new DevExpress.XtraRichEdit.RichEditControl();
            this.richEditControlStackTrace = new DevExpress.XtraRichEdit.RichEditControl();
            this.gridControlLog = new DevExpress.XtraGrid.GridControl();
            this.gridViewLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCmdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpentTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.splitContainerControl1item = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Panel1item = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.logBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewLoggerlayoutControl1ConvertedLayout)).BeginInit();
            this.ViewLoggerlayoutControl1ConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel1item)).BeginInit();
            this.SuspendLayout();
            // 
            // logBindingSource
            // 
            this.logBindingSource.DataSource = typeof(Mlc.Shell.IO.Log);
            // 
            // loggerBindingSource
            // 
            this.loggerBindingSource.DataSource = typeof(Mlc.Shell.IO.Logger);
            // 
            // ViewLoggerlayoutControl1ConvertedLayout
            // 
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.richEditControlMessage);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.richEditControlStackTrace);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.gridControlLog);
            this.ViewLoggerlayoutControl1ConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewLoggerlayoutControl1ConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.ViewLoggerlayoutControl1ConvertedLayout.Name = "ViewLoggerlayoutControl1ConvertedLayout";
            this.ViewLoggerlayoutControl1ConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-1077, 280, 650, 400);
            this.ViewLoggerlayoutControl1ConvertedLayout.Root = this.layoutControlGroup1;
            this.ViewLoggerlayoutControl1ConvertedLayout.Size = new System.Drawing.Size(1077, 503);
            this.ViewLoggerlayoutControl1ConvertedLayout.TabIndex = 4;
            // 
            // richEditControlMessage
            // 
            this.richEditControlMessage.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControlMessage.Location = new System.Drawing.Point(6, 364);
            this.richEditControlMessage.Name = "richEditControlMessage";
            this.richEditControlMessage.Size = new System.Drawing.Size(669, 133);
            this.richEditControlMessage.TabIndex = 6;
            // 
            // richEditControlStackTrace
            // 
            this.richEditControlStackTrace.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControlStackTrace.Location = new System.Drawing.Point(684, 22);
            this.richEditControlStackTrace.Name = "richEditControlStackTrace";
            this.richEditControlStackTrace.Size = new System.Drawing.Size(387, 475);
            this.richEditControlStackTrace.TabIndex = 5;
            // 
            // gridControlLog
            // 
            this.gridControlLog.DataSource = this.logBindingSource;
            this.gridControlLog.Location = new System.Drawing.Point(6, 6);
            this.gridControlLog.MainView = this.gridViewLog;
            this.gridControlLog.Name = "gridControlLog";
            this.gridControlLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1});
            this.gridControlLog.Size = new System.Drawing.Size(669, 333);
            this.gridControlLog.TabIndex = 4;
            this.gridControlLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLog});
            // 
            // gridViewLog
            // 
            this.gridViewLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridViewLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIndex,
            this.colCmdName,
            this.colDescription,
            this.colLevel,
            this.colType,
            this.colStartTime,
            this.colSpentTime});
            this.gridViewLog.CustomizationFormBounds = new System.Drawing.Rectangle(-1073, 472, 210, 172);
            this.gridViewLog.GridControl = this.gridControlLog;
            this.gridViewLog.Name = "gridViewLog";
            this.gridViewLog.OptionsSelection.MultiSelect = true;
            this.gridViewLog.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewLog.OptionsView.ShowGroupPanel = false;
            this.gridViewLog.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLog.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLog.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIndex, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewLog.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewLog_FocusedRowChanged);
            // 
            // colIndex
            // 
            this.colIndex.AppearanceCell.Options.UseTextOptions = true;
            this.colIndex.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.FieldName = "Index";
            this.colIndex.Name = "colIndex";
            this.colIndex.OptionsColumn.FixedWidth = true;
            this.colIndex.Visible = true;
            this.colIndex.VisibleIndex = 0;
            this.colIndex.Width = 46;
            // 
            // colCmdName
            // 
            this.colCmdName.FieldName = "CmdName";
            this.colCmdName.Name = "colCmdName";
            this.colCmdName.Visible = true;
            this.colCmdName.VisibleIndex = 1;
            this.colCmdName.Width = 94;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 201;
            // 
            // colLevel
            // 
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            this.colLevel.Visible = true;
            this.colLevel.VisibleIndex = 3;
            this.colLevel.Width = 183;
            // 
            // colType
            // 
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.Width = 183;
            // 
            // colStartTime
            // 
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 4;
            this.colStartTime.Width = 74;
            // 
            // colSpentTime
            // 
            this.colSpentTime.FieldName = "SpentTime";
            this.colSpentTime.Name = "colSpentTime";
            this.colSpentTime.Visible = true;
            this.colSpentTime.VisibleIndex = 5;
            this.colSpentTime.Width = 83;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.DisplayFormat.FormatString = "hh.mm.ss.fff";
            this.repositoryItemTimeEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.EditFormat.FormatString = "hh.mm.ss.fff";
            this.repositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.splitterItem1,
            this.layoutControlItem3,
            this.splitterItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1077, 503);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlLog;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(673, 337);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.richEditControlStackTrace;
            this.layoutControlItem2.Location = new System.Drawing.Point(678, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(391, 495);
            this.layoutControlItem2.Text = "ERROR Stack trace";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(92, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(673, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 495);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.richEditControlMessage;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 342);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(673, 153);
            this.layoutControlItem3.Text = "Log Message";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(92, 13);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.Location = new System.Drawing.Point(0, 337);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(673, 5);
            // 
            // splitContainerControl1item
            // 
            this.splitContainerControl1item.GroupBordersVisible = false;
            this.splitContainerControl1item.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1item.Name = "splitContainerControl1item";
            this.splitContainerControl1item.Size = new System.Drawing.Size(868, 567);
            this.splitContainerControl1item.Text = "splitContainerControl1";
            // 
            // Panel1item
            // 
            this.Panel1item.GroupBordersVisible = false;
            this.Panel1item.Location = new System.Drawing.Point(0, 0);
            this.Panel1item.Name = "Panel1item";
            this.Panel1item.Size = new System.Drawing.Size(868, 567);
            this.Panel1item.Text = "Panel1";
            // 
            // ViewLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ViewLoggerlayoutControl1ConvertedLayout);
            this.Name = "ViewLogger";
            this.Size = new System.Drawing.Size(1077, 503);
            ((System.ComponentModel.ISupportInitialize)(this.logBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewLoggerlayoutControl1ConvertedLayout)).EndInit();
            this.ViewLoggerlayoutControl1ConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel1item)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource loggerBindingSource;
        private System.Windows.Forms.BindingSource logBindingSource;
        private DevExpress.XtraLayout.LayoutControl ViewLoggerlayoutControl1ConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup splitContainerControl1item;
        private DevExpress.XtraLayout.LayoutControlGroup Panel1item;
        private DevExpress.XtraGrid.GridControl gridControlLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLog;
        private DevExpress.XtraGrid.Columns.GridColumn colIndex;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCmdName;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSpentTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraRichEdit.RichEditControl richEditControlStackTrace;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraRichEdit.RichEditControl richEditControlMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
    }
}
