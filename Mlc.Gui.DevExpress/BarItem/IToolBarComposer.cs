﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Drawing;
using System.Windows.Forms;


namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Interface IlMenuComposer
	/// </summary>
	public interface IMenuComposer
	{
		/// <summary>
		/// Gets the <see cref="DevExpress.XtraBars.BarItem"/> at the specified index.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem this[Enum index] { get; }
		/// <summary>
		/// Gets or sets the item data coll.
		/// </summary>
		/// <value>The item data coll.</value>
		ItemMenuDataCollection ItemDataColl { get; set; }
		/// <summary>
		/// Gets or sets the large size image.
		/// </summary>
		/// <value>The large size image.</value>
		Size LargeSizeImage { get; set; }
		/// <summary>
		/// Gets or sets the size image.
		/// </summary>
		/// <value>The size image.</value>
		Size SizeImage { get; set; }
		/// <summary>
		/// Adds the item to pop up.
		/// </summary>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="index">The index.</param>
		/// <param name="beginGroup">The begin group.</param>
		void AddItemToPopUp(PopupMenu popupContextMenu, Enum index, bool beginGroup);
		/// <summary>
		/// Gets the bar button item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetBarButtonItem(Enum index);
		/// <summary>
		/// Gets the bar docking menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetBarDockingMenuItem(Enum index);
		/// <summary>
		/// Gets the bar edit check item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarEditItem.</returns>
		BarEditItem GetBarEditCheckItem(Enum index);

		/// <summary>
		/// Gets the bar edit MRU edit item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="index">The index.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="width">The width.</param>
		/// <param name="maxFilterHistory">The maximum filter history.</param>
		/// <param name="defaultFilter">The default filter.</param>
		/// <param name="characterCasing">The character casing.</param>
		/// <returns>BarEditItem.</returns>
		BarEditItem GetBarEditMRUEditItem(string key, Enum index, string[] filters = null, int width = 100, int maxFilterHistory = 20, string defaultFilter = "", CharacterCasing characterCasing = CharacterCasing.Upper);
		/// <summary>
		/// Gets the bar list item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarListItem.</returns>
		BarListItem GetBarListItem(Enum index);
		/// <summary>
		/// Gets the bar static item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetBarStaticItem(Enum index);
		/// <summary>
		/// Gets the bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetBarSubItem(Enum index);
		/// <summary>
		/// Gets the bar workspace menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetBarWorkspaceMenuItem(Enum index);
		/// <summary>
		/// Gets the skin bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>DevExpress.XtraBars.BarItem.</returns>
		BarItem GetSkinBarSubItem(Enum index);
		/// <summary>
		/// Sets the pop up menu.
		/// </summary>
		/// <param name="gridView">The grid view.</param>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="setContextMenuRequest">The set context menu request.</param>
		void SetPopUpMenu(GridView gridView, PopupMenu popupContextMenu, EventHandlerSetContextMenuRequest setContextMenuRequest);
		/// <summary>
		/// Ends the initialize composer.
		/// </summary>
		void EndInitComposer();
		/// <summary>
		/// Begins the initialize composer.
		/// </summary>
		/// <param name="imagesFolder">The images folder.</param>
		/// <param name="largeImagesFolder">The large images folder.</param>
		/// <returns>bool.</returns>
		bool BeginInitComposer(string imagesFolder = "", string largeImagesFolder = "");
	}

	/// <summary>
	/// Interface IToolMenuComposer
	/// </summary>
	/// <seealso cref="Mlc.Gui.DevExpressTools.IMenuComposer" />
	public interface IToolBarComposer : IMenuComposer
	{
		BarManager BarManager { get; set; }
		Bar Bar { get; set; }
	}

	/// <summary>
	/// Interface IRibbonMenuComposer
	/// </summary>
	/// <seealso cref="Mlc.Gui.DevExpressTools.IMenuComposer" />
	public interface IRibbonComposer : IMenuComposer
	{
		RibbonControl Ribbon { get; set; }
		RibbonPageGroup ribbonPage { get; set; }
	}
}