﻿using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Mlc.Gui.DevExpressTools
{

	public class RibbonComposer : IRibbonComposer
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields        
		/// <summary>
		/// The image collection
		/// </summary>
		private ImageCollection imageCollection;
		/// <summary>
		/// The image large collection
		/// </summary>
		private ImageCollection imageLargeCollection;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="RibbonComposer"/> class.
		/// </summary>
		/// <param name="ribbon">The ribbon.</param>
		public RibbonComposer(RibbonControl ribbon)
		{
			this.Ribbon = ribbon;
		}
		#endregion

		#region CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the ribbon.
		/// </summary>
		/// <value>The ribbon.</value>
		public RibbonControl Ribbon { get; set; } = null;
		/// <summary>
		/// Gets or sets the ribbon page.
		/// </summary>
		/// <value>The ribbon page.</value>
		public RibbonPageGroup ribbonPage { get; set; } = null;
		/// <summary>
		/// Gets or sets the size image.
		/// </summary>
		/// <value>The size image.</value>
		public Size SizeImage { get; set; } = new Size(24, 24);
		/// <summary>
		/// Gets or sets the large images folder.
		/// </summary>
		/// <value>The large images folder.</value>
		public Size LargeSizeImage { get; set; } = new Size(32, 32);
		/// <summary>
		/// Gets or sets the item data coll.
		/// </summary>
		/// <value>The item data coll.</value>
		public ItemMenuDataCollection ItemDataColl { get; set; } = new ItemMenuDataCollection();
		public RibbonStatusBar Bar { get; set; }

		/// <summary>
		/// Gets the <see cref="BarItem"/> at the specified index.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarItem this[Enum index]
		{
			get
			{
				BarItem barItem = null;
				string key = index.ToString();
				barItem = this.Ribbon.Items[key];
				return barItem;
			}
		}

		/// <summary>
		/// Gets or sets the bar filters collection.
		/// </summary>
		/// <value>The bar filters collection.</value>
		public BarItemTextMgrCollection BarFiltersCollection { get; set; } = new();
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Initializes the menu composer.
		/// </summary>
		/// <param name="ribbon">The ribbon.</param>
		/// <param name="imagesFolder">The images folder.</param>
		/// <param name="largeImagesFolder">The large images folder.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool BeginInitComposer(string imagesFolder = "", string largeImagesFolder = "")
		{

			this.Ribbon.BeginInit();
			return commonInitMenuComposer(ref imagesFolder, ref largeImagesFolder);
		}

		/// <summary>
		/// Sets the categories.
		/// </summary>
		public void EndInitComposer()
		{
			this.Ribbon.EndInit();

			//Category cat = new Category("Temi elenco", new Guid("5dba386b-80f5-46b7-815e-60cf0c4f9e5e"));
			//if (!ItemDataColl.DicCategories.ContainsKey(cat.Guid))
			//    ItemDataColl.DicCategories.Add(cat.Guid, cat);

			//Dictionary<Guid, Category> dicCat = new Dictionary<Guid, Category>();
			//foreach (BarItem item in this.BarManager.Items)
			//{
			//    if (item.CategoryGuid == new Guid("f96a77cc-41a0-4295-8f5a-5c2410a21ef7"))
			//        item.CategoryGuid = cat.Guid;

			//    try
			//    {
			//        if (ItemDataColl.DicCategories.ContainsKey(item.CategoryGuid))
			//        {
			//            Category cate = ItemDataColl.DicCategories[item.CategoryGuid];
			//            if (!dicCat.ContainsKey(cate.Guid))
			//                dicCat.Add(cate.Guid, cate);
			//        }
			//    }
			//    catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			//}

			//foreach (Category cate in dicCat.Values)
			//    this.BarManager.Categories.Add(new BarManagerCategory(cate.Name, cate.Guid));
		}

		/// <summary>
		/// Gets the bar button item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarButtonItem.</returns>
		public BarItem GetBarButtonItem(Enum index)
		{
			return this.getBarItem(index, new BarButtonItem(Ribbon.Manager, ""));
		}


		/// <summary>
		/// Gets the skin bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		public BarItem GetSkinBarSubItem(Enum index)
		{
			return this.getBarItem(index, new SkinBarSubItem());
		}

		/// <summary>
		/// Gets the bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarItem GetBarSubItem(Enum index)
		{
			return this.getBarItem(index, new BarSubItem(Ribbon.Manager, ""));
		}

		/// <summary>
		/// Gets the bar workspace menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarWorkspaceMenuItem.</returns>
		public BarItem GetBarWorkspaceMenuItem(Enum index)
		{
			return getBarItem(index, new BarWorkspaceMenuItem());
		}


		/// <summary>
		/// Gets the bar workspace menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarWorkspaceMenuItem.</returns>
		public BarItem GetBarDockingMenuItem(Enum index)
		{
			return getBarItem(index, new BarDockingMenuItem());
		}

		/// <summary>
		/// Gets the bar static item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarStaticItem.</returns>
		public BarItem GetBarStaticItem(Enum index)
		{
			return getBarItem(index, new BarStaticItem());
		}

		/// <summary>
		/// Gets the bar list item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarListItem GetBarListItem(Enum index)
		{
			return (BarListItem)getBarItem(index, new BarListItem());
		}

		/// <summary>
		/// Sets the pop up menu.
		/// </summary>
		/// <param name="gridView">The grid view.</param>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="setContextMenuRequest">The set context menu request.</param>
		public void SetPopUpMenu(GridView gridView, PopupMenu popupContextMenu, EventHandlerSetContextMenuRequest setContextMenuRequest)
		{
			new GridPopUpMgr(gridView, popupContextMenu).SetContextMenuRequest += setContextMenuRequest;
		}

		/// <summary>
		/// Adds the item to pop up.
		/// </summary>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="index">The index.</param>
		/// <param name="beginGroup">if set to <c>true</c> [begin group].</param>
		public void AddItemToPopUp(PopupMenu popupContextMenu, Enum index, bool beginGroup)
		{
			this.Ribbon.BeginInit();
			popupContextMenu.BeginInit();

			BarItem barItem = this.Ribbon.Items[index.ToString()];
			if (barItem != null)
			{
				popupContextMenu.AddItem(barItem);
				popupContextMenu.LinksPersistInfo[popupContextMenu.LinksPersistInfo.Count - 1].BeginGroup = beginGroup;
			}
			popupContextMenu.EndInit();
			this.Ribbon.EndInit();
		}


		///// <summary>
		///// Gets the bar edit item.
		///// </summary>
		///// <param name="index">The index.</param>
		///// <param name="width">The width.</param>
		///// <returns>BarItem.</returns>
		//public BarEditItem GetBarEditTextItem(Enum index, int width = 100)
		//{
		//	BarEditItem barEditItem = this.getBarEditItem(index, new RepositoryItemTextEdit());
		//	barEditItem.EditWidth = width;
		//	return barEditItem;
		//}

		/// <summary>
		/// Gets the bar edit MRU edit item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="filterChanged">The filter changed.</param>
		/// <param name="defaultFilter">The default filter.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="maxFilterHistory">The maximum filter history.</param>
		/// <param name="width">The width.</param>
		/// <returns>BarEditItem.</returns>
		public BarEditItem GetBarEditMRUEditItem(string key, Enum index, string[] filters = null, int width = 100, int maxFilterHistory = 20, string defaultFilter = "", CharacterCasing characterCasing = CharacterCasing.Upper)
		{
			if (!this.BarFiltersCollection.ContainsKey(key))
			{
				RepositoryItemMRUEdit repositoryItemMRUEdit = new();
				repositoryItemMRUEdit.DropDownRows = 15;
				repositoryItemMRUEdit.MaxItemCount = maxFilterHistory;
				if (filters != null)
					repositoryItemMRUEdit.Items.AddRange(filters);

				BarEditItem barEditItem = this.getBarEditItem(index, repositoryItemMRUEdit);
				barEditItem.EditWidth = width;

				BarItemTextMgr filterMgr = new(barEditItem, characterCasing);
				this.BarFiltersCollection.Add(key, filterMgr);

				repositoryItemMRUEdit.KeyDown += (s, e) =>
				{
					if (e.KeyCode == Keys.Enter)
						if (s is MRUEdit mruedit)
							filterMgr.SetFilter(mruedit.EditValue.ToString());
				};

				repositoryItemMRUEdit.SelectedIndexChanged += (s, e) =>
				{
					if (s is MRUEdit mruedit)
						filterMgr.SetFilter(mruedit.EditValue.ToString());
				};

			}
			return this.BarFiltersCollection[key].BarItem;
		}

		/// <summary>
		/// Gets the bar edit check item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarEditItem GetBarEditCheckItem(Enum index)
		{
			BarEditItem barEditItem = this.getBarEditItem(index, new RepositoryItemCheckEdit());
			return barEditItem;
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the bar edit item.
		/// </summary>
		/// <param name="bar">The bar.</param>
		/// <param name="index">The index.</param>
		/// <param name="repositoryItem">The repository item.</param>
		/// <returns>DevExpress.XtraBars.BarEditItem.</returns>
		private BarEditItem getBarEditItem(Enum index, RepositoryItem repositoryItem)
		{
			BarEditItem barEditItem = (BarEditItem)getBarItem(index, new BarEditItem());
			barEditItem.Edit = repositoryItem;
			return barEditItem;
		}

		/// <summary>
		/// Gets the bar item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="barItem">The bar item.</param>
		/// <returns>BarItem.</returns>
		/// <exception cref="Exception">Impossibile definire l'item di menu</exception>
		private BarItem getBarItem(Enum index, BarItem barItem)
		{
			if (this.ItemDataColl.ContainsKey(index))
			{
				try
				{
					this.setItemData(index, barItem);
					this.Ribbon.Items.Add(barItem);
					this.ribbonPage.ItemLinks.Add(barItem);
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

			}
			else
				throw new Exception("Impossibile definire l'item di menu");

			return barItem;

		}

		/// <summary>
		/// Sets the item data.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="barItem">The bar item.</param>
		private void setItemData(Enum index, BarItem barItem)
		{
			ItemMenuData itemMenuData = this.ItemDataColl[index];
			barItem.Name = itemMenuData.Key;
			barItem.Caption = itemMenuData.Caption;
			barItem.ImageOptions.Image = imageCollection.Images[itemMenuData.Key];
			barItem.ImageOptions.LargeImage = imageLargeCollection.Images[itemMenuData.Key];
			barItem.Alignment = itemMenuData.ItemAlignment;
			barItem.PaintStyle = itemMenuData.PaintStyle;
			barItem.Id = barItem.Manager.GetNewItemId();
			//this.itemId++;
			if (itemMenuData.TipTitle != "" && itemMenuData.TipContext != "")
			{
				SuperToolTipSetupArgs args = new SuperToolTipSetupArgs();
				args.Title.Text = itemMenuData.TipTitle;
				args.Contents.Text = itemMenuData.TipContext;
				SuperToolTip sTooltip = new SuperToolTip();
				sTooltip.Setup(args);
				barItem.SuperTip = sTooltip;
			}
		}

		/// <summary>
		/// Commons the initialize menu composer.
		/// </summary>
		/// <param name="imagesFolder">The images folder.</param>
		/// <param name="largeImagesFolder">The large images folder.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		private bool commonInitMenuComposer(ref string imagesFolder, ref string largeImagesFolder)
		{
			bool retVal = false;
			try
			{
				SysInfo sysInfo = new();
				imagesFolder = string.IsNullOrEmpty(imagesFolder) ? $"{sysInfo.ExecutableFolder}\\Resources\\Menu" : imagesFolder;
				largeImagesFolder = string.IsNullOrEmpty(largeImagesFolder) ? imagesFolder : largeImagesFolder;

				this.imageCollection = new ImageCollectionMgr().ImageCollectionFromFolder(SizeImage, imagesFolder);
				this.imageLargeCollection = new ImageCollectionMgr().ImageCollectionFromFolder(LargeSizeImage, largeImagesFolder);
				//this.itemId = 0;
				retVal = true;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}

		public bool InitMenuComposer(BarManager barManager, string imagesFolder = "", string largeImagesFolder = "")
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		/// <summary>
		/// Class BarItemTextMgrCollection.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		public class BarItemTextMgrCollection : Dictionary<string, BarItemTextMgr> { }
		#endregion

	}



	/// <summary>
	/// Class Category.
	/// </summary>
	public class Category
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Category"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="guid">The unique identifier.</param>
		public Category(string name, Guid guid)
		{
			this.Name = name;
			this.Guid = guid;
		}
		/// <summary>
		/// Gets or sets the category unique identifier.
		/// </summary>
		/// <value>The category unique identifier.</value>
		public Guid Guid { get; set; }
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }
	}


	public class ItemMenuDataCollection : Dictionary<Enum, ItemMenuData>
	{
		/// <summary>
		/// The dic categories
		/// </summary>
		public Dictionary<Guid, Category> DicCategories = new Dictionary<Guid, Category>();

		private Category category = null;

		public Category Category
		{
			get { return this.category; }
			set
			{
				this.category = value;
				if (!this.DicCategories.ContainsKey(this.category.Guid))
					this.DicCategories.Add(this.category.Guid, category);
			}
		}

		public void Add(ItemMenuData itemMenuData)
		{
			itemMenuData.Category = this.Category;
			if (!this.ContainsKey(itemMenuData.Index))
				this.Add(itemMenuData.Index, itemMenuData);
		}
	}
	public class ItemMenuData
	{


		public ItemMenuData(Enum index)
			: this(index, index.ToString(), string.Empty, string.Empty)
		{
		}

		public ItemMenuData(Enum index, string caption)
			: this(index, caption, string.Empty, string.Empty)
		{
		}

		public ItemMenuData(Enum index, string caption, string tipContext)
			: this(index, caption, caption, tipContext)
		{
		}

		public ItemMenuData(Enum index, string caption, string tipTitle, string tipContext)
		{
			this.Index = index;
			this.Caption = caption;
			this.TipTitle = tipTitle;
			this.TipContext = tipContext;
		}

		public Enum Index { get; set; }
		public string Key => Index.ToString(); //{ get; set; }
		public string Name => $"Button{this.Key}"; //{ get; set; }
		public string Caption { get; set; }
		public string TipTitle { get; set; }
		public string TipContext { get; set; }
		public BarItemLinkAlignment ItemAlignment { get; set; } = BarItemLinkAlignment.Default;
		public BarItemPaintStyle PaintStyle { get; set; } = BarItemPaintStyle.Standard;
		public Category Category { get; set; } ///= new Guid("968481df-4ed7-4579-b2df-90672892f2b6");

		#region Embedded Types
		/// <summary>
		/// Class BarItemTextMgrCollection.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		public class BarItemTextMgrCollection : Dictionary<string, BarItemTextMgr> { }
		#endregion

	}


}
