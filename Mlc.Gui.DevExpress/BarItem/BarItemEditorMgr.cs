﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 12-28-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-28-2017
// ***********************************************************************
// <copyright file="BarItemEditorMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars;
using System;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class BarItemEditorMgr.
    /// </summary>
    public class BarItemEditorMgr
    {

        private BarEditItem barEditItem = null;
        public string TextRepository = null;

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="BarItemEditorMgr"/> class.
        /// </summary>
        /// <param name="barEditItem">The bar edit item.</param>
        public BarItemEditorMgr(BarEditItem barEditItem)
        {
            this.barEditItem = barEditItem;
            if (barEditItem.Edit is RepositoryItemTextEdit)
                ((RepositoryItemTextEdit)barEditItem.Edit).EditValueChanged += BarItemEditorMgr_EditValueChanged;

            barEditItem.Edit.KeyDown += edit_KeyDown;
        }

        #endregion


        /// <summary>
        /// Simulates the enter key.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SimulatesEnterKey(string filterText)
        {
            this.barEditItem.EditValue = filterText;
            this.TextRepository = filterText;
            this.OnEnterKeyPressed(new EventArgsEnterKeyPressed(this.TextRepository));
        }


        #region Private Event Handlers
        /// <summary>
        /// Handles the KeyDown event of the edit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void edit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                this.OnEnterKeyPressed(new EventArgsEnterKeyPressed(this.TextRepository));
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the BarItemEditorMgr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BarItemEditorMgr_EditValueChanged(object sender, EventArgs e)
        {
            this.TextRepository = ((TextEdit)sender).EditValue.ToString();
        }
        #endregion




        #region Event


        #region Dichiarazione Evento EnterKeyPressed
        /// <summary>
        /// Definizione dell'evento.
        /// </summary>
        public event EventHandlerEnterKeyPressed EnterKeyPressed;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnEnterKeyPressed(EventArgsEnterKeyPressed e)
        {
            // Se ci sono ricettori in ascolto ...
            if (EnterKeyPressed != null)
                // viene lanciato l'evento
                EnterKeyPressed(this, e);
        }
        #endregion

        #endregion
    }
}
