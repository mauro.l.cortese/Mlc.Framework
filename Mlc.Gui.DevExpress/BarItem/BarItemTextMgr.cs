﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 12-19-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 12-19-2017
// ***********************************************************************
// <copyright file="BarItemMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using Mlc.Shell;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class BarItemChecker.
	/// </summary>
	public static class BarItemChecker
	{
		/// <summary>
		/// Determines whether [is item text] [the specified bar item].
		/// </summary>
		/// <param name="barItem">The bar item.</param>
		/// <returns><c>true</c> if [is item text] [the specified bar item]; otherwise, <c>false</c>.</returns>
		public static bool IsItemText(this BarItem barItem)
		{
			bool retVal = false;
			if (barItem is BarEditItem)
			{
				RepositoryItem rep = ((BarEditItem)barItem).Edit;
				retVal = (rep != null && rep is RepositoryItemTextEdit);
			}
			return retVal;
		}
	}

	/// <summary>
	/// Class BarItemMgr.
	/// </summary>
	public class BarItemTextMgr
	{
		#region Fields
		/// <summary>
		/// The enter key pressed
		/// </summary>
		private bool enterKeyPressed = false;
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The bar item
		/// </summary>
		public BarEditItem BarItem { get; private set; } = null;

		/// <summary>
		/// The text
		/// </summary>
		private string filterText = null;
		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
		public string FilterText
		{
			get => this.filterText;
			set
			{
				this.filterText = value;
				if (this.BarItem != null)
					this.BarItem.EditValue = value;
				Application.DoEvents();
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance has bar item.
		/// </summary>
		/// <value><c>true</c> if this instance has bar item; otherwise, <c>false</c>.</value>
		public bool HasBarItem { get => this.BarItem != null; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Sets the bar item.
		/// </summary>
		/// <param name="barEditFilter">The bar edit filter.</param>
		public void SetBarItem(BarEditItem barEditFilter) => this.BarItem = barEditFilter;

		/// <summary>
		/// Initializes a new instance of the <see cref="BarItemMgr" /> class.
		/// </summary>
		/// <param name="barItem">The bar item.</param>
		/// <param name="characterCasing">The character casing.</param>
		public BarItemTextMgr(BarEditItem barItem, CharacterCasing characterCasing)
		{
			this.BarItem = ((BarEditItem)barItem);
			RepositoryItem rep = this.BarItem.Edit;
			if (rep is RepositoryItemTextEdit)
			{
				RepositoryItemTextEdit repositoryItemTextEdit = (RepositoryItemTextEdit)rep;
				repositoryItemTextEdit.ValidateOnEnterKey = false;
				repositoryItemTextEdit.CharacterCasing = characterCasing;
				repositoryItemTextEdit.Validating += repositoryItemTextEdit_Validating;
				repositoryItemTextEdit.EditValueChanged += repositoryItemTextEdit_EditValueChanged;
				repositoryItemTextEdit.KeyDown += repositoryItemTextEdit_KeyDown;
			}
		}
		#endregion
		#endregion

		#region Internal
		#region Method
		/// <summary>
		/// Sets the filter.
		/// </summary>
		/// <param name="text">The text.</param>
		internal void SetFilter(string text)
		{
			this.FilterText = text;
			this.OnFilterTextChanged(new(this.FilterText != null ? this.FilterText : string.Empty));
		}
		#endregion
		#endregion

		#region Private
		#region Event Handlers
		/// <summary>
		/// Handles the KeyDown event of the repositoryItemTextEdit control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
		private void repositoryItemTextEdit_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				this.enterKeyPressed = true;
			else
				this.enterKeyPressed = false;
		}


		/// <summary>
		/// Handles the EditValueChanged event of the RepositoryItemTextEdit control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		private void repositoryItemTextEdit_EditValueChanged(object sender, EventArgs e)
		{
			this.FilterText = ((TextEdit)sender).Text;
		}

		/// <summary>
		/// Handles the Validating event of the Rite control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
		private void repositoryItemTextEdit_Validating(object sender, CancelEventArgs e)
		{
			try
			{
				this.FilterText = ((TextEdit)sender).Text;
				if (this.enterKeyPressed)
				{
					this.enterKeyPressed = false;
					this.OnFilterTextChanged(new(this.FilterText != null ? this.FilterText : string.Empty));
				}
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		#endregion

		#region Dichiarazione Evento FilterTextChanged
		/// <summary>
		/// Definizione dell'evento.
		/// </summary>
		public event EventHandlerFilterTextChanged FilterTextChanged;

		/// <summary>
		/// Metodo per il lancio dell'evento.
		/// </summary>  
		/// <param name="e">Dati dell'evento</param>
		protected virtual void OnFilterTextChanged(EventArgsFilterTextChanged e)
		{
			// Se ci sono ricettori in ascolto ...
			if (FilterTextChanged != null)
				// viene lanciato l'evento
				FilterTextChanged(this, e);
		}
		#endregion

		#region Definizione Evento FilterTextChanged
		/// <summary>
		/// Delegato per la gestione dell'evento.
		/// </summary>
		public delegate void EventHandlerFilterTextChanged(object sender, EventArgsFilterTextChanged e);

		/// <summary>
		/// Classe per gli argomenti della generazione di un evento.
		/// </summary>
		public class EventArgsFilterTextChanged : EventArgs
		{
			#region Costruttori
			/// <summary>
			/// Inizilizza una nuova istanza
			/// </summary>
			/// <param name="filterText"></param>
			public EventArgsFilterTextChanged(string filterText) => this.FilterText = filterText;
			#endregion

			#region Proprietà
			/// <summary>
			/// Restituisce o imposta il ...
			/// </summary>
			public string FilterText { get; set; } = string.Empty;
			#endregion
		}
		#endregion
	}
}


