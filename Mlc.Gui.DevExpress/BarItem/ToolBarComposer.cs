﻿using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using DevExpress.XtraEditors;
using Mlc.Shell;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;

namespace Mlc.Gui.DevExpressTools
{

	public class ToolBarComposer : IToolBarComposer
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields        
		/// <summary>
		/// The image collection
		/// </summary>
		private ImageCollection imageCollection;
		/// <summary>
		/// The image large collection
		/// </summary>
		private ImageCollection imageLargeCollection;
		/// <summary>
		/// The d x menu items
		/// </summary>
		private Dictionary<Enum, DXMenuItem> dXMenuItems = new Dictionary<Enum, DXMenuItem>();
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ToolBarComposer"/> class.
		/// </summary>
		/// <param name="barManager">The bar manager.</param>
		public ToolBarComposer(BarManager barManager)
		{
			this.BarManager = barManager;
		}
		#endregion

		#region CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the bar manager.
		/// </summary>
		/// <value>The bar manager.</value>
		public BarManager BarManager { get; set; } = null;
		/// <summary>
		/// Gets or sets the bar.
		/// </summary>
		/// <value>The bar.</value>
		public Bar Bar { get; set; } = null;
		/// <summary>
		/// Gets or sets the size image.
		/// </summary>
		/// <value>The size image.</value>
		public Size SizeImage { get; set; } = new(24, 24);
		/// <summary>
		/// Gets or sets the large images folder.
		/// </summary>
		/// <value>The large images folder.</value>
		public Size LargeSizeImage { get; set; } = new(32, 32);
		/// <summary>
		/// Gets or sets the item data coll.
		/// </summary>
		/// <value>The item data coll.</value>
		public ItemMenuDataCollection ItemDataColl { get; set; } = new();

		/// <summary>
		/// Gets the <see cref="BarItem"/> at the specified index.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarItem this[Enum index]
		{
			get
			{
				BarItem barItem = null;
				string key = index.ToString();
				barItem = this.BarManager.Items[key];
				return barItem;
			}
		}

		/// <summary>
		/// Gets or sets the bar filters collection.
		/// </summary>
		/// <value>The bar filters collection.</value>
		public BarItemTextMgrCollection BarFiltersCollection { get; set; } = new();
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Adds the item to pop up.
		/// </summary>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="index">The index.</param>
		/// <param name="beginGroup">if set to <c>true</c> [begin group].</param>
		public void AddItemToPopUp(PopupMenu popupContextMenu, Enum index, bool beginGroup = false)
		{
			this.BarManager.BeginInit();
			popupContextMenu.BeginInit();
			BarItem barItem = this.BarManager.Items[index.ToString()];
			if (barItem != null)
			{
				popupContextMenu.AddItem(barItem);
				popupContextMenu.LinksPersistInfo[popupContextMenu.LinksPersistInfo.Count - 1].BeginGroup = beginGroup;
			}
			popupContextMenu.EndInit();
			this.BarManager.EndInit();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ToolBarComposer" /> class.
		/// </summary>
		/// <param name="imagesFolder">The images folder.</param>
		/// <param name="largeImagesFolder">The large images folder.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool BeginInitComposer(string imagesFolder = "", string largeImagesFolder = "")
		{
			this.BarManager.BeginInit();
			return InitToolBarImages(ref imagesFolder, ref largeImagesFolder);
		}

		/// <summary>
		/// Sets the categories.
		/// </summary>
		public void EndInitComposer()
		{
			this.BarManager.EndInit();

			Category cat = new Category("Temi elenco", new Guid("5dba386b-80f5-46b7-815e-60cf0c4f9e5e"));
			if (!ItemDataColl.DicCategories.ContainsKey(cat.Guid))
				ItemDataColl.DicCategories.Add(cat.Guid, cat);

			Dictionary<Guid, Category> dicCat = new Dictionary<Guid, Category>();
			foreach (BarItem item in this.BarManager.Items)
			{
				if (item.CategoryGuid == new Guid("f96a77cc-41a0-4295-8f5a-5c2410a21ef7"))
					item.CategoryGuid = cat.Guid;

				try
				{
					if (ItemDataColl.DicCategories.ContainsKey(item.CategoryGuid))
					{
						Category cate = ItemDataColl.DicCategories[item.CategoryGuid];
						if (!dicCat.ContainsKey(cate.Guid))
							dicCat.Add(cate.Guid, cate);
					}
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			}

			foreach (Category cate in dicCat.Values)
				this.BarManager.Categories.Add(new BarManagerCategory(cate.Name, cate.Guid));
		}

		/// <summary>
		/// Gets the bar button item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarButtonItem.</returns>
		public BarItem GetBarButtonItem(Enum index) => this.getBarItem(index, new BarButtonItem());

		/// <summary>
		/// Gets the bar toggle switch item.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="index">The index.</param>
		/// <param name="boxItems">The box items.</param>
		/// <returns>BarEditItem.</returns>
		public BarEditItem GetBarCheckedComboBoxEdit<T>(Enum index, T boxItems) where T : Enum
		{
			BarEditItem retVal = (BarEditItem)getBarItem(index, new BarEditItem());
			RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit = new()
			{
				AllowMultiSelect = true,
				AutoHeight = false,
			};

			string[] items = Enum.GetNames(typeof(T));
			foreach (string item in items)
				if (item != "None")
					repositoryItemCheckedComboBoxEdit.Items.Add(item);

			retVal.Edit = repositoryItemCheckedComboBoxEdit;
			return retVal;
		}

		/// <summary>
		/// Gets the bar check item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarCheckItem.</returns>
		public BarCheckItem GetBarCheckItem(Enum index) => (BarCheckItem)this.getBarItem(index, new BarCheckItem());

		/// <summary>
		/// Gets the bar workspace menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarWorkspaceMenuItem.</returns>
		public BarItem GetBarDockingMenuItem(Enum index) => getBarItem(index, new BarDockingMenuItem());

		/// <summary>
		/// Gets the bar edit check item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarEditItem GetBarEditCheckItem(Enum index)
		{
			BarEditItem barEditItem = this.getBarEditItem(index, new RepositoryItemCheckEdit());
			return barEditItem;
		}

		/// <summary>
		/// Gets the bar edit MRU edit item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="index">The index.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="width">The width.</param>
		/// <param name="maxFilterHistory">The maximum filter history.</param>
		/// <param name="defaultFilter">The default filter.</param>
		/// <param name="characterCasing">The character casing.</param>
		/// <returns>BarEditItem.</returns>
		public BarEditItem GetBarEditMRUEditItem(string key, Enum index, string[] filters = null, int width = 100, int maxFilterHistory = 20, string defaultFilter = "", CharacterCasing characterCasing = CharacterCasing.Upper)
		{
			if (!this.BarFiltersCollection.ContainsKey(key))
			{
				RepositoryItemMRUEdit repositoryItemMRUEdit = new();
				repositoryItemMRUEdit.DropDownRows = 15;
				repositoryItemMRUEdit.MaxItemCount = maxFilterHistory;
				repositoryItemMRUEdit.CharacterCasing = characterCasing;
				if (filters != null)
					repositoryItemMRUEdit.Items.AddRange(filters);

				BarEditItem barEditItem = this.getBarEditItem(index, repositoryItemMRUEdit);
				barEditItem.EditWidth = width;
				barEditItem.EditValue = defaultFilter;

				BarItemTextMgr filterMgr = new(barEditItem, characterCasing);
				this.BarFiltersCollection.Add(key, filterMgr);

				repositoryItemMRUEdit.KeyDown += (s, e) =>
				{
					if (e.KeyCode == Keys.Enter && filterMgr != null)
						if (s is MRUEdit mruedit)
						{
							if (mruedit.EditValue == null)
								mruedit.EditValue = String.Empty;
							filterMgr.SetFilter(mruedit.EditValue.ToString());
						}
				};

				repositoryItemMRUEdit.SelectedIndexChanged += (s, e) =>
				{
					if (s is MRUEdit mruedit && filterMgr != null)
						filterMgr.SetFilter(mruedit.EditValue.ToString());
				};

				repositoryItemMRUEdit.Items.CollectionChanged += (s, e) =>
				{
					if (s is MRUEdit mruedit && filterMgr != null)
						filterMgr.SetFilter(mruedit.EditValue.ToString());
				};
			}
			return this.BarFiltersCollection[key].BarItem;
		}

		/// <summary>
		/// Gets the bar list item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarListItem GetBarListItem(Enum index) => (BarListItem)getBarItem(index, new BarListItem());

		/// <summary>
		/// Gets the bar static item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarStaticItem.</returns>
		public BarItem GetBarStaticItem(Enum index) => getBarItem(index, new BarStaticItem());

		/// <summary>
		/// Gets the bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarItem.</returns>
		public BarItem GetBarSubItem(Enum index)
		{
			BarItem retval = null;
			try
			{
				retval = this.getBarItem(index, new BarSubItem());
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retval;
		}

		/// <summary>
		/// Gets the bar toggle switch item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarEditItem.</returns>
		public BarEditItem GetBarToggleSwitchItem(Enum index)
		{
			BarEditItem retVal = (BarEditItem)getBarItem(index, new BarEditItem());
			RepositoryItemToggleSwitch repositoryItemToggleSwitchMoveBtn = new()
			{
				Name = $"repItemToggleSwitch{index}",
				OffText = "Off",
				OnText = "On",
				ShowText = true,
			};
			retVal.Edit = repositoryItemToggleSwitchMoveBtn;
			return retVal;
		}

		/// <summary>
		/// Gets the bar workspace menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>BarWorkspaceMenuItem.</returns>
		public BarItem GetBarWorkspaceMenuItem(Enum index) => getBarItem(index, new BarWorkspaceMenuItem());

		/// <summary>
		/// Gets the skin bar sub item.
		/// </summary>
		/// <param name="index">The index.</param>
		public BarItem GetSkinBarSubItem(Enum index) => this.getBarItem(index, new SkinBarSubItem());

		/// <summary>
		/// Gets the tree list menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="beginGroup">if set to <c>true</c> [begin group].</param>
		/// <returns>DXMenuItem.</returns>
		public DXMenuItem GetTreeListMenuItem(Enum index, bool beginGroup = false) //, bool beginGroup, EventHandler on_Click)
		{
			DXMenuItem dxMenuItem = null;
			if (!this.dXMenuItems.ContainsKey(index))
			{
				if (this.ItemDataColl.ContainsKey(index))
				{
					try
					{
						ItemMenuData itemMenuData = this.ItemDataColl[index];
						dxMenuItem = new DXMenuItem(itemMenuData.Caption); //, new EventHandler(on_Click));
						dxMenuItem.ImageOptions.Image = this.imageCollection.Images[itemMenuData.Key];
						dxMenuItem.ImageOptions.LargeImage = this.imageLargeCollection.Images[itemMenuData.Key];
						dxMenuItem.BeginGroup = beginGroup;
						this.dXMenuItems.Add(index, dxMenuItem);
					}
					catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				}
				else
					throw new Exception("Impossibile definire l'item di menu");
			}

			if (this.dXMenuItems.ContainsKey(index))
				return this.dXMenuItems[index];
			else return null;
		}

		/// <summary>
		/// Commons the initialize menu composer.
		/// </summary>
		/// <param name="imagesFolder">The images folder.</param>
		/// <param name="largeImagesFolder">The large images folder.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool InitToolBarImages(ref string imagesFolder, ref string largeImagesFolder)
		{
			bool retVal = false;
			try
			{
				imagesFolder = string.IsNullOrEmpty(imagesFolder) ? $"{new SysInfo().ExecutableFolder}\\Resources\\Menu" : imagesFolder;
				largeImagesFolder = string.IsNullOrEmpty(largeImagesFolder) ? imagesFolder : largeImagesFolder;

				this.imageCollection = new ImageCollectionMgr().ImageCollectionFromFolder(this.SizeImage, imagesFolder);
				this.imageLargeCollection = new ImageCollectionMgr().ImageCollectionFromFolder(this.LargeSizeImage, largeImagesFolder);
				//this.itemId = 0;
				retVal = true;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			return retVal;
		}

		/// <summary>
		/// Sets the accordion menu item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>Image.</returns>
		public bool SetAccordionMenuItem(DevExpress.XtraBars.Navigation.AccordionControlElement accordionItem, Enum index)
		{
			bool retVal = false;
			if (this.ItemDataColl.ContainsKey(index))
			{
				ItemMenuData itemMenuData = this.ItemDataColl[index];
				accordionItem.Text = itemMenuData.Caption;
				accordionItem.SuperTip = new SuperToolTip();
				accordionItem.SuperTip.Items.Add(new ToolTipTitleItem() { Text = itemMenuData.TipTitle });
				accordionItem.SuperTip.Items.Add(new ToolTipItem() { Text = itemMenuData.TipContext });
				accordionItem.ImageOptions.Image = imageLargeCollection.Images[itemMenuData.Key];
				retVal = true;
			}
			return retVal;
		}

		/// <summary>
		/// Sets the bar.
		/// </summary>
		/// <param name="barEnum">The bar enum.</param>
		public bool SetBar(Enum barEnum)
		{
			string key = $"Bar{Enum.GetName(barEnum.GetType(), barEnum)}";
			this.Bar = this.BarManager.Bars[key];
			return this.Bar != null ? true : false;
		}

		/// <summary>
		/// Sets the pop up menu.
		/// </summary>
		/// <param name="gridView">The grid view.</param>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="setContextMenuRequest">The set context menu request.</param>
		public void SetPopUpMenu(GridView gridView, PopupMenu popupContextMenu, EventHandlerSetContextMenuRequest setContextMenuRequest)
		{
			new GridPopUpMgr(gridView, popupContextMenu).SetContextMenuRequest += setContextMenuRequest;
		}

		/// <summary>
		/// Sets the pop up menu.
		/// </summary>
		/// <param name="treeList">The tree list.</param>
		/// <param name="popupContextMenu">The popup context menu.</param>
		/// <param name="setContextMenuRequest">The set context menu request.</param>
		/// <exception cref="NotImplementedException"></exception>
		public void SetPopUpMenu(TreeList treeList, PopupMenu popupContextMenu, EventHandlerSetContextMenuRequest setContextMenuRequest)
		{
			new TreePopUpMgr(treeList, popupContextMenu).SetContextMenuRequest += setContextMenuRequest;
		}
		#endregion

		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the bar edit item.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="repositoryItem">The repository item.</param>
		/// <returns>DevExpress.XtraBars.BarEditItem.</returns>
		private BarEditItem getBarEditItem(Enum index, RepositoryItem repositoryItem)
		{
			BarEditItem barEditItem = (BarEditItem)getBarItem(index, new BarEditItem());
			barEditItem.Edit = repositoryItem;
			return barEditItem;
		}


		/// <summary>
		/// News the method.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="barItem">The bar item.</param>
		/// <returns>BarItem.</returns>
		private BarItem getBarItem(Enum index, BarItem barItem)
		{
			if (this.ItemDataColl.ContainsKey(index))
			{
				//this.BarManager.BeginInit();
				try
				{
					this.BarManager.Items.Add(barItem);
					this.setItemData(index, barItem);
					this.Bar.BeginUpdate();
					this.Bar.AddItem(barItem);
					this.Bar.EndUpdate();
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
				//finally { this.BarManager.EndInit(); }
			}
			else
				throw new Exception($"Impossibile definire l'item di menu per {index}");

			return barItem;
		}

		/// <summary>
		/// Sets the item data.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="barItem">The bar item.</param>
		private void setItemData(Enum index, BarItem barItem)
		{
			ItemMenuData itemMenuData = this.ItemDataColl[index];
			barItem.Name = itemMenuData.Key;
			barItem.Caption = itemMenuData.Caption;
			barItem.ImageOptions.Image = this.imageCollection.Images[itemMenuData.Key];
			barItem.ImageOptions.LargeImage = this.imageLargeCollection.Images[itemMenuData.Key];
			barItem.Alignment = itemMenuData.ItemAlignment;
			barItem.PaintStyle = itemMenuData.PaintStyle;
			barItem.Id = barItem.Manager.GetNewItemId();
			barItem.CategoryGuid = itemMenuData.Category.Guid;

			//this.itemId++;
			if (itemMenuData.TipTitle != "" && itemMenuData.TipContext != "")
			{
				SuperToolTipSetupArgs args = new SuperToolTipSetupArgs();
				args.Title.Text = itemMenuData.TipTitle;
				args.Contents.Text = itemMenuData.TipContext;
				SuperToolTip sTooltip = new SuperToolTip();
				sTooltip.Setup(args);
				barItem.SuperTip = sTooltip;
			}
		}
		#endregion

		#region Event Handlers
		#endregion
		#endregion

		#region Event Definitions
		#endregion

		#region Embedded Types
		/// <summary>
		/// Class BarItemTextMgrCollection.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.String, Mlc.Gui.DevExpressTools.BarItemTextMgr}" />
		public class BarItemTextMgrCollection : Dictionary<string, BarItemTextMgr> { }
		#endregion

	}
}
