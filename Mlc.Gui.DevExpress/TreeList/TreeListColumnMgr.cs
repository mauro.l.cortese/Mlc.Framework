﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList.Columns;
using Mlc.Shell;
using Mlc.Data;
using System;
using System.Data;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Classe Singleton pattern
	/// </summary>
	public class TreeListColumnMgr : InPlaceRepository
	{
		#region Campi
		/// <summary>Variabile per safe thread</summary>
		private static object syncRoot = new Object();
		/// <summary>Istanza della classe</summary>
		private static volatile TreeListColumnMgr instance = null;
		#endregion

		#region Costruttori
		/// <summary>
		/// Costruttore privato non permette l'istanziamento diretto di questa classe.
		/// </summary>
		private TreeListColumnMgr()
		{ }
		#endregion

		#region Proprietà
		#endregion

		#region Metodi pubblici
		/// <summary>
		/// Restituisce sempre la stessa istanza della classe.
		/// </summary>
		public static TreeListColumnMgr GetInstance()
		{
			// se l'istanza non è ancora stata allocata
			if (instance == null)
			{
				lock (syncRoot)
				{
					// ottengo una nuova istanza
					if (instance == null)
						instance = new TreeListColumnMgr();
				}
			}
			// restituisco sempre la stessa istanza 
			return instance;
		}


		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">if set to <c>true</c> [allow focus].</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		public static void FormatColumn(TreeListColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment)
		{
			FormatColumn(column, allowFocus, horzAlignment, 0);
		}


		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">if set to <c>true</c> [allow focus].</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		/// <param name="width">The width.</param>
		public static void FormatColumn(TreeListColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment, int width)
		{
			FormatColumn(column, allowFocus, horzAlignment, width, false, true);
		}

		/// <summary>
		/// Formats the specified column.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="allowFocus">if set to <c>true</c> [allow focus].</param>
		/// <param name="horzAlignment">The horz alignment.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public static void FormatColumn(TreeListColumn column, bool allowFocus, DevExpress.Utils.HorzAlignment horzAlignment, int width, bool fixedWidth, bool allowSize)
		{
			column.OptionsColumn.AllowFocus = allowFocus;
			column.AppearanceCell.TextOptions.HAlignment = horzAlignment;
			column.AppearanceHeader.TextOptions.HAlignment = horzAlignment;
			if (width != 0)
				column.Width = width;
			column.OptionsColumn.FixedWidth = fixedWidth;
			column.OptionsColumn.AllowSize = allowSize;

		}


		/// <summary>
		/// Sets the column for icon.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForIcon(TreeListColumn column, string keyRepository, int width, bool fixedWidth, bool allowSize)
		{
			try
			{
				column.ColumnEdit = base.GetRepository(keyRepository, typeof(RepositoryItemImageComboBox));
				column.Width = width;
				column.OptionsColumn.FixedWidth = fixedWidth;
				column.OptionsColumn.AllowSize = allowSize;
				column.OptionsColumn.AllowFocus = false;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}


		/// <summary>
		/// Sets the column forlook up.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="table">The table.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForlookUp(TreeListColumn column, DataTable table, bool fixedWidth, bool allowSize)
		{
			this.SetColumnForlookUp(column, column.FieldName, table, column.Width, fixedWidth, allowSize);
		}

		/// <summary>
		/// Sets the column for combo.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="table">The table.</param>
		/// <param name="width">The width.</param>
		/// <param name="fixedWidth">if set to <c>true</c> [fixed width].</param>
		/// <param name="allowSize">if set to <c>true</c> [allow size].</param>
		public void SetColumnForlookUp(TreeListColumn column, string keyRepository, DataTable table, int width, bool fixedWidth, bool allowSize)
		{
			try
			{
				column.ColumnEdit = base.GetRepository(keyRepository, typeof(RepositoryItemLookUpEdit), table);
				column.Width = width;
				column.OptionsColumn.FixedWidth = fixedWidth;
				column.OptionsColumn.AllowSize = allowSize;
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

		}

		/// <summary>
		/// Formats the column.
		/// </summary>
		/// <param name="formatColumnData">The format column data.</param>
		public static void FormatColumn(TreeListColumnFormatData formatColumnData)
		{
			if (formatColumnData.Column != null)
			{
				formatColumnData.Column.OptionsColumn.AllowFocus = formatColumnData.AllowFocus;
				formatColumnData.Column.OptionsColumn.ReadOnly = formatColumnData.ReadOnly;
				formatColumnData.Column.AppearanceCell.TextOptions.HAlignment = formatColumnData.HorzAlignment;
				formatColumnData.Column.AppearanceHeader.TextOptions.HAlignment = formatColumnData.HorzAlignment;
				formatColumnData.Column.Fixed = formatColumnData.Fixed;
				formatColumnData.Column.OptionsColumn.AllowSize = formatColumnData.AllowSize;
				formatColumnData.Column.OptionsColumn.FixedWidth = formatColumnData.FixedWidth;

				if (formatColumnData.Format != Format.None)
				{
					switch (formatColumnData.Format)
					{
						case Format.Currency:
							formatColumnData.Column.Format.FormatType = FormatType.Numeric;
							formatColumnData.Column.Format.FormatString = "c2";
							break;
						case Format.Percent:
							break;
						case Format.Date:
							break;
						default:
							break;
					}
				}

				if (formatColumnData.Width != 0)
					formatColumnData.Column.Width = formatColumnData.Width;
			}
		}
		#endregion
	}
}
