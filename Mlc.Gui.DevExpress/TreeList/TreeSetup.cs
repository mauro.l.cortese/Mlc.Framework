﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using Mlc.Data;
using System;
using System.Reflection;
using System.Collections.Generic;
using DevExpress.XtraTreeList.Data;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Gestisce l'aspetto dei controlli GridView
	/// </summary>
	public sealed class TreeSetup
	{
		/// <summary>GridView da gestire</summary>
		private TreeList treeList = null;
		/// <summary>GridView da gestire</summary>
		private Type row = null;

		/// <summary>
		/// Inizializza una nuova istanza
		/// </summary>
		/// <param name="treeList">Istanza GridView da gestire</param>
		public TreeSetup(TreeList treeList, Type row)
		{
			this.treeList = treeList;
			this.row = row;

			if (this.treeList != null)
				this.treeList.RowHeight = 24;
		}

		/// <summary>
		/// Initializes the grid.
		/// </summary>
		public void InitTree(bool readOnly)
		{
			if (this.row == null) return;

			this.treeList.Columns.Clear();
			PropertyInfo[] propInfos = this.row.GetProperties();

			foreach (PropertyInfo prop in propInfos)
			{
				object[] attrs = prop.GetCustomAttributes(typeof(FieldAttribute), true);
				if (attrs.Length == 1)
				{
					FieldAttribute fieldAttr = attrs[0] as FieldAttribute;
					if (fieldAttr.Visible)
					{
						TreeListColumn tlc = new TreeListColumn();
						tlc.Name = prop.Name; // string.Format("col{0}",prop.Name);
						tlc.FieldName = prop.Name;
						tlc.Visible = true;
						tlc.Caption = prop.Name;
						tlc.OptionsColumn.ReadOnly = readOnly;
						switch (fieldAttr.HAlign)
						{
							case HorzAlignment.Left:
								tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
								tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
								break;
							case HorzAlignment.Center:
								tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
								tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
								break;
							case HorzAlignment.Right:
								tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
								tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
								break;
						}
						this.treeList.Columns.Add(tlc);
					}
				}
			}
		}
	}
}