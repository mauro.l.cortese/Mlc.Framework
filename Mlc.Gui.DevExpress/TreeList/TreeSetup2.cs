﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 11-15-2022
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 11-20-2022
// ***********************************************************************
// <copyright file="TreeSetup.cs" company="MLC">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using Mlc.Data;
using System;
using System.Reflection;
using System.Collections.Generic;
using DevExpress.XtraTreeList.Data;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class TreeSetup. This class cannot be inherited.
	/// </summary>
	public sealed class TreeSetup2
	{
		#region Fields
		/// <summary>
		/// The tree list
		/// </summary>
		private TreeList treeList = null;
		/// <summary>
		/// The row item
		/// </summary>
		private Type itemType = null;
		/// <summary>
		/// The row relation
		/// </summary>
		private Type relationType = null;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="TreeSetup" /> class.
		/// </summary>
		/// <param name="treeList">The tree list.</param>
		/// <param name="itemType">The row item.</param>
		/// <param name="relationType">The row relation.</param>
		public TreeSetup2(TreeList treeList, Type itemType, Type relationType = null)
		{
			this.treeList = treeList;
			this.itemType = itemType;
			this.relationType = relationType;

			if (this.treeList != null)
				this.treeList.RowHeight = 24;
		}
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets or sets the item columns.
		/// </summary>
		/// <value>The item columns.</value>
		public Dictionary<string, TreeListColumn> ItemColumns { get; set; } = new();

		/// <summary>
		/// Gets or sets the relation columns.
		/// </summary>
		/// <value>The relation columns.</value>
		public Dictionary<string, TreeListColumn> RelationColumns { get; set; } = new();
		public List<string> ExclusionColumns { get; private set; }
		public bool HideExcludedColumns { get; private set; }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Sets the exclusion columns.
		/// </summary>
		/// <param name="exclusionList">The exclusion list.</param>
		/// <param name="hideExcludedColumns">if set to <c>true</c> [hide exclusion columns].</param>
		internal void SetExclusionColumns(List<string> exclusionList, bool hideExcludedColumns = false)
		{
			this.ExclusionColumns = exclusionList;
			this.HideExcludedColumns = hideExcludedColumns;
		}

		/// <summary>
		/// Initializes the tree.
		/// </summary>
		/// <param name="readOnly">if set to <c>true</c> [read only].</param>
		public void InitTree(bool readOnly)
		{
			if (this.itemType == null) return;

			TreeListColumn col;
			this.treeList.Columns.Clear();
			ciclaColonne(this.itemType.GetProperties(), this.ItemColumns);
			if (this.relationType != null)
				ciclaColonne(this.relationType.GetProperties(), this.RelationColumns);

			void ciclaColonne(PropertyInfo[] propInfos, Dictionary<string, TreeListColumn> colums)
			{
				if (this.HideExcludedColumns)
					foreach (PropertyInfo prop in propInfos)
					{
						col = this.addColumn(readOnly, prop);
						if (col != null)
						{
							colums.Add(col.FieldName, col);
							if (this.ExclusionColumns.Contains(col.FieldName))
							{
								col.Visible = false;
								col.OptionsColumn.ReadOnly = true;
							}
						}
					}
				else
				{
					if (this.ExclusionColumns != null)
						foreach (PropertyInfo prop in propInfos)
							if (!this.ExclusionColumns.Contains(prop.Name))
							{
								col = this.addColumn(readOnly, prop);
								if (col != null)
									colums.Add(col.FieldName, col);
							}
				}
			}
		}
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Gets the column.
		/// </summary>
		/// <param name="readOnly">if set to <c>true</c> [read only].</param>
		/// <param name="prop">The property.</param>
		/// <returns>TreeListColumn.</returns>
		private TreeListColumn addColumn(bool readOnly, PropertyInfo prop)
		{
			TreeListColumn tlc = null;
			object[] attrs = prop.GetCustomAttributes(typeof(FieldAttribute), true);
			if (attrs.Length == 1)
			{
				FieldAttribute fieldAttr = attrs[0] as FieldAttribute;
				tlc = this.treeList.Columns.ColumnByFieldName(prop.Name);
				if (fieldAttr.Visible && tlc == null)
				{
					tlc = new();
					tlc.Name = prop.Name; // string.Format("col{0}",prop.Name);
					tlc.FieldName = prop.Name;
					tlc.Visible = true;
					tlc.Caption = prop.Name;
					tlc.OptionsColumn.ReadOnly = readOnly;
					tlc.UnboundType = this.getType(prop.PropertyType);

					switch (fieldAttr.HAlign)
					{
						case HorzAlignment.Left:
							tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
							tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
							break;
						case HorzAlignment.Center:
							tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
							tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
							break;
						case HorzAlignment.Right:
							tlc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
							tlc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
							break;
					}
					this.treeList.Columns.Add(tlc);
				}
			}
			return tlc;
		}

		/// <summary>
		/// Gets the type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>UnboundColumnType.</returns>
		private UnboundColumnType getType(Type type)
		{
			UnboundColumnType retVal = UnboundColumnType.Bound;
			if (type == typeof(string))
				retVal = UnboundColumnType.String;
			else if (type == typeof(bool))
				retVal = UnboundColumnType.Boolean;
			else if (type == typeof(int) || type == typeof(Int16) || type == typeof(Int64) || type == typeof(long))
				retVal = UnboundColumnType.Integer;
			else if (type == typeof(float) || type == typeof(double) || type == typeof(decimal))
				retVal = UnboundColumnType.Decimal;
			else if (type == typeof(DateTime))
				retVal = UnboundColumnType.DateTime;
			return retVal;
		}

	}

	#endregion

	#region Event Handlers
	#endregion
	#endregion
}
