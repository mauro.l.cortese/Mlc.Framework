﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-24-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-24-2017
// ***********************************************************************
// <copyright file="GridPopUpMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraBars;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class GridPopUpMgr.
    /// </summary>
    public class TreePopUpMgr
    {
        /// <summary>
        /// The popup menu
        /// </summary>
        private PopupMenu popupMenu;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreePopUpMgr"/> class.
        /// </summary>
        /// <param name="gridView">The grid view.</param>
        /// <param name="popupMenu">The popup menu.</param>
        public TreePopUpMgr(TreeList treeList, PopupMenu popupMenu)
        {
			treeList.OptionsMenu.EnableNodeMenu = true;
			//treeList.OptionsMenu.EnableNodeMenu = false;
			treeList.OptionsMenu.ShowExpandCollapseItems = false;
			treeList.PopupMenuShowing += treeList_PopupMenuShowing;
            treeList.MenuManager = popupMenu.Manager;
			this.popupMenu = popupMenu;
        }

        /// <summary>
        /// Handles the PopupMenuShowing1 event of the treeList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTreeList.PopupMenuShowingEventArgs"/> instance containing the event data.</param>
        private void treeList_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            TreeList treeList = (TreeList)sender;
            TreeListNode node = treeList.CalcHitInfo(e.Point).Node;
            treeList.SetFocusedNode(node);
            this.OnSetContextMenuRequest(new EventArgsSetContextMenuRequest(node));
            this.popupMenu.ShowPopup(this.popupMenu.Manager, treeList.PointToScreen(e.Point));
        }


        #region Dichiarazione Evento SetContextMenuRequest
        /// <summary>
        /// Definizione dell'evento.
        /// </summary>
        public event EventHandlerSetContextMenuRequest SetContextMenuRequest;
        
        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnSetContextMenuRequest(EventArgsSetContextMenuRequest e)
        {
            // Se ci sono ricettori in ascolto ...
            if (SetContextMenuRequest != null)
                // viene lanciato l'evento
                SetContextMenuRequest(this, e);
        }
        #endregion

    }
}
