﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;
using Mlc.Data;
using System.Reflection;
using System;
using System.Data;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Gestisce l'aspetto dei controlli GridView
    /// </summary>
    public class TreeNodeWriter
    {

        private DsRowMapBase row;

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="treeList">Istanza GridView da gestire</param>
        public TreeNodeWriter(DsRowMapBase row)
        {
            this.row = row;
        }

        public object GetCellValue(string fieldName)
        {
            return this.row.GetValue(fieldName);
        }
    }
}