﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 11-19-2022
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 11-21-2022
// ***********************************************************************
// <copyright file="TreeLoaderBase.cs" company="MLC">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using Mlc.Data;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class TreeLoaderBase.
	/// </summary>
	/// <typeparam name="I"></typeparam>
	/// <typeparam name="R"></typeparam>
	public class TreeLoaderBase<I, R> where I : IDataRowWrapper<I> where R : IDataRowWrapper<R>
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The node creator
		/// </summary>
		private readonly NodeCreator nodeCreator;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="TreeLoaderBase{I, R}" /> class.
		/// </summary>
		/// <param name="loaderConfig">The loader configuration.</param>
		public TreeLoaderBase(TreeLoaderConfig loaderConfig)
		{
			this.lConf = loaderConfig;
			this.nodeCreator = new(this.lConf);
			this.ClearTree();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TreeLoaderBase" /> class.
		/// </summary>
		/// <param name="treeList">The tree list.</param>
		public TreeLoaderBase(TreeList treeList)
			: this(new TreeLoaderConfig() { TreeList = treeList })
		{ }
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// The l conf
		/// </summary>
		private TreeLoaderConfig lConf = new();
		/// <summary>
		/// Gets or sets the loader configuration.
		/// </summary>
		/// <value>The loader configuration.</value>
		public TreeLoaderConfig LoaderConfig { get { return this.lConf; } set { this.lConf = value; } }
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Attaches the event handler.
		/// </summary>
		public void AttachEventHandler()
		{
			this.lConf.TreeList.CellValueChanged += nodeCellValueChanged;
			this.lConf.TreeList.FocusedNodeChanged += focusedNodeChanged;
		}
		/// <summary>
		/// Attaches the event handler.
		/// </summary>
		public void DetachEventHandler()
		{
			this.lConf.TreeList.CellValueChanged -= nodeCellValueChanged;
			this.lConf.TreeList.FocusedNodeChanged -= focusedNodeChanged;
		}



		/// <summary>
		/// Initializes the tree.
		/// </summary>
		/// <param name="itemsGrid">The items grid.</param>
		/// <param name="relationGrid">The relation grid.</param>
		/// <param name="readOnly">if set to <c>true</c> [read only].</param>
		public void InitTree(GridControl itemsGrid, GridControl relationGrid, bool readOnly = false)
		{
			this.lConf.ItemsGrid = itemsGrid;
			this.lConf.RelationGrid = relationGrid;
			this.InitTree((DataTable)this.lConf.ItemsGrid.DataSource, (DataTable)this.lConf.RelationGrid.DataSource, readOnly);

			attachEventSource(this.lConf.ItemsGrid.DefaultView);
			attachEventSource(this.lConf.RelationGrid.DefaultView);

			void attachEventSource(DevExpress.XtraGrid.Views.Base.BaseView view)
			{
				if (view is GridView gridView)
				{
					gridView.CellValueChanged -= GridViewCellValueChanged;
					gridView.CellValueChanged += GridViewCellValueChanged;
				}
			}
		}

		/// <summary>
		/// Initializes the tree.
		/// </summary>
		/// <param name="itemsTable">The items table.</param>
		/// <param name="relationTable">The relation table.</param>
		/// <param name="readOnly">if set to <c>true</c> [read only].</param>
		public void InitTree(DataTable itemsTable, DataTable relationTable, bool readOnly = false)
		{
			this.ClearTree();
			this.lConf.ItemsTable = itemsTable;
			this.lConf.RelationTable = relationTable;
			this.lConf.ReadOnly = readOnly;
			this.populateColumns();
		}

		/// <summary>
		/// Loads the tree.
		/// </summary>
		public void LoadTree()
		{
			this.lConf.TreeList.ClearNodes();
			try
			{
				this.beginTreeListEdit();
				this.loadTree();
				this.AttachEventHandler();
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			finally { this.endTreeListEdit(); }
		}

		/// <summary>
		/// Expands the first node.
		/// </summary>
		public void ExpandFirstNode()
		{
			foreach (TreeListNode node in this.nodeCreator.RootNodeList)
				node.Expanded = true;
		}

		/// <summary>
		/// Expands the first node.
		/// </summary>
		public void ExpandAllNode()
		{
			foreach (TreeListNode node in this.nodeCreator.RootNodeList)
				node.ExpandAll();
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public virtual void ClearNodes() => this.lConf.TreeList.ClearNodes();

		/// <summary>
		/// Clears the tree.
		/// </summary>
		public virtual void ClearTree()
		{
			this.ClearNodes();
			this.lConf.TreeList.Columns.Clear();
		}
		#region Metodi da testare
		/// <summary>
		/// Updates the nodes.
		/// </summary>
		/// <param name="relation">The relation.</param>
		/// <param name="col">The col. Null update all fields</param>
		/// <param name="refreshDataSource">if set to <c>true</c> [refresh data source].</param>
		public void UpdateNodes(R relation, TreeListColumn col = null, bool refreshDataSource = false)
			=> this.updateRelationNodes(relation.DataRow, col, refreshDataSource);


		/// <summary>
		/// Updates the nodes.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="col">The col. Null update all fields</param>
		/// <param name="refreshDataSource">if set to <c>true</c> [refresh data source].</param>
		public void UpdateNodes(I item, TreeListColumn col = null, bool refreshDataSource = false)
			=> this.updateItemNodes(item.DataRow, col, refreshDataSource);

		/// <summary>
		/// Updates the nodes.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="refreshDataSource">if set to <c>true</c> [refresh data source].</param>
		public void UpdateNodes(DataRow dataRow, string fieldName, bool refreshDataSource = false)
		{
			if (dataRow != null)
			{
				int idItem = (int)dataRow[this.lConf.IdItem];
				TreeListColumn col = this.lConf.TreeList.Columns.ColumnByFieldName(fieldName);

				NodeMapping nodMap = this.lConf.NodeItemMapping;
				if (nodMap.ContainsKey(idItem) && col != null)
					foreach (TreeListNode node in nodMap[idItem])
						this.updateNode(typeof(I), node, col);
			}
			if (refreshDataSource && this.lConf.ItemsGrid != null)
				this.lConf.ItemsGrid.RefreshDataSource();

			Application.DoEvents();
		}
		#endregion

		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Populates the columns.
		/// </summary>
		private void populateColumns()
		{
			if (this.lConf.TreeList != null && this.lConf.TreeList.Columns.Count == 0)
			{
				TreeSetup2 treeSetup;
				if (this.lConf.ShowRelationData)
					treeSetup = new(this.lConf.TreeList, typeof(I), typeof(R));
				else
					treeSetup = new(this.lConf.TreeList, typeof(I));

				treeSetup.SetExclusionColumns(new List<string>()
				{
					this.lConf.IdItem,
					this.lConf.IdRelation,
					this.lConf.IdParentField,
					this.lConf.IdChildField,
				}, this.lConf.HideExcludedColumns);

				treeSetup.InitTree(this.lConf.ReadOnly);

				this.lConf.ItemColumns = treeSetup.ItemColumns;
				this.lConf.RelationColumns = treeSetup.RelationColumns;
			}
		}

		/// <summary>
		/// Loads the tree.
		/// </summary>
		private void loadTree()
		{
			try
			{
				DataRow[] rootRows = this.lConf.RelationTable.Select($"{this.lConf.IdParentField}={this.lConf.IdChildField}");
				// loads all root nodes
				foreach (DataRow rowRel in rootRows)
				{
					int idItem = (int)rowRel[this.lConf.IdChildField];
					DataRow[] rootItems = this.lConf.ItemsTable.Select($"IdItem = {idItem}");
					if (rootItems.Length == 1)
						this.nodeCreator.AddNewNode(idItem, rootItems[0], rowRel, true);
				}
				// for each root node loads the children
				foreach (TreeListNode rootNode in this.nodeCreator.RootNodeList)
					this.loadChildrenRecursiv(rootNode);
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			this.ExpandFirstNode();
		}

		/// <summary>
		/// Loads the children recursiv.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		private void loadChildrenRecursiv(TreeListNode parentNode)
		{
			if (parentNode.Tag is NodeRelationTag<I, R> nodRelTag)
				try
				{
					DataRow[] relRows = this.lConf.RelationTable.Select($"{this.lConf.IdParentField} = {nodRelTag.IdItem} and {this.lConf.IdParentField}<>{this.lConf.IdChildField}");
					if (relRows.Length != 0)
						foreach (DataRow rowRel in relRows)
						{
							int idChild = (int)rowRel[this.lConf.IdChildField];
							DataRow[] childRows = this.lConf.ItemsTable.Select($"IdItem = {idChild}");
							foreach (DataRow rowItem in childRows)
								this.loadChildrenRecursiv(this.nodeCreator.AddNewNode(parentNode, idChild, rowItem, rowRel));
						}
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
		}

		/// <summary>
		/// Begins the tree list edit.
		/// </summary>
		private void beginTreeListEdit()
		{
			this.lConf.TreeList.SuspendLayout();
			this.lConf.TreeList.BeginUpdate();
			this.lConf.TreeList.BeginUnboundLoad();
		}

		/// <summary>
		/// Ends the tree list edit.
		/// </summary>
		private void endTreeListEdit()
		{
			this.lConf.TreeList.EndUnboundLoad();
			this.lConf.TreeList.EndUpdate();
			this.lConf.TreeList.ResumeLayout();
		}

		/// <summary>
		/// Updates the nodes.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		/// <param name="col">The col. Null update all fields</param>
		/// <param name="refreshDataSource">if set to <c>true</c> [refresh data source].</param>
		private void updateItemNodes(DataRow dataRow, TreeListColumn col = null, bool refreshDataSource = false)
		{
			if (dataRow != null)
			{
				NodeMapping nodMap = this.lConf.NodeItemMapping;
				int idItem = (int)dataRow[this.lConf.IdItem];
				if (nodMap.ContainsKey(idItem))
					foreach (TreeListNode node in nodMap[idItem])
						this.updateNode(typeof(I), node, col);
			}
			if (refreshDataSource && this.lConf.ItemsGrid != null)
				this.lConf.ItemsGrid.RefreshDataSource();

			Application.DoEvents();
		}

		/// <summary>
		/// Updates the nodes.
		/// </summary>
		/// <param name="dataRow">The data row.</param>
		/// <param name="col">The col. Null update all fields</param>
		/// <param name="refreshDataSource">if set to <c>true</c> [refresh data source].</param>
		private void updateRelationNodes(DataRow dataRow, TreeListColumn col = null, bool refreshDataSource = false)
		{
			if (dataRow != null)
			{
				NodeMapping nodMap = this.lConf.NodeItemMapping;
				int idRelation = (int)dataRow[this.lConf.IdRelation];
				if (nodMap.ContainsKey(idRelation))
					foreach (TreeListNode node in nodMap[idRelation])
						this.updateNode(typeof(R), node, col);
			}
			if (refreshDataSource && this.lConf.RelationGrid != null)
				this.lConf.RelationGrid.RefreshDataSource();

			Application.DoEvents();
		}

		/// <summary>
		/// Refreshes the nodes.
		/// </summary>
		/// <param name="rmp">The RMP.</param>
		/// <param name="column">The col.</param>
		private void updateNodes(IDataRowWrapper rmp, TreeListColumn column)
		{
			Type type = rmp.GetType();
			if (type == typeof(I))
				refresh(this.lConf.NodeItemMapping, (int)rmp.DataRow[lConf.IdItem]);
			else if (type == typeof(R))
				refresh(this.lConf.NodeRelationMapping, (int)rmp.DataRow[lConf.IdRelation]);

			void refresh(NodeMapping nodeMap, int id)
			{
				if (nodeMap != null && nodeMap.ContainsKey(id))
					foreach (TreeListNode node in nodeMap[id])
						this.updateNode(type, node, column);
			}
		}

		/// <summary>
		/// Updates the node.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="node">The node.</param>
		/// <param name="column">The col. Null update all fields</param>
		private void updateNode(Type type, TreeListNode node, TreeListColumn column = null)
		{
			if (node.Tag is NodeRelationTag<I, R> nodeRelTag)
			{
				if (type == typeof(I))
				{
					if (column != null)
						node.SetValue(column, nodeRelTag.RowItem[column.FieldName]);
					else
						foreach (TreeListColumn col in this.lConf.ItemColumns.Values)
							node.SetValue(col, nodeRelTag.RowItem[col.FieldName]);
				}
				else if (type == typeof(R))
				{
					if (column != null)
						node.SetValue(column, nodeRelTag.RowRelation[column.FieldName]);
					else
						foreach (TreeListColumn col in this.lConf.RelationColumns.Values)
							node.SetValue(col, nodeRelTag.RowRelation[col.FieldName]);
				}
			}
		}

		/// <summary>
		/// Refreshes the nodes.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="id">The identifier item.</param>
		private void refreshNodes(Type type, int id)
		{
			try
			{
				this.DetachEventHandler();
				if (type == typeof(I))
					foreach (TreeListNode node in this.lConf.NodeItemMapping[id])
						updateNode(type, node);

				else if (type == typeof(R))
					foreach (TreeListNode node in this.lConf.NodeRelationMapping[id])
						updateNode(type, node);
			}
			catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
			finally { this.AttachEventHandler(); }
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Handles the CellValueChanged event of the gridViewItems control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs" /> instance containing the event data.</param>
		protected virtual void GridViewCellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
		{
			if (sender is GridView gridView)
			{
				DataRow row = gridView.GetDataRow(e.RowHandle);
				if (gridView.GridControl.Equals(this.lConf.ItemsGrid))
					this.refreshNodes(typeof(I), (int)row[this.lConf.IdItem]);
				else if (gridView.GridControl.Equals(this.lConf.RelationGrid))
					this.refreshNodes(typeof(R), (int)row[this.lConf.IdRelation]);
			}
		}

		/// <summary>
		/// Handles the CellValueChanged event of the treeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CellValueChangedEventArgs" /> instance containing the event data.</param>
		protected virtual void nodeCellValueChanged(object sender, CellValueChangedEventArgs e)
		{
			if (e.Node.Tag is NodeRelationTag<I, R> nodRelTag)
			{
				//IDataRowWrapper<T> rmp  where T : IDataRowWrapper<T>;
				//DataRowWrapperBase<I> rmp = lConf.GetNodeDataSourceByField(e);
				//DataRowWrapperBase rmp = lConf.GetNodeDataSourceByField(e);
				IDataRowWrapper rmp = lConf.GetNodeDataSourceByField(e);



				object oldValue = rmp[e.Column.FieldName];

				object value = e.Node.GetValue(e.Column);

				if (rmp != null)
				{
					bool allowUdate = false;
					Type type = rmp.GetType();
					if (type == typeof(I))
					{
						EventArgsItemRowChanged eventArgs = new EventArgsItemRowChanged((I)rmp, e.Column, value, oldValue);
						this.OnItemRowChanged(eventArgs);
						allowUdate = eventArgs.AllowUpdate;
					}
					else if (type == typeof(R))
					{
						EventArgsRelationRowChanged eventArgs = new EventArgsRelationRowChanged((R)rmp, e.Column, value, oldValue);
						this.OnRelationRowChanged(eventArgs);
						allowUdate = eventArgs.AllowUpdate;
					}

					if (allowUdate)
					{
						rmp[e.Column.FieldName] = value;
						this.updateNodes(rmp, e.Column);

						if (lConf.RefreshDataSource)
							if (type is I && this.lConf.ItemsGrid != null)
								this.lConf.ItemsGrid.RefreshDataSource();
							else if (type is R && this.lConf.RelationGrid != null)
								this.lConf.RelationGrid.RefreshDataSource();
					}
					else
						e.Node.SetValue(e.Column, oldValue);
				}
			}
		}



		/// <summary>
		/// Focuseds the node changed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="FocusedNodeChangedEventArgs"/> instance containing the event data.</param>
		protected virtual void focusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
		{
			if (e.Node != null)
				if (e.Node.Tag is NodeRelationTag<I, R> nodRelTag)
				{
					this.OnFocusedNodeChanged(new EventArgsFocusedNodeChanged(
						e.Node,
						e.OldNode,
						nodRelTag.Item,
						nodRelTag.Relation
						));
				}
		}

		#endregion
		#endregion

		#region Event Definitions


		#region Event FocusedNodeChanged
		/// <summary>
		/// Event FocusedNodeChanged.
		/// </summary>
		public event EventHandlerFocusedNodeChanged FocusedNodeChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>  
		/// <param name="e">Event data</param>
		protected virtual void OnFocusedNodeChanged(EventArgsFocusedNodeChanged e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.FocusedNodeChanged?.Invoke(this, e);
		}
		#endregion

		#region FocusedNodeChanged event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		public delegate void EventHandlerFocusedNodeChanged(object sender, EventArgsFocusedNodeChanged e);

		/// <summary>
		/// FocusedNodeChanged arguments.
		/// </summary>
		public class EventArgsFocusedNodeChanged : System.EventArgs
		{
			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsFocusedNodeChanged"/> class.
			/// </summary>
			/// <param name="node">The node.</param>
			/// <param name="oldNode">The old node.</param>
			/// <param name="item">The item.</param>
			/// <param name="relation">The relation.</param>
			public EventArgsFocusedNodeChanged(TreeListNode node, TreeListNode oldNode, I item, R relation)
			{
				this.Node = node;
				this.OldNode = oldNode;
				this.Item = item;
				this.Relation = relation;
			}
			#endregion

			#region Properties
			/// <summary>
			/// Gets or sets the node.
			/// </summary>
			/// <value>The node.</value>
			public TreeListNode Node { get; set; }
			/// <summary>
			/// Gets the old node.
			/// </summary>
			/// <value>The old node.</value>
			public TreeListNode OldNode { get; }
			/// <summary>
			/// Gets the item.
			/// </summary>
			/// <value>The item.</value>
			public I Item { get; }
			/// <summary>
			/// Gets the relation.
			/// </summary>
			/// <value>The relation.</value>
			public R Relation { get; }
			#endregion
		}
		#endregion



		#region Event ItemRowChanged
		/// <summary>
		/// Event ItemRowChanged.
		/// </summary>
		public event EventHandlerItemRowChanged ItemRowChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnItemRowChanged(EventArgsItemRowChanged e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.ItemRowChanged?.Invoke(this, e);
		}
		#endregion

		#region Event RelationRowChanged
		/// <summary>
		/// Event ItemRowChanged.
		/// </summary>
		public event EventHandlerRelationRowChanged RelationRowChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnRelationRowChanged(EventArgsRelationRowChanged e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.RelationRowChanged?.Invoke(this, e);
		}
		#endregion


		#region RowChanged event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		public delegate void EventHandlerItemRowChanged(object sender, EventArgsItemRowChanged e);

		/// <summary>
		/// Delegate for event management.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		public delegate void EventHandlerRelationRowChanged(object sender, EventArgsRelationRowChanged e);

		/// <summary>
		/// ItemRowChanged arguments.
		/// </summary>
		public class EventArgsItemRowChanged : EventArgsRowChanged
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsItemRowChanged" /> class.
			/// </summary>
			/// <param name="item">The item.</param>
			/// <param name="column">The column.</param>
			/// <param name="newValue">The new value.</param>
			/// <param name="oldValue">The old value.</param>
			public EventArgsItemRowChanged(I item, TreeListColumn column, object newValue, object oldValue)
				: base(column, newValue, oldValue) => this.Item = item;
			/// <summary>
			/// Get or set the property
			/// </summary>
			/// <value>The item.</value>
			public I Item { get; private set; }
		}

		/// <summary>
		/// ItemRowChanged arguments.
		/// </summary>
		public class EventArgsRelationRowChanged : EventArgsRowChanged
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsRelationRowChanged" /> class.
			/// </summary>
			/// <param name="relation">The relation.</param>
			/// <param name="column">The column.</param>
			/// <param name="newValue">The new value.</param>
			/// <param name="oldValue">The old value.</param>
			public EventArgsRelationRowChanged(R relation, TreeListColumn column, object newValue, object oldValue)
				: base(column, newValue, oldValue) => this.Relation = relation;
			/// <summary>
			/// Get or set the property
			/// </summary>
			/// <value>The relation.</value>
			public R Relation { get; private set; }
		}

		/// <summary>
		/// ItemRowChanged arguments.
		/// </summary>
		public class EventArgsRowChanged : System.EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsRowChanged" /> class.
			/// </summary>
			/// <param name="column">The column.</param>
			/// <param name="newValue">The new value.</param>
			/// <param name="oldValue">The old value.</param>
			public EventArgsRowChanged(TreeListColumn column, object newValue, object oldValue)
			{
				this.Column = column;
				this.NewValue = newValue;
				this.OldValue = oldValue;
			}

			/// <summary>
			/// Gets the column.
			/// </summary>
			/// <value>The column.</value>
			public TreeListColumn Column { get; private set; }

			/// <summary>
			/// Creates new value.
			/// </summary>
			/// <value>The new value.</value>
			public object NewValue { get; private set; }

			/// <summary>
			/// Gets the old value.
			/// </summary>
			/// <value>The old value.</value>
			public object OldValue { get; private set; }

			/// <summary>
			/// Gets a value indicating whether [allow update].
			/// </summary>
			/// <value><c>true</c> if [allow update]; otherwise, <c>false</c>.</value>
			public bool AllowUpdate { get; set; } = true;

		}

		#endregion

		#endregion

		#region Embedded Types
		/// <summary>
		/// Class NodeMapping.
		/// Implements the <see cref="System.Collections.Generic.Dictionary{System.Int32, System.Collections.Generic.List{DevExpress.XtraTreeList.Nodes.TreeListNode}}" />
		/// </summary>
		/// <seealso cref="System.Collections.Generic.Dictionary{System.Int32, System.Collections.Generic.List{DevExpress.XtraTreeList.Nodes.TreeListNode}}" />
		internal class NodeMapping : Dictionary<int, List<TreeListNode>>
		{
			/// <summary>
			/// Adds the specified identifier relation.
			/// </summary>
			/// <param name="id">The identifier relation.</param>
			/// <param name="node">The node.</param>
			/// <returns>TreeListNode.</returns>
			internal TreeListNode AddNode(int id, TreeListNode node)
			{
				this.addNodeToList(id, node);
				return node;
			}

			/// <summary>
			/// Adds the node to list.
			/// </summary>
			/// <param name="id">The identifier.</param>
			/// <param name="node">The node.</param>
			protected void addNodeToList(int id, TreeListNode node)
			{
				if (!this.ContainsKey(id))
					this.Add(id, new());

				this[id].Add(node);
			}
		}

		/// <summary>
		/// Class NodeCreator.
		/// </summary>
		private class NodeCreator
		{
			/// <summary>
			/// The root node
			/// </summary>
			/// <value>The root node list.</value>
			public List<TreeListNode> RootNodeList { get; } = new();

			/// <summary>
			/// The l conf
			/// </summary>
			private TreeLoaderConfig lConf;

			/// <summary>
			/// Gets or sets the loader configuration.
			/// </summary>
			/// <value>The loader configuration.</value>
			internal TreeLoaderConfig LoaderConfig { get => this.lConf; private set => this.lConf = value; }

			/// <summary>
			/// Initializes a new instance of the <see cref="NodeCreator" /> class.
			/// </summary>
			/// <param name="lConf">The l conf.</param>
			internal NodeCreator(TreeLoaderConfig lConf) => this.lConf = lConf;

			/// <summary>
			/// Adds the specified identifier child.
			/// </summary>
			/// <param name="idItem">The identifier item.</param>
			/// <param name="rowItem">The row item.</param>
			/// <param name="rowRel">The row relative.</param>
			/// <param name="isRoot">if set to <c>true</c> [is root].</param>
			/// <returns>TreeListNode.</returns>
			internal TreeListNode AddNewNode(int idItem, DataRow rowItem, DataRow rowRel, bool isRoot = false)
				=> this.AddNewNode(null, idItem, rowItem, rowRel, isRoot);

			/// <summary>
			/// Adds the specified root node.
			/// </summary>
			/// <param name="parentNode">The parent node.</param>
			/// <param name="idItem">The identifier item.</param>
			/// <param name="rowItem">The row item.</param>
			/// <param name="rowRel">The row relative.</param>
			/// <param name="isRoot">if set to <c>true</c> [is root].</param>
			/// <returns>TreeListNode.</returns>
			internal TreeListNode AddNewNode(TreeListNode parentNode, int idItem, DataRow rowItem, DataRow rowRel, bool isRoot = false)
			{
				TreeListNode node = this.lConf.TreeList.AppendNode(rowItem, parentNode);
				try
				{
					if (this.lConf.RelationColumns.Count != 0)
						foreach (TreeListColumn col in this.lConf.RelationColumns.Values)
							node.SetValue(col, rowRel[col.FieldName]);
				}
				catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }

				node.Tag = new NodeRelationTag<I, R>(idItem, rowItem, rowRel);
				if (isRoot)
					this.RootNodeList.Add(node);

				this.lConf.NodeItemMapping.AddNode(idItem, node);
				this.lConf.NodeRelationMapping.AddNode((int)rowRel[this.lConf.IdRelation], node);
				return node;
			}
		}

		/// <summary>
		/// Class TreeLoaderBaseConfig.
		/// </summary>
		public class TreeLoaderConfig
		{
			/// <summary>
			/// Gets the node item mapping.
			/// </summary>
			/// <value>The node item mapping.</value>
			internal NodeMapping NodeItemMapping { get; } = new();

			/// <summary>
			/// Gets the node relation mapping.
			/// </summary>
			/// <value>The node relation mapping.</value>
			internal NodeMapping NodeRelationMapping { get; } = new();

			/// <summary>
			/// Gets or sets the tree list.
			/// </summary>
			/// <value>The tree list.</value>
			public TreeList TreeList { get; set; }

			/// <summary>
			/// Gets the identifier item child.
			/// </summary>
			/// <value>The identifier item child.</value>
			public string IdItem { get; set; } = "IdItem";

			/// <summary>
			/// Gets the identifier item child.
			/// </summary>
			/// <value>The identifier item child.</value>
			public string IdRelation { get; set; } = "IdRelation";

			/// <summary>
			/// Gets the identifier item child.
			/// </summary>
			/// <value>The identifier item child.</value>
			public string IdChildField { get; set; } = "IdChild";

			/// <summary>
			/// Gets the identifier parent field.
			/// </summary>
			/// <value>The identifier parent field.</value>
			public string IdParentField { get; set; } = "IdParent";

			/// <summary>
			/// Gets the relation table.
			/// </summary>
			/// <value>The relation table.</value>
			public DataTable RelationTable { get; set; }

			/// <summary>
			/// Gets the items table.
			/// </summary>
			/// <value>The items table.</value>
			public DataTable ItemsTable { get; set; }

			/// <summary>
			/// Gets the items grid.
			/// </summary>
			/// <value>The items grid.</value>
			public GridControl ItemsGrid { get; set; }

			/// <summary>
			/// Gets the relation grid.
			/// </summary>
			/// <value>The relation grid.</value>
			public GridControl RelationGrid { get; set; }

			/// <summary>
			/// Gets or sets a value indicating whether [show relation data].
			/// </summary>
			/// <value><c>true</c> if [show relation data]; otherwise, <c>false</c>.</value>
			public bool ShowRelationData { get; set; } = false;

			/// <summary>
			/// Gets a value indicating whether [read only].
			/// </summary>
			/// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
			public bool ReadOnly { get; set; } = true;

			/// <summary>
			/// Gets the item columns.
			/// </summary>
			/// <value>The item columns.</value>
			public Dictionary<string, TreeListColumn> ItemColumns { get; internal set; }

			/// <summary>
			/// Gets the relation columns.
			/// </summary>
			/// <value>The relation columns.</value>
			public Dictionary<string, TreeListColumn> RelationColumns { get; internal set; }

			/// <summary>
			/// Gets a value indicating whether [hide excluded columns].
			/// </summary>
			/// <value><c>true</c> if [hide excluded columns]; otherwise, <c>false</c>.</value>
			public bool HideExcludedColumns { get; set; }

			/// <summary>
			/// Gets or sets a value indicating whether [refresh data source].
			/// </summary>
			/// <value><c>true</c> if [refresh data source]; otherwise, <c>false</c>.</value>
			public bool RefreshDataSource { get; set; } = true;

			/// <summary>
			/// Gets the node data source by field.
			/// </summary>
			/// <param name="e">The <see cref="CellValueChangedEventArgs" /> instance containing the event data.</param>
			/// <returns>DsRowMapBase.</returns>
			internal IDataRowWrapper GetNodeDataSourceByField(CellValueChangedEventArgs e) 
			{
				object retVal = null;
				string fieldName = e.Column.FieldName;
				if (e.Node.Tag is NodeRelationTag<I, R> nodRel)
					if (this.ItemColumns.ContainsKey(fieldName))
						retVal = nodRel.Item;
					else if (this.RelationColumns.ContainsKey(fieldName))
						retVal= nodRel.Relation;
				return (IDataRowWrapper)retVal;
			}
		}
	}
	#endregion
}


