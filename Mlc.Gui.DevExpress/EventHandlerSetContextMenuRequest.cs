﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Gui.DevExpressTools
{
    #region Definizione Evento SetContextMenuRequest
    /// <summary>
    /// Delegato per la gestione dell'evento.
    /// </summary>
    public delegate void EventHandlerSetContextMenuRequest(object sender, EventArgsSetContextMenuRequest e);

    /// <summary>
    /// Classe per gli argomenti della generazione di un evento.
    /// </summary>
    public class EventArgsSetContextMenuRequest : System.EventArgs
    {
        #region Costruttori
        /// <summary>
        /// Inizilizza una nuova istanza
        /// </summary>
        /// <param name="campo"></param>
        public EventArgsSetContextMenuRequest(object item)
        {
            this.item = item;
        }
        #endregion

        #region Proprietà
        /// <summary></summary>
        private object item;
        /// <summary>
        /// Restituisce o imposta il ...
        /// </summary>
        public object Item { get { return this.item; } set { this.item = value; } }
        #endregion
    }
    #endregion

}
