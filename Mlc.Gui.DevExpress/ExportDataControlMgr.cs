﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 11-22-2023
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 11-22-2023
// ***********************************************************************
// <copyright file="ExportToExcelMgr.cs" company="MLC">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using Mlc.Shell;
using Mlc.Shell.IO;
using DevExpress.XtraGrid;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class ExportToExcelMgr.
	/// </summary>
	public class ExportDataControlMgr
	{
		/// <summary>The tree list</summary>
		private TreeList treeList;

		/// <summary>The grid control</summary>
		private GridControl gridControl;


		/// <summary>
		/// Enum ExportMode
		/// </summary>
		public enum ExportMode { Csv, Txt, Xlsx, Pdf }


		/// <summary>
		/// Initializes a new instance of the <see cref="ExportDataControlMgr" /> class.
		/// </summary>
		public ExportDataControlMgr() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportDataControlMgr" /> class.
		/// </summary>
		/// <param name="treeList">The tree list.</param>
		public ExportDataControlMgr(TreeList treeList) => this.treeList = treeList;

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportDataControlMgr"/> class.
		/// </summary>
		/// <param name="gridControl">The grid control.</param>
		public ExportDataControlMgr(GridControl gridControl) => this.gridControl = gridControl;

		/// <summary>
		/// Saves the and open.
		/// </summary>
		public void Save(ExportMode mode, bool openFile = true)
		{
			FileSystemMgr fsm = new (new SysInfo().ExecutableTempFolder);

			if (!fsm.Exists)
				fsm.Create(FileSystemTypes.Directory);

			if (fsm.Exists)
			{
				string file = Path.Combine(fsm.Path, $"{DateTime.Now:yyyyMMdd-HHMMss}.{mode}").ToLower();
				switch (mode)
				{
					case ExportMode.Csv:
						if (this.treeList != null) this.treeList.ExportToCsv(file); else if (this.gridControl != null) this.gridControl.ExportToCsv(file);
						break;
					case ExportMode.Txt:
						if (this.treeList != null) this.treeList.ExportToText(file); else if (this.gridControl != null) this.gridControl.ExportToText(file);
						break;
					case ExportMode.Xlsx:
						if (this.treeList != null) this.treeList.ExportToXlsx(file); else if (this.gridControl != null) this.gridControl.ExportToXlsx(file);
						break;
					case ExportMode.Pdf:
						if (this.treeList != null) this.treeList.ExportToPdf(file); else if (this.gridControl != null) this.gridControl.ExportToPdf(file);
						break;
				}
				if (openFile)
					Process.Start(file);
			}
		}
	}
}
