﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : Cortese Mauro Luigi
// Created          : 11-22-2017
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 11-22-2017
// ***********************************************************************
// <copyright file="LayoutMgr.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraTreeList;
using DevExpress.XtraBars.Ribbon;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.IO;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Class LayoutMgr.
    /// </summary>
    public class LayoutMgr
    {
        #region Private Constants                   
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        #endregion

        #region Public Properties
        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Saves the layout.
        /// </summary>
        /// <param name="devControls">The dev controls.</param>
        public void SaveLayout(List<object> devControls, string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (Directory.Exists(folder))
                foreach (object obj in devControls)
                    try
                    {
                        if (obj is GridView)
                            new GridAppearance((GridView)obj).SaveLayout($"{folder}\\{((GridView)obj).Name.GetFirstMaius(true)}.xml");
                        else if (obj is TreeList)
                            ((TreeList)obj).SaveLayoutToXml($"{folder}\\{((TreeList)obj).Name.GetFirstMaius(true)}.xml");
                        else if (obj is LayoutControl)
                            ((LayoutControl)obj).SaveLayoutToXml($"{folder}\\{((LayoutControl)obj).Name.GetFirstMaius(true)}.xml");
                        else if (obj is DockManager)
                            ((DockManager)obj).SaveLayoutToXml($"{folder}\\DockManager.xml");
                        else if (obj is BarManager)
                            ((BarManager)obj).SaveLayoutToXml($"{folder}\\BarManager.xml");
                        else if (obj is RibbonControl)
                            ((RibbonControl)obj).SaveLayoutToXml($"{folder}\\Ribbon.xml");
                    }
                    catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }

        /// <summary>
        /// Loads the layout.
        /// </summary>
        /// <param name="devControls">The dev controls.</param>
        /// <param name="folder">The folder.</param>
        public void LoadLayout(List<object> devControls, string folder)
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (Directory.Exists(folder))
                foreach (object obj in devControls)
                    try
                    {
                        if (obj is GridView)
                            try {new GridAppearance((GridView)obj).LoadLayout($"{folder}\\{((GridView)obj).Name.GetFirstMaius(true)}.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                        else if (obj is TreeList)
                            try { ((TreeList)obj).RestoreLayoutFromXml($"{folder}\\{((TreeList)obj).Name.GetFirstMaius(true)}.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                        else if (obj is LayoutControl)
                            try { ((LayoutControl)obj).RestoreLayoutFromXml($"{folder}\\{((LayoutControl)obj).Name.GetFirstMaius(true)}.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                        else if (obj is DockManager)
                            try { ((DockManager)obj).RestoreLayoutFromXml($"{folder}\\DockManager.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                        else if (obj is BarManager)
                            try { ((BarManager)obj).RestoreLayoutFromXml($"{folder}\\BarManager.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                        else if (obj is RibbonControl)
                            try { ((RibbonControl)obj).RestoreLayoutFromXml($"{folder}\\Ribbon.xml"); }
                            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
                    }
                    catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion
    }
}
