﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mlc.Shell.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace Mlc.Gui.DevExpressTools
{
    public partial class ViewLogger : UserControl, IViewLogger
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewLogger"/> class.
        /// </summary>
        public ViewLogger()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties
        private Logger logger = null;
        public Logger Logger
        {
            get { return this.logger; }
            set
            {
                this.logger = value;
                if (this.logger != null)
                    this.gridControlLog.DataSource = this.logger;
            }
        }

        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public void DataRefresh()
        {
            if (this.gridControlLog != null)
                this.gridControlLog.RefreshDataSource();
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion

        private void gridViewLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            GridView gw = (GridView)sender;
            object o = gw.GetRow(e.FocusedRowHandle);

            if (o is Log)
            {
                Log log = (Log)o;
                if (log.Level == LogLevel.Error)
                {
                    this.richEditControlStackTrace.Text = log.CmdData;
                    layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    splitterItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else
                {
                    layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    splitterItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                }
                this.richEditControlMessage.Text = log.Message;
            }
        }
    }
}
