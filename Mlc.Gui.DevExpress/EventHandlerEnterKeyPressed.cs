﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mlc.Gui.DevExpressTools
{
    #region Definizione Evento EnterKeyPressed
    /// <summary>
    /// Delegato per la gestione dell'evento.
    /// </summary>
    public delegate void EventHandlerEnterKeyPressed(object sender, EventArgsEnterKeyPressed e);

    /// <summary>
    /// Classe per gli argomenti della generazione di un evento.
    /// </summary>
    public class EventArgsEnterKeyPressed : System.EventArgs
    {
        #region Costruttori
        /// <summary>
        /// Inizilizza una nuova istanza
        /// </summary>
        /// <param name="value"></param>
        public EventArgsEnterKeyPressed(string value)
        {
            this.value = value;
        }
        #endregion

        #region Proprietà
        /// <summary></summary>
        private string value;
        /// <summary>
        /// Restituisce o imposta il ...
        /// </summary>
        public string Value { get { return this.value; } set { value = this.value; } }
        #endregion
    }
    #endregion
}
