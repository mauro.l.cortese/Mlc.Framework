﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using Mlc.Data;
using Mlc.Shell;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class NodeRelationTag.
	/// </summary>
	/// <typeparam name="I"></typeparam>
	/// <typeparam name="R"></typeparam>
	public class NodeRelationTag<I, R> where I : DsRowMapBase where R : DsRowMapBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="NodeRelationTag{I, R}"/> class.
		/// </summary>
		/// <param name="idChild">The identifier child.</param>
		/// <param name="rowItem">The row item.</param>
		/// <param name="rowRelation">The row relative.</param>
		public NodeRelationTag(Int64 idChild, DataRow rowItem, DataRow rowRelation)
		{
			IdItem = idChild;
			RowItem = rowItem;
			RowRelation = rowRelation;
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <value>The item.</value>
		public I Item
		{
			get
			{
				I item = (I)Activator.CreateInstance(typeof(I));
				item.DataRow = this.RowItem;
				return item;
			}
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <value>The item.</value>
		public R Relation
		{
			get
			{
				R item = (R)Activator.CreateInstance(typeof(R));
				item.DataRow = this.RowRelation;
				return item;
			}
		}

		/// <summary>
		/// Gets the identifier child.
		/// </summary>
		/// <value>The identifier child.</value>
		public Int64 IdItem { get; }

		/// <summary>
		/// Gets the row item.
		/// </summary>
		/// <value>The row item.</value>
		public DataRow RowItem { get; }

		/// <summary>
		/// Gets the row relative.
		/// </summary>
		/// <value>The row relative.</value>
		public DataRow RowRelation { get; }
	}

}
