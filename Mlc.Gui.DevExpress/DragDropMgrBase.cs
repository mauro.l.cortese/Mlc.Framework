﻿// ***********************************************************************
// Assembly         : Mlc.Gui.DevExpress
// Author           : mauro.luigi.cortese
// Created          : 05-06-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 09-11-2020
// ***********************************************************************
// <copyright file="DragDropMgrBase.cs" company="Microsoft">
//     Copyright © Microsoft 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace Mlc.Gui.DevExpressTools
{
	/// <summary>
	/// Class DragDropMgrBase.
	/// </summary>
	public class DragDropMgrBase
	{
		#region Constants
		#endregion

		#region Enumerations
		#endregion

		#region Fields
		/// <summary>
		/// The hit information
		/// </summary>
		private GridHitInfo hitInfo = null;
		/// <summary>
		/// The memory editor show mode
		/// </summary>
		private EditorShowMode memEditorShowMode;
		/// <summary>
		/// The allow drop
		/// </summary>
		private bool allowDrop;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DragDropMgrBase" /> class.
		/// </summary>
		/// <param name="gridview">The gridview.</param>
		/// <param name="treeList">The tree list.</param>
		/// <exception cref="Exception">I controlli passati NON sono inizializzati (NULL)</exception>
		public DragDropMgrBase(GridView gridview, TreeList treeList)
		{
			this.GridView = gridview;
			this.GridControl = gridview?.GridControl;
			this.TreeList = treeList;

			//this.TreeList.OptionsDragAndDrop.DragNodesMode = DragNodesMode.Single;

			if (GridControl == null | TreeList == null)
				throw new Exception("I controlli passati NON sono inizializzati (NULL)");

		}
		#endregion

		#region Event Handlers - CommandsEngine
		#endregion

		#region Public
		#region Properties
		/// <summary>
		/// Gets the grid control.
		/// </summary>
		/// <value>The grid control.</value>
		public GridControl GridControl { get; internal set; }
		/// <summary>
		/// Gets the grid view.
		/// </summary>
		/// <value>The grid view.</value>
		public GridView GridView { get; internal set; }
		/// <summary>
		/// Gets the tree list.
		/// </summary>
		/// <value>The tree list.</value>
		public TreeList TreeList { get; internal set; }

		/// <summary>
		/// The reorder tree node enabled
		/// </summary>
		private bool reorderTreeNodeEnabled;

		/// <summary>
		/// Gets or sets a value indicating whether [reorder tree node enabled].
		/// </summary>
		/// <value><c>true</c> if [reorder tree node enabled]; otherwise, <c>false</c>.</value>
		public bool ReorderTreeNodeEnabled
		{
			get { return this.reorderTreeNodeEnabled; }
			set
			{
				this.reorderTreeNodeEnabled = value;
				this.setEventHandler();
			}
		}

		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Disables the drag drop.
		/// </summary>
		public void DisableDragDrop()
		{
			this.disableDragDropFromGrid();
			this.disableDragDropForNodes();
		}
		#endregion
		#endregion

		#region Internal
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		#endregion
		#endregion

		#region Protected
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method
		/// <summary>
		/// Drops the data.
		/// </summary>
		/// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
		protected virtual void DropData(DragEventArgs e)
		{

		}
		#endregion
		#endregion

		#region Private
		#region Properties
		#endregion

		#region Method Static
		#endregion

		#region Method

		/// <summary>
		/// Sets the event handler.
		/// </summary>
		private void setEventHandler()
		{
			if (this.ReorderTreeNodeEnabled)
			{
				this.disableDragDropFromGrid();
				this.enableDragDropForNodes();
				this.TreeList.ClearSorting();
			}
			else
			{
				this.disableDragDropForNodes();
				this.enableDragDropFromGrid();
			}
		}

		/// <summary>
		/// Enables the drag drop for nodes.
		/// </summary>
		private void enableDragDropForNodes()
		{
			this.allowDrop = this.TreeList.AllowDrop;
			this.TreeList.AllowDrop = true;
			this.TreeList.OptionsDragAndDrop.DragNodesMode = DragNodesMode.Single;
			//this.TreeList.DragOver += treeList_DragOver;
			this.TreeList.DragDrop += treeList_Node_DragDrop;
			this.TreeList.CalcNodeDragImageIndex += treeList_CalcNodeDragImageIndex;
		}

		/// <summary>
		/// Enables the drag drop from grid.
		/// </summary>
		private void enableDragDropFromGrid()
		{
			this.memEditorShowMode = this.GridView.OptionsBehavior.EditorShowMode;
			this.allowDrop = this.TreeList.AllowDrop;
			this.GridView.OptionsBehavior.EditorShowMode = EditorShowMode.Click;
			this.TreeList.AllowDrop = true;
			this.GridControl.MouseDown += gridControl_MouseDown;
			this.GridControl.MouseMove += gridControl_MouseMove;
			this.TreeList.DragEnter += treeList_DragEnter;
			//this.TreeList.DragOver += treeList_DragOver;
			this.TreeList.DragDrop += treeList_DragDrop;

		}

		/// <summary>
		/// Disables the drag drop for nodes.
		/// </summary>
		private void disableDragDropForNodes()
		{
			this.TreeList.AllowDrop = this.allowDrop;
			//this.TreeList.DragOver -= treeList_DragOver;
			this.TreeList.DragDrop -= treeList_Node_DragDrop;
			this.TreeList.CalcNodeDragImageIndex -= treeList_CalcNodeDragImageIndex;
		}


		/// <summary>
		/// Disables the drag drop from grid.
		/// </summary>
		private void disableDragDropFromGrid()
		{
			this.TreeList.AllowDrop = this.allowDrop;
			this.GridControl.MouseDown -= gridControl_MouseDown;
			this.GridControl.MouseMove -= gridControl_MouseMove;
			this.TreeList.DragEnter -= treeList_DragEnter;
			this.TreeList.DragOver -= treeList_DragEnter;
			this.TreeList.DragDrop -= treeList_DragDrop;
		}

		/// <summary>
		/// Gets the drag drop effect.
		/// </summary>
		/// <param name="tl">The tl.</param>
		/// <param name="dragNode">The drag node.</param>
		/// <returns>DragDropEffects.</returns>
		private DragDropEffects getDragDropEffect(TreeList tl, TreeListNode dragNode)
		{
			TreeListNode targetNode;
			Point p = tl.PointToClient(Control.MousePosition);
			targetNode = tl.CalcHitInfo(p).Node;

			if (dragNode != null && targetNode != null
				&& dragNode != targetNode
				&& dragNode.ParentNode == targetNode.ParentNode)
				return DragDropEffects.Move;
			else
				return DragDropEffects.None;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the MouseDown event of the gridControl control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
		private void gridControl_MouseDown(object sender, MouseEventArgs e)
		{
			this.hitInfo = this.GridView.CalcHitInfo(new Point(e.X, e.Y));
		}

		/// <summary>
		/// Handles the MouseMove event of the gridControl control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
		private void gridControl_MouseMove(object sender, MouseEventArgs e)
		{
			if (this.hitInfo == null) return;
			if (e.Button != MouseButtons.Left) return;
			Rectangle dragRect = new Rectangle(new Point(
				this.hitInfo.HitPoint.X - SystemInformation.DragSize.Width / 2,
				this.hitInfo.HitPoint.Y - SystemInformation.DragSize.Height / 2), SystemInformation.DragSize);
			if (!(this.hitInfo.RowHandle == GridControl.InvalidRowHandle) && !dragRect.Contains(new Point(e.X, e.Y)))
			{
				Object data = this.GridView.GetRow(this.hitInfo.RowHandle);
				this.GridControl.DoDragDrop(data, DragDropEffects.Copy);
			}
		}

		/// <summary>
		/// Handles the DragEnter event of the treeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
		private void treeList_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
		}

		/// <summary>
		/// Handles the DragDrop event of the treeList_Node control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
		private void treeList_Node_DragDrop(object sender, DragEventArgs e)
		{
			// drag node
			TreeListNode dragNode, targetNode;
			TreeList tl = sender as TreeList;
			Point p = tl.PointToClient(new Point(e.X, e.Y));

			dragNode = e.Data.GetData(typeof(TreeListNode)) as TreeListNode;
			targetNode = tl.CalcHitInfo(p).Node;

			tl.SetNodeIndex(dragNode, tl.GetNodeIndex(targetNode));
			e.Effect = DragDropEffects.None;

			this.OnNodePositionChanged(new EventArgsNodePositionChanged(dragNode, targetNode));
		}


		/// <summary>
		/// Handles the DragDrop event of the treeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
		private void treeList_DragDrop(object sender, DragEventArgs e)
		{
			this.DropData(e);
		}


		/// <summary>
		/// Handles the DragOver event of the treeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Windows.Forms.DragEventArgs" /> instance containing the event data.</param>
		private void treeList_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			TreeListNode dragNode = e.Data.GetData(typeof(TreeListNode)) as TreeListNode;
			e.Effect = this.getDragDropEffect(sender as TreeList, dragNode);

			e.Effect = DragDropEffects.Copy;
			e.Effect = DragDropEffects.None;
		}

		/// <summary>
		/// Handles the CalcNodeDragImageIndex event of the treeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CalcNodeDragImageIndexEventArgs" /> instance containing the event data.</param>
		private void treeList_CalcNodeDragImageIndex(object sender, CalcNodeDragImageIndexEventArgs e)
		{
			TreeList tl = sender as TreeList;
			if (this.getDragDropEffect(tl, tl.FocusedNode) == DragDropEffects.None)
				e.ImageIndex = -1;  // no icon
			else
				e.ImageIndex = 1;  // the reorder icon (a curved arrow)
		}

		#endregion
		#endregion

		#region Event Definitions

		#region Event AfterAddDroppedNode
		/// <summary>
		/// Event AfterAddDroppedNode.
		/// </summary>
		public event EventHandlerAfterAddDroppedNode AfterAddDroppedNode;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnAfterAddDroppedNode(EventArgsAfterAddDroppedNode e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.AfterAddDroppedNode?.Invoke(this, e);
		}
		#endregion

		#region AfterAddDroppedNode event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		public delegate void EventHandlerAfterAddDroppedNode(object sender, EventArgsAfterAddDroppedNode e);

		/// <summary>
		/// AfterAddDroppedNode arguments.
		/// </summary>
		public class EventArgsAfterAddDroppedNode : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsNodePositionChanged" /> class.
			/// </summary>
			/// <param name="newNode">The drag node.</param>
			/// <param name="targetNode">The target node.</param>
			public EventArgsAfterAddDroppedNode(TreeListNode newNode, TreeListNode targetNode)
			{
				this.NewNode = newNode;
				this.TargetNode = targetNode;
			}
			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			/// <value>The drag node.</value>
			public TreeListNode NewNode { get; private set; }
			/// <summary>
			/// Gets the target node.
			/// </summary>
			/// <value>The target node.</value>
			public TreeListNode TargetNode { get; private set; }
			#endregion
		}
		#endregion


		#region Event NodePositionChanged
		/// <summary>
		/// Event NodePositionChanged.
		/// </summary>
		public event EventHandlerNodePositionChanged NodePositionChanged;

		/// <summary>
		/// Method to raise the event
		/// </summary>
		/// <param name="e">Event data</param>
		protected virtual void OnNodePositionChanged(EventArgsNodePositionChanged e)
		{
			// If there are receivers listening ...
			// ... the event is raised
			this.NodePositionChanged?.Invoke(this, e);
		}
		#endregion

		#region NodePositionChanged event arguments definition
		/// <summary>
		/// Delegate for event management.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		public delegate void EventHandlerNodePositionChanged(object sender, EventArgsNodePositionChanged e);

		/// <summary>
		/// Class EventArgsNodePositionChanged.
		/// Implements the <see cref="System.EventArgs" />
		/// </summary>
		/// <seealso cref="System.EventArgs" />
		public class EventArgsNodePositionChanged : System.EventArgs
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="EventArgsNodePositionChanged" /> class.
			/// </summary>
			/// <param name="dragNode">The drag node.</param>
			/// <param name="targetNode">The target node.</param>
			public EventArgsNodePositionChanged(TreeListNode dragNode, TreeListNode targetNode)
			{
				this.DragNode = dragNode;
				this.TargetNode = targetNode;
			}
			#endregion

			#region Properties
			/// <summary>
			/// Get or set the property
			/// </summary>
			/// <value>The drag node.</value>
			public TreeListNode DragNode { get; private set; }
			/// <summary>
			/// Gets the target node.
			/// </summary>
			/// <value>The target node.</value>
			public TreeListNode TargetNode { get; private set; }
			#endregion
		}
		#endregion

		#endregion

		#region Embedded Types
		#endregion
	}
}
