﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mlc.Shell.IO;

namespace Mlc.Gui.DevExpressTools
{
    public partial class ViewSqlLogger : UserControl, IViewLogger
    {

        #region Private Constants        
        #endregion

        #region Private Enumerations
        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields
        #endregion

        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewSqlLogger"/> class.
        /// </summary>
        public ViewSqlLogger()
        {
            InitializeComponent();
            this.sqlEditorControl.Clear();
        }

        #endregion

        #region Public Properties
        private Logger logger = null;
        public Logger Logger
        {
            get { return this.logger; }
            set
            {
                this.logger = value;
                if (this.logger != null)
                    this.gridControlLog.DataSource = this.logger;
            }
        }

        #endregion

        #region Public Static Method
        #endregion

        #region Public Method
        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.sqlEditorControl.Clear();
        }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public void DataRefresh()
        {
            if (this.gridControlLog != null)
                this.gridControlLog.RefreshDataSource();
        }
        #endregion

        #region Public Event Handlers - CommandsEngine
        #endregion

        #region Private Event Handlers
        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridViewLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Log log = (Log)this.gridViewLog.GetRow(e.FocusedRowHandle);
            this.labelIndex.Text = log.Index.ToString();
            this.labelCommand.Text = log.CmdName;
            this.labelTime.Text = log.StartTime.ToString("hh.mm.ss ffff");

            this.labelControlTitle.Text = log.Description;
            this.labelControlMessage.Text = log.Message;
            this.sqlEditorControl.SqlText = log.CmdData;
        }


        /// <summary>
        /// Handles the RowCountChanged event of the gridViewLog control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void gridViewLog_RowCountChanged(object sender, EventArgs e)
        {
            if (this.Logger != null)
            {
                int handle = this.gridViewLog.GetRowHandle(this.Logger.Count - 1);
                this.gridViewLog.FocusedRowHandle = handle;
                this.gridViewLog.ClearSelection();
                this.gridViewLog.SelectRow(handle);
                Application.DoEvents();
            }
        }
        #endregion

        #region Private Method
        #endregion

        #region Event
        #endregion

        #region Nested Types
        #endregion

    }
}
