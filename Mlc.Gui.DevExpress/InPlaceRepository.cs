﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

namespace Mlc.Gui.DevExpressTools
{
	public abstract class InPlaceRepository
	{

		/// <summary>
		/// The rep collection
		/// </summary>
		private Dictionary<string, RepositoryItem> repCollection = new Dictionary<string, RepositoryItem>();
		/// <summary>
		/// The list images small collection
		/// </summary>
		private Dictionary<string, ImageCollection> listImagesSmallCollection = new Dictionary<string, ImageCollection>();
		/// <summary>
		/// The list images large collection
		/// </summary>
		private Dictionary<string, ImageCollection> listImagesLargeCollection = new Dictionary<string, ImageCollection>();


		internal RepositoryItem GetRepository(string keyRepository, Type typeRepository)
		{
			RepositoryItem retVal = null;

			if (!repCollection.ContainsKey(keyRepository))
			{
				switch (typeRepository.Name)
				{
					case "RepositoryItemImageComboBox":
						RepositoryItemImageComboBox riicb = new RepositoryItemImageComboBox();
						riicb.AutoHeight = false;
						riicb.Name = keyRepository;
						repCollection.Add(keyRepository, riicb);
						retVal = riicb;

						string exeFolder = string.Format("{0}\\Resources\\Repositories\\{1}", Environment.CurrentDirectory, keyRepository);
						ImageCollectionMgr icm = new ImageCollectionMgr();

						this.listImagesSmallCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(20, 20), exeFolder + "\\Small"));
						riicb.SmallImages = this.listImagesSmallCollection[keyRepository];

						this.listImagesLargeCollection.Add(keyRepository, icm.ImageCollectionFromFolder(new Size(32, 32), exeFolder + "\\Large"));
						riicb.LargeImages = this.listImagesLargeCollection[keyRepository];

						foreach (ImageCollectionMgr.IconNameParser inp in icm.IconParserList)
							riicb.Items.Add(new ImageComboBoxItem(inp.name, inp.value, inp.index));

						break;
				}
			}

			retVal = repCollection[keyRepository];
			return retVal;
		}


		/// <summary>
		/// Gets the repository.
		/// </summary>
		/// <param name="keyRepository">The key repository.</param>
		/// <returns>RepositoryItem.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		protected RepositoryItem GetRepository(string keyRepository, Type typeRepository, DataTable table)
		{
			RepositoryItem retVal = null;

			if (!repCollection.ContainsKey(keyRepository))
			{
				switch (typeRepository.Name)
				{
					case "RepositoryItemLookUpEdit":
						RepositoryItemLookUpEdit riglue = new RepositoryItemLookUpEdit();
						riglue.Name = keyRepository;
						riglue.AutoHeight = false;
						riglue.DataSource = table;
						riglue.DisplayMember = "Value";
						riglue.ValueMember = "Key";
						riglue.PopulateColumns();
						repCollection.Add(keyRepository, riglue);
						retVal = riglue;
						break;
				}
			}

			retVal = repCollection[keyRepository];
			return retVal;
		}

		/// <summary>
		/// Gets the repository.
		/// </summary>
		/// <param name="keyRepository">The key repository.</param>
		/// <param name="type">The type.</param>
		/// <param name="editMask">The format string.</param>
		/// <returns>RepositoryItem.</returns>
		internal RepositoryItem GetRepository(string keyRepository, Type typeRepository, string editMask)
		{
			RepositoryItem retVal = null;

			if (!repCollection.ContainsKey(keyRepository))
			{
				switch (typeRepository.Name)
				{
					case "RepositoryItemTextEdit":
						RepositoryItemTextEdit rite = new RepositoryItemTextEdit();
						rite.AutoHeight = false;
						rite.Name = keyRepository;
						repCollection.Add(keyRepository, rite);
						retVal = rite;
						rite.Mask.EditMask = editMask;
						rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
						rite.Mask.UseMaskAsDisplayFormat = true;
						break;
				}
			}

			retVal = repCollection[keyRepository];
			return retVal;
		}
	}
}
