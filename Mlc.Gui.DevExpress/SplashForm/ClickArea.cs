using System.Drawing;

namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Definisce una area del form
    /// </summary>
    public class ClickArea
    {
        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="name">Nome dell'area</param>
        /// <param name="rectArea">Rettangolo dell'area</param>
        public ClickArea(string name, Rectangle rectArea)
            : this(name, new Point(rectArea.X, rectArea.Y), new Size(rectArea.Width, rectArea.Height))
        { }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="name">Nome dell'area</param>
        /// <param name="location">Punto di posizionamento sul form</param>
        /// <param name="image">Immagine associata all'area</param>
        public ClickArea(string name, Point location, Image image)
            : this(name, location, image.Size)
        {
            this.areaImage = image;
        }

        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="name">Nome dell'area</param>
        /// <param name="location">Punto di posizionamento sul form</param>
        /// <param name="image">Immagine associata all'area</param>
        public ClickArea(string name, Rectangle rectArea, Image image)
            : this(name, new Point(rectArea.X, rectArea.Y), new Size(rectArea.Width, rectArea.Height))
        {
            this.areaImage = image;
        }


        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="name">Nome dell'area</param>
        /// <param name="location">Punto di posizionamento sul form</param>
        /// <param name="size">Dimensione dell'area</param>
        public ClickArea(string name, Point location, Size size)
        {
            this.name = name;
            this.location = location;
            this.areaSize = size;
        }

        #endregion

        #region Proprietà
        /// <summary>Nome dell'area</summary>
        private string name;
        /// <summary>
        /// Restituisce o imposta il nome dell'area
        /// </summary>    
        public string Name { get { return this.name; } set { this.name = value; } }

        /// <summary>Posizione dell'area</summary>
        private Point location;
        /// <summary>
        /// Restituisce la posizione dell'area
        /// </summary>    
        public Point Location { get { return this.location; } }

        /// <summary>Dimensione dell'area</summary>
        private Size areaSize;
        /// <summary>
        /// Restituisce la dimensione dell'area
        /// </summary>    
        public Size AreaSize { get { return this.areaSize; } }

        /// <summary>Immagine per l'area</summary>
        private Image areaImage = null;
        /// <summary>
        /// Restituisce l'immagine per l'area
        /// </summary>    
        public Image AreaImage { get { return this.areaImage; } }
        #endregion
    }
}
