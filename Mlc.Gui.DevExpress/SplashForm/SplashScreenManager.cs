using System.Drawing.Drawing2D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices; //Needed


namespace Mlc.Gui.DevExpressTools
{
    /// <summary>
    /// Gestore per uno splash form
    /// </summary>
    public class SplashScreenManager
    {
        #region Costanti
        private const byte AC_SRC_ALPHA = 1;
        private const byte AC_SRC_OVER = 0;
        private const int ULW_ALPHA = 2;
        #endregion

        #region Campi
        [DllImport("gdi32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr CreateCompatibleDC(IntPtr hDC);
        [DllImport("gdi32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern bool DeleteDC(IntPtr hdc);
        [DllImport("gdi32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern bool DeleteObject(IntPtr hObject);
        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);
        [DllImport("gdi32.dll", ExactSpelling = true)]
        private static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        [DllImport("user32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr GetDC(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true, ExactSpelling = true)]
        private static extern bool UpdateLayeredWindow(IntPtr hwnd, IntPtr hdcDst, ref Point pptDst, ref Size psize, IntPtr hdcSrc, ref Point pprSrc, int crKey, ref BLENDFUNCTION pblend, int dwFlags);
        private List<Image> backGroudFrameScaleList = null;
        private bool firstExecute = true;
        private System.Windows.Forms.Timer timerAutoClose = new System.Windows.Forms.Timer();

        /// <summary>Istanza dello spash form</summary>
        private Form form = null;
        /// <summary>Immagine dello sfondo dello splash form</summary>
        private Image backGroudImage = null;
        /// <summary>Tempo oltre al quale lo splash form deve chiudersi da solo</summary>
        private static DateTime timeAutoClose;
        /// <summary>Determina se è stata richiesta la chiusura del form (per la sospensione dei thread)</summary>
        private bool formCloseRequest = false;
        #endregion

        #region Costruttori
        /// <summary>
        /// Inizializza una nuova istanza
        /// </summary>
        /// <param name="form">Istanza dello spash form</param>
        /// <param name="backGroudImage">Immagine dello sfondo dello splash form</param>
        public SplashScreenManager(Form form, Image backGroudImage)
        {
            this.form = form;
            this.backGroudImage = (Image)backGroudImage.Clone();
            this.form.Size = this.backGroudImage.Size;
            this.startPoint = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2, Screen.PrimaryScreen.WorkingArea.Height);
            this.endPoint = this.startPoint;
            this.form.Activated += new EventHandler(form_Activated);
            this.form.KeyDown += new KeyEventHandler(form_KeyDown);
            this.form.KeyPreview = true;
            this.form.Click += new EventHandler(form_Click);
            this.form.MouseDown += new MouseEventHandler(form_MouseDown);
            this.form.MouseMove += new MouseEventHandler(form_MouseMove);
        }
        #endregion

        #region Proprietà
        /// <summary>Tempo in secondi per la chiusura automatica del form</summary>
        private int autoCloseDelay = 0;
        /// <summary>
        /// Restituisce o imposta il tempo in secondi per la chiusura automatica del form
        /// </summary>
        public int AutoCloseDelay { get { return this.autoCloseDelay; } set { this.autoCloseDelay = value; } }

        /// <summary>Tempo in millisecondi di passaggio tra un frame e quello successivo</summary>
        private int frameInterval = 30;
        /// <summary>
        /// Restituisce o imposta il tempo in millisecondi di passaggio tra un frame e quello successivo
        /// </summary>
        public int FrameInterval { get { return this.frameInterval; } set { this.frameInterval = value; } }

        /// <summary>Numero di frame per l'animazione dello splash form</summary>
        private int frameCount = 20;
        /// <summary>
        /// Restituisce o imposta il numero di frame per l'animazione dello splash form
        /// </summary>
        public int FrameCount { get { return this.frameCount; } set { this.frameCount = value; this.fillBackGroudImageScaleList(); } }

        /// <summary>Punto da cui compare lo splash form</summary>
        private Point startPoint;
        /// <summary>
        /// Restituisce o imposta il punto da cui compare lo splash form
        /// </summary>
        public Point StartPoint { get { return this.startPoint; } set { this.startPoint = value; } }

        /// <summary>Punto in cui scompare lo splash form</summary>
        private Point endPoint;
        /// <summary>
        /// Restituisce o imposta il punto in cui scompare lo splash form
        /// </summary>
        public Point EndPoint { get { return this.endPoint; } set { this.endPoint = value; } }

        /// <summary>Abilita il calcolo randomico dei punti di comparsa e scomparsa del form</summary>
        private bool randomLocationSplash = false;
        /// <summary>
        /// Restituisce o imposta un valore che abilita il calcolo randomico dei punti di comparsa e scomparsa del form
        /// </summary>    
        public bool RandomLocationSplash { get { return this.randomLocationSplash; } set { this.randomLocationSplash = value; } }

        /// <summary>Valore minimo dell'altezza del frame più piccolo del form</summary>
        private int frameMinHeight = 10;
        /// <summary>
        /// Restituisce o imposta il valore minimo dell'altezza del frame più piccolo del form
        /// </summary>    
        public int FrameMinHeight { get { return this.frameMinHeight; } set { this.frameMinHeight = value; } }

        /// <summary>Abilita il calcolo randomico dei punti di comparsa e scomparsa del form</summary>
        private bool endOpenAnimation = false;
        /// <summary>
        /// Restituisce un valore che determina se l'animazione è terminata
        /// </summary>    
        public bool EndOpenAnimation { get { return this.endOpenAnimation; } }

        /// <summary>Collezione delle aree cliccabili</summary>
        private ClickAreaCollections areaCollections = new ClickAreaCollections();
        /// <summary>
        /// Restituisce o imposta la collezione delle aree cliccabili
        /// </summary>    
        public ClickAreaCollections AreaCollections { get { return this.areaCollections; } set { this.areaCollections = value; } }

        #endregion

        #region Metodi pubblici
        /// <summary>
        /// Disegna il form splash 
        /// </summary>
        public void DrawSplashForm()
        {
            this.DrawSplashForm(this.backGroudImage);
        }

        /// <summary>
        /// Disegna il form splash 
        /// </summary>
        /// <param name="backGroudImage">Immagine da utilizzare</param>
        public void DrawSplashForm(Image backGroudImage)
        {
            IntPtr screenDc = SplashScreenManager.GetDC(IntPtr.Zero);
            IntPtr memDc = SplashScreenManager.CreateCompatibleDC(screenDc);
            IntPtr hBitmap = IntPtr.Zero;
            IntPtr oldBitmap = IntPtr.Zero;
            try
            {
                //Display-image
                Bitmap bmp = new Bitmap(backGroudImage);
                hBitmap = bmp.GetHbitmap(Color.FromArgb(0));  //Set the fact that background is transparent
                oldBitmap = SplashScreenManager.SelectObject(memDc, hBitmap);

                //Display-rectangle
                Size size = bmp.Size;
                Point pointSource = new Point(0, 0);
                Point topPos = new Point(this.form.Left, this.form.Top);

                //Set up blending options
                SplashScreenManager.BLENDFUNCTION blend = new SplashScreenManager.BLENDFUNCTION();
                blend.BlendOp = SplashScreenManager.AC_SRC_OVER;
                blend.BlendFlags = 0;
                blend.SourceConstantAlpha = 255;
                blend.AlphaFormat = SplashScreenManager.AC_SRC_ALPHA;

                SplashScreenManager.UpdateLayeredWindow(this.form.Handle, screenDc, ref topPos, ref size, memDc, ref pointSource, 0, ref blend, SplashScreenManager.ULW_ALPHA);

                //Clean-up
                bmp.Dispose();
                SplashScreenManager.ReleaseDC(IntPtr.Zero, screenDc);
                if (hBitmap != IntPtr.Zero)
                {
                    SplashScreenManager.SelectObject(memDc, oldBitmap);
                    SplashScreenManager.DeleteObject(hBitmap);
                }
                SplashScreenManager.DeleteDC(memDc);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Apertura animata dello spash form
        /// </summary>
        public void Open()
        {
            if (this.firstExecute)
            {
                if (this.backGroudFrameScaleList == null)
                    this.fillBackGroudImageScaleList();

                if (this.backGroudFrameScaleList.Count > 1)
                {
                    Point fp = this.getCenterForm();
                    int Dx = (fp.X - this.startPoint.X) / this.backGroudFrameScaleList.Count;
                    int Dy = (fp.Y - this.startPoint.Y) / this.backGroudFrameScaleList.Count;

                    // Animazione ...
                    Point pos = this.startPoint;
                    for (int i = this.backGroudFrameScaleList.Count - 1; i >= 0; i--)
                    {
                        this.DrawSplashForm(this.backGroudFrameScaleList[i]);
                        this.setCenterForm(pos, this.backGroudFrameScaleList[i].Size);
                        Thread.Sleep(this.frameInterval);
                        pos = new Point(pos.X + Dx, pos.Y + Dy);
                    }
                }
                else
                    this.DrawSplashForm(this.backGroudFrameScaleList[0]);

                if (this.autoCloseDelay > 0)
                {
                    timeAutoClose = DateTime.Now + new TimeSpan(0, 0, this.autoCloseDelay);
                    new System.Threading.Thread(this.autoClose).Start();
                }
                this.firstExecute = false;
                this.endOpenAnimation = true;
            }
            this.OnOpenSplashEnded(new EventArgs());
        }

        /// <summary>
        /// Chiusura animata dello spash form
        /// </summary>
        public void Close()
        {
            this.Close(false);
        }

        /// <summary>
        /// Chiusura animata dello spash form
        /// </summary>
        /// <param name="onlyHide">Determina se chiudere o nscondere il form</param>
        public void Close(bool onlyHide)
        {
            this.formCloseRequest = true;

            if (this.backGroudFrameScaleList.Count > 1)
            {
                if (this.backGroudFrameScaleList == null)
                    this.fillBackGroudImageScaleList();

                Point fp = this.getCenterForm();
                int Dx = (fp.X - this.endPoint.X) / this.backGroudFrameScaleList.Count * -1;
                int Dy = (fp.Y - this.endPoint.Y) / this.backGroudFrameScaleList.Count * -1;
                Point pos = this.getCenterForm();

                // Animazione ...
                for (int i = 0; i < this.backGroudFrameScaleList.Count; i++)
                {
                    this.DrawSplashForm(this.backGroudFrameScaleList[i]);
                    this.setCenterForm(pos, this.backGroudFrameScaleList[i].Size);
                    Thread.Sleep(this.frameInterval);
                    pos = new Point(pos.X + Dx, pos.Y + Dy);
                }
            }
            if (onlyHide)
                this.form.Hide();
            else
                this.form.Close();

            this.OnCloseSplashEnded(new EventArgs());
        }

        #endregion

        #region Metodi per eventi
        /// <summary>
        /// Occorre alla pressione dei tasti
        /// </summary>
        private void form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        /// <summary>
        /// Occorre all'attivazione del form
        /// </summary>
        private void form_Activated(object sender, EventArgs e)
        {
            // Aggancio l'evento una sola volta
            this.form.Activated -= new System.EventHandler(this.form_Activated);
            new System.Threading.Thread(this.Open).Start();
        }


        /// <summary>
        /// Occorre all'evento di chiusura automatica
        /// </summary>
        private void timerAutoClose_Tick(object sender, EventArgs e)
        {
            this.timerAutoClose.Enabled = false;
            this.Close();
        }

        private Point click;

        /// <summary>
        /// 
        /// </summary>
        private void form_MouseDown(object sender, MouseEventArgs e)
        {
            click = e.Location;
        }

        /// <summary>
        /// 
        /// </summary>
        private void form_Click(object sender, EventArgs e)
        {
            if (this.AreaCollections.HitTest(click))
                this.AreaCollections.OnClick(new ClickAreaCollections.EventArgsClick(this.AreaCollections.CurrentArea));
        }

        /// <summary>
        /// 
        /// </summary>
        private void form_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.AreaCollections.HitTest(e.Location))
                this.form.Cursor = Cursors.Hand;
            else
                this.form.Cursor = Cursors.Default;
        }
        #endregion

        #region Metodi privati
        /// <summary>
        /// Popola la lista dei frame dello splash form
        /// </summary>
        private void fillBackGroudImageScaleList()
        {
            this.backGroudFrameScaleList = new List<Image>();

            if (this.frameCount == 1)
            {
                this.backGroudFrameScaleList.Add(this.backGroudImage);
            }
            else
            {
                // calcolo il fattore di scala per ottenere il numero di frame previsto
                // mi baso sull'altezza del form
                // considero che il frame più piccolo abbia la proprietà Height = 10
                float KFactor = ((float)(this.form.Height - this.frameMinHeight) / (this.frameCount - 1)) / this.form.Height;

                for (int i = 0; i < this.frameCount; i++)
                {
                    float scaleFactor = 1 - KFactor * i;
                    SizeF imgSize = new SizeF(this.backGroudImage.Size.Width * scaleFactor, this.backGroudImage.Size.Height * scaleFactor);
                    Bitmap img = new Bitmap((int)imgSize.Width, (int)imgSize.Height);
                    Graphics graphics = Graphics.FromImage(img);
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(this.backGroudImage, 0, 0, imgSize.Width, imgSize.Height);
                    this.backGroudFrameScaleList.Add(img);
                }
            }
        }

        /// <summary>
        /// Imposta la posizione del form con il suo centro nel punto specificato
        /// </summary>
        /// <param name="point">Punto in cui posizinare il centro del form</param>
        /// <param name="size">Dimensione da utilizzare per il posizionamento</param>
        private void setCenterForm(Point point, Size size)
        {
            int X = point.X - size.Width / 2;
            int Y = point.Y - size.Height / 2;
            this.form.Location = new Point(X, Y);
        }

        /// <summary>
        /// Restituisce il punto centrale del form
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Point getCenterForm()
        {
            int X = this.form.Location.X + this.form.Width / 2;
            int Y = this.form.Location.Y + this.form.Height / 2;
            return new Point(X, Y);
        }


        /// <summary>
        /// Metodo per esecuzione in un Thread separato per la chiusura automatica del form
        /// </summary>
        private void autoClose()
        {
            while (timeAutoClose > DateTime.Now)
            {
                if (formCloseRequest)
                    break;
                continue;
            }

            if (!formCloseRequest)
                this.Close();
        }
        #endregion

        #region Tipi nidificati
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct BLENDFUNCTION
        {
            public byte BlendOp;
            public byte BlendFlags;
            public byte SourceConstantAlpha;
            public byte AlphaFormat;
        }
        #endregion

        #region Definizione eventi


        #region Evento OpenSplashEnded
        /// <summary>
        /// Delegato per la gestione dell'evento che occorre al termine dell'apertura del form.
        /// </summary>
        public event EventHandler OpenSplashEnded;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnOpenSplashEnded(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (OpenSplashEnded != null)
                // viene lanciato l'evento
                OpenSplashEnded(this, e);
        }
        #endregion

        #region Evento CloseSplashEnded
        /// <summary>
        /// Delegato per la gestione dell'evento che occorre al termine della chiusura del form.
        /// </summary>
        public event EventHandler CloseSplashEnded;

        /// <summary>
        /// Metodo per il lancio dell'evento.
        /// </summary>  
        /// <param name="e">Dati dell'evento</param>
        protected virtual void OnCloseSplashEnded(EventArgs e)
        {
            // Se ci sono ricettori in ascolto ...
            if (CloseSplashEnded != null)
                // viene lanciato l'evento
                CloseSplashEnded(this, e);
        }
        #endregion

        #endregion
    }
}

