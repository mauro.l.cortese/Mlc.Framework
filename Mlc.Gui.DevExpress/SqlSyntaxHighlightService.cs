﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Office.Utils;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit.Services;
using System;
using System.Linq;
using Mlc.Shell;



namespace Mlc.Gui.DevExpressTools
{



    public class SqlSyntaxHighlightService : ISyntaxHighlightService {
        #region #parsetokens
        readonly Document document;
        SyntaxHighlightProperties defaultSettings = new SyntaxHighlightProperties() { ForeColor = Color.Black };
        SyntaxHighlightProperties keywordSettings = new SyntaxHighlightProperties() { ForeColor = Color.Blue };
        SyntaxHighlightProperties stringSettings = new SyntaxHighlightProperties() { ForeColor = Color.Red };
        SyntaxHighlightProperties beginEndSettings = new SyntaxHighlightProperties() { ForeColor = Color.DarkViolet };
        SyntaxHighlightProperties datetypeSettings = new SyntaxHighlightProperties() { ForeColor = Color.Brown };
        SyntaxHighlightProperties functionSettings = new SyntaxHighlightProperties() { ForeColor = Color.DarkViolet }; 
        SyntaxHighlightProperties variableSettings = new SyntaxHighlightProperties() { ForeColor = Color.LightSkyBlue };
        SyntaxHighlightProperties commentSettings = new SyntaxHighlightProperties() { ForeColor = Color.DarkGreen };
        SyntaxHighlightProperties numericSettings = new SyntaxHighlightProperties() { ForeColor = Color.Brown };

        #region DataTypes
        string[] keywords = new string[] {
            "AND",
            "AS",
            "BETWEEN",
            "CASE",
            "CREATE",
            "DECLARE",
            "DELETE",
            "DISTINCT",
            "DROP",
            "ELSE",
            "EXEC",
            "EXECUTE",
            "FROM",
            "IDENTITY",
            "IF",
            "INSERT",
            "INTO",
            "LIKE",
            "NOT",
            "NULL",
            "OFF",
            "ON",
            "OR",
            "OVER",
            "PROC",
            "PROCEDURE",
            "SELECT",
            "SET",
            "TABLE",
            "UPDATE",
            "USE",
            "WHEN",
            "WHERE",
            "WHILE",
            "WITH",
            };
        #endregion


        #region DataTypes
        string[] beginEnd = new string[] {
            "BEGIN",
            "COMMIT",
            "END",
            "ROLLBACK",
            "SAVE",
            "TRAN",
            "TRANSACTION",
            };
        #endregion


        #region DataTypes
        string[] datatypes = new string[] {
            "BIT",
            "CHAR",
            "DECIMAL",
            "IMAGE",
            "INT",
            "INT",
            "INTEGER",
            "NUMERIC",
            "TEXT",
            "TINYINT",
            "UNSIGNED",
            "VARCHAR",
            };
        #endregion

        #region Functions
        string[] functions = new string[] {
            "AVG",
            "CHAR_LENGTH",
            "CONVERT",
            "COUNT",
            "DATEADD",
            "DATEDIFF",
            "DATEPART",
            "GETDATE",
            "ISNULL",
            "LIST",
            "LTRIM",
            "MAX",
            "MIN",
            "ROW_NUMBER",
            "RTRIM",
            "SUBSTRING",
            "SUBSTRING",
            "SUM",
            };
        #endregion

        public SqlSyntaxHighlightService(Document document) {
            this.document = document;
        }

        private List<SyntaxHighlightToken> ParseTokens() {
            List<SyntaxHighlightToken> tokens = new List<SyntaxHighlightToken>();
            DocumentRange[] ranges = null;
            System.Text.RegularExpressions.Regex expr;

            expr = new System.Text.RegularExpressions.Regex(@"(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(--.*)");
            ranges = document.FindAll(expr);
            for (int i = 0; i < ranges.Length; i++)
            {
                if (!IsRangeInTokens(ranges[i], tokens))
                    tokens.Add(new SyntaxHighlightToken(ranges[i].Start.ToInt(), ranges[i].Length, commentSettings));
            }

            // search for quotation marks
            ranges = document.FindAll("'", SearchOptions.None);
            for (int i = 0; i < ranges.Length / 2; i++) {
                tokens.Add(new SyntaxHighlightToken(ranges[i * 2].Start.ToInt(), 
                    ranges[i * 2 + 1].Start.ToInt() - ranges[i * 2].Start.ToInt() + 1, stringSettings));
            }

            ranges = document.FindAll("\"", SearchOptions.None);
            for (int i = 0; i < ranges.Length / 2; i++)
            {
                tokens.Add(new SyntaxHighlightToken(ranges[i * 2].Start.ToInt(),
                    ranges[i * 2 + 1].Start.ToInt() - ranges[i * 2].Start.ToInt() + 1, stringSettings));
            }
            // search for keywords
            for (int i = 0; i < keywords.Length; i++) {
                ranges = document.FindAll(keywords[i], SearchOptions.None | SearchOptions.WholeWord);

                for (int j = 0; j < ranges.Length; j++) {
                    if (!IsRangeInTokens(ranges[j], tokens))
                        tokens.Add(new SyntaxHighlightToken(ranges[j].Start.ToInt(), ranges[j].Length, keywordSettings));
                }
            }

            for (int i = 0; i < beginEnd.Length; i++)
            {
                ranges = document.FindAll(beginEnd[i], SearchOptions.None | SearchOptions.WholeWord);

                for (int j = 0; j < ranges.Length; j++)
                {
                    if (!IsRangeInTokens(ranges[j], tokens))
                        tokens.Add(new SyntaxHighlightToken(ranges[j].Start.ToInt(), ranges[j].Length, beginEndSettings));
                }
            }

            for (int i = 0; i < datatypes.Length; i++)
            {
                ranges = document.FindAll(datatypes[i], SearchOptions.None | SearchOptions.WholeWord);

                for (int j = 0; j < ranges.Length; j++)
                {
                    if (!IsRangeInTokens(ranges[j], tokens))
                        tokens.Add(new SyntaxHighlightToken(ranges[j].Start.ToInt(), ranges[j].Length, datetypeSettings));
                }
            }

            for (int i = 0; i < functions.Length; i++)
            {
                ranges = document.FindAll(functions[i], SearchOptions.None | SearchOptions.WholeWord);

                for (int j = 0; j < ranges.Length; j++)
                {
                    if (!IsRangeInTokens(ranges[j], tokens))
                        tokens.Add(new SyntaxHighlightToken(ranges[j].Start.ToInt(), ranges[j].Length, functionSettings));
                }
            }

            expr = new System.Text.RegularExpressions.Regex(@"(@\w*)");
            ranges = document.FindAll(expr);
            for (int i = 0; i < ranges.Length; i++)
            {
                if (!IsRangeInTokens(ranges[i], tokens))
                    tokens.Add(new SyntaxHighlightToken(ranges[i].Start.ToInt(), ranges[i].Length, variableSettings));
            }


            expr = new System.Text.RegularExpressions.Regex(@"( [0-9].[0-9]\w*)|( [0-9]\w*)");
            ranges = document.FindAll(expr);
            for (int i = 0; i < ranges.Length; i++)
            {
                if (!IsRangeInTokens(ranges[i], tokens))
                    tokens.Add(new SyntaxHighlightToken(ranges[i].Start.ToInt(), ranges[i].Length, numericSettings));
            }

            // order tokens by their start position
            tokens.Sort(new SyntaxHighlightTokenComparer());
            // fill in gaps in document coverage
            AddPlainTextTokens(tokens);
            return tokens;
        }

        private void AddPlainTextTokens(List<SyntaxHighlightToken> tokens) {
            int count = tokens.Count;
            if (count == 0) {
                tokens.Add(new SyntaxHighlightToken(0, document.Range.End.ToInt(), defaultSettings));
                return;
            }
            tokens.Insert(0, new SyntaxHighlightToken(0, tokens[0].Start, defaultSettings));
            for (int i = 1; i < count; i++) {
                tokens.Insert(i * 2, new SyntaxHighlightToken(tokens[i * 2 - 1].End,
 tokens[i * 2].Start - tokens[i * 2 - 1].End, defaultSettings));
            }
            tokens.Add(new SyntaxHighlightToken(tokens[count * 2 - 1].End,
 document.Range.End.ToInt() - tokens[count * 2 - 1].End, defaultSettings));
        }

        private bool IsRangeInTokens(DocumentRange range, List<SyntaxHighlightToken> tokens) {
            for (int i = 0; i < tokens.Count; i++) {
                if (range.Start.ToInt() >= tokens[i].Start && range.End.ToInt() <= tokens[i].End)
                    return true;
            }
            return false;
        }
        #endregion #parsetokens

        #region #ISyntaxHighlightServiceMembers
        public void ForceExecute() {
            Execute();
        }
        public void Execute() {
            try
            {
                document.ApplySyntaxHighlight(ParseTokens());
            }
            catch (Exception ex) { ExceptionsRegistry.GetInstance().Add(ex); }
        }
        #endregion #ISyntaxHighlightServiceMembers
    }
    #region #SyntaxHighlightTokenComparer
    public class SyntaxHighlightTokenComparer : IComparer<SyntaxHighlightToken> {
        public int Compare(SyntaxHighlightToken x, SyntaxHighlightToken y) {
            return x.Start - y.Start;
        }
    }
    #endregion #SyntaxHighlightTokenComparer

    /*
    public class SqlSyntaxHighlightService : ISyntaxHighlightService
    {
        #region #parsetokens
        readonly Document document;
        SyntaxHighlightProperties defaultSettings = new SyntaxHighlightProperties() { ForeColor = Color.Black };
        SyntaxHighlightProperties keywordSettings = new SyntaxHighlightProperties() { ForeColor = Color.Blue };
        SyntaxHighlightProperties stringSettings = new SyntaxHighlightProperties() { ForeColor = Color.Red };

        string[] keywords = new string[] {
                 "NOCOUNT","AND","AS","ASC","BETWEEN","BY","CASE","CREATE","FROM","IDENTITY","IF","INSERT","INT","LIKE","NOT","NULL","OFF","ON","OR","ORDER","OVER","SELECT","SET" ,"TABLE","THEN","USE","WHEN","WHERE","WITH" };

        public SqlSyntaxHighlightService(Document document)
        {
            this.document = document;
        }

        private List<SyntaxHighlightToken> ParseTokens()
        {
            List<SyntaxHighlightToken> tokens = new List<SyntaxHighlightToken>();
            DocumentRange[] ranges = null;
            // search for quotation marks
            ranges = document.FindAll("'", SearchOptions.None);
            for (int i = 0; i < ranges.Length / 2; i++)
            {
                tokens.Add(new SyntaxHighlightToken(ranges[i * 2].Start.ToInt(),
                    ranges[i * 2 + 1].Start.ToInt() - ranges[i * 2].Start.ToInt() + 1, stringSettings));
            }
            // search for keywords
            for (int i = 0; i < keywords.Length; i++)
            {
                ranges = document.FindAll(keywords[i], SearchOptions.CaseSensitive | SearchOptions.WholeWord);

                for (int j = 0; j < ranges.Length; j++)
                {
                    if (!IsRangeInTokens(ranges[j], tokens))
                        tokens.Add(new SyntaxHighlightToken(ranges[j].Start.ToInt(), ranges[j].Length, keywordSettings));
                }
            }
            // order tokens by their start position
            tokens.Sort(new SyntaxHighlightTokenComparer());
            // fill in gaps in document coverage
            AddPlainTextTokens(tokens);
            return tokens;
        }

        private void AddPlainTextTokens(List<SyntaxHighlightToken> tokens)
        {
            int count = tokens.Count;
            if (count == 0)
            {
                tokens.Add(new SyntaxHighlightToken(0, document.Range.End.ToInt(), defaultSettings));
                return;
            }
            tokens.Insert(0, new SyntaxHighlightToken(0, tokens[0].Start, defaultSettings));
            for (int i = 1; i < count; i++)
            {
                tokens.Insert(i * 2, new SyntaxHighlightToken(tokens[i * 2 - 1].End, tokens[i * 2].Start - tokens[i * 2 - 1].End, defaultSettings));
            }
            tokens.Add(new SyntaxHighlightToken(tokens[count * 2 - 1].End, document.Range.End.ToInt() - tokens[count * 2 - 1].End, defaultSettings));
        }

        private bool IsRangeInTokens(DocumentRange range, List<SyntaxHighlightToken> tokens)
        {
            return tokens.Any(t => IsIntersect(range, t));
        }
        bool IsIntersect(DocumentRange range, SyntaxHighlightToken token)
        {
            int start = range.Start.ToInt();
            if (start >= token.Start && start < token.End)
                return true;
            int end = range.End.ToInt() - 1;
            if (end >= token.Start && end < token.End)
                return true;
            return false;
        }
        #endregion #parsetokens

        #region #ISyntaxHighlightServiceMembers
        public void ForceExecute()
        {
            Execute();
        }
        public void Execute()
        {
            document.ApplySyntaxHighlight(ParseTokens());
        }
        #endregion #ISyntaxHighlightServiceMembers
    }

    #region #SyntaxHighlightTokenComparer
    public class SyntaxHighlightTokenComparer : IComparer<SyntaxHighlightToken>
    {
        public int Compare(SyntaxHighlightToken x, SyntaxHighlightToken y)
        {
            return x.Start - y.Start;
        }
    }
    #endregion #SyntaxHighlightTokenComparer
*/
}
