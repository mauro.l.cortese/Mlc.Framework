﻿namespace Mlc.Gui.DevExpressTools
{
    partial class ViewSqlLogger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlEditorControl = new Mlc.Gui.DevExpressTools.SqlEditorControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelCommand = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelIndex = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelTime = new DevExpress.XtraEditors.LabelControl();
            this.logBindingSource = new System.Windows.Forms.BindingSource();
            this.loggerBindingSource = new System.Windows.Forms.BindingSource();
            this.ViewLoggerlayoutControl1ConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlTitle = new DevExpress.XtraEditors.LabelControl();
            this.labelControlMessage = new DevExpress.XtraEditors.LabelControl();
            this.gridControlLog = new DevExpress.XtraGrid.GridControl();
            this.gridViewLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colCmdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitContainerControl1item = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Panel1item = new DevExpress.XtraLayout.LayoutControlGroup();
            this.SqlEditorControlitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1item = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Panel2item = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gridControl1item = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.richEditControlMessage = new DevExpress.XtraRichEdit.RichEditControl();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewLoggerlayoutControl1ConvertedLayout)).BeginInit();
            this.ViewLoggerlayoutControl1ConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SqlEditorControlitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel2item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // sqlEditorControl
            // 
            this.sqlEditorControl.Location = new System.Drawing.Point(6, 137);
            this.sqlEditorControl.Name = "sqlEditorControl";
            this.sqlEditorControl.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.sqlEditorControl.Size = new System.Drawing.Size(1026, 432);
            this.sqlEditorControl.SqlText = "";
            this.sqlEditorControl.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelCommand);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelIndex);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelTime);
            this.panelControl1.Location = new System.Drawing.Point(6, 6);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(5);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(5);
            this.panelControl1.Size = new System.Drawing.Size(1026, 36);
            this.panelControl1.TabIndex = 2;
            // 
            // labelCommand
            // 
            this.labelCommand.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelCommand.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCommand.Appearance.Options.UseBackColor = true;
            this.labelCommand.Appearance.Options.UseFont = true;
            this.labelCommand.Appearance.Options.UseTextOptions = true;
            this.labelCommand.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelCommand.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCommand.Location = new System.Drawing.Point(131, 7);
            this.labelCommand.Name = "labelCommand";
            this.labelCommand.Size = new System.Drawing.Size(734, 22);
            this.labelCommand.TabIndex = 1;
            this.labelCommand.Text = "CmdName";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl2.Location = new System.Drawing.Point(89, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(42, 22);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Cmd:";
            // 
            // labelIndex
            // 
            this.labelIndex.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelIndex.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIndex.Appearance.Options.UseBackColor = true;
            this.labelIndex.Appearance.Options.UseFont = true;
            this.labelIndex.Appearance.Options.UseTextOptions = true;
            this.labelIndex.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelIndex.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelIndex.Location = new System.Drawing.Point(52, 7);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(37, 22);
            this.labelIndex.TabIndex = 2;
            this.labelIndex.Text = "Index";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl3.Location = new System.Drawing.Point(7, 7);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(45, 22);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Index:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl1.Location = new System.Drawing.Point(865, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 22);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "At time:";
            // 
            // labelTime
            // 
            this.labelTime.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelTime.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.Appearance.Options.UseBackColor = true;
            this.labelTime.Appearance.Options.UseFont = true;
            this.labelTime.Appearance.Options.UseTextOptions = true;
            this.labelTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelTime.Location = new System.Drawing.Point(925, 7);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(94, 22);
            this.labelTime.TabIndex = 1;
            this.labelTime.Text = "Index";
            // 
            // logBindingSource
            // 
            this.logBindingSource.DataSource = typeof(Mlc.Shell.IO.Log);
            // 
            // loggerBindingSource
            // 
            this.loggerBindingSource.DataSource = typeof(Mlc.Shell.IO.Logger);
            // 
            // ViewLoggerlayoutControl1ConvertedLayout
            // 
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.richEditControlMessage);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.labelControlTitle);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.labelControlMessage);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.sqlEditorControl);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.panelControl1);
            this.ViewLoggerlayoutControl1ConvertedLayout.Controls.Add(this.gridControlLog);
            this.ViewLoggerlayoutControl1ConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewLoggerlayoutControl1ConvertedLayout.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.ViewLoggerlayoutControl1ConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.ViewLoggerlayoutControl1ConvertedLayout.Name = "ViewLoggerlayoutControl1ConvertedLayout";
            this.ViewLoggerlayoutControl1ConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-1680, 122, 650, 400);
            this.ViewLoggerlayoutControl1ConvertedLayout.Root = this.layoutControlGroup1;
            this.ViewLoggerlayoutControl1ConvertedLayout.Size = new System.Drawing.Size(1450, 575);
            this.ViewLoggerlayoutControl1ConvertedLayout.TabIndex = 4;
            // 
            // labelControlTitle
            // 
            this.labelControlTitle.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControlTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControlTitle.Appearance.Options.UseBackColor = true;
            this.labelControlTitle.Appearance.Options.UseFont = true;
            this.labelControlTitle.Appearance.Options.UseTextOptions = true;
            this.labelControlTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControlTitle.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControlTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlTitle.Location = new System.Drawing.Point(10, 50);
            this.labelControlTitle.Name = "labelControlTitle";
            this.labelControlTitle.Size = new System.Drawing.Size(1018, 22);
            this.labelControlTitle.StyleController = this.ViewLoggerlayoutControl1ConvertedLayout;
            this.labelControlTitle.TabIndex = 5;
            this.labelControlTitle.Text = "CmdName";
            // 
            // labelControlMessage
            // 
            this.labelControlMessage.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControlMessage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControlMessage.Appearance.Options.UseBackColor = true;
            this.labelControlMessage.Appearance.Options.UseFont = true;
            this.labelControlMessage.Appearance.Options.UseTextOptions = true;
            this.labelControlMessage.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControlMessage.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControlMessage.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlMessage.Location = new System.Drawing.Point(10, 84);
            this.labelControlMessage.Name = "labelControlMessage";
            this.labelControlMessage.Size = new System.Drawing.Size(1018, 45);
            this.labelControlMessage.StyleController = this.ViewLoggerlayoutControl1ConvertedLayout;
            this.labelControlMessage.TabIndex = 4;
            this.labelControlMessage.Text = "CmdName";
            // 
            // gridControlLog
            // 
            this.gridControlLog.DataSource = this.logBindingSource;
            this.gridControlLog.Location = new System.Drawing.Point(1041, 6);
            this.gridControlLog.MainView = this.gridViewLog;
            this.gridControlLog.Name = "gridControlLog";
            this.gridControlLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1});
            this.gridControlLog.Size = new System.Drawing.Size(403, 563);
            this.gridControlLog.TabIndex = 1;
            this.gridControlLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLog});
            // 
            // gridViewLog
            // 
            this.gridViewLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gridViewLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIndex,
            this.colTime,
            this.colCmdName});
            this.gridViewLog.CustomizationFormBounds = new System.Drawing.Rectangle(-1073, 470, 210, 172);
            this.gridViewLog.GridControl = this.gridControlLog;
            this.gridViewLog.Name = "gridViewLog";
            this.gridViewLog.OptionsSelection.MultiSelect = true;
            this.gridViewLog.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewLog.OptionsView.ShowGroupPanel = false;
            this.gridViewLog.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLog.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLog.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIndex, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewLog.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewLog_FocusedRowChanged);
            this.gridViewLog.RowCountChanged += new System.EventHandler(this.gridViewLog_RowCountChanged);
            // 
            // colIndex
            // 
            this.colIndex.AppearanceCell.Options.UseTextOptions = true;
            this.colIndex.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.FieldName = "Index";
            this.colIndex.Name = "colIndex";
            this.colIndex.OptionsColumn.FixedWidth = true;
            this.colIndex.Visible = true;
            this.colIndex.VisibleIndex = 0;
            this.colIndex.Width = 46;
            // 
            // colTime
            // 
            this.colTime.ColumnEdit = this.repositoryItemTimeEdit1;
            this.colTime.FieldName = "Time";
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.ReadOnly = true;
            this.colTime.Width = 58;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.DisplayFormat.FormatString = "hh.mm.ss.fff";
            this.repositoryItemTimeEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.EditFormat.FormatString = "hh.mm.ss.fff";
            this.repositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // colCmdName
            // 
            this.colCmdName.FieldName = "CmdName";
            this.colCmdName.Name = "colCmdName";
            this.colCmdName.Visible = true;
            this.colCmdName.VisibleIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.splitContainerControl1item});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1450, 575);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // splitContainerControl1item
            // 
            this.splitContainerControl1item.GroupBordersVisible = false;
            this.splitContainerControl1item.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Panel1item,
            this.Panel2item});
            this.splitContainerControl1item.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1item.Name = "splitContainerControl1item";
            this.splitContainerControl1item.Size = new System.Drawing.Size(1442, 567);
            this.splitContainerControl1item.Text = "splitContainerControl1";
            // 
            // Panel1item
            // 
            this.Panel1item.GroupBordersVisible = false;
            this.Panel1item.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.SqlEditorControlitem,
            this.panelControl1item,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.Panel1item.Location = new System.Drawing.Point(0, 0);
            this.Panel1item.Name = "Panel1item";
            this.Panel1item.Size = new System.Drawing.Size(1030, 567);
            this.Panel1item.Text = "Panel1";
            // 
            // SqlEditorControlitem
            // 
            this.SqlEditorControlitem.Control = this.sqlEditorControl;
            this.SqlEditorControlitem.Location = new System.Drawing.Point(0, 131);
            this.SqlEditorControlitem.Name = "SqlEditorControlitem";
            this.SqlEditorControlitem.Size = new System.Drawing.Size(1030, 436);
            this.SqlEditorControlitem.TextSize = new System.Drawing.Size(0, 0);
            this.SqlEditorControlitem.TextVisible = false;
            // 
            // panelControl1item
            // 
            this.panelControl1item.Control = this.panelControl1;
            this.panelControl1item.Location = new System.Drawing.Point(0, 0);
            this.panelControl1item.MaxSize = new System.Drawing.Size(0, 40);
            this.panelControl1item.MinSize = new System.Drawing.Size(5, 40);
            this.panelControl1item.Name = "panelControl1item";
            this.panelControl1item.Size = new System.Drawing.Size(1030, 40);
            this.panelControl1item.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.panelControl1item.TextSize = new System.Drawing.Size(0, 0);
            this.panelControl1item.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControlMessage;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 57);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(19, 57);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlItem1.Size = new System.Drawing.Size(1030, 57);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControlTitle;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(18, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlItem2.Size = new System.Drawing.Size(1030, 34);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // Panel2item
            // 
            this.Panel2item.GroupBordersVisible = false;
            this.Panel2item.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gridControl1item,
            this.splitterItem1});
            this.Panel2item.Location = new System.Drawing.Point(1030, 0);
            this.Panel2item.Name = "Panel2item";
            this.Panel2item.Size = new System.Drawing.Size(412, 567);
            this.Panel2item.Text = "Panel2";
            // 
            // gridControl1item
            // 
            this.gridControl1item.Control = this.gridControlLog;
            this.gridControl1item.Location = new System.Drawing.Point(5, 0);
            this.gridControl1item.Name = "gridControl1item";
            this.gridControl1item.Size = new System.Drawing.Size(407, 567);
            this.gridControl1item.TextSize = new System.Drawing.Size(0, 0);
            this.gridControl1item.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(0, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 567);
            // 
            // richEditControlMessage
            // 
            this.richEditControlMessage.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControlMessage.Location = new System.Drawing.Point(6, 457);
            this.richEditControlMessage.Name = "richEditControlMessage";
            this.richEditControlMessage.Size = new System.Drawing.Size(1026, 112);
            this.richEditControlMessage.TabIndex = 7;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.richEditControlMessage;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 435);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1030, 132);
            this.layoutControlItem3.Text = "Log Message";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ViewSqlLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ViewLoggerlayoutControl1ConvertedLayout);
            this.Name = "ViewSqlLogger";
            this.Size = new System.Drawing.Size(1450, 575);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewLoggerlayoutControl1ConvertedLayout)).EndInit();
            this.ViewLoggerlayoutControl1ConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SqlEditorControlitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel2item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private SqlEditorControl sqlEditorControl;
        private System.Windows.Forms.BindingSource loggerBindingSource;
        private System.Windows.Forms.BindingSource logBindingSource;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelTime;
        private DevExpress.XtraEditors.LabelControl labelCommand;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelIndex;
        private DevExpress.XtraLayout.LayoutControl ViewLoggerlayoutControl1ConvertedLayout;
        private DevExpress.XtraEditors.LabelControl labelControlMessage;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup splitContainerControl1item;
        private DevExpress.XtraLayout.LayoutControlGroup Panel1item;
        private DevExpress.XtraLayout.LayoutControlItem SqlEditorControlitem;
        private DevExpress.XtraLayout.LayoutControlItem panelControl1item;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup Panel2item;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.LabelControl labelControlTitle;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gridControlLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLog;
        private DevExpress.XtraGrid.Columns.GridColumn colIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCmdName;
        private DevExpress.XtraLayout.LayoutControlItem gridControl1item;
        private DevExpress.XtraRichEdit.RichEditControl richEditControlMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}
