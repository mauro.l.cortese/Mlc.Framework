using System;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using Mlc.Shell.IO;
using Mlc.Shell;
using Mlc.Data;

namespace Mlc.NUnit
{
	public class IOTests
	{
		[SetUp]
		public void Setup()
		{
		}


		[Test, Order(1)]
		public void TestSizeConverter()
		{

			SizeConverter sc = new SizeConverter(90871686);
			Assert.AreEqual(90871686, sc.Byte);
			Console.WriteLine($"{sc.Kilobyte} kB");
			Assert.AreEqual(88741.88f, sc.Kilobyte);
			Console.WriteLine($"{sc.Megabyte} MB");
			Assert.AreEqual(86.6619949f, sc.Megabyte);
			Console.WriteLine($"{sc.Gigabyte} GB");
			Assert.AreEqual(0.084630854f, sc.Gigabyte);
			Console.WriteLine(sc);
			Assert.AreEqual("90871686 byte (88741,880 kB - 86,662 MB - 0,085 GB)", sc.ToString());
		}
	}
}