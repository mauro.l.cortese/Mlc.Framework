// ***********************************************************************
// Assembly         : Mlc.NUnit
// Author           : mauro.luigi.cortese
// Created          : 04-08-2020
//
// Last Modified By : mauro.luigi.cortese
// Last Modified On : 04-29-2020
// ***********************************************************************
// <copyright file="TestLogger.cs" company="Mlc.NUnit">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using NUnit.Framework;
using Mlc.Shell.IO;
using Mlc.Shell;

namespace Mlc.NUnit
{
	/// <summary>
	/// Class LoggerTests.
	/// </summary>
	public class LoggerTests
	{
		/// <summary>
		/// Setups this instance.
		/// </summary>
		[SetUp]
		public void Setup()
		{
			Assert.AreEqual(default(DateTime), Logger20.StartLogger);
			Assert.AreEqual(default(TimeSpan), Logger20.ElapsedTime);
			// Gets the instance of the log management class
			Logger20 logger = Logger20.GetInstance();
			System.Threading.Thread.Sleep(100);
			Assert.AreNotEqual(default(DateTime), Logger20.StartLogger);
			Assert.AreNotEqual(default(TimeSpan), Logger20.ElapsedTime);
		}

		/// <summary>
		/// Tears down.
		/// </summary>
		[TearDown]
		public void TearDown()
		{
			// Release every resource associated with the log handlers, after calling the Dispose() method 
			// it will be possible to reconfigure the Logger instance with new handlers
			Logger20.GetInstance().Dispose();
		}


		/// <summary>
		/// Defines the test method TestFileHandler.
		/// </summary>
		[Test, Order(1)]
		public void TestFileHandler()
		{
			// Configures a handler to log into a file
			// File name = "test_log_01"
			// The fields of table are defined by the LogRow instance
			// The debug level is set to Info
			// The log file will be saved in the executable folder
			LogHandlerFile logFileHandler = (LogHandlerFile)Logger20.AddHandler(
				new LogHandlerFile(
					Logger20.Level.Info,
					"test_log_01"
					));

			//Logger.AddHandler(new LogHandlerFile(Logger.Level.Debug, "test_log_01", "D:\\Logs\\") { Rotation = LogHandlerFile.FileRotation.Seconds, Interval = 5 });
			//Logger20.AddHandler(new LogHandlerFile(Logger20.Level.Debug, "test_log_01", "D:\\Logs\\"));

			Assert.IsNotNull(logFileHandler);
			Assert.IsInstanceOf(typeof(LogHandlerFile), logFileHandler);
			logFileHandler.Delete();

			DirectoryAssert.Exists(logFileHandler.Folder);
			FileAssert.DoesNotExist(logFileHandler.FullPath);
			Assert.AreEqual(new SysInfo().ExecutableFolder, logFileHandler.Folder);
			Logger20 logger = Logger20.GetInstance();
			Logger20.Log log = logger.DefaultLog;
			Assert.IsInstanceOf(typeof(Logger20.Log), log);
			Assert.AreEqual("test_log_01", log.Name);

			// Wait before writing the first log
			System.Threading.Thread.Sleep(100);

			// The next log message will be ignored because the level.Debug is lower than level.Info
			log.Debug("Test Debug log message");
			FileAssert.DoesNotExist(logFileHandler.FullPath);

			// The next log message will be considered because the level.Warning is upper than level.Info
			log.Warning("Test Warning log message");
			FileAssert.Exists(logFileHandler.FullPath);

			string logText = System.IO.File.ReadAllText(logFileHandler.FullPath);
			Console.WriteLine(logText);
			//Assert.AreEqual("| W | Test Warning log message", logText.Trim().Substring(39));

			logFileHandler.Delete();
			FileAssert.DoesNotExist(logFileHandler.FullPath);
		}

		/// <summary>
		/// Defines the test method TestFileRotation.
		/// </summary>
		[Test, Order(2)]
		public void TestFileRotation()
		{
			// Configures a handler to log into a file
			// File name = "test_log_01"
			// The fields of table are defined by the LogRow instance
			// The debug level is set to Info
			// The log file will be saved in the executable folder
			LogHandlerFile logFileHandler = new LogHandlerFile(
					Logger20.Level.Info,
					"test_log_rotation"
					)
			{
				Rotation = LogHandlerFile.FileRotation.Minutes,
				Interval = 1,
				BackupFiles = 3
			};

			DateTime start = new DateTime(2020, 4, 30, 8, 25, 28);
			DateTime nextLog = logFileHandler.GetNextTime(start, LogHandlerFile.FileRotation.Daily, 2);
			Assert.AreEqual(new DateTime(2020, 5, 2, 0, 0, 0), nextLog);

			nextLog = logFileHandler.GetNextTime(start, LogHandlerFile.FileRotation.Hourly, 3);
			Assert.AreEqual(new DateTime(2020, 4, 30, 11, 0, 0), nextLog);

			nextLog = logFileHandler.GetNextTime(start, LogHandlerFile.FileRotation.Minutes, 5);
			Assert.AreEqual(new DateTime(2020, 4, 30, 8, 30, 0), nextLog);

			Logger20.AddHandler(logFileHandler);
			Logger20.Log log = Logger20.GetInstance().DefaultLog;

			log.Info("First log");
			System.Threading.Thread.Sleep(new TimeSpan(0, 1, 5));
			log.Info("Second log");
			System.Threading.Thread.Sleep(new TimeSpan(0, 1, 5));
			log.Info("Third log");
			System.Threading.Thread.Sleep(new TimeSpan(0, 1, 5));
			log.Info("Fourth log");
			System.Threading.Thread.Sleep(new TimeSpan(0, 1, 5));
			log.Info("Fifth file");
		}

		/// <summary>
		/// Defines the test method TestFileRotation.
		/// </summary>
		[Test, Order(3)]
		public void TestFileNotRotation()
		{
			// Configures a handler to log into a file
			// File name = "test_log_01"
			// The fields of table are defined by the LogRow instance
			// The debug level is set to Info
			// The log file will be saved in the executable folder
			LogHandlerFile logFileHandler = new LogHandlerFile(
					Logger20.Level.Info,
					"test_log_not_rotation"
					)
			{
				Rotation = LogHandlerFile.FileRotation.None,
				BackupFiles = 5
			};

			Logger20.AddHandler(logFileHandler);
			Logger20.Log log = Logger20.GetInstance().DefaultLog;
			log.Info("First log");
			logFileHandler.FileWriting = false;
			log.Info("Second log");
			logFileHandler.FileWriting = false;
			log.Info("Third log");
			logFileHandler.FileWriting = false;
			log.Info("Fourth log");
			logFileHandler.FileWriting = false;
			log.Info("Fifth log");
			log.Info("Sixth log");
			log.Info("Seventh log");
		}

		/// <summary>
		/// Defines the test method TestDataRowHandler.
		/// </summary>
		[Test, Order(4)]
		public void TestDataRowHandler()
		{
			// Configures a handler to log into a DataTable
			// DataTable name = TableRowDebug
			// The fields of table are defined by the LogRow instance
			// The debug level is set to Info
			LogHandlerDataRow<LogRow> logHandler = (LogHandlerDataRow<LogRow>)Logger20.AddHandler(
				new LogHandlerDataRow<LogRow>(
					Logger20.Level.Info,
					"TableRowDebug"
					));

			Assert.IsNotNull(logHandler);
			Assert.IsInstanceOf(typeof(LogHandlerDataRow<LogRow>), logHandler);
			Assert.AreEqual(0, logHandler.TableLog.Rows.Count);
			Logger20 logger = Logger20.GetInstance();

			Logger20.Log log = logger.DefaultLog;
			Assert.IsInstanceOf(typeof(Logger20.Log), log);
			Assert.AreEqual("TableRowDebug", log.Name);

			// The next log message will be ignored because the level.Debug is lower than level.Info
			log.Debug("Test Debug log message");
			Assert.AreEqual(0, ((LogHandlerDataRow<LogRow>)log["TableRowDebug"]).TableLog.Rows.Count);

			// The next log message will be considered because the level.Warning is upper than level.Info
			log.Warning("Test Warning log message");
			Assert.AreEqual(1, ((LogHandlerDataRow<LogRow>)log["TableRowDebug"]).TableLog.Rows.Count);
		}
	}
}