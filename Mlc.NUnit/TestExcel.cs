using System;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
using System.IO;
using NUnit.Framework;
using Mlc.Shell.IO;
using Mlc.Office.Excel;
using Mlc.Data;
using Mlc.Shell;

namespace Mlc.NUnit
{
	public class ExcelTests
	{
		[SetUp]
		public void Setup()
		{
		}


		[Test, Order(1)]
		public void TestGetAllTableNames()
		{
			Console.WriteLine($"new SysInfo().ExecutableFolder => {new SysInfo().ExecutableFolder}");
			Console.WriteLine($"Environment.CurrentDirectory => {Environment.CurrentDirectory}");
			Console.WriteLine($"AppDomain.CurrentDomain.BaseDirectory => {AppDomain.CurrentDomain.BaseDirectory}");

			string folder = Path.Combine(new SysInfo().ExecutableFolder, "Data");

			string file = Path.Combine(folder, "TesxtTables.xlsx");

			OpenXmlExcel openXmlExcel = new OpenXmlExcel(file);
			OpenXmlExcel.Tables tables = openXmlExcel.GetAllTableNames();

			Assert.AreEqual(3, tables.Count);
			Assert.AreEqual("Foglio1\\MedBom_01", tables[0].ToString());
			Assert.AreEqual("Foglio2\\EprBom_02", tables[1].ToString());
			Assert.AreEqual("Foglio2\\EprBom_03", tables[2].ToString());

			foreach (OpenXmlExcel.Table table in tables)
				Console.WriteLine(table);

		}
	}
}