using System;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using Mlc.Shell.IO;
using Mlc.Shell;
using Mlc.Data;

namespace Mlc.NUnit
{
	public class TestDataRowWrapper
	{
		private DataTable table = null;
		private const int rowCount = 10;

		private const string defaultFields = "{" +
				"\"Id\": 10," +
				"\"Name\": \"Default Name\"," +
				"\"Description\": \"Description for default item\"," +
				"\"Date\": \"2023-03-05T10:30:00\"," +
				"}";

		#region Test
		[SetUp]
		public void Setup()
		{
			// Define DataTable
			table = new("TestTable");
			table.Columns.AddRange(new DataColumn[] {
				new ("Id",typeof(int)),
				new ("Name",typeof(string)),
				new ("Description",typeof(string)),
				new ("Date",typeof(DateTime)),
			});
		}

		[Test, Order(1)]
		public void TestDrwInstanceWithDataTable()
		{
			// Create a new instance without adding it to the table
			TestItem i = new TestItem(this.table.NewRow());
			Assert.Zero(i.Table.Rows.Count);
			Assert.AreEqual(string.Empty, i.Name);
			Assert.AreEqual(0, i.Id);
			Assert.AreEqual(string.Empty, i.Description);
			Assert.AreEqual(default(DateTime), i.Date);

			// Create a new instance and add it to the table
			i = new TestItem(this.table.NewRow(), true);
			Assert.AreEqual(1, i.Table.Rows.Count);
			Assert.AreEqual(string.Empty, i.Name);
			Assert.AreEqual(0, i.Id);
			Assert.AreEqual(string.Empty, i.Description);
			Assert.AreEqual(default(DateTime), i.Date);

		}

		[Test, Order(2)]
		public void TestDrwInstanceWithoutDataTable()
		{
			TestItem tItem = new TestItem() { TableName = "TestTable" };
			Assert.AreEqual(1, tItem.Table.Rows.Count);
			Assert.AreEqual("TestTable", tItem.TableName);
			Assert.AreEqual(0, tItem.Id);
			Assert.AreEqual(string.Empty, tItem.Name);
			Assert.AreEqual(string.Empty, tItem.Description);
			Assert.AreEqual(default(DateTime), tItem.Date);

			//Assert.IsTrue(tItem.AddToTable(1));
			//Assert.AreEqual(2,tItem.Table.Rows.Count);
			//Assert.IsFalse(tItem.AddToTable());
			//Assert.AreEqual(1, tItem.Table.Rows.Count);

		}


		[Test, Order(3)]
		public void TestDrwIndex()
		{
			TestItem i = new TestItem() { TableName = "TestTable" };
			i.Id = 1;
			Assert.AreEqual(1, i.Id);
			Assert.AreEqual(1, i[TestItem.Fields.Id]);
			i[TestItem.Fields.Id] = 2;

		}

		[Test, Order(3)]
		public void TestGetTable()
		{
			DataTable table = new TestItem() { TableName = "TestTable" }.Table;
			Assert.IsNotNull(table);
			Assert.AreEqual("TestTable", table.TableName);
			Assert.AreEqual(1, table.Rows.Count);
			Assert.AreEqual(4, table.Columns.Count);
		}

		[Test, Order(4)]
		public void TestFillTable()
		{
			DataTable table = this.fillTable();
			Assert.IsNotNull(table);
			Assert.AreEqual("TestTable", table.TableName);
			Assert.AreEqual(rowCount, table.Rows.Count);
			Assert.AreEqual(4, table.Columns.Count);

			TestItem di = new (table);

			Assert.AreEqual(di.Table.Rows.Count, table.Rows.Count);

			//string text = di.ToTableText();
		}


		[Test, Order(5)]
		public void TestClone()
		{
			DataTable table = this.fillTable();

			TestItem di1 = new(table.Rows[0]);
			Assert.AreEqual(1, di1.Id);
			Assert.AreEqual("Name-01", di1.Name);

			TestItem di2 = di1.Clone();

			di2.Name += "_changed";
			Assert.AreEqual(di1.Name, di2.Name);
		}

		/// <summary>
		/// Defines the test method TestDataWrapperIterator.
		/// </summary>
		[Test, Order(6)]
		public void TestIterator()
		{
			DataTable table = this.fillTable();
			TestItem di = new(table.Rows[0]);
			DataWrapperIterator<TestItem> dwi = di.GetIterator();

			int cnt = 0;
			Console.WriteLine("First iteration");
			while (dwi.MoveNext())
			{
				Assert.AreEqual(typeof(TestItem), dwi.DataRowWrapper.GetType());

				cnt++;
				Console.WriteLine(di.ToString());
			}
			Assert.AreEqual(table.Rows.Count, cnt);

			// repeat
			cnt = 0;
			Console.WriteLine("Second iteration");
			while (dwi.MoveNext())
			{
				cnt++;
				Console.WriteLine(di.ToString());
			}
			Assert.AreEqual(table.Rows.Count, cnt);
		}


		/// <summary>
		/// Defines the test method TestDataWrapperIterator.
		/// </summary>
		[Test, Order(7)]
		public void TestSelect()
		{
			DataTable table = this.fillTable();
			TestItem di = new(table);
			TestItem ds = di.Select($"{TestItem.Fields.Id}>=3 AND {TestItem.Fields.Id}<=5");

			Assert.AreEqual(3, ds.Table.Rows.Count);

			DataWrapperIterator<TestItem> dwi = ds.GetIterator();

			int cnt = 0;
			while (dwi.MoveNext())
			{
				cnt++;
				Console.WriteLine(ds.ToString());
			}
			Assert.AreEqual(ds.Table.Rows.Count, cnt);

		}


		/// <summary>
		/// Defines the test method TestDataWrapperIterator.
		/// </summary>
		[Test, Order(8)]
		public void TestValidator()
		{
			//++ Link for Text Regex 
			//++ https://regex101.com/r/BMJrYd/2

			DataTable table = this.fillTable();
			TestItem di = new(table);
			DataWrapperIterator<TestItem> dwi = di.GetIterator();
			while (dwi.MoveNext())
				di.Name = $"LPC{di.Id:000000}D-{di.Id:0000}-00";

			di.Table.Rows[3][TestItem.Fields.Name] = "LPZ000000D-0000-00";
			di.Table.Rows[5][TestItem.Fields.Name] = "LPC000000K-0000-00";
			di.Table.Rows[3][TestItem.Fields.Description] = "Descr?izione /; con caratteri | sbagliati?";
			di.Table.Rows[4][TestItem.Fields.Description] = "Descrizione, con caratteri sbagliati";

			DataWrapperValidator dwv = di.GetValidator(TestItem.Fields.Id, new()
			{
				{ TestItem.Fields.Name, new(@"^\b[LF][PAO][PCT]\d{6}[DS]-\d{4}-\d{2}\b$", DataWrapperValidator.RegexRule.MatchMode.Match) },
				{ TestItem.Fields.Description, new(@"[/];|,|[?]|[|]", DataWrapperValidator.RegexRule.MatchMode.NoMatch) },
			});


			Assert.NotNull(dwv.TableErr);
			Assert.AreEqual(3, dwv.TableErr.Rows.Count);

			Assert.AreEqual("LPZ000000D-0000-00", dwv.TableErr.Rows[0][TestItem.Fields.Name]);
			Assert.AreEqual("LPC000000K-0000-00", dwv.TableErr.Rows[1][TestItem.Fields.Name]);
			Assert.AreEqual(DBNull.Value, dwv.TableErr.Rows[2][TestItem.Fields.Name]);

			Assert.AreEqual("Descr?izione /; con caratteri | sbagliati?", dwv.TableErr.Rows[0][TestItem.Fields.Description]);
			Assert.AreEqual(DBNull.Value, dwv.TableErr.Rows[1][TestItem.Fields.Description]);
			Assert.AreEqual("Descrizione, con caratteri sbagliati", dwv.TableErr.Rows[2][TestItem.Fields.Description]);

			Assert.AreEqual(2, dwv.Rules.Count);
			Assert.IsTrue(dwv.Rules[TestItem.Fields.Name].Contains("LPC000000K-0000-00"));
			Assert.IsTrue(dwv.Rules[TestItem.Fields.Name].Contains("LPZ000000D-0000-00"));

			Assert.IsTrue(dwv.Rules[TestItem.Fields.Description].Contains("Descr?izione /; con caratteri | sbagliati?"));
			Assert.IsTrue(dwv.Rules[TestItem.Fields.Description].Contains("Descrizione, con caratteri sbagliati"));
		}


		/// <summary>
		/// Defines the test method TestJSon.
		/// </summary>
		[Test, Order(9)]
		public void TestJSon()
		{
			// create new instance and initialize some properties
			TestItem di01 = new()
			{
				Date = new DateTime(2023, 02, 25, 20, 30, 5),
				Name = "DefaultName",
				Description = "Description for default item",
				TableName = "DefaultTableName"
			};
			string json = di01.ToJson();

			// create new instance with json properties
			TestItem di02 = TestItem.FromJson(json);

			Assert.AreEqual(di01.Id, di02.Id);
			Assert.AreEqual(di01.Name, di02.Name);
			Assert.AreEqual(di01.Description, di02.Description);
			Assert.AreEqual(di01.TableName, di02.TableName);

			Console.WriteLine(json);

			TestItem di03 = TestItem.FromJson("{" +
				"\"Id\": 0," +
				"\"Name\": \"DefaultName\"," +
				"\"Description\": \"Description for default item\"," +
				"\"Date\": \"2023-02-25T20:30:05\"," +
				"\"TableName\": \"DefaultTableName\"," +
				"}");

			Assert.AreEqual(di01.Id, di03.Id);
			Assert.AreEqual(di01.Name, di03.Name);
			Assert.AreEqual(di01.Description, di03.Description);
			Assert.AreEqual(di01.TableName, di03.TableName);

		}


		/// <summary>
		/// Defines the test method TestInitializeDefaultValues.
		/// </summary>
		[Test, Order(10)]
		public void TestInitializeDefaultValues_01()
		{
			DataTable table = this.fillTable(true);
			TestItem ti = new(table);

			DataWrapperIterator<TestItem> dwi = ti.GetIterator();
			while (dwi.MoveNext())
			{
				Assert.AreEqual(0, ti.Id);
				Assert.AreEqual("", ti.Name);
				Assert.AreEqual("", ti.Description);
				Assert.AreEqual(default(DateTime), ti.Date);
			}

			ti.SetToDefaultValues(TestItem.FromJson(defaultFields));

			dwi = ti.GetIterator();
			while (dwi.MoveNext())
			{
				Assert.AreEqual(10, ti.Id);
				Assert.AreEqual("Default Name", ti.Name);
				Assert.AreEqual("Description for default item", ti.Description);
				Assert.AreEqual(new DateTime(2023, 03, 05, 10, 30, 00), ti.Date);
			}
		}

		/// <summary>
		/// Defines the test method TestInitializeDefaultValues.
		/// </summary>
		[Test, Order(10)]
		public void TestInitializeDefaultValues_02()
		{
			DataTable table = this.fillTable();
			TestItem ti = new(table);

			DataWrapperIterator<TestItem> dwi = ti.GetIterator();
			while (dwi.MoveNext())
				ti.Name = string.Empty;

			dwi = ti.GetIterator();
			while (dwi.MoveNext())
			{
				Assert.AreNotEqual(0, ti.Id);
				Assert.AreEqual("", ti.Name);
				Assert.AreNotEqual("", ti.Description);
				Assert.AreNotEqual(default(DateTime), ti.Date);
			}

			ti.SetToDefaultValues(TestItem.FromJson(defaultFields));

			dwi = ti.GetIterator();
			while (dwi.MoveNext())
			{
				Assert.AreNotEqual(0, ti.Id);
				Assert.AreEqual("Default Name", ti.Name);
				Assert.AreNotEqual("Description for default item", ti.Description);
				Assert.AreNotEqual(new DateTime(2023, 03, 05, 10, 30, 00), ti.Date);
			}
		}
		#endregion

		#region Private Method
		/// <summary>
		/// Fills the table.
		/// </summary>
		/// <returns>DataTable.</returns>
		private DataTable fillTable(bool defVal = false)
		{
			TestItem di = new() { TableName = "TestTable" };
			this.table = di.Table;
			di.AddToTable(rowCount);
			int cnt = 0;
			DataWrapperIterator<TestItem> dwi = new(di);
			while (dwi.MoveNext())
			{
				cnt++;
				di.Id = defVal ? 0 : cnt;
				di.Name = defVal ? "" : $"Name-{cnt:00}";
				di.Description = defVal ? "" : $"Description-{cnt:00}";
				di.Date = defVal ? default(DateTime) : DateTime.Now;
			}
			return this.table;
		}
		#endregion

		#region DataRowWrapperBase classe di prova
		/// <summary>
		/// Class DrwItem.
		/// Implements the <see cref="Mlc.Data.DataRowWrapper{Mlc.NUnit.TestDataRowWrapper.TestItem}" />
		/// </summary>
		/// <seealso cref="Mlc.Data.DataRowWrapper{Mlc.NUnit.TestDataRowWrapper.TestItem}" />
		public class TestItem : DataRowWrapper<TestItem>
		{
			/// <summary>Class Fields.</summary>
			public class Fields
			{
				/// <summary>
				/// The identifier
				/// </summary>
				public const string Id = "Id";
				/// <summary>
				/// The name
				/// </summary>
				public const string Name = "Name";
				/// <summary>
				/// The description
				/// </summary>
				public const string Description = "Description";
				/// <summary>
				/// The date
				/// </summary>
				public const string Date = "Date";
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TestItem"/> class.
			/// </summary>
			public TestItem() : base() { }

			/// <summary>
			/// Initializes a new instance of the <see cref="TestItem"/> class.
			/// </summary>
			/// <param name="table">The table.</param>
			public TestItem(DataTable table) : base(table) { }

			/// <summary>
			/// Initializes a new instance of the <see cref="TestItem"/> class.
			/// </summary>
			/// <param name="newRow">The new row.</param>
			/// <param name="addToTable">if set to <c>true</c> [add to table].</param>
			public TestItem(DataRow newRow, bool addToTable = false) : base(newRow, addToTable) { }


			/// <summary>
			/// Gets or sets the identifier.
			/// </summary>
			/// <value>The identifier.</value>
			[Field(Visible = true, HAlign = HorzAlignment.Right)]
			public virtual int Id { get => (int)base[Fields.Id]; set => base[Fields.Id] = value; }
			/// <summary>
			/// Gets or sets the name.
			/// </summary>
			/// <value>The name.</value>
			[Field(Visible = true, HAlign = HorzAlignment.Left)]
			public virtual string Name { get => (string)base[Fields.Name]; set => base[Fields.Name] = value; }
			/// <summary>
			/// Gets or sets the description.
			/// </summary>
			/// <value>The description.</value>
			[Field(Visible = true, HAlign = HorzAlignment.Left)]
			public virtual string Description { get => (string)base[Fields.Description]; set => base[Fields.Description] = value; }
			/// <summary>
			/// Gets or sets the date.
			/// </summary>
			/// <value>The date.</value>
			[Field(Visible = true)]
			public virtual DateTime Date { get => (DateTime)base[Fields.Date]; set => base[Fields.Date] = value; }
		}
		#endregion
	}
}