using System;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using Mlc.Shell.IO;
using Mlc.Shell;
using Mlc.Data;

namespace Mlc.NUnit
{
	public class DataTests
	{
		private DataTable tableUsers = null;

		[SetUp]
		public void Setup()
		{
			// Create a new DataTable
			this.tableUsers = new DataTable("UserMainTable");
			this.tableUsers.Columns.AddRange(new DataColumn[] {
				new DataColumn("Id", typeof(int)),
				new DataColumn("Name", typeof(string)),
				new DataColumn("Surname", typeof(string)),
				new DataColumn("BirtDay", typeof(DateTime)),
				new DataColumn("FloatValue", typeof(float)),
				new DataColumn("IntegerValue", typeof(int)),
			});

			// Fill table
			this.tableUsers.Rows.Add(new object[] { 1, "Mauro", "Cortese", new DateTime(1972, 5, 22), 102.12873526f, 123 });
			this.tableUsers.Rows.Add(new object[] { 2, "Camilla", "Bianchi", new DateTime(1973, 6, 2), 117.12873526f, 123 });
			this.tableUsers.Rows.Add(new object[] { 3, "Giovanni", "Rossi", new DateTime(1974, 7, 20), 132.12873526f, 123 });
			this.tableUsers.Rows.Add(new object[] { 4, "Andrea", "Albertino", new DateTime(1975, 8, 12), 147.12873526f, 123 });
			this.tableUsers.Rows.Add(new object[] { 5, "Maurizio", "Cantone", new DateTime(1976, 9, 15), 162.12873526f, 123 });
			this.tableUsers.Rows.Add(new object[] { 6, "Katia", "Pollesa", new DateTime(1977, 5, 28), 177.12873526f, 123 });

		}


		[Test, Order(1)]
		public void TestMapping()
		{
			DataMapper<User>.Rows rows = new User().GetDictionary(this.tableUsers, "Id");
			Assert.AreEqual(6, rows.Count);
			Assert.AreEqual(1, rows["1"].Id);
			Assert.AreEqual("Cortese", rows["1"].Surname);
			Assert.AreEqual("Mauro", rows["1"].Name);
			DataTable table = DataTableTools.GetTableFromType(typeof(User));
		}

		[Test, Order(1)]
		public void TestGetTableFromType()
		{
			DataTable table = DataTableTools.GetTableFromType(typeof(User));
			table.TableName = "UserTestTable";

			Assert.AreEqual(this.tableUsers.Columns.Count, table.Columns.Count);

			for (int i = 0; i < this.tableUsers.Columns.Count; i++)
			{
				Assert.AreEqual(this.tableUsers.Columns[i].ColumnName, table.Columns[i].ColumnName);
				Assert.AreEqual(this.tableUsers.Columns[i].DataType, table.Columns[i].DataType);

			}

			for (int i = 0; i < this.tableUsers.Rows.Count; i++)
				table.ImportRow(this.tableUsers.Rows[i]);

			Assert.AreEqual(this.tableUsers.Rows.Count, table.Rows.Count);

			// Add new row from type
			User user = new User(7, "Giacomo", "Leopardi", new DateTime(1978, 6, 29), 125.67f, 1939);

			user.Add(table);
			try { user.Add(this.tableUsers); }
			catch (Exception ex)
			{ Assert.IsInstanceOf(typeof(TableAlreadyExistsException), ex); }

			user.ResetMapping();
			user.Add(this.tableUsers);
		}


		internal class User : DataMapper<User>
		{
			public User()
			{
			}

			public User(int id, string name, string surname, DateTime birtDay, float floatValue, int integerValue)
			{
				Id = id;
				Name = name ?? throw new ArgumentNullException(nameof(name));
				Surname = surname ?? throw new ArgumentNullException(nameof(surname));
				BirtDay = birtDay;
				FloatValue = floatValue;
				IntegerValue = integerValue;
			}

			public int Id { get; set; }
			public string Name { get; set; }
			public string Surname { get; set; }
			public DateTime BirtDay { get; set; }
			public float FloatValue { get; set; }
			public int IntegerValue { get; set; }
		}

	}
}