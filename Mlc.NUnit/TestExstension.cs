using Mlc.Shell;
using NUnit.Framework;
using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Mlc.NUnit
{
	public class ExstensionTests
	{

		[SetUp]
		public void Setup()
		{

		}

		[Test, Order(1)]
		public void DateTimeSerialize8601()
		{
			Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
			//Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("it-IT");

			DateTime date1 = new(2022, 11, 05, 10, 15, 32, DateTimeKind.Utc);
			string strDate = date1.Serialize();
			Assert.AreEqual("2022-11-05T10:15:32.000", strDate);
			DateTime date2 = "2022-11-05T10:15:32.000".ToDateTime();
			Assert.AreEqual(date1, date2);

			Console.WriteLine($"From date {date1} to {date2}");

			Console.WriteLine("13/11/2022".ToDateTime());
		}

		[Test, Order(2)]
		public void NormalizeTextCode()
		{
			string charsToReplace =   @",;_\ =";
			string replacementChars = @"..-/--";
			string wrongChars = @"&'()[]{}\^@%?�$";
			string singleChars = @"-/*";

			string[,] codes = new string[,] {
				 {@"SA47\DRN71M4"                    ,@"SA47/DRN71M4"           ,""}
				,{@"569^926"                         ,@"569926"                 ,""}
				,{@"56@9926"                         ,@"569926"                 ,""}
				,{@"   569%926"                      ,@"569926"                 ,""}
				,{@"SA37\DRN71[M4]"                  ,@"SA37/DRN71M4"           ,""}
				,{@"WA30///TDRN71M4\\/FDG"           ,@"WA30/TDRN71M4/FDG"      ,""}
				,{@"5931-M03*006-8,8 ZN	B"           ,@"5931-M03*006-8.8-ZN-B"  ,""}
				,{ "5931	M03**\r\n**006 8.8-ZN=B" ,@"5931-M03*006-8.8-ZN-B"  ,""}
			};

			for (int i = 0; i < codes.Length/3; i++)
				codes[i,2] = codes[i,0]
					.Trim()
					.ToUpper()
					.RemoveNewLine()
					.RemoveTab(true,1)
					.Normalize(charsToReplace, replacementChars, wrongChars, null)
					.RemoveDuplicateChars(singleChars);

			for (int i = 0; i < codes.Length / 3; i++)
			{
				Assert.AreEqual(codes[i, 1], codes[i, 2]);
				Console.WriteLine($"Code: {codes[i, 0]} \r\n  =>: {codes[i, 2]}");
			}

		}

		[Test, Order(3)]
		public void NormalizeTextDescription()
		{
			string charsToReplace = @",;_\";
			string replacementChars = @"..-/";
			string wrongChars = @"&'[]{}\^@%?�$";
			string singleChars = @". -/*";

			string[,] codes = new string[,] {
				 {@"PIASTRA"                                                                                                                                ,@"PIASTRA" ,""}
				,{@"   UNIT� DI VALVOLE MPAL-VI 34P-MS8-D-U5A-5J+CK"                                                                                        ,@"UNIT� DI VALVOLE MPAL-VI 34P-MS8-D-U5A-5J+CK" ,""}
				,{@"UNIT� DI VALVOLE MPAL-VI 34P-CX-UE5ATS5AEEU-4JKK7J	50E-F36GCQS-L+N   "                                                                 ,@"UNIT� DI VALVOLE MPAL-VI 34P-CX-UE5ATS5AEEU-4JKK7J 50E-F36GCQS-L+N" ,""}
				,{@"VITE TCEI UNI 5931 M3 X 6 8.8 ZINCATO BIANCO    "                                                                                       ,@"VITE TCEI UNI 5931 M3 X 6 8.8 ZINCATO BIANCO" ,""}
				,{@"  MOTORID. I=51.3 NA=28 KW=0.37 V=230/400 50HZ V.MOT=1415 IP54 IE3 155(F) AC.�20 FC,M2AB PM.180/X IEC"                                  ,@"MOTORID. I=51.3 NA=28 KW=0.37 V=230/400 50HZ V.MOT=1415 IP54 IE3 155(F) AC.�20 FC.M2AB PM.180/X IEC" ,""}
				,{@"MOTORID. I=75 NA=19 KW=0,37 V=230/400 50HZ V,MOT=1415 IP54 IE3 155(F) AC.�20 FC.M6AB PM.0/X IECT=BR. REAZ. LUB.ALIM. CLP PG-460-NSF-H1" ,@"MOTORID. I=75 NA=19 KW=0.37 V=230/400 50HZ V.MOT=1415 IP54 IE3 155(F) AC.�20 FC.M6AB PM.0/X IECT=BR. REAZ. LUB.ALIM. CLP PG-460-NSF-H1" ,""}
			};

			for (int i = 0; i < codes.Length / 3; i++)
				codes[i, 2] = codes[i, 0]
					.Trim()
					.ToUpper()
					.RemoveNewLine(true)
					.RemoveTab(true,1)
					.Normalize(charsToReplace, replacementChars, wrongChars, null)
					.RemoveDuplicateChars(singleChars);

			for (int i = 0; i < codes.Length / 3; i++)
			{
				Assert.AreEqual(codes[i, 1], codes[i, 2]);
				Console.WriteLine($"Code: {codes[i, 0]} \r\n  =>: {codes[i, 2]}");
			}
		}
	}
}